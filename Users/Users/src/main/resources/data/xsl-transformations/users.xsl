<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:z1="https://www.serviszaradsapatentima.rs/users">
<xsl:template match="/">
        <html>
            <head>

                <style>
                    #naslov{
                    text-align: center;
                    font-size: 20pt;
                    margin-bottom: 0;
                    }
                    #opis{
                    margin-top: 0;
                    text-align: center;
                    font-size: 12pt;
                    }
                    #uputstvo{
                    text-align: center;
                    font-size: 12pt;
                    margin-bottom: 0;
                    }
                    #bordiraj{
                    margin-top: 0;
                    margin-bottom: 0;
                    border-style: solid;
                    border-width: thin;
                    }
                    #page{
                    margin-top: 2.97cm;
                    margin-bottom: 2.97cm;
                    margin-top: 2.1cm;
                    margin-bottom: 2.1cm;
                    }
                    table{
                    width:100%;
                    position: flex;
                    }
                    table, td, th{
                    border: 1px solid black;
                    border-collapse: collapse;
                    }

                </style>
            </head>
            <body>
                <div id="page">
                    <p id="naslov"><b>User</b> </p>
                    <br/>
                    <div>
                        <p id="bordiraj">
                            e-mail:&#160;<i><xsl:value-of select="//z1:podnosilac_prijave//z1:email"></xsl:value-of>&#09;</i>&#160;&#160;&#160;
                        </p>
                        <p id="bordiraj">
                            uloga:&#160;<i><xsl:value-of select="//z1:podnosilac_prijave//z1:uloga"></xsl:value-of>&#09;</i>&#160;&#160;&#160;
                        </p>
                        <p id="bordiraj">
                            password:&#160;<i><xsl:value-of select="//z1:podnosilac_prijave//z1:password"></xsl:value-of>&#09;</i>&#160;&#160;&#160;
                        </p>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>