@javax.xml.bind.annotation.XmlSchema(
        namespace = "https://www.serviszaradsapatentima.rs/users",
        elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED,
        xmlns = {@javax.xml.bind.annotation.XmlNs(prefix = "", namespaceURI = "http://www.serviszaradsapatentima.rs/users"),
                @javax.xml.bind.annotation.XmlNs(prefix = "xs", namespaceURI = "http://www.w3.org/2001/XMLSchema"),
                @javax.xml.bind.annotation.XmlNs(prefix = "r", namespaceURI = "http://www.w3.org/ns/rdfa#"),
                @javax.xml.bind.annotation.XmlNs(prefix = "pred", namespaceURI = "http://www.serviszaradsapatentima.rs/rdf/database/predicate/")
        })
package www.serviszaradsapatentima.rs.users.model.users;