package www.serviszaradsapatentima.rs.users.service;

import org.apache.commons.io.FileUtils;
import www.serviszaradsapatentima.rs.users.exception.EntityNotFoundException;
import www.serviszaradsapatentima.rs.users.exception.InvalidXmlDatabaseException;
import www.serviszaradsapatentima.rs.users.exception.InvalidXmlException;
import www.serviszaradsapatentima.rs.users.exception.XmlDatabaseException;
import www.serviszaradsapatentima.rs.users.helper.UUIDHelper;
import www.serviszaradsapatentima.rs.users.helper.XmlConversionAgent;
import www.serviszaradsapatentima.rs.users.model.users.Users;
import www.serviszaradsapatentima.rs.users.repository.AbstractXmlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xmldb.api.base.XMLDBException;
import www.serviszaradsapatentima.rs.users.transformation.XSLTransformer;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.List;

import static www.serviszaradsapatentima.rs.users.helper.XQueryExpressions.X_QUERY_FIND_ALL_ZIG_EXPRESSION;
import static www.serviszaradsapatentima.rs.users.helper.XQueryExpressions.X_UPDATE_REMOVE_ZIG_BY_ID_EXPRESSION;

@Service
public class ZigService implements AbstractXmlService<Users> {
    private final String jaxbContextPath = "www.serviszaradsapatentima.rs.users.model.users";

    private static final String SPARQL_NAMED_GRAPH_URI = "/sparql/metadata";

    public static final String OUTPUT_FOLDER_XML = "output_xml";
    public static final String OUTPUT_FOLDER_PDF = "output_pdf";
    public static final String OUTPUT_FOLDER_HTML = "output_html";
    public static final String OUTPUT_FOLDER_METADATA = "output_metadata";

    @Autowired
    private AbstractXmlRepository<Users> zigRepository;

    @Autowired
    private XmlConversionAgent<Users> zigConverionAgent;

    @Autowired
    private UUIDHelper uuidHelper;

    @Autowired
    private RDFService rdfService;

    public void injectRepositoryProperties() {
        this.zigRepository.injectRepositoryProperties(
                "/db/intelektualna_svojina/users",
                jaxbContextPath,
                X_QUERY_FIND_ALL_ZIG_EXPRESSION,
                X_UPDATE_REMOVE_ZIG_BY_ID_EXPRESSION);
    }

    @Override
    public List<Users> findAll() {
        injectRepositoryProperties();

        try {
            return this.zigRepository.getAllEntities();
        } catch (XMLDBException e) {
            throw new XmlDatabaseException(e.getMessage());
        } catch (JAXBException e) {
            throw new InvalidXmlDatabaseException(Users.class, e.getMessage());
        }
    }

    @Override
    public Users findById(String entityId) {
        injectRepositoryProperties();

        try {
            Users zig = this.zigRepository.getEntity(entityId);
            if (zig == null) {
                throw new EntityNotFoundException(entityId, Users.class);
            }
            return zig;
        } catch (JAXBException e) {
            throw new XmlDatabaseException(e.getMessage());
        } catch (XMLDBException e) {
            throw new InvalidXmlDatabaseException(Users.class, e.getMessage());
        }
    }



    @Override
    public Users create(String entityXml) {
        injectRepositoryProperties();
        Users zig;

        try {
            zig = this.zigConverionAgent.unmarshall(entityXml, this.jaxbContextPath);
            this.handleMetadata(zig);
        } catch (JAXBException e) {
            throw new InvalidXmlException(Users.class, e.getMessage());
        }

        try {
            zig = zigRepository.createEntity(zig);
        } catch (JAXBException e) {
            throw new XmlDatabaseException(e.getMessage());
        } catch (XMLDBException e) {
            throw new InvalidXmlDatabaseException(Users.class, e.getMessage());
        }

        //EKSTRAKCIJA METAPODATAKA >> RDF
        try {
            //radimo marsalovanje >> treba rdfa

            String xmlEntitet = this.zigConverionAgent.marshall(zig, this.jaxbContextPath);
            //System.out.println(xmlEntitet);

            if (!rdfService.save(xmlEntitet, SPARQL_NAMED_GRAPH_URI)) {
                System.out.println("[ERROR] Neuspesno cuvanje metapodataka zahteva u RDF DB.");
            }
        } catch (JAXBException e) {
            throw new XmlDatabaseException(e.getMessage());
        }
        return zig;
    }

    @Override
    public Users update(String entityXml) {
        injectRepositoryProperties();

        Users zig;
        try {
            zig = this.zigConverionAgent.unmarshall(entityXml, this.jaxbContextPath);
        } catch (JAXBException e) {
            throw new InvalidXmlException(Users.class, e.getMessage());
        }
        try {
            if (!this.zigRepository.updateEntity(zig)) {
                throw new EntityNotFoundException(zig.getPodnosilacPrijave().getEmail().getValue(), Users.class);
            }
            return zig;

        } catch (JAXBException e) {
            throw new XmlDatabaseException(e.getMessage());
        } catch (XMLDBException e) {
            throw new InvalidXmlException(Users.class, e.getMessage());
        }
    }

    @Override
    public String getRdfaString(String brojPrijave) throws JAXBException {
        injectRepositoryProperties();

        Users dokument;
        dokument = this.findById(brojPrijave);
        String entityXml = this.zigConverionAgent.marshall(dokument, this.jaxbContextPath);
        System.out.println(entityXml);
        return entityXml;

    }

    @Override
    public boolean deleteById(String entityId) {
        injectRepositoryProperties();
        try {
            return this.zigRepository.deleteEntity(entityId);
        } catch (XMLDBException e) {
            throw new XmlDatabaseException(e.getMessage());
        }
    }


    private void handleMetadata(Users zig) {
        zig.getPodnosilacPrijave().setVocab("http://www.serviszaradsapatentima.rs/rdf/database/predicate");
        zig.getPodnosilacPrijave().setAbout("http://www.serviszaradsapatentima.rs/rdf/database/users/" + zig.getPodnosilacPrijave().getEmail().getValue());

        zig.getPodnosilacPrijave().getEmail().setDatatype("xs:#string");
        zig.getPodnosilacPrijave().getEmail().setProperty("pred:email");

    }

    public ByteArrayInputStream generatePDF(String dokumentId) throws Exception {
        String xslFile = "src/main/resources/data/xsl-transformations/users.xsl";
        String outputHtmlFile = "src/main/resources/data/xsl-transformations/users.html";
        String outputPdfFile = "src/main/resources/data/xsl-transformations/users.pdf";
        String outputXmlFile = "src/main/resources/data/instance/users.xml";

        XSLTransformer xslTransformer = new XSLTransformer();
        xslTransformer.setXSLT_FILE(xslFile);
        xslTransformer.setOUTPUT_FILE_HTML(outputHtmlFile);
        xslTransformer.setOUTPUT_FILE_PDF(outputPdfFile);


        Users zigXml = this.findById(dokumentId);

        System.out.println("************************************ MMMMMMMMMMMMMMMMMMMMMmmmmm");
        System.out.println(zigXml.toString());

        try {
            this.zigConverionAgent.marshallToFile(
                    zigXml,
                    this.jaxbContextPath,
                    outputXmlFile
            );
            xslTransformer.generatePDF_HTML(outputXmlFile);
            return new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(outputPdfFile)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
