package www.serviszaradsapatentima.rs.users.controller;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.xml.sax.SAXException;
import www.serviszaradsapatentima.rs.users.model.users.KolekcijaUsers;
import www.serviszaradsapatentima.rs.users.model.users.Users;
import www.serviszaradsapatentima.rs.users.service.RDFService;
import www.serviszaradsapatentima.rs.users.service.ZigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@Controller
@RequestMapping(value = "api/users")
public class UserController {
    @Autowired
    ZigService zigService;

    @Autowired
    RDFService rdfService;

    @GetMapping(produces = MediaType.APPLICATION_XML_VALUE)
    ResponseEntity<KolekcijaUsers> findAll() {
        KolekcijaUsers kolekcijaZigova = new KolekcijaUsers();
        kolekcijaZigova.setUsers(this.zigService.findAll());
        return new ResponseEntity<>(kolekcijaZigova, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    ResponseEntity<Users> findOne(@PathVariable("id") String id) {
        return new ResponseEntity<>(this.zigService.findById(id), HttpStatus.OK);
    }

    @PostMapping(produces = MediaType.APPLICATION_XML_VALUE)
    ResponseEntity<Users> create(@RequestBody String body) {
        return new ResponseEntity<>(this.zigService.create(body), HttpStatus.OK);
    }

    @PutMapping(produces = MediaType.APPLICATION_XML_VALUE)
    ResponseEntity<Users> update(@RequestBody String body) {
        return new ResponseEntity<>(this.zigService.update(body), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    ResponseEntity<Void> delete(@PathVariable("id") String id) {
        this.zigService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/metadata/rdf/{dokumentId}")
    ResponseEntity<InputStreamResource> getMetadataRdf(@PathVariable String dokumentId) {
        ByteArrayInputStream is;
        try {
            String xmlEntity = this.zigService.getRdfaString(dokumentId);
            System.out.println(xmlEntity);
            is = new ByteArrayInputStream(this.rdfService.getRDFAsRDF(xmlEntity).getBytes());
        }
        catch (IOException | SAXException | JAXBException | TransformerException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline: filename=zig-metadata.rdf");

        return new ResponseEntity<>(new InputStreamResource(is), headers, HttpStatus.OK);
    }

    @GetMapping(value = "/metadata/json/{dokumentId}")
    ResponseEntity<InputStreamResource> getMetadataJson(@PathVariable String dokumentId) {
        ByteArrayInputStream is;
        try {
            String xmlEntity = this.zigService.getRdfaString(dokumentId);
            System.out.println(xmlEntity);
            is = new ByteArrayInputStream(this.rdfService.getRDFAsJSON(xmlEntity).getBytes());
        }
        catch (IOException | SAXException | JAXBException | TransformerException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline: filename=zig-metadata.json");

        return new ResponseEntity<>(new InputStreamResource(is), headers, HttpStatus.OK);
    }

    @GetMapping(value = "/pdf/{dokumentId}")
    ResponseEntity<InputStreamResource> getPdf(@PathVariable String dokumentId) throws Exception {
        this.zigService.generatePDF(dokumentId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
