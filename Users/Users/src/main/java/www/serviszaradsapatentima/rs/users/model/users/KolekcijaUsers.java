package www.serviszaradsapatentima.rs.users.model.users;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"users"})
@XmlRootElement(name = "kolekcija-zigova")
public class KolekcijaUsers {
    @XmlElement(name = "zig")
    protected List<Users> users;

    public List<Users> getUsers() {
        if (users == null) {
            users = new ArrayList<>();
        }
        return users;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }
}
