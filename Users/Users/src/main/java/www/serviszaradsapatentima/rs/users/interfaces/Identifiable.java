package www.serviszaradsapatentima.rs.users.interfaces;

import www.serviszaradsapatentima.rs.users.model.users.PodnosilacPrijave;

public interface Identifiable {

    PodnosilacPrijave getPodnosilacPrijave();

}
