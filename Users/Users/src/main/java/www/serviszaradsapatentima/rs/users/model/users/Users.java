
package www.serviszaradsapatentima.rs.users.model.users;

import www.serviszaradsapatentima.rs.users.interfaces.Identifiable;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="podnosilac_prijave" type="{https://www.serviszaradsapatentima.rs/users}podnosilac_prijave"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "podnosilacPrijave"
})
@XmlRootElement(name = "users")
public class Users implements Identifiable {

    @XmlElement(name = "podnosilac_prijave", required = true)
    protected PodnosilacPrijave podnosilacPrijave;

    /**
     * Gets the value of the podnosilacPrijave property.
     * 
     * @return
     *     possible object is
     *     {@link PodnosilacPrijave }
     *     
     */
    public PodnosilacPrijave getPodnosilacPrijave() {
        return podnosilacPrijave;
    }

    /**
     * Sets the value of the podnosilacPrijave property.
     * 
     * @param value
     *     allowed object is
     *     {@link PodnosilacPrijave }
     *     
     */
    public void setPodnosilacPrijave(PodnosilacPrijave value) {
        this.podnosilacPrijave = value;
    }

}
