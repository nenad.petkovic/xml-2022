package www.serviszaradsapatentima.rs.users.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {

    private Long id;
    private String id_string;
    private Class classObject;

    public EntityNotFoundException(Long id, Class classObject) {
        this.id = id;
        this.classObject = classObject;
    }

    public EntityNotFoundException(String id_string, Class classObject) {
        this.id_string = id_string;
        this.classObject = classObject;
    }

    public String getId_string() {
        return id_string;
    }

    public void setId_string(String id_string) {
        this.id_string = id_string;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Class getClassObject() {
        return classObject;
    }

    public void setClassObject(Class classObject) {
        this.classObject = classObject;
    }
}
