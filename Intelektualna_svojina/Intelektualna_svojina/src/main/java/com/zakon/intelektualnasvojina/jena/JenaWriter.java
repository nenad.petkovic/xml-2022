package com.zakon.intelektualnasvojina.jena;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class JenaWriter {

    private static final String GRAPH_URI = "graph_metadata";

    public static void saveRDF(String rdf_filepath) throws IOException {
        JenaAuthUtils.ConnectionProperties connectionProperties = JenaAuthUtils.loadProperties();

        Model model = ModelFactory.createDefaultModel();
        model.read(rdf_filepath);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        model.write(outputStream, SparqlUtils.NTRIPLES);
        System.out.println("Renderovanje RDF-a ... ");
        model.write(System.out, SparqlUtils.RDF_XML);

        UpdateRequest updateRequest = UpdateFactory.create();
        UpdateProcessor updateProcessor = UpdateExecutionFactory.createRemote(updateRequest, connectionProperties.endpoint);
        updateProcessor.execute();

        System.out.println("Pisanje tripleta grafu po imenu \"" + GRAPH_URI + "\" ... ");
        String sparqlUpdate = SparqlUtils.insertData(connectionProperties.dataEndpoint + GRAPH_URI,
                new String(outputStream.toByteArray()));
        System.out.println(sparqlUpdate);

        UpdateRequest updates = UpdateFactory.create(sparqlUpdate);
        updateProcessor = UpdateExecutionFactory.createRemote(updates, connectionProperties.updateEndpoint);
        updateProcessor.execute();

    }

}
