package com.zakon.intelektualnasvojina.jena;

import org.apache.commons.text.StringSubstitutor;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.RDFNode;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class JenaReader {
    private static final String QUERY_FILE_PATH = "src/main/resources/sparql.rq";

    private  JenaReader(){}

    // ArrayList<String> ZA POVRAT KAO
    // Map<String, String> parameters
    public static void executeQuery() throws IOException {
        JenaAuthUtils.ConnectionProperties connectionProperties = JenaAuthUtils.loadProperties();
        String sparqlQueryTemplate = readFile(QUERY_FILE_PATH, StandardCharsets.UTF_8);
        //String sparqlQuery = StringSubstitutor.replace(sparqlQueryTemplate) DAKLE OVDE BI SE MENJALO AKO SE RAZLIKUJU QUERY I QUERY TEMPLATE
        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(connectionProperties.queryEndpoint, sparqlQueryTemplate);
        ResultSet resultSet = queryExecution.execSelect();
        String varName;
        RDFNode varValue;
        while (resultSet.hasNext()) {
            // dobavlja jedan odgovor query-ja
            QuerySolution querySolution = resultSet.next();

            Iterator<String> iterator = querySolution.varNames();
            while (iterator.hasNext()) {
                varName = iterator.next();
                varValue = querySolution.get(varName);
                System.out.println(varName + " : " + varValue);
            }
        }
        System.out.println("#####################################################");
        ResultSetFormatter.outputAsXML(System.out, resultSet);
        queryExecution.close();
        System.out.println("QUERY EXECUTION COMPLETED SUCCESSFULLY");
        System.out.println("#####################################################");
    }

    public static String readFile(String pth, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(pth));
        return new String(encoded, encoding);
    }

}