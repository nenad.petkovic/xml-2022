package com.zakon.intelektualnasvojina.service;

import com.zakon.intelektualnasvojina.model.p1Obrazac.resenjeDto.Resenje;
import com.zakon.intelektualnasvojina.repository.ResenjeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xmldb.api.base.XMLDBException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

@Service
public class ResenjeService {

    private final String jaxbPutanja = "com.autorska.prava.AutorskaPrava.model.a1Obrazac.resenjeDto";

    @Autowired
    public ResenjeRepository resenjeRepository;




    public void saveResenjeXml(String xml, String brojResenja) throws Exception {

        System.out.println("Poslato:::");
        System.out.println(xml);
        System.out.println("--------------------------------------------------");

        File file = new File("src/main/resources/static/resenjeInstanca.xml");

        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = xml.getBytes();
        //write byte array to file
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(Resenje.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("src/main/resources/static/resenje.xsd"));
        //Schema schema = sf.newSchema(new File("../xml-documents/seme/Interesovanje.xsd"));

        unmarshaller.setSchema(schema);

        Resenje resenje = (Resenje) unmarshaller.unmarshal(new File("src/main/resources/static/resenjeInstanca.xml"));
        //sad od prosledjenog stringa treba da napravimo xml fajl i da ga sacuvamo.

        //interesovanje.setBrojPrijave(UUID.randomUUID().toString());

        //generisemo string koji predstavlja broj prijave automatski..
        //autorsko.setBrojPrijave(brojPrijave.toString());
        resenje.setBrojResenja(brojResenja);
        System.out.println("Postavljeni broj resenja je: "  + resenje.getBrojResenja());

        //handleMetadata(autorsko); //prosirivanje metapodacima
        resenjeRepository.saveResenjeXml(resenje); //cuvanje u exist bazi podataka
    }


    /**** metoda vraca string sadrzaj xml dokumenta sa prosledjenim id-ijem **/
    public Object findResenjeById(String id) {
        return  resenjeRepository.findResenjeXml(id);
    }

    /*** metoda vraca objekat entiteta sa prosledjenim id-ijem iz baze ***/
    public Resenje findDokumentById(String idEntita) throws Exception {

        try {
            Resenje resenje = this.resenjeRepository.findResenjeObjekat(idEntita);
            if (resenje == null)
                throw new Exception("Nije pronadjen zahtev sa tim id-ijem u bazi...");
            System.out.println("Nije pronadjen zahtev sa tim id-ijem u bazi...");
            return resenje;
        } catch (XMLDBException e) {
            throw new Exception(e.getMessage());
        } catch (JAXBException e) {
            throw new Exception("Desio se jax b exception...");
        }
    }


}
