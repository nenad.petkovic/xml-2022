//package com.zakon.intelektualnasvojina.controller;
//
//import com.zakon.intelektualnasvojina.service.SearchService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
//@RequestMapping(value = "/api/search")
//public class SearchController {
//
//    @Autowired
//    private SearchService searchService;
//
//    @PostMapping(value = "/ime", consumes = MediaType.APPLICATION_JSON_VALUE)
//    ResponseEntity<List<String>> pretragaIme(@RequestBody String ime) {
//        return new ResponseEntity<>(searchService.searchByIme(ime), HttpStatus.OK);
//    }
//
//    @PostMapping(value = "/prezime", consumes = MediaType.APPLICATION_JSON_VALUE)
//    ResponseEntity<List<String>> pretragaPrezime(@RequestBody String prezime) {
//        return new ResponseEntity<>(searchService.searchByPrezime(prezime), HttpStatus.OK);
//    }
//
//    @PostMapping(value = "/brojPrijave", consumes = MediaType.APPLICATION_JSON_VALUE)
//    ResponseEntity<List<String>> pretragaBrojPrijave(@RequestBody String brojPrijave) {
//        return new ResponseEntity<>(searchService.searchByBrojPrijave(brojPrijave), HttpStatus.OK);
//    }
//
//    @PostMapping(value = "/nazivsrpski", consumes = MediaType.APPLICATION_JSON_VALUE)
//    ResponseEntity<List<String>> pretraganazivsrpski(@RequestBody String nazivsrpski) {
//        return new ResponseEntity<>(searchService.searchBynazivsrpski(nazivsrpski), HttpStatus.OK);
//    }
//
//    @PostMapping(value = "/nazivengleski", consumes = MediaType.APPLICATION_JSON_VALUE)
//    ResponseEntity<List<String>> pretraganazivengleski(@RequestBody String nazivengleski) {
//        return new ResponseEntity<>(searchService.searchBynazivengleski(nazivengleski), HttpStatus.OK);
//    }
//
//    @PostMapping(value = "/datumpodnosenja", consumes = MediaType.APPLICATION_JSON_VALUE)
//    ResponseEntity<List<String>> pretragadatumpodnosenja(@RequestBody String datumpodnosenja) {
//        return new ResponseEntity<>(searchService.searchBydatumpodnosenja(datumpodnosenja), HttpStatus.OK);
//    }
//
//    @PostMapping(value = "/datumprijema", consumes = MediaType.APPLICATION_JSON_VALUE)
//    ResponseEntity<List<String>> pretragadatumprijema(@RequestBody String datumprijema) {
//        return new ResponseEntity<>(searchService.searchBydatumprijema(datumprijema), HttpStatus.OK);
//    }
//
//}
