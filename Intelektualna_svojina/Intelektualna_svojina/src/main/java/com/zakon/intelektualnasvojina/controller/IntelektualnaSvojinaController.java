package com.zakon.intelektualnasvojina.controller;

import com.zakon.intelektualnasvojina.service.IntelektualnaSvojinaService;
import com.zakon.intelektualnasvojina.service.ResenjeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/api/zahtevZaPriznanjePatenta")
public class IntelektualnaSvojinaController {

    @Autowired
    private IntelektualnaSvojinaService intelektualnaSvojinaService;

    @Autowired
    private ResenjeService resenjeService;

    @PostMapping(value = "/createZahtevZaPriznanjePatenta/{brojPrijave}")
    public ResponseEntity<String> writePatentXml(@RequestBody String xml, @PathVariable String brojPrijave) {
        try {
            intelektualnaSvojinaService.saveIntelektualnaSvojina(xml, brojPrijave);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
        }
    }

    @PostMapping(value = "/prihvatiZahtev/{id}")
    public ResponseEntity<String> prihvatiZahtev(@RequestBody String resenjeXML, @PathVariable String id) {
        try{
            this.resenjeService.saveResenjeXml(resenjeXML, id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping(value = "/odbijahtev")
    public ResponseEntity<String> odbijahtev(@RequestBody String id) {
        try {
            intelektualnaSvojinaService.odbijahtev(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/kreiraj")
    public ResponseEntity<String> napisi(@RequestBody String xml) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/readZahtevZaPriznanjePatenta/{id}", produces = "application/xml")
    public ResponseEntity<Object> findPatent(@PathVariable String id) {
        try {
            Object svojina = intelektualnaSvojinaService.findIntelektualnaSvojinaById(id);
            return new ResponseEntity<>(svojina, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/generatePDF/{id}")
    public void generatePDF(@PathVariable String id, HttpServletResponse response) {
        response.setContentType("application/pdf");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=ZahtevZaPriznanjePatenta" + id + ".pdf";
        response.setHeader(headerKey, headerValue);
        Object svojina = intelektualnaSvojinaService.findIntelektualnaSvojinaById(id);
        String svojinaString = svojina.toString();
        try {
            this.intelektualnaSvojinaService.generatePDF(id, response, svojinaString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @GetMapping(value = "generateHTML/{id}", produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public String generateHTML(@PathVariable String id) throws JAXBException, IOException, SAXException {
        Object svojina = intelektualnaSvojinaService.findIntelektualnaSvojinaById(id);
        return intelektualnaSvojinaService.generateHTMLL(svojina, id);
    }

    @GetMapping("/fusekiSearch/")
    public ResponseEntity<String> searchFromRDF() throws IOException {
        intelektualnaSvojinaService.searchByMetadata();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/getAll", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> getAll() {
        return new ResponseEntity<>(intelektualnaSvojinaService.getAll(), HttpStatus.OK);
    }

}
