package com.zakon.intelektualnasvojina.service;

import com.lowagie.text.DocumentException;
import com.zakon.intelektualnasvojina.additional.XSLTransormation;
import com.zakon.intelektualnasvojina.jena.JenaReader;
import com.zakon.intelektualnasvojina.jena.JenaWriter;
import com.zakon.intelektualnasvojina.jena.MetadataExt;
import com.zakon.intelektualnasvojina.model.p1Obrazac.ZahtevZaPriznanjePatenta;
import com.zakon.intelektualnasvojina.repository.IntelektualnaSvojinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletResponse;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.util.Arrays;
import java.util.List;

@Service
public class IntelektualnaSvojinaService {

    @Autowired
    public IntelektualnaSvojinaRepository intelektualnaSvojinaRepository;
    private final MetadataExt metadataExt;

    public IntelektualnaSvojinaService(MetadataExt metadataExt) {
        this.metadataExt = metadataExt;
    }

    public void saveIntelektualnaSvojina(String xml, String brojPrijave) throws Exception {
        File file = new File("src/main/resources/static/instance1.xml");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        byte[] bytes = xml.getBytes();
        bufferedOutputStream.write(bytes);
        bufferedOutputStream.close();
        fileOutputStream.close();

        javax.xml.bind.JAXBContext context = JAXBContext.newInstance(ZahtevZaPriznanjePatenta.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(new File("src/main/resources/static/SZRSP_schema.xsd"));

        unmarshaller.setSchema(schema);

        ZahtevZaPriznanjePatenta zahtev = (ZahtevZaPriznanjePatenta) unmarshaller.unmarshal(new
                File("src/main/resources/static/instance1.xml"));

//        zahtev.getPopunjavaZavod().setBrojPrijave(brojPrijave.toString());
        ZahtevZaPriznanjePatenta.setBrojPrijave(brojPrijave);
        intelektualnaSvojinaRepository.saveIntelektualnaSvojina(zahtev);

        String RDF_OUTPUT_FILE = "src/main/resources/static/rdfOutput.rdf";
        OutputStream out = new FileOutputStream(new File(RDF_OUTPUT_FILE));
        metadataExt.extractMetadata(xml, out);
        JenaWriter.saveRDF(RDF_OUTPUT_FILE);
    }

    public Object findIntelektualnaSvojinaById(String id) {
        return intelektualnaSvojinaRepository.findIntelektualnuSvojinuXml(id);
    }

    public void searchByMetadata() throws IOException {
        JenaReader.executeQuery();
    }

    public void generatePDF(String id, HttpServletResponse response, String svojinaString) 
            throws IOException, DocumentException, SAXException, JAXBException {
        String xslFile = "src/main/resources/static/ZahtevZaPriznanjePatentaXSLT.xsl";
        String outputHtmlFile = "src/main/resources/static/ZahtevZaPriznanjePatenta_" + id + ".html";
        String outputPdfFile = "src/main/resources/static/ZahtevZaPriznanjePatenta_" + id + ".pdf";
        String outputXmlFile = "src/main/resources/static/ZahtevZaPriznanjePatenta_" + id + ".xml";
        XSLTransormation xslTransormation = new XSLTransormation();
        xslTransormation.setXSLT_FILE(xslFile);
        xslTransormation.setOUTPUT_FILE_HTML(outputHtmlFile);
        xslTransormation.setOUTPUT_FILE_PDF(outputPdfFile);

        File file = new File("src/main/resources/static/instanca.xml");
        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = svojinaString.getBytes();
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(ZahtevZaPriznanjePatenta.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("src/main/resources/static/SZRSP_schema.xsd"));
        unmarshaller.setSchema(schema);
        ZahtevZaPriznanjePatenta zahtevZaPriznanjePatenta = (ZahtevZaPriznanjePatenta)
                unmarshaller.unmarshal(new File("src/main/resources/static/instanca.xml"));

        try {
            JAXBContext context2 = JAXBContext.newInstance("com.zakon.intelektualnasvojina.model.p1Obrazac");
            Marshaller marshaller = context2.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            System.out.println(System.getProperty("user.dir"));
            System.out.println("Putanja je:" + outputXmlFile);
            OutputStream outputStream = new FileOutputStream(outputXmlFile);
            marshaller.marshal(zahtevZaPriznanjePatenta, outputStream);
            outputStream.flush();
            outputStream.close();
            xslTransormation.generatePDF_HTML(outputXmlFile, response);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String generateHTMLL(Object svojina, String id) throws JAXBException, IOException, SAXException {
        File file = new File("src/main/resources/static/instanca.xml");
        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        return svojina.toString();
    }

    public String generateHTML(Object svojina, String id) throws JAXBException, IOException, SAXException {
        File file = new File("src/main/resources/static/instanca.xml");
        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        String svojinaString = svojina.toString();
        byte[] bytes = svojinaString.getBytes();
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(ZahtevZaPriznanjePatenta.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("src/main/resources/static/SZRSP_schema.xsd"));
        unmarshaller.setSchema(schema);
        ZahtevZaPriznanjePatenta zahtevZaPriznanjePatenta = (ZahtevZaPriznanjePatenta)
                unmarshaller.unmarshal(new File("src/main/resources/static/instanca.xml"));

        String xslFile = "src/main/resources/static/ZahtevZaPriznanjePatentaXSLT.xsl";
        String outputHtmlFile = "src/main/resources/static/ZahtevZaPriznanjePatenta_" + id + ".html";
        XSLTransormation xslTransormation = new XSLTransormation();
        xslTransormation.setXSLT_FILE(xslFile);
        xslTransormation.setOUTPUT_FILE_HTML(outputHtmlFile);
        JAXBContext context2 = JAXBContext.newInstance("com.zakon.intelektualnasvojina.model.p1Obrazac");
        Marshaller marshaller = context2.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        OutputStream outputStream = new FileOutputStream(xslTransormation.getOUTPUT_FILE_HTML());
        marshaller.marshal(zahtevZaPriznanjePatenta, outputStream);
        outputStream.flush();
        outputStream.close();
        return xslTransormation.generateResultHTML(xslTransormation.getOUTPUT_FILE_HTML());
    }

    public String getAll() {
        List<String> povratak = Arrays.asList(intelektualnaSvojinaRepository.getAll());
        String str = "";
        for (String entity : povratak) {
            str+=entity;
            str+=",";
        }
        StringBuffer sb= new StringBuffer(str);
        sb.deleteCharAt(sb.length()-1);
        return sb.toString();
    }

    public void prihvatiZahtev(String id) throws Exception {
        ZahtevZaPriznanjePatenta zahtevZaPriznanjePatenta = (ZahtevZaPriznanjePatenta)
                intelektualnaSvojinaRepository.findIntelektualnuSvojinuXml(id);
        zahtevZaPriznanjePatenta.setSTATUS("prihvacen");
        intelektualnaSvojinaRepository.saveIntelektualnaSvojina(zahtevZaPriznanjePatenta);
    }

    public void odbijahtev(String id) throws Exception {
        ZahtevZaPriznanjePatenta zahtevZaPriznanjePatenta = (ZahtevZaPriznanjePatenta)
                intelektualnaSvojinaRepository.findIntelektualnuSvojinuXml(id);
        zahtevZaPriznanjePatenta.setSTATUS("odbijen");
        intelektualnaSvojinaRepository.saveIntelektualnaSvojina(zahtevZaPriznanjePatenta);
    }
}
