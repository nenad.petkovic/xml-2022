package com.zakon.intelektualnasvojina.repository;

import com.zakon.intelektualnasvojina.additional.DbConnection;
import com.zakon.intelektualnasvojina.model.p1Obrazac.ZahtevZaPriznanjePatenta;
import org.exist.xmldb.EXistResource;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Repository
public class IntelektualnaSvojinaRepository {
    public final String ZAHTEV_ZA_PRIZNANJE_PATENTA = "/db/zahtevi_Za_Priznanje_Patenta";

    public Object findIntelektualnuSvojinuXml(String id) {
        String collectionId = ZAHTEV_ZA_PRIZNANJE_PATENTA;
        Collection collection = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        Object xml = null;

        try {
            collection = (Collection) DbConnection.getOrCreateCollection(collectionId);
            collection.setProperty(OutputKeys.INDENT, "yes");
            System.out.println("Pretraga po ID: " + id);
            res = (XMLResource) collection.getResource(id);
            JAXBContext context = JAXBContext.newInstance(ZahtevZaPriznanjePatenta.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new File("src/main/resources/static/SZRSP_schema.xsd"));
            unmarshaller.setSchema(schema);

            ZahtevZaPriznanjePatenta zahtev = (ZahtevZaPriznanjePatenta) unmarshaller.unmarshal(res.getContentAsDOM());
            zahtev.setBrojPrijave(id);

            JAXBContext zahtevContext = JAXBContext.newInstance(ZahtevZaPriznanjePatenta.class);

            Marshaller marshaller = zahtevContext.createMarshaller();

            SchemaFactory zahtevSchemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema zahtevSchema = zahtevSchemaFactory.newSchema(new File("src/main/resources/static/SZRSP_schema.xsd"));
            marshaller.setSchema(zahtevSchema);

            marshaller.marshal(zahtev, os);
            XMLResource xmlResource = (XMLResource) res;
            xmlResource.setContent(os);

            xml = xmlResource.getContent();
            System.out.println("Citanje:\n####################################################################\n");
            System.out.println(xml.toString());

        } catch (Exception e) {
//            throw new RuntimeException(e);
            e.printStackTrace();
        } finally {
            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(collection != null) {
                try {
                    collection.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return xml;
    }

    public void saveIntelektualnaSvojina(ZahtevZaPriznanjePatenta zahtevZaPriznanjePatenta) throws Exception {
        String collectionId = ZAHTEV_ZA_PRIZNANJE_PATENTA;
        Collection collection = null;
        XMLResource resource = null;
        OutputStream outputStream = new ByteArrayOutputStream();

        try {
            System.out.println("Dobavljanje kolekcije: " + collectionId);
            collection = (Collection) DbConnection.getOrCreateCollection(collectionId);
            resource = (XMLResource) collection.createResource(ZahtevZaPriznanjePatenta.getBrojPrijave(), XMLResource.RESOURCE_TYPE);
            JAXBContext jaxbContext = JAXBContext.newInstance(ZahtevZaPriznanjePatenta.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(zahtevZaPriznanjePatenta, outputStream);
            resource.setContent(outputStream);
            collection.storeResource(resource);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resource != null) {
                try {
                    ((EXistResource) resource).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
            if (collection != null) {
                try {
                    collection.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }

    }

    private String readFile(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, StandardCharsets.UTF_8);
    }

    public String[] getAll() {
        String collectionId = ZAHTEV_ZA_PRIZNANJE_PATENTA;
        Collection collection = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        Object xml = null;
        try {
            collection = (Collection) DbConnection.getOrCreateCollection(collectionId);
            collection.setProperty(OutputKeys.INDENT, "yes");
            return collection.listResources();
        } catch (XMLDBException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
