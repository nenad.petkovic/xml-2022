package com.zakon.intelektualnasvojina.additional;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.modules.CollectionManagementService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DbConnection {

    private static String connectionUri = "xmldb:sov://%1$s:%2$s/exist/xmlrpc";

    static public class ConnectionProperties {
        public String host;
        public int port = 8080;
        public String user;
        public String password;
        public String driver;
        public String uri;

        public ConnectionProperties(Properties properties) {
            super();

            user = properties.getProperty("conn.user").trim();
            password = properties.getProperty("conn.password").trim();

            host = properties.getProperty("conn.host").trim();
            port = Integer.parseInt(properties.getProperty("conn.port"));

            uri = String.format(connectionUri, host, port);

            driver = properties.getProperty("conn.driver").trim();
        }
    }

    public static ConnectionProperties loadProperties() throws IOException {
        String propsName = "exist.properties";
        InputStream propStream = openStream(propsName);
        if (propStream == null)
            throw new IOException("Neuspesno citanje podesavanja: " + propsName);
        Properties properties = new Properties();
        properties.load(propStream);
        return new ConnectionProperties(properties);
    }

    public static InputStream openStream(String fileName) throws IOException {
        return DbConnection.class.getClassLoader().getResourceAsStream(fileName);
    }

    public static Collection getOrCreateCollection(String collectionUri) throws Exception {
        return getOrCreateCollection(collectionUri, 0);
    }

    private static Collection getOrCreateCollection(String collectionUri, int pathSegmentOffset) throws Exception {
        ConnectionProperties properties = loadProperties();
        Class cl = Class.forName(properties.driver);
        Database database = (Database) cl.newInstance();
        database.setProperty("create-database", "true");
        database.setProperty("database-id", "sov");
        database.setProperty("configuration", "C:/eXist-db/test/conf.xml");
        DatabaseManager.registerDatabase(database);

        org.xmldb.api.base.Collection col = DatabaseManager.getCollection(properties.uri + collectionUri, properties.user, properties.password);

        // create the collection if it does not exist
        if(col == null) {

            if(collectionUri.startsWith("/")) {
                collectionUri = collectionUri.substring(1);
            }

            String pathSegments[] = collectionUri.split("/");

            if(pathSegments.length > 0) {
                StringBuilder path = new StringBuilder();

                for(int i = 0; i <= pathSegmentOffset; i++) {
                    path.append("/" + pathSegments[i]);
                }

                org.xmldb.api.base.Collection startCol = DatabaseManager.getCollection(properties.uri + path, properties.user, properties.password);

                if (startCol == null) {

                    // child collection does not exist

                    String parentPath = path.substring(0, path.lastIndexOf("/"));
                    org.xmldb.api.base.Collection parentCol = DatabaseManager.getCollection(properties.uri + parentPath, properties.user, properties.password);

                    CollectionManagementService mgt = (CollectionManagementService) parentCol.getService("CollectionManagementService", "1.0");

                    System.out.println("[INFO] Creating the collection: " + pathSegments[pathSegmentOffset]);
                    col = mgt.createCollection(pathSegments[pathSegmentOffset]);

                    col.close();
                    parentCol.close();

                } else {
                    startCol.close();
                }
            }
            return getOrCreateCollection(collectionUri, ++pathSegmentOffset);
        } else {
            return col;
        }

    }

}
