package com.zakon.intelektualnasvojina.repository;

import com.zakon.intelektualnasvojina.additional.DbConnection;
import com.zakon.intelektualnasvojina.model.p1Obrazac.resenjeDto.Resenje;
import org.exist.xmldb.EXistResource;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Repository
public class ResenjeRepository {


    public final String RESENJE_COLLECTION_NAME = "/db/resenjaPatentiLista";



    public Resenje findResenjeObjekat(String id){
        //metoda vraca objekat resenja pronadjen po id-iju
        String collectionId = RESENJE_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        Resenje xmlDokument = null;

        try {
            col = DbConnection.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            System.out.println("**********Id po kome trazimo je: "+ id);
            res = (XMLResource)col.getResource(id);

            JAXBContext context = JAXBContext.newInstance(Resenje.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("src/main/resources/static/resenje.xsd"));
            unmarshaller.setSchema(schema);

            // convert XML file to object
            Resenje resenje = (Resenje) unmarshaller.unmarshal(res.getContentAsDOM());
            resenje.setBrojResenja(id); //moramo postaviti borj prijave posto je u pitanju staticki atribut koji se menja
            xmlDokument = resenje;


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return xmlDokument;
    }

    public Object findResenjeXml(String id) {
        String collectionId = RESENJE_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        Object xml = null;

        try {
            col = DbConnection.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            System.out.println("**********Id po kome trazimo je: "+ id);
            res = (XMLResource)col.getResource(id);

            JAXBContext context = JAXBContext.newInstance(Resenje.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("src/main/resources/static/resenje.xsd"));
            unmarshaller.setSchema(schema);

            // convert XML file to object
            Resenje zahtev = (Resenje) unmarshaller.unmarshal(res.getContentAsDOM());
            zahtev.setBrojResenja(zahtev.getBrojResenja()); //moramo postaviti borj prijave posto je u pitanju staticki atribut koji se menja

            System.out.println(">>>>>>>>>>>>>>Broj resenja koji je postavljen je: " + zahtev.getBrojResenja());
            JAXBContext zahtevContext = JAXBContext.newInstance(Resenje.class);

            // create an instance of `Unmarshaller`
            Marshaller marshaller = zahtevContext.createMarshaller();

            SchemaFactory zahtevsf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema zahtevschema = zahtevsf.newSchema(new File("src/main/resources/static/resenje.xsd"));
            marshaller.setSchema(zahtevschema);

            marshaller.marshal(zahtev, os);
            XMLResource xmlResource = (XMLResource) res;
            xmlResource.setContent(os);
            xml = xmlResource.getContent();
            System.out.println("********* Procitano je: ");
            System.out.println(xml.toString());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return xml;
    }


    public void saveResenjeXml(Resenje resenje) throws Exception {
        String collectionId = RESENJE_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();

        try {

            //extractAndSaveMetadata();
            //System.out.println("********** PROCITANI METAPODACI *******************");
            //readSaglasnostMetadata();
            System.out.println("***************************************************");

            System.out.println("[INFO] Retrieving the collection: " + collectionId);
            col = DbConnection.getOrCreateCollection(collectionId);


            res = (XMLResource) col.createResource(resenje.getBrojResenja()+"", XMLResource.RESOURCE_TYPE);
            JAXBContext jaxbContext = JAXBContext.newInstance(Resenje.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);


            jaxbMarshaller.marshal(resenje, os);

            res.setContent(os);

            col.storeResource(res);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            //don't forget to cleanup
            if (res != null) {
                try {
                    ((EXistResource) res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if (col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
    }

    private String readFile(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, StandardCharsets.UTF_8);

    }
}
