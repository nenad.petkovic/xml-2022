<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:szrsp="https://www.serviszaradsapatentima.rs">
    <xsl:template match="/">

        <html>

            <head>
                <style>
                    #page{
                    }

                    .form-control {
                    border: none; border-bottom: 2px solid; border-bottom-style: dotted; width: 100%; margin-bottom: 20px;
                    }

                    #okvir{
                    border-style:solid;
                    }

                    #kontejner{
                    display: inline-block;
                    }

                    #Popunjava_zavod{
                    width: 400px; float: left; margin-right: 20px;
                    }

                    #nazivObrasca{
                    margin: 10px; width: 100%; float:left;
                    }
                </style>
            </head>

            <body>

                <div id="page">



                    <div id="kontejner">
                        <div id="Popunjava_zavod">
                            <p id="Broj_prijave" style="display: none; color: black; font-size: 10pt;text-transform:uppercase;"> <strong> <xsl:value-of select="//szrsp:Broj_prijave"/> </strong></p>
                            <p id="Datum_prijema" style="display: none; color: black; font-size: 10pt;text-transform:uppercase;"> <strong> <xsl:value-of select="//szrsp:Datum_prijema"/> </strong></p>
                            <p id="Priznati_datum_podnosenja" style="display: none; color: black; font-size: 10pt;text-transform:uppercase;"> <strong> <xsl:value-of select="//szrsp:Priznati_datum_podnosenja"/> </strong></p>
                            <p id="Pecat_potpis" style="display: none; color: black; font-size: 10pt;">Pecat/potpis</p>

                        </div>
                        <div id="nazivObrasca">
                            <h2> Obrazac P-1</h2>
                        </div>
                    </div>

                    <h3 style="text-align:center;"> Zahtev priznanje patenta</h3>

                    <hr style="height:7px; color:black"/>
                    <br/>

                    <div id="Naziv_pronalaska">
                        <p><b>Polje broj I --- Naziv pronalaska</b></p>
                        <p><i>Naziv pronalaska treba da jasno i sažeto izražava suštinu pronalaska i ne sme da sadrži izmišljene ili komercijalne nazive, žigove, imena, šifre, uobičajene skraćenice za proizvode i sl.</i></p>
                        <p>Na srpskom jeziku: <xsl:value-of select="//szrsp:Srpski"/> </p>
                        <p>Na engleskom jeziku: <xsl:value-of select="//szrsp:Engleski"/> </p>
                    </div>

                    <hr/>

                    <div id="Podnosilac_prijave">
                        <p><b>Polje broj II --- Podnosilac prijave</b></p>
                        <p>Ime i prezime / Poslovno ime: <xsl:value-of select="//szrsp:Podnosilac_prijave//szrsp:Ime"/> <xsl:value-of select="//szrsp:Podnosilac_prijave//szrsp:Prezime"/></p>
                        <p>Ulica i broj, poštanski broj, mesto i država: <xsl:value-of select="//szrsp:Podnosilac_prijave//szrsp:Adresa"/></p>
                        <p>Broj telefona: <xsl:value-of select="//szrsp:Podnosilac_prijave//szrsp:Broj_telefona"/></p>
                        <p>Broj faksa: <xsl:value-of select="//szrsp:Podnosilac_prijave//szrsp:Broj_faklsa"/></p>
                        <p>E-posta: <xsl:value-of select="//szrsp:Podnosilac_prijave//szrsp:Eposta"/></p>
                        <p>Drzavljanstvo: <xsl:value-of select="//szrsp:Podnosilac_prijave//szrsp:Drzavljanstvo"/></p>
                    </div>

                    <hr/>

                    <p>Pronalazac ne zeli da bude naveden u prijavi: <xsl:value-of select="//szrsp:Pronalazac_ne_zeli_da_bude_naveden_u_prijavi"/></p>

                    <div id="Pronalazac">
                        <p><b>Polje broj III --- Pronalazac</b></p>
                        <p style="color:gray; font-size:10px"><i>Ovo polje se popunjava samo u slucaju da podnosilac prijave nije pronalazac</i></p>
                        <p>Ime i prezime / Poslovno ime: <xsl:value-of select="//szrsp:Pronalazac//szrsp:Ime"/> <xsl:value-of select="//szrsp:Podnosilac_prijave//szrsp:Prezime"/></p>
                        <p>Ulica i broj, poštanski broj, mesto i država: <xsl:value-of select="//szrsp:Pronalazac//szrsp:Adresa"/></p>
                        <p>Broj telefona: <xsl:value-of select="//szrsp:Pronalazac//szrsp:Broj_telefona"/></p>
                        <p>Broj faksa: <xsl:value-of select="//szrsp:Pronalazac//szrsp:Broj_faklsa"/></p>
                        <p>E-posta: <xsl:value-of select="//szrsp:Pronalazac//szrsp:Eposta"/></p>
                        <p>Drzavljanstvo: <xsl:value-of select="//szrsp:Pronalazac//szrsp:Drzavljanstvo"/></p>
                    </div>

                    <hr/>

                    <div id="Punomocnik">
                        <p><b>Polje broj IV --- Punomocnik</b></p>
                        <p>Vrsta predstavnika <xsl:value-of select="//szrsp:vrsta_predstavnika"/></p>
                        <p>Ime i prezime / Poslovno ime: <xsl:value-of select="//szrsp:Punomocnik//szrsp:Ime"/> <xsl:value-of select="//szrsp:Podnosilac_prijave//szrsp:Prezime"/></p>
                        <p>Ulica i broj, poštanski broj, mesto i država: <xsl:value-of select="//szrsp:Punomocnik//szrsp:Adresa"/></p>
                        <p>Broj telefona: <xsl:value-of select="//szrsp:Punomocnik//szrsp:Broj_telefona"/></p>
                        <p>Broj faksa: <xsl:value-of select="//szrsp:Punomocnik//szrsp:Broj_faklsa"/></p>
                        <p>E-posta: <xsl:value-of select="//szrsp:Punomocnik//szrsp:Eposta"/></p>
                        <p>Drzavljanstvo: <xsl:value-of select="//szrsp:Punomocnik//szrsp:Drzavljanstvo"/></p>
                    </div>

                    <hr/>

                    <div id="Adresa_za_dostavljanje">
                        <p><b>Polje broj V --- Adresa za dostavljanje</b></p>
                        <p style="color:gray; font-size:10px"><i>(ovo polje se popunjava ako podnosilac prijave, zajednički predstavnik ili punomoćnik želi da se dostavljanje podnesaka vrši na drugoj adresi od njegove navedene adrese)</i></p>
                        <p><xsl:value-of select="//szrsp:Adresa_za_dostavljanje"/></p>
                    </div>

                    <hr/>

                    <div id="Nacin_dostavljanja">
                        <p><b>Polje broj VI --- Nacin dostavljanja</b></p>
                        <p><xsl:value-of select="//szrsp:Nacin_dostavljanja"/></p>
                    </div>

                    <hr/>

                    <div id="Nadovezivanje">
                        <p><b>Polje broj VII --- Dopunska/Izdvojena prijava</b></p>
                        <p style="color:gray; font-size:10px"><i>Ovo polje je opciono</i></p>
                        <p>ID osnovne prijave: <xsl:value-of select="//szrsp:ID_osnovne_prijave"/></p>
                        <p>Datum: <xsl:value-of select="//szrsp:Nadovezivanje//szrsp:Datum"/></p>
                    </div>

                    <hr/>

                    <div id="Pravo_prvenstva_ranije_prijave">
                        <p><b>Polje broj VIII --- Pravo prvenstva ranije prijave</b></p>
                        <p style="color:gray; font-size:10px"><i>Ovo polje je opciono</i></p>
                        <p>Datum podnosenja ranije prijave: <xsl:value-of select="//szrsp:Datum_podnosenja_ranije_prijave"/></p>
                        <p>ID ranije prijave: <xsl:value-of select="//szrsp:ID_ranije_prijave"/></p>
                        <p>Dvoslovna oznaka <xsl:value-of select="//szrsp:Dvoslovna_oznaka"/></p>
                    </div>


                </div>

            </body>

        </html>


    </xsl:template>
</xsl:stylesheet>