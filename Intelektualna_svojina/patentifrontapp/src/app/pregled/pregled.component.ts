import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as FileSaver from 'file-saver';
import { xml2js } from 'xml-js';
import { OfficialServiceService } from '../official-service.service';

@Component({
  selector: 'app-pregled',
  templateUrl: './pregled.component.html',
  styleUrls: ['./pregled.component.css']
})
export class PregledComponent {

  idDokumentaZaPregled: any = '';

  //podaci o poslatom zahtevu
  brojZahteva: any = '';
  podnosilac: any = '';
  nazivDela: any = '';
  emailPodnosioca: any = '';


  //izbor statusa zahteva
  prihvacenZahtev: boolean = false;
  odbijenZahtev: boolean = false;

  obrazlozenje: string = '';

  constructor( private route: ActivatedRoute,
               private officialServiceService: OfficialServiceService
  ) { }



  ngOnInit(): void {
    //podaci o id-iju dokumenta koji pregledamo iz Path parametara putanje>> preuzimamo
    this.route.queryParamMap.subscribe(
      (params) => {
        this.idDokumentaZaPregled = params.get('idDokumenta');

        console.log("Id dokumenta koji prikazujemo je: " + this.idDokumentaZaPregled);
        this.ucitajDokument(this.idDokumentaZaPregled);

        this.prihvacenZahtev = true;
        this.odbijenZahtev = false;

      }
    )

  }

  ucitajDokument(id: string){
    //ucitavamo dokument sa prosledjenim id-ijem >> ucitavamo njegove podatke u json formatu
   
      this.officialServiceService.getZahtevZaAutorskoPravoPoId(id).subscribe(
        (response: any)=>{
          //console.log("Izvrseno rezultat je: " + response);
          //console.log(typeof response);
  
          if (response == null){
            alert("Dokument sa tim id-ijem ne postoji..");
          }
          
          let konverzija = xml2js(response);
          
          console.log(konverzija);

          console.log("############################");
          console.log(konverzija.elements[0].elements[4].elements[0].elements[0]);

          //postavljamo nakon ucitavanja vrednosti osnovnih podataka o tom zahtevu
          this.brojZahteva = konverzija.elements[0].elements[1].elements[0].text;

          //naziv autorskog dela
          this.nazivDela=konverzija.elements[0].elements[3].elements[0].elements[0].text;
        
          //ime podnosioca
          this.podnosilac = konverzija.elements[0].elements[4].elements[0].elements[0].text;

          //email podnosioca
          this.emailPodnosioca = konverzija.elements[0].elements[4].elements[4].elements[0].text;
        }
      );
  }


  /****** izbor da li je zahtev prihvacen ili ne *******/
  handleChangeRadioButton(event: any){
    let target= event.target;

    if(target.checked){
      if (target.value== "PRIHVACEN" ){
        this.prihvacenZahtev = true;
        this.odbijenZahtev = false;
      }
      if( target.value == "ODBIJEN"){
        this.prihvacenZahtev = false;
        this.odbijenZahtev = true;
      }
    }
    
  }


  
  /*** izabrano generisanje html ****/
  generisiHtml(event: any){
      
      this.officialServiceService.getDokumentXHTML( this.brojZahteva, 'tip').subscribe(
        (response: any) =>
          {
            console.log("Pronadjeni dokument...");
            console.log(response);
            
            var blob = new Blob([response], {type: "text/plain;charset=utf-8"});
            FileSaver.saveAs(blob, "ZahtevZaAutorskoPravo"+this.brojZahteva+".xhtml");

          }
      );
  }


  generisiPdf(event: any){
      this.officialServiceService.getDokumentPDF(this.brojZahteva).subscribe(
        (response: any) =>
          {
            var blob = new Blob([response], {type: "text/plain;charset=utf-8"});
            FileSaver.saveAs(blob, "ZahtevZaAutorskoPravo"+this.brojZahteva+".pdf");

          }
      );
    }

    posaljiResenje(event: any){
      let imeUlogovanogSluzbenika:string = 'mika';
      let prezimeUlogovanogSluzbenika: string= 'mikic';
      let statusResenja: string = '';
       //proverimo da li je resenje odbijeno ili je prihvaceno
       if(this.prihvacenZahtev == true){
        //resenje je prihvaceno
        let datumPrihvatanja: Date = new Date();

        let formatiranDatumPrihvatanja: string = '';
        formatiranDatumPrihvatanja += datumPrihvatanja.getFullYear();
        formatiranDatumPrihvatanja += '-' + '02';//datumPrihvatanja.getMonth();
        formatiranDatumPrihvatanja += '-' + '07';//datumPrihvatanja.getDay();
        
        console.log("Datum prihvatanja zahteva je: "+ formatiranDatumPrihvatanja);
        
        statusResenja = `
            <PrihvacenZahtev>
              <SifraZahteva>`+ this.brojZahteva +`</SifraZahteva>
              <DatumPrihvatanja>` + formatiranDatumPrihvatanja + `</DatumPrihvatanja>
            </PrihvacenZahtev>
        `;
      } else{
        //resenje odbijeno
        let datumOdbijanja = new Date();
        
        let formatiranDatumOdbijanja: string = '';
        formatiranDatumOdbijanja += datumOdbijanja.getFullYear();
        formatiranDatumOdbijanja += '-' + '02';//datumOdbijanja.getMonth();
        formatiranDatumOdbijanja += '-' + '07';//datumOdbijanja.getDay();

        statusResenja=`
            <OdbijenZahtev>
              <Obrazlozenje>` + this.obrazlozenje+ ` </Obrazlozenje>
              <DatumOdbijanja>` + formatiranDatumOdbijanja + `</DatumOdbijanja>
            </OdbijenZahtev>
        `;
      }

      let broj: string = "1";
      
      let xmlResenje: string = `<?xml version="1.0" encoding="UTF-8"?>
      <res:Resenje
          xmlns:res="http://www.ftn.uns.ac.rs/resenje"
          xmlns="http://www.ftn.uns.ac.rs/resenje"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://www.ftn.uns.ac.rs/resenje file:/C:/Users/DM/Desktop/resenje.xsd" BrojResenja="` + broj + `">
          <res:TipZahtevaNaKojiSeResenjeOdnosi>AUTORSKO_DELO</res:TipZahtevaNaKojiSeResenjeOdnosi>
          <res:Sluzbenik>
              <res:Ime>` + imeUlogovanogSluzbenika +`</res:Ime>
              <res:Prezime>` + prezimeUlogovanogSluzbenika + `</res:Prezime>
          </res:Sluzbenik>
          <res:ReferencaNaZahtev>`+ this.brojZahteva +`</res:ReferencaNaZahtev>
          <res:StatusZahteva>
              `+ statusResenja +`
          </res:StatusZahteva>
      </res:Resenje>`;

      console.log(xmlResenje);

      /// poziv metode na backendu za slanje resenja
      this.officialServiceService.posaljiResenje(xmlResenje, this.brojZahteva).subscribe(
        (response: any) =>
          {
            alert("zahtev sacuvan!");
          }
      );

    }

}
