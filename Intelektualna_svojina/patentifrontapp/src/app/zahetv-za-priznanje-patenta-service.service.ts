import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { ZahtevZaPriznanjePatentaComponent } from 'src/app/zahtev-za-priznanje-patenta/zahtev-za-priznanje-patenta.component';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ZahtevZaPriznanjePatenta} from 'src/app/model/zahtevZaPriznanjePatenta'


@Injectable({
  providedIn: 'root'
})
export class ZahetvZaPriznanjePatentaServiceService {

  // Define API
  apiURL = 'http://localhost:8085/api/';
  private authHeder = new HttpHeaders();
  //private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  private headers = new HttpHeaders({ "Content-Type": "application/json" });

  constructor(private http:HttpClient) { }


  public add(noviZahtevXml:any, brojPrijave:any): Observable<string> {
    console.log(noviZahtevXml)
    return this.http.post(
      `${this.apiURL}zahtevZaPriznanjePatenta/createZahtevZaPriznanjePatenta/`+brojPrijave,noviZahtevXml, {responseType: "text"}
    );
  }


  handleError(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }





}
