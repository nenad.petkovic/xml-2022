export class ZahtevZaPriznanjePatenta {
    brojPrijave: string;
    popunjavaZavod: PopunjavaZavod;
    nazivPronalaska: NazivPronalaska;
    podnosilacPrijave: Covek;
    pronaloazacNeZeli: boolean;
    pronalazac: Covek;
    punomocnik: Covek
    adresaZaDostavljanje: Adresa;
    nadovezivanje: Nadovezivanje;
    pravoPrvenstva: PravoPrvenstva;

    constructor(obj?:any) {
        this.brojPrijave = (obj && obj.brojPrijave) || "";
        this.popunjavaZavod = (obj && obj.popunjavaZavod) || new PopunjavaZavod();
        this.nazivPronalaska = (obj && obj.nazivPronalaska) || new NazivPronalaska();
        this.podnosilacPrijave = (obj && obj.podnosilacPrijave) || new Covek();
        this.pronaloazacNeZeli = (obj && obj.pronaloazacNeZeli) || false;
        this.pronalazac = (obj && obj.pronalazac) || new Covek();
        this.punomocnik = (obj && obj.punomocnik) || new Covek();
        this.adresaZaDostavljanje = (obj && obj.adresaZaDostavljanje) || new Adresa();
        this.nadovezivanje = (obj && obj.nadovezivanje) || new Nadovezivanje();
        this.pravoPrvenstva = (obj && obj.pravoPrvenstva) || new PravoPrvenstva();
    }
}

export class PopunjavaZavod {
    datumPrijema: Date;
    priznatiDatumPodnosenja: Date;
    pecatPotpis: string;

    constructor(obj?:any) {
        this.datumPrijema = (obj && obj.datumPrijema) || undefined;
        this.priznatiDatumPodnosenja = (obj && obj.priznatiDatumPodnosenja) || undefined;
        this.pecatPotpis = (obj && obj.pecatPotpis) || "";
    }
}

export class NazivPronalaska {
    nazivPronalaskaSrpski: string;
    nazivPronalaskaEngleski: string;

    constructor(obj?:any) {
        this.nazivPronalaskaSrpski = (obj && obj.nazivPronalaskaSrpski) || "";
        this.nazivPronalaskaEngleski = (obj && obj.nazivPronalaskaEngleski) || "";
    }
}

export class Covek {
    ime: string;
    prezime: string;
    adresa: Adresa;
    drzavljanstvo: string;
    brTel: string;
    brFaksa: string;
    email: string;

    constructor(obj?:any) {
        this.ime = (obj && obj.ime) || "";
        this.prezime = (obj && obj.prezime) || "";
        this.adresa = (obj && obj.adresa) || new Adresa();
        this.drzavljanstvo = (obj && obj.drzavljanstvo) || "";
        this.brTel = (obj && obj.brTel) || "";
        this.brFaksa = (obj && obj.brFaksa) || "";
        this.email = (obj && obj.email) || "";
    }
}

export class Adresa {
    ulica: string;
    broj: string;
    posBroj: string;
    mesto: string;
    drzava: string;
    
    constructor(obj?:any) {
        this.ulica = (obj && obj.ulica) || "";
        this.broj = (obj && obj.broj) || "";
        this.posBroj = (obj && obj.posBroj) || "";
        this.mesto = (obj && obj.mesto) || "";
        this.drzava = (obj && obj.drzava) || "";
    }
}

export class Nadovezivanje {
    brojDopune: string;
    datumDopune: Date;

    constructor(obj?:any) {
        this.brojDopune = (obj && obj.brojDopune) || "";
        this.datumDopune = (obj && obj.datumDopune) || "";
    }
}

export class PravoPrvenstva {
    datumRanijePrijave: Date;
    brojRanijePrijave: string;
    dvoslovnaOznaka: string;

    constructor(obj?:any) {
        this.datumRanijePrijave = (obj && obj.datumRanijePrijave) || undefined;
        this.brojRanijePrijave = (obj && obj.brojRanijePrijave) || "";
        this.dvoslovnaOznaka = (obj && obj.dvoslovnaOznaka) || "";
    }
}