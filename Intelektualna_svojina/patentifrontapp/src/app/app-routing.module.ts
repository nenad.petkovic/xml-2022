import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ZahtevZaPriznanjePatentaComponent } from './zahtev-za-priznanje-patenta/zahtev-za-priznanje-patenta.component';
import { OfficialComponent } from './official/official.component';
import { PregledComponent } from './pregled/pregled.component';

const routes: Routes = [
  {path: 'zahtevZaPriznanjePatenta', component: ZahtevZaPriznanjePatentaComponent},
  {path: 'official', component: OfficialComponent},
  {path: 'pregled', component: PregledComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
