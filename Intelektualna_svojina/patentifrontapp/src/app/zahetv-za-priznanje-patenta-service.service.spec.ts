import { TestBed } from '@angular/core/testing';

import { ZahetvZaPriznanjePatentaServiceService } from './zahetv-za-priznanje-patenta-service.service';

describe('ZahetvZaPriznanjePatentaServiceService', () => {
  let service: ZahetvZaPriznanjePatentaServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ZahetvZaPriznanjePatentaServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
