import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { window } from 'rxjs';
//import { Metapodaci } from 'src/app/model/metapodaci';
import { OfficialServiceService } from 'src/app/official-service.service';
//import * as txml from 'txml';
//import { xml2json } from 'xml-js';
import {json2xml, xml2js} from 'xml-js';
import { saveAs } from 'file-saver';
import * as FileSaver from 'file-saver';
import { Router } from '@angular/router';
import { ZahtevZaPriznanjePatenta } from '../model/zahtevZaPriznanjePatenta';


@Component({
  selector: 'app-official',
  templateUrl: './official.component.html',
  styleUrls: ['./official.component.css']
})
export class OfficialComponent implements OnInit {

  zahtevZaPriznanjePatenta: ZahtevZaPriznanjePatenta = new ZahtevZaPriznanjePatenta();
  //pronadjeniObjekatDokumenta: any;

  //servis 
  //sluzbenikPretragaService: SluzbenikPretragaService = new SluzbenikPretragaService();

  constructor(private officialService: OfficialServiceService, private router: Router) { }

  //izabrani tip podnosioca zahteva za pretragu
  
  podatak: string = '';
  rezultatiPretrage: any[] = []

  ngOnInit(): void {
    console.log("######################################################")
    this.ucitajSveZahteve();
  }

  //ucitavanje svih zahteva iz baze
  ucitajSveZahteve(){

    this.officialService.ucitajSveZahteve().subscribe(
      (response: any) => {
        
        const myArray = response.split(",");
        
        //prolazimo kroz listu i konvertujemo odgovor u objekte koje pushujemo u listu koja se prikazuje
        for (let id of myArray) {
          this.rezultatiPretrage.push(id);
        }

      }
    );
  }

  onSubmit(forma: NgForm) {
    console.log("Kliknut je submit...");
  }


  pretragaPoImenuPodnosioca(event: any) {}
  pretragaPoPrezimenuPodnosioca(event: any) {}
  pretragaPoImenuSrpskom(event: any) {}
  pretragaPoImenuEngleskom(event: any) {}
  pretragaPoDatumu(event: any) {}


  // //******* PRETRAGA PO IMENU I PREZIMENU - FIZICKOG LICA *******/
  // pretragaPoImenuIPrezimenu(event: any) {
  //   console.log("Vrsi se pretraga po imenu i prezimenu fizickog lica " + this.imePretraga + " " + this.prezimePretraga);
  //   this.rezultatiPretrage = []; //cistimo listu ako je pre bilo rezultata

  //   //objekat koji saljemo sa vrednostima metapodataka
  //   let metapodatakJson= {
  //       "imePretraga": this.imePretraga,
  //       "prezimePretraga": this.prezimePretraga,
  //       "poslovnoImePretraga": null
  //   }


  //   //pogadjamo metodu za pretragu metapodataka na serveru
  //   this.sluzbenikPretragaService.pretragaPoImenuIPrezimenu(metapodatakJson).subscribe(
  //     {
  //       next: (response: any) => {
  //         console.log("Uspesno izvrseno..");
  //         console.log(response);
  //         for( let  odg of response){
  //           //*** rezultati pretrage ce nam biti objekti sa podesenim povratnim vrendostima za dokument sa tim id-ijem ***/

  //           this.sluzbenikPretragaService.getZahtevZaAutorskoPravoPoId(odg).subscribe(
  //             (response: any)=>{
  //                   console.log("Objekat je: " );
  //                   console.log(response);

  //                   let objekatJson = xml2js(response);
  //                   console.log(objekatJson);
  //                   //pravimo objekat sa kljucevima
  //                   let brojPrijave = objekatJson.elements[0].attributes.BrojPrijave;
  //                   let imePodnosioca = objekatJson.elements[0].elements[1].elements[0].elements[0].elements[0].text;
  //                   let emailPodnosiocaa = objekatJson.elements[0].elements[1].elements[2].elements[0].text;

  //                   if(emailPodnosiocaa == undefined){
  //                     emailPodnosiocaa = objekatJson.elements[0].elements[1].elements[1].elements[0].text; //nema broja telefona

  //                   }
  //                   let naslovDela = objekatJson.elements[0].elements[2].elements[0].elements[0].text;
                    
  //                   let zahtev = new AutorskoPravo();
  //                   zahtev.dokument_id = brojPrijave;
  //                   zahtev.podaci_o_podnosiocu_zahteva.fizickoLice.ime = imePodnosioca;
  //                   zahtev.podaci_o_autorskom_delu.naslovDela = naslovDela;
  //                   zahtev.podaci_o_podnosiocu_zahteva.email = emailPodnosiocaa;

  //                   this.rezultatiPretrage.push(zahtev);

  //                   console.log("Email je: " + emailPodnosiocaa);

            
  //                 }

  //           );
  //         }
  //         console.log("Vrste dela su: " + this.rezultatiPretrage);

         
          
  //         if (response.length == 0){
  //           //nema rezultata pretrage
  //           alert("Nema rezultata pretrage..");
  //         }
  //       },
  //       error: (error: HttpErrorResponse) => {
  //         console.log("Desio se error..");
  //       }
  //     }
  //   )

   
  // }
  // //******* PRETRAGA PO POSLOVNOM IMENU - PRAVNA LICA *******/
  // pretragaPoPoslovnomImenu(event: any) {
  //   this.rezultatiPretrage = [];
  //   console.log("Vrsi se pretraga po imenu pravnog lica " + this.poslovnoImePretraga );
    
  //   //objekat koji saljemo sa vrednostima metapodataka
  //   let metapodatakJson= {
  //       "poslovnoImePretraga": this.poslovnoImePretraga
  //   }


  //   //pogadjamo metodu za pretragu metapodataka na serveru
  //   this.sluzbenikPretragaService.pretragaPoPoslovnomImenu(metapodatakJson).subscribe(
  //     {
  //       next: (response: any) => {
  //         console.log("Uspesno izvrseno..");
  //         console.log(response);
  //         for( let  odg of response){
  //           //*** rezultati pretrage ce nam biti objekti sa podesenim povratnim vrendostima za dokument sa tim id-ijem ***/

  //           this.sluzbenikPretragaService.getZahtevZaAutorskoPravoPoId(odg).subscribe(
  //             (response: any)=>{
                   
  //                   let objekatJson = xml2js(response);
  //                   //pravimo objekat sa kljucevima
  //                   let brojPrijave = objekatJson.elements[0].attributes.BrojPrijave;
  //                   let imePodnosioca = objekatJson.elements[0].elements[1].elements[0].elements[0].elements[0].text;
  //                   let emailPodnosioca = objekatJson.elements[0].elements[1].elements[2].elements[0].text;
  //                   if(emailPodnosioca == undefined){
  //                     emailPodnosioca = objekatJson.elements[0].elements[1].elements[1].elements[0].text; //nema broja telefona

  //                   }
  //                   let naslovDela = objekatJson.elements[0].elements[2].elements[0].elements[0].text;
                    
  //                   let zahtev = new AutorskoPravo();
  //                   zahtev.dokument_id = brojPrijave;
  //                   zahtev.podaci_o_podnosiocu_zahteva.fizickoLice.ime = imePodnosioca;
  //                   zahtev.podaci_o_autorskom_delu.naslovDela = naslovDela;
  //                   zahtev.podaci_o_podnosiocu_zahteva.email = emailPodnosioca;

  //                   this.rezultatiPretrage.push(zahtev);

            
  //                 }

  //           );
  //         }

  //         if (response.length == 0){
  //           //nema rezultata pretrage
  //           alert("Nema rezultata pretrage..");
  //         }
  //       },
  //       error: (error: HttpErrorResponse) => {
  //         console.log("Desio se error..");
  //       }
  //     }
  //   )

   
  // }

  // //******* PRETRAGA PO EMAILU - PRAVNA LICA *******/
  // pretragaPoEmailu(event: any) {
  //   this.rezultatiPretrage = [];
  //   console.log("Ovo su rezu" + this.rezultatiPretrage);
  //   console.log("Vrsi se pretraga po emailu:  " + this.emailPretraga );
    
  //   //objekat koji saljemo sa vrednostima metapodataka
  //   let metapodatakJson= {
  //       "emailPretraga": this.emailPretraga
  //   }


  //   //pogadjamo metodu za pretragu metapodataka na serveru
  //   this.sluzbenikPretragaService.pretragaPoEmailu(metapodatakJson).subscribe(
  //     {
  //       next: (response: any) => {
  //         console.log("Uspesno izvrseno..");
  //         console.log("Odgovor: " + response);

  //         for( let  odg of response){
  //           //*** rezultati pretrage ce nam biti objekti sa podesenim povratnim vrendostima za dokument sa tim id-ijem ***/

  //           this.sluzbenikPretragaService.getZahtevZaAutorskoPravoPoId(odg).subscribe(
  //             (response: any)=>{
  //                   let objekatJson = xml2js(response);
  //                   //pravimo objekat sa kljucevima
  //                   let brojPrijave = objekatJson.elements[0].attributes.BrojPrijave;
  //                   let imePodnosioca = objekatJson.elements[0].elements[1].elements[0].elements[0].elements[0].text;
  //                   let emailPodnosioca = objekatJson.elements[0].elements[1].elements[2].elements[0].text;
  //                   if(emailPodnosioca == undefined){
  //                     emailPodnosioca =  objekatJson.elements[0].elements[1].elements[1].elements[0].text;
  //                   }
  //                   let naslovDela = objekatJson.elements[0].elements[2].elements[0].elements[0].text;
                    
  //                   let zahtev = new AutorskoPravo();
  //                   zahtev.dokument_id = brojPrijave;
  //                   zahtev.podaci_o_podnosiocu_zahteva.fizickoLice.ime = imePodnosioca;
  //                   zahtev.podaci_o_autorskom_delu.naslovDela = naslovDela;
  //                   zahtev.podaci_o_podnosiocu_zahteva.email = emailPodnosioca;

  //                   this.rezultatiPretrage.push(zahtev);

            
  //                 }

  //           );
  //         }
  //         console.log("Duzina responsa: " + response.length);
  //         if (response.length == 0){
  //           //nema rezultata pretrage
  //           alert("Nema rezultata pretrage..");
  //         }
  //       },
  //       error: (error: HttpErrorResponse) => {
  //         console.log("Desio se error..");
  //       }
  //     }
  //   )
   
  // }

  // //******* PRETRAGA PO nazivu dela *******/
  // pretragaPoNaslovuDela(event: any) {
  //   this.rezultatiPretrage = []; //ciscenje liste
  //   console.log("Vrsi se pretraga po  naslovu dela:  " + this.naslovDelaPretraga );
    
  //   //objekat koji saljemo sa vrednostima metapodataka
  //   let metapodatakJson= {
  //       "naslovPretraga": this.naslovDelaPretraga
  //   }


  //   //pogadjamo metodu za pretragu metapodataka na serveru
  //   this.sluzbenikPretragaService.pretragaPoNazivuDela(metapodatakJson).subscribe(
  //     {
  //       next: (response: any) => {
  //         console.log("Uspesno izvrseno..");
  //         console.log("Odgovor je: " + response);
          
  //         for( let  odg of response){
  //           //*** rezultati pretrage ce nam biti objekti sa podesenim povratnim vrendostima za dokument sa tim id-ijem ***/

  //           this.sluzbenikPretragaService.getZahtevZaAutorskoPravoPoId(odg).subscribe(
  //             (response: any)=>{
  //                   let objekatJson = xml2js(response);
  //                   //pravimo objekat sa kljucevima
  //                   let brojPrijave = objekatJson.elements[0].attributes.BrojPrijave;
  //                   let imePodnosioca = objekatJson.elements[0].elements[1].elements[0].elements[0].elements[0].text;
  //                   let emailPodnosioca = objekatJson.elements[0].elements[1].elements[2].elements[0].text;
  //                   if(emailPodnosioca == undefined){
  //                     emailPodnosioca =  objekatJson.elements[0].elements[1].elements[1].elements[0].text;
  //                   }
  //                   let naslovDela = objekatJson.elements[0].elements[2].elements[0].elements[0].text;
                    
  //                   let zahtev = new AutorskoPravo();
  //                   zahtev.dokument_id = brojPrijave;
  //                   zahtev.podaci_o_podnosiocu_zahteva.fizickoLice.ime = imePodnosioca;
  //                   zahtev.podaci_o_autorskom_delu.naslovDela = naslovDela;
  //                   zahtev.podaci_o_podnosiocu_zahteva.email = emailPodnosioca;

  //                   this.rezultatiPretrage.push(zahtev);

            
  //                 }

  //           );
  //         }

  //         if (response.length == 0){
  //           //nema rezultata pretrage
  //           alert("Nema rezultata pretrage..");
  //         }
  //       },
  //       error: (error: HttpErrorResponse) => {
  //         console.log("Desio se error..");
  //       }
  //     }
  //   )
   
  // }
  

  // //******* PRETRAGA PO nazivu dela alternativnom *******/
  // pretragaPoAlternativnomNazivuDela(event: any) {
  //   this.rezultatiPretrage = [];
  //   console.log("Vrsi se pretraga po alternativnom nazivu:  " + this.alternativniNaslovDelaPretraga );
    
  //   //objekat koji saljemo sa vrednostima metapodataka
  //   let metapodatakJson= {
  //       "alternativniNaslovPretraga": this.alternativniNaslovDelaPretraga
  //   }


  //   //pogadjamo metodu za pretragu metapodataka na serveru
  //   this.sluzbenikPretragaService.pretragaPoAlternativnomNazivuDela(metapodatakJson).subscribe(
  //     {
  //       next: (response: any) => {
  //         console.log("Uspesno izvrseno..");
  //         console.log(response);
  //         for( let  odg of response){
  //           //*** rezultati pretrage ce nam biti objekti sa podesenim povratnim vrendostima za dokument sa tim id-ijem ***/

  //           this.sluzbenikPretragaService.getZahtevZaAutorskoPravoPoId(odg).subscribe(
  //             (response: any)=>{
  //                   let objekatJson = xml2js(response);
  //                   //pravimo objekat sa kljucevima
  //                   let brojPrijave = objekatJson.elements[0].attributes.BrojPrijave;
  //                   let imePodnosioca = objekatJson.elements[0].elements[1].elements[0].elements[0].elements[0].text;
  //                   let emailPodnosioca = objekatJson.elements[0].elements[1].elements[2].elements[0].text;
  //                   if(emailPodnosioca == undefined){
  //                     emailPodnosioca =  objekatJson.elements[0].elements[1].elements[1].elements[0].text;
  //                   }
  //                   let naslovDela = objekatJson.elements[0].elements[2].elements[0].elements[0].text;
                    
  //                   let zahtev = new AutorskoPravo();
  //                   zahtev.dokument_id = brojPrijave;
  //                   zahtev.podaci_o_podnosiocu_zahteva.fizickoLice.ime = imePodnosioca;
  //                   zahtev.podaci_o_autorskom_delu.naslovDela = naslovDela;
  //                   zahtev.podaci_o_podnosiocu_zahteva.email = emailPodnosioca;

  //                   this.rezultatiPretrage.push(zahtev);

            
  //                 }

  //           );
  //         }
  //         if (response.length == 0){
  //           //nema rezultata pretrage
  //           alert("Nema rezultata pretrage..");
  //         }
  //       },
  //       error: (error: HttpErrorResponse) => {
  //         console.log("Desio se error..");
  //       }
  //     }
  //   )
   
  // }


  // //pretraga dokumenta po id-iju
  // getDokumentAutorskoPravo(id: string){
  //   this.sluzbenikPretragaService.getZahtevZaAutorskoPravoPoId(id).subscribe(
  //     (response: any)=>{
  //       console.log("Izvrseno rezultat je: " + response);
  //       console.log(typeof response);

  //       if (response == null){
  //         alert("Dokument sa tim id-ijem ne postoji..");
  //       }
        
  //       let konverzija = xml2js(response);
        
  //       console.log(konverzija);

  //       //ispis svih atributa
  //       console.log("Atribut broj prijave je: " );
  //       console.log(konverzija.elements[0].attributes.BrojPrijave);

  //       //naziv autorskog dela
  //       console.log("Naziv autorskog dela je: ")
  //       console.log(konverzija.elements[0].elements[2].elements[0].elements[0].text)
        
  //       //podnosioci zahteva su
  //       if(konverzija.elements[0].elements[1].elements[0].elements[0].elements[0].text != null){
  //         console.log("ispisujemo ime podnosioca zahteva: ");
  //         console.log(konverzija.elements[0].elements[1].elements[0].elements[0].elements[0].text);
  //       }

  //       //email podnosioca zahteva:
  //       console.log("Email podnosioca zhateva: ");
  //       console.log(konverzija.elements[0].elements[1].elements[2].elements[0].text);
  //       return konverzija;


  //       //this.rezultatiPretrage.push()
        
  //     }

  //   );
  // }

  //******* pregledanje izabranog dokumenta *********//
  pregledajDokumentKlik(event: any):void{
    //gadjamo metodu servisa za pregled dokumenta

    let target = event.target;
    console.log("**Id dugmeta na koje smo kliknuli je: "  + target.id);

    //prelazimo na novu stranicu >> nova komponenta za pregled celog dokumenta
    this.router.navigate(["pregled"], {queryParams:  {idDokumenta: target.id }});

    /*this.xmlPdfServis.getDokumentXHTML('A-8343886105781104134', 'tip').subscribe(
      (response: any) =>
        {
          console.log("Pronadjeni dokument...");
          console.log(response);

          document.open("", '_blank');
          document.write(response);
          
          var blob = new Blob([response], {type: "text/plain;charset=utf-8"});
          FileSaver.saveAs(blob, "ZahtevZaAutorskoPravo"+'A-8343886105781104134'+".xhtml");

        }
    );*/

  }

}
