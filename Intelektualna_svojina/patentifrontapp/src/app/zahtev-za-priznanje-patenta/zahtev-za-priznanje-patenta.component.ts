import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ZahetvZaPriznanjePatentaServiceService } from '../zahetv-za-priznanje-patenta-service.service';
import { ZahtevZaPriznanjePatenta, PopunjavaZavod, NazivPronalaska, Covek, Adresa, Nadovezivanje, PravoPrvenstva} from 'src/app/model/zahtevZaPriznanjePatenta';

@Component({
  selector: 'app-zahtev-za-priznanje-patenta',
  templateUrl: './zahtev-za-priznanje-patenta.component.html',
  styleUrls: ['./zahtev-za-priznanje-patenta.component.css']
})
export class ZahtevZaPriznanjePatentaComponent implements OnInit{
  
  zahtevZaPriznanjePatenta: ZahtevZaPriznanjePatenta = new ZahtevZaPriznanjePatenta();

  constructor(private zahetvZaPriznanjePatentaServiceService: ZahetvZaPriznanjePatentaServiceService) {
    
  }

  brojPrijave = Math.trunc(Math.random() * (999999 - 99999) + 99999);

  xmlZahtev: string = '';

  selektovanPodnosilacJeIPronalac:boolean = false;
  selekcijaPronalazacNeZeli:boolean = false;
  punomocnik:string = '';
  nacinDostavljanja:string = '';
  dopuna:string = '';


  ngOnInit(): void {

    //this.selektovanPodnosilacJeIPronalac = false;
    
  }

  /****************** b u t t o n    c h a n g e s ************************/

  handleChangeRadioButton(event: any) {
    let target = event.target;
    if (target.checked) {
      if (target.value == "DA") {
        this.selektovanPodnosilacJeIPronalac = true;
      }
      if (target.value == "NE") {
        this.selektovanPodnosilacJeIPronalac = false;
      }
    }
  }

  handleChangepronalazacNeZeliButton(event: any) {
    if (this.selekcijaPronalazacNeZeli == false) {
      this.selekcijaPronalazacNeZeli = true;
    }
    else {
      this.selekcijaPronalazacNeZeli = false;
    }
  }

  handleChangePunomocnik(event: any) {
    let target = event.target;
    if (target.checked) {
      this.punomocnik = target.value;
    }
  }

  handleChangeNacinDostavljanja(event: any) {
    let target = event.target;
    if (target.checked) {
      this.nacinDostavljanja = target.value;
    }
  }

  handleChangeDopuna(event: any) {
    let target = event.target;
    if (target.checked) {
      this.dopuna = target.value;
    }
  }


  /*************************** v a l i d a c i j a ***************************/

  validacijaEmail(): boolean{
    let paternValidacije = new RegExp('[a-zA-z0-9\\.]+[@]{1,1}[a-z\\.a-z]+');
    if (this.selektovanPodnosilacJeIPronalac || this.selekcijaPronalazacNeZeli) {
      return (paternValidacije.test(this.zahtevZaPriznanjePatenta.podnosilacPrijave.email)
            && paternValidacije.test(this.zahtevZaPriznanjePatenta.punomocnik.email)
            );
    } else {
      return (paternValidacije.test(this.zahtevZaPriznanjePatenta.podnosilacPrijave.email)
            && paternValidacije.test(this.zahtevZaPriznanjePatenta.pronalazac.email)
            && paternValidacije.test(this.zahtevZaPriznanjePatenta.punomocnik.email)
            );
    }
  }

  validacijaBrojaTelefona(): boolean{
    let paternValidacije = new RegExp("[\+]?[0-9]+");
    let duzina1 = this.zahtevZaPriznanjePatenta.podnosilacPrijave.brTel.length;
    let duzina2 = this.zahtevZaPriznanjePatenta.punomocnik.brTel.length;

    if ((duzina1 < 8 && duzina1 >15) && (duzina2 < 8 && duzina2 >15)) {
      return false;
    }
    
    if (this.selektovanPodnosilacJeIPronalac || this.selekcijaPronalazacNeZeli) {
      return (paternValidacije.test(this.zahtevZaPriznanjePatenta.podnosilacPrijave.brTel)
            && paternValidacije.test(this.zahtevZaPriznanjePatenta.punomocnik.brTel)
            );
    } else {
      return (paternValidacije.test(this.zahtevZaPriznanjePatenta.podnosilacPrijave.brTel)
            && paternValidacije.test(this.zahtevZaPriznanjePatenta.pronalazac.brTel)
            && paternValidacije.test(this.zahtevZaPriznanjePatenta.punomocnik.brTel)
            );
    }
  }

  validacijaBroja(vrednostKojuValidiramo: any): boolean {
    let paternBroj = new RegExp("[0-9]+");
    return paternBroj.test(vrednostKojuValidiramo);
  }

  validacijaAdrese(adresa: any): boolean{
    let validna = true;

    if(adresa.mesto == '' || adresa.broj == '' || adresa.posBroj=='' || adresa.ulica=='') {
      console.log("Nisu uneseni podaci o adresi...");
      alert("nisu uneseni podaci o adresi");
      validna =  false;
    }
    else if(this.validacijaBroja(adresa.posBroj)== false || this.validacijaBroja(adresa.broj) == false) {
      console.log("Neispravan unos (nije broj...)");
      alert("Neispravan unos (nije broj...)");
      validna = false;
    }
    return validna;
  }

  validacijaForma(): boolean {
    /*********PROVERA IMENA */
    if(this.zahtevZaPriznanjePatenta.nazivPronalaska.nazivPronalaskaSrpski=='' && this.zahtevZaPriznanjePatenta.nazivPronalaska.nazivPronalaskaEngleski=='') {
      alert("Nije unesen naziv");
      return false;
    }
    /********PROVERA PODNOSIOCA */
    if (this.zahtevZaPriznanjePatenta.podnosilacPrijave.ime == '' && this.zahtevZaPriznanjePatenta.podnosilacPrijave.prezime== '') {
      alert("Nisu uneseni podaci o podnosiocu!");
      return false;
    }
    let podnosilac = this.zahtevZaPriznanjePatenta.podnosilacPrijave;
    let adresaPodnosilac = this.validacijaAdrese(podnosilac.adresa);
    if (!adresaPodnosilac) {
      alert("Molimo unesite adresu podnosioca zahteva u ispravnom formatu..");
      return false;
    }
    /*********PROVERA PRONALAZACA */
    if (!this.selektovanPodnosilacJeIPronalac && !this.selekcijaPronalazacNeZeli) {
      if (this.zahtevZaPriznanjePatenta.pronalazac.ime == '' && this.zahtevZaPriznanjePatenta.pronalazac.prezime== '') {
        alert("Nisu uneseni podaci o pronalazacu!");
        return false;
      }
      let pronalazac = this.zahtevZaPriznanjePatenta.pronalazac;
      let adresaPronalazac = this.validacijaAdrese(pronalazac.adresa);
      if (!adresaPronalazac) {
        alert("Molimo unesite adresu pronalazaca zahteva u ispravnom formatu..");
        return false;
      }
    }
    /**********PROVERA PUNOMOCNIKA */
    if (this.zahtevZaPriznanjePatenta.punomocnik.ime == '' && this.zahtevZaPriznanjePatenta.punomocnik.prezime== '') {
      alert("Nisu uneseni podaci o punomocniku!");
      return false;
    }
    let punomocnik = this.zahtevZaPriznanjePatenta.punomocnik;
    let adresaPunomocnik = this.validacijaAdrese(punomocnik.adresa);
    if (!adresaPunomocnik) {
      alert("Molimo unesite adresu punomocnika zahteva u ispravnom formatu..");
      return false;
    }
    if (this.punomocnik=='') {
      console.log("Nije odabrana vrsta punomocnika");
      alert("Nije odabrana vrsta punomocnika");
      return false;      
    }
    /***************PROVERA ADRESE ZA DOSTAVLJANJE */
    if (this.zahtevZaPriznanjePatenta.adresaZaDostavljanje.broj != '' ||
        this.zahtevZaPriznanjePatenta.adresaZaDostavljanje.mesto != '' ||
        this.zahtevZaPriznanjePatenta.adresaZaDostavljanje.posBroj != '' ||
        this.zahtevZaPriznanjePatenta.adresaZaDostavljanje.ulica != '') {
          let adresazadostavljanje = this.validacijaAdrese(this.zahtevZaPriznanjePatenta.adresaZaDostavljanje);
          if (!adresazadostavljanje) {
            alert("Molimo Vas ili unesite adresu za dostavljanje ispravnom formatu ili ostavite prazno");
            return false;
          }
    }
    /**********email validacije */
    if (!this.validacijaEmail()) {
      console.log("Neispravno unet mejl!");
      alert("Neispravno unet mejl!");
      return false;
    }
    /************brojevi telefona validacije */
    if (!this.validacijaBrojaTelefona()) {
      console.log("Neispravan broj telefona!")
      alert("Neispravan broj telefona!");
      return false;
    }

    if (this.nacinDostavljanja == '') {
      console.log("Nije unesen nacin dostavljanja");
      alert("Nije unesen nacin dostavljanja");
      return false;
      
    }
    /**************** ONDA VALJA */
    return true;
  }


  onSubmit(form: NgForm) {

    let imenaString = `<Naziv_pronalaska>
      <Srpski property="nazivsrpski">`+this.zahtevZaPriznanjePatenta.nazivPronalaska.nazivPronalaskaSrpski+`</Srpski>
      <Engleski property="nazivengleski">`+this.zahtevZaPriznanjePatenta.nazivPronalaska.nazivPronalaskaEngleski+`</Engleski>
      </Naziv_pronalaska>`;

    let podnosilacString = `<Podnosilac_prijave Podnosilac_prijave_je_i_pronalazac="`+this.selektovanPodnosilacJeIPronalac+`">
      <Ime property="ime">`+this.zahtevZaPriznanjePatenta.podnosilacPrijave.ime+`</Ime>
      <Prezime property="prezime">`+this.zahtevZaPriznanjePatenta.podnosilacPrijave.prezime+`</Prezime>
      <Adresa>
          <Ulica>`+this.zahtevZaPriznanjePatenta.podnosilacPrijave.adresa.ulica+`</Ulica>
          <broj>`+this.zahtevZaPriznanjePatenta.podnosilacPrijave.adresa.broj+`</broj>
          <Postanski_broj>`+this.zahtevZaPriznanjePatenta.podnosilacPrijave.adresa.posBroj+`</Postanski_broj>
          <Mesto>`+this.zahtevZaPriznanjePatenta.podnosilacPrijave.adresa.mesto+`</Mesto>
          <Drzava>`+this.zahtevZaPriznanjePatenta.podnosilacPrijave.adresa.drzava+`</Drzava>
      </Adresa>
      <Broj_telefona>`+this.zahtevZaPriznanjePatenta.podnosilacPrijave.brTel+`</Broj_telefona>
      <Eposta>`+this.zahtevZaPriznanjePatenta.podnosilacPrijave.email+`</Eposta>
      </Podnosilac_prijave>`;

    let pronalazacnezeliString = `<Pronalazac_ne_zeli_da_bude_naveden_u_prijavi>`+
      this.selekcijaPronalazacNeZeli+`</Pronalazac_ne_zeli_da_bude_naveden_u_prijavi>`;

    let pronalazacString = ``;
    if (!this.selektovanPodnosilacJeIPronalac && !this.selekcijaPronalazacNeZeli) {
      pronalazacString = `<Pronalazac>
        <Ime>`+this.zahtevZaPriznanjePatenta.punomocnik.ime+`</Ime>
        <Prezime>`+this.zahtevZaPriznanjePatenta.punomocnik.prezime+`</Prezime>
        <Adresa>
            <Ulica>`+this.zahtevZaPriznanjePatenta.punomocnik.adresa.ulica+`</Ulica>
            <broj>`+this.zahtevZaPriznanjePatenta.punomocnik.adresa.broj+`</broj>
            <Postanski_broj>`+this.zahtevZaPriznanjePatenta.punomocnik.adresa.posBroj+`</Postanski_broj>
            <Mesto>`+this.zahtevZaPriznanjePatenta.punomocnik.adresa.mesto+`</Mesto>
            <Drzava>`+this.zahtevZaPriznanjePatenta.punomocnik.adresa.drzava+`</Drzava>
        </Adresa>
        <Broj_telefona>`+this.zahtevZaPriznanjePatenta.punomocnik.brTel+`</Broj_telefona>
        <Eposta>`+this.zahtevZaPriznanjePatenta.punomocnik.email+`</Eposta>
        </Pronalazac>`;
    }

    let punomocnikString = `<Punomocnik>
      <Ime>`+this.zahtevZaPriznanjePatenta.punomocnik.ime+`</Ime>
      <Prezime>`+this.zahtevZaPriznanjePatenta.punomocnik.prezime+`</Prezime>
      <Adresa>
          <Ulica>`+this.zahtevZaPriznanjePatenta.punomocnik.adresa.ulica+`</Ulica>
          <broj>`+this.zahtevZaPriznanjePatenta.punomocnik.adresa.broj+`</broj>
          <Postanski_broj>`+this.zahtevZaPriznanjePatenta.punomocnik.adresa.posBroj+`</Postanski_broj>
          <Mesto>`+this.zahtevZaPriznanjePatenta.punomocnik.adresa.mesto+`</Mesto>
          <Drzava>`+this.zahtevZaPriznanjePatenta.punomocnik.adresa.drzava+`</Drzava>
      </Adresa>
      <Broj_telefona>`+this.zahtevZaPriznanjePatenta.punomocnik.brTel+`</Broj_telefona>
      <Eposta>`+this.zahtevZaPriznanjePatenta.punomocnik.email+`</Eposta>
      <vrsta_predstavnika>`+this.punomocnik+`</vrsta_predstavnika>
      </Punomocnik>`;

    let adresazadostavljanjeString = ``;
    if (this.zahtevZaPriznanjePatenta.adresaZaDostavljanje.broj != '' ||
        this.zahtevZaPriznanjePatenta.adresaZaDostavljanje.mesto != '' ||
        this.zahtevZaPriznanjePatenta.adresaZaDostavljanje.posBroj != '' ||
        this.zahtevZaPriznanjePatenta.adresaZaDostavljanje.ulica != '') {
          adresazadostavljanjeString = `<Adresa_za_dostavljanje>
            <Ulica>`+this.zahtevZaPriznanjePatenta.adresaZaDostavljanje.ulica+`</Ulica>
            <broj>`+this.zahtevZaPriznanjePatenta.adresaZaDostavljanje.broj+`</broj>
            <Postanski_broj>`+this.zahtevZaPriznanjePatenta.adresaZaDostavljanje.posBroj+`</Postanski_broj>
            <Mesto>`+this.zahtevZaPriznanjePatenta.adresaZaDostavljanje.mesto+`</Mesto>
            <Drzava>Srbija</Drzava>
            </Adresa_za_dostavljanje>`;
    }

    let nacindostavljanjaString = `<Nacin_dostavljanja>`+this.nacinDostavljanja+`</Nacin_dostavljanja>`;

    let dopunaString = ``;
    if (this.dopuna != '' ||
        this.zahtevZaPriznanjePatenta.nadovezivanje.brojDopune != '') {
          dopunaString = `<Nadovezivanje>
            <newElement>`+this.dopuna+`</newElement>
            <ID_osnovne_prijave>`+this.zahtevZaPriznanjePatenta.nadovezivanje.brojDopune+`</ID_osnovne_prijave>
            <Datum>`+`20`+this.zahtevZaPriznanjePatenta.nadovezivanje.datumDopune.toString()+`</Datum>
            </Nadovezivanje>`;
    }

    let prvenstvoString = ``;
    if (this.zahtevZaPriznanjePatenta.pravoPrvenstva.brojRanijePrijave != '' ||
        this.zahtevZaPriznanjePatenta.pravoPrvenstva.dvoslovnaOznaka != '') {
          prvenstvoString = `<Pravo_prvenstva_ranije_prijave>
            <Datum_podnosenja_ranije_prijave>`+`20`+
            this.zahtevZaPriznanjePatenta.pravoPrvenstva.datumRanijePrijave.toString()+`</Datum_podnosenja_ranije_prijave>
            <ID_ranije_prijave>`+this.zahtevZaPriznanjePatenta.pravoPrvenstva.brojRanijePrijave+`</ID_ranije_prijave>
            <Dvoslovna_oznaka>`+this.zahtevZaPriznanjePatenta.pravoPrvenstva.dvoslovnaOznaka+`</Dvoslovna_oznaka>
            </Pravo_prvenstva_ranije_prijave>`;
    }

    this.xmlZahtev = `<?xml version="1.0" encoding="UTF-8"?>
      <Zahtev_za_priznanje_patenta xmlns="https://www.serviszaradsapatentima.rs"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="https://www.serviszaradsapatentima.rs file:/C:/Users/Luka/IdeaProjects/XML/xml-2022/Intelektualna_svojina/Intelektualna_svojina/src/main/resources/static/SZRSP_schema.xsd">
          <STATUS>cekanje</STATUS>
          <Broj_prijave property="brojprijave">`+this.brojPrijave+`</Broj_prijave>
          <Popunjava_zavod>
              <Datum_prijema property="datumprijema">2023-02-07</Datum_prijema>
              <Priznati_datum_podnosenja property="datumpodnosenja">2023-02-07</Priznati_datum_podnosenja>
              <Pecat_potpis>pecat_potpis</Pecat_potpis>
          </Popunjava_zavod>
          `+imenaString+podnosilacString+pronalazacnezeliString+pronalazacString+punomocnikString+adresazadostavljanjeString
          +nacindostavljanjaString+dopunaString+prvenstvoString+`
      </Zahtev_za_priznanje_patenta>
    `;

    let validnaForma = this.validacijaForma();


    if(validnaForma){
      console.log("Slanje zahteva na backend...");

      this.zahetvZaPriznanjePatentaServiceService.add(this.xmlZahtev, this.brojPrijave).subscribe({
        next: (response: any) => {
          console.log('Uspesno poslato:', response);
          //window.location.href = '/moji-dokumenti'
          alert("Vas zahtev je poslat..."); 
        },
        error: (error: HttpErrorResponse) => {
          console.log(error.message);
          //alert('Zahtev je vec kreiran..');
          
        },
      });
    }
    else{
      console.log("Podaci uneseni u formi su nevalidni...");
    }



  }


}
