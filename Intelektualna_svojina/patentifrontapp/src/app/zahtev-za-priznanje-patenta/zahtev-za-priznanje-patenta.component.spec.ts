import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZahtevZaPriznanjePatentaComponent } from './zahtev-za-priznanje-patenta.component';

describe('ZahtevZaPriznanjePatentaComponent', () => {
  let component: ZahtevZaPriznanjePatentaComponent;
  let fixture: ComponentFixture<ZahtevZaPriznanjePatentaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZahtevZaPriznanjePatentaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ZahtevZaPriznanjePatentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
