import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ZahtevZaPriznanjePatentaComponent } from './zahtev-za-priznanje-patenta/zahtev-za-priznanje-patenta.component';
import { OfficialComponent } from './official/official.component';
import { PregledComponent } from './pregled/pregled.component';

@NgModule({
  declarations: [
    AppComponent,
    ZahtevZaPriznanjePatentaComponent,
    OfficialComponent,
    PregledComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
