import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OfficialServiceService {

  // Define API
  apiURL = 'http://localhost:8085/api/';
  private authHeder = new HttpHeaders();
  //private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  private headers = new HttpHeaders();

  
  constructor( private http:HttpClient){
   this.headers.append('Access-Control-Allow-Origin', '*');
   this.headers.append('Access-Control-Allow-Credentials', 'true');
  }

 public pretragaPoImenuIPrezimenu( metapodaci: any): Observable<any> {
   console.log(metapodaci)

   return this.http.post<any>(
     `${this.apiURL}pretraga/pretragaMetapodaci/imePrezime`,metapodaci, {  responseType: 'json'}
   );
 }

 public pretragaPoPoslovnomImenu(metapodaci: any): Observable<any>{
   return this.http.post(
     `${this.apiURL}pretraga/pretragaMetapodaci/poslovnoIme`, metapodaci, {responseType: 'json'}
   )
 }

 public pretragaPoNazivuDela(metapodaci: any): Observable<any>{
   console.log("Pogodjena metoda...");
   console.log("Metapodaci" + metapodaci);
   return this.http.post(
     `${this.apiURL}pretraga/pretragaMetapodaci/naslovDela`, metapodaci, {responseType: 'json'}
   )
 }
 public pretragaPoAlternativnomNazivuDela(metapodaci: any): Observable<any>{
   return this.http.post(
     `${this.apiURL}pretraga/pretragaMetapodaci/alternativniNaslovDela`, metapodaci, {responseType: 'json'}
   )

 }
 public getZahtevZaAutorskoPravoPoId(id: any){
   console.log("Gadjamo metodu...");
   return this.http.get(
     `${this.apiURL}zahtevZaPriznanjePatenta/readZahtevZaPriznanjePatenta/`+id, {responseType: "text"}
   )
 }
 

 /******* slanje resenja  ****/
 public posaljiResenje(resenje: any, brojZahteva:any): Observable<Blob>{
   return this.http.post(
     `${this.apiURL}zahtevZaPriznanjePatenta/prihvatiZahtev/`+brojZahteva, resenje, {responseType: "blob"}
   );
 }

 //ucitavanje svih zahteva u redosledu datuma
 public ucitajSveZahteve():Observable<any>{
   return this.http.get(`${this.apiURL}zahtevZaPriznanjePatenta/getAll`, {responseType:'text'});
 }







 public getDokumentXHTML(id: string, tipDokumenta: string): Observable<string> {
  return this.http.get(
    this.apiURL + 'zahtevZaPriznanjePatenta/generateHTML/' + id,
    { responseType: 'text' }
  );
}

public getDokumentPDF(id: string): Observable<Blob> {
  return this.http.get(
    this.apiURL + 'zahtevZaPriznanjePatenta/generatePDF/'+ id,
    { responseType: 'blob' }
  );
}






}
