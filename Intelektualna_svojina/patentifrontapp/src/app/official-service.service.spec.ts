import { TestBed } from '@angular/core/testing';

import { OfficialServiceService } from './official-service.service';

describe('OfficialServiceService', () => {
  let service: OfficialServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfficialServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
