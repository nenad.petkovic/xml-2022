<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:res="http://www.ftn.uns.ac.rs/resenje">
<xsl:template match="/">
        <html>
            <head>
                <style>
                    #page{
                    }

                    #title{
                    text-align: center;
                    }

                    #grupa{
                    padding-left: 50px;
                    }

                    .form-control {
                    border: none; border-bottom: 2px solid; border-bottom-style: dotted; width: 100%; margin-bottom: 20px;
                    }

                    #okvir{
                    border-style:solid;
                    }

                    #kontejner{
                    display: inline-block;
                    }

                    #okvirZavod{
                    width: 400px; float: left; margin-right: 20px;
                    }

                    #nazivObrasca{
                    margin: 10px; width: 100%; float:left;
                    }

                    #kontejnerBrojTelEmail{
                    display: inline-block;
                    }

                    #emailDiv{
                    float:left; margin: 10px;border: 2px solid black; width: 50%;
                    }

                    #brojTelefonaDiv{
                    float:left; margin: 10px; border: 2px solid black; width: 50%;
                    }

                </style>
            </head>
            <body>
                <div id="page">
                    <div id="kontejner">
                        <div id="okvirZavod">
                            <p id="nazivUstanove" style="display: none; color: black; font-size: 10pt;text-transform:uppercase;"> Tip zahteva: <strong> <xsl:value-of select="//res:TipZahtevaNaKojiSeResenjeOdnosi"/> </strong></p>
                            <p id="adresaZavoda" style="display: none; color: black; font-size: 10pt;">
                               Sluzbenik: <xsl:value-of select="//res:Sluzbenik"></xsl:value-of>
                            </p>

                        </div>

                        <div id="nazivObrasca">
                            <p>Broj resenja: <strong> <xsl:value-of select="//res:BrojResenja"></xsl:value-of> </strong> </p>
                        </div>

                    </div>

                    <h3 style="text-align:center;"> Resenje o zahtevu za autorska i srodna prava </h3>

                    <hr/>
                    <br/>

                    <div id="podnosilacZahtevaPodaci">
                        <p> Status zahteva:</p>
                        <br/>
                        <br/>
                        <xsl:if test="//res:PrihvacenZahtev">
                            <!-- ako je zahtev prihvacen prikazujemo sledece informacije -->
                            <p> Vas zahtev je prihvacen.. </p>
                            <br/>
                            <p>
                                Datum:<xsl:value-of select="//res:DatumPrihvatanja"></xsl:value-of>
                            </p>
                            <p>Sifra zahteva: <xsl:value-of select="//res:SifraZahteva"></xsl:value-of> </p>

                        </xsl:if>

                        <xsl:if test="//res:OdbijenZahtev">

                            <p>Nazalost vas zahtev je odbijen..</p>
                            <br/>
                            <p>Obrazlozenje:  <xsl:value-of select="//res:Obrazlozenje"> </xsl:value-of></p>
                            <p>Datum: <xsl:value-of select="//res:DatumOdbijanja"></xsl:value-of></p>
                        </xsl:if>

                    </div>


                </div>

    </body>

        </html>
    </xsl:template>
</xsl:stylesheet>