<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:a1="http://www.ftn.uns.ac.rs/obrazacA1">
<xsl:template match="/">
        <html>
            <head>
                <style>
                    #page{
                    }

                    #title{
                    text-align: center;
                    }

                    #grupa{
                    padding-left: 50px;
                    }

                    .form-control {
                    border: none; border-bottom: 2px solid; border-bottom-style: dotted; width: 100%; margin-bottom: 20px;
                    }

                    #okvir{
                    border-style:solid;
                    }

                    #kontejner{
                    display: inline-block;
                    }

                    #okvirZavod{
                    width: 400px; float: left; margin-right: 20px;
                    }

                    #nazivObrasca{
                    margin: 10px; width: 100%; float:left;
                    }

                    #kontejnerBrojTelEmail{
                    display: inline-block;
                    }

                    #emailDiv{
                    float:left; margin: 10px;border: 2px solid black; width: 50%;
                    }

                    #brojTelefonaDiv{
                    float:left; margin: 10px; border: 2px solid black; width: 50%;
                    }

                </style>
            </head>
            <body>
                <div id="page">
                    <div id="kontejner">
                        <div id="okvirZavod">
                            <p id="nazivUstanove" style="display: none; color: black; font-size: 10pt;text-transform:uppercase;"> <strong> <xsl:value-of select="//a1:NazivUstanove"/> </strong></p>
                            <p id="adresaZavoda" style="display: none; color: black; font-size: 10pt;">
                                <xsl:value-of select="//a1:Ulica"></xsl:value-of> <xsl:value-of select="//a1:Broj"></xsl:value-of>
                            </p>

                        </div>

                        <div id="nazivObrasca">
                            <h2> Obrazac A-1</h2>
                        </div>

                    </div>

                    <h3 style="text-align:center;"> Zahtev za unosenje u evidenciju i deponovanje autorskih dela</h3>

                    <hr/>
                    <br/>

                    <div id="podnosilacZahtevaPodaci">
                        <p>1. Podnosilac - ime, prezime, adresa i drzavljanstvo autora ili drugog nosioca autorskog prava ako je podnosilac fizicko lice, odnosno poslovno ime i sediste nosioca autorskog prava ako je podnosilac pravno lice:

                        </p>
                        <p>
                            <xsl:value-of select="//a1:FizickoLice"></xsl:value-of>
                            <xsl:value-of select="//a1:PravnoLice"></xsl:value-of>
                            <xsl:value-of select="//a1:Drzavljanstvo"></xsl:value-of>
                        </p>
                        <div id="kontejnerBrojTelEmail">
                            <div id="emailDiv">
                                <p>e-mail: <xsl:value-of select="//a1:E-mail"></xsl:value-of> </p>
                            </div>

                            <div id="brojTelefonaDiv">
                                <p>telefon: <xsl:value-of select="//a1:BrojTelefona"></xsl:value-of> </p>
                            </div>
                        </div>
                    </div>

                    <br/>
                    <div id="pseudonimZnakAutora">
                        <p>2. Pseudonim ili znak autora(ako ga ima): </p>
                        <xsl:value-of select="//a1:PseudonimAutora"></xsl:value-of>
                        <xsl:value-of select="//a1:ZnakAutora"></xsl:value-of>

                    </div>

                    <br/>

                    <div id="podaciPunomocnika">
                        <p>3. Ime, prezime i adresa punomocnika, ako se prijava podnosi preko punomocnika: </p>
                        <p>
                            <xsl:value-of select="//a1:Punomocnik"></xsl:value-of>
                        </p>
                    </div>

                    <br/>
                    <div id="podaciOAutorskomDelu">
                        <p>4. Naslov autorskog dela, odnosno alternativni naslov, ako ga ima, po kome autorsko delo moze da se identifikuje: </p>
                        <p>
                            <xsl:value-of select="//a1:NaslovAutorskogDela"></xsl:value-of>
                            <xsl:value-of select="//a1:AlternativniNaslovAutorskogDela"></xsl:value-of>
                        </p>
                    </div>

                    <br/>
                    <div id="podaciOdeluNaKomeSeZasniva">
                        <p>5. Podaci o autorkom delu na kome se zasniva delo prerade, ako je u pitanju autorsko delo prerade, kao i podatak o autoru izvornog dela: </p>
                        <p>
                            <xsl:value-of select="//a1:NaslovDelaNaKomeSeZasniva"></xsl:value-of>
                            <xsl:value-of select="//a1:AutoriIzvornogDela"></xsl:value-of>
                        </p>
                    </div>

                    <br/>
                    <div id="vrstaAutrskogDela">
                        <p>6. Podaci o vrsti autorskog dela (knjizevno delo, likovno delo, racunarski program i dr.): </p>
                        <p>
                            <xsl:value-of select="//a1:VrstaAutorskogDela"></xsl:value-of>
                        </p>
                    </div>

                    <br/>
                    <div>
                        <p>7. Podaci o formi zapisa autroskog dela (stampani tekst, opticki disk i slicno): </p>
                        <p>
                            <xsl:value-of select="//a1:FormaZapisaDela"></xsl:value-of>
                        </p>
                    </div>

                    <br/>
                    <div>
                        <p>8. Podaci o autoru ako podnosilac prijave iz tacke 1. ovog zahteva nije autor i to: prezime, ime, adresa, i dravljanstvo autora(grupe autora ili koatura), ako su u pitanju jedan ili vise autora koji nisu zivi, imena autora i godine smrti autora ako je u pitanju autorsko delo anonimnog autora navod ada je autorkso delo anonimnog autora: </p>

                            <xsl:if test="//a1:PodnosilacZahteva!='AUTOR'">
                                <!-- Za svakog autora ili koautora ispisujemo podatke -->
                                <xsl:for-each select="//a1:AutoriDela">
                                    <!-- ako su u pitanju jedan ili vise autora >> zivi su-->
                                    <xsl:if test="//a1:ZivAutor">
                                        <xsl:value-of select="//a1:ZivAutor"></xsl:value-of>
                                        <!--ako je autor anoniman navod -->
                                        <xsl:if test="a1:ZivAutor/AnonimniAutor='DA'">
                                            <p>Autor je anoniman..</p>
                                        </xsl:if>
                                    </xsl:if>
                                    <!--ako je delo mrtvog autora-->
                                    <xsl:if test="//a1:PreminuoAutor">
                                        <xsl:value-of select="//a1:PreminuoAutor"></xsl:value-of>
                                    </xsl:if>

                                    <!-- ako je u pitanju grupa koautora-->

                                </xsl:for-each>

                            </xsl:if>
                    </div>

                    <br/>
                    <div>
                        <p>9. Podatak da li je u pitanju autorsko delo stvoreno u radnom odnosu: </p>
                        <p>
                            <xsl:value-of select="//a1:DeloStvorenoURadnomOdnosu"></xsl:value-of>
                        </p>
                    </div>

                    <br/>
                    <div>
                        <p>10. Nacin korscenja autrskog dela ili nameravani nacin korsicenja autrskog dela: </p>
                        <p>
                            <xsl:value-of select="//a1:NacinKoriscenjaDela"></xsl:value-of>
                        </p>

                    </div>

                    <br/>
                    <div style="display: inline-block;">
                            <div style="float:left; width: 60%;">
                                <p>11.</p>
                            </div>

                            <div style="width: 100%; float: left;">
                                <br/>
                                <br/>
                                <br/>
                                <hr/>
                                <p>Podnosilac prijave, nosilac prava</p>
                                <p>(mesto za potpis fizickog lica, odnosno potpis zastupnika pravnog lica ili ovlascenog predstavnika u pravnom licu)</p>
                            </div>
                    </div>

                </div>


    </body>

        </html>
    </xsl:template>
</xsl:stylesheet>