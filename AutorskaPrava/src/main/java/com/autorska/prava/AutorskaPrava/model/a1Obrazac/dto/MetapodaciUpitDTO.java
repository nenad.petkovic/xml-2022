package com.autorska.prava.AutorskaPrava.model.a1Obrazac.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement
public class MetapodaciUpitDTO {
    /*private Map<String, String> mapaMetapodataka = new HashMap<String, String>();

    public Map<String, String> getAddressMap() {
        return mapaMetapodataka;
    }

    public void setAddressMap(Map<String, String> metadataMap) {
        this.mapaMetapodataka = metadataMap;*/
    private String imePretraga;
    private String prezimePretraga;
    private String poslovnoImePretraga;

    private String emailPretraga;

    private String naslovPretraga;

    private String alternativniNaslovPretraga;

    private String vrstaDelaPretraga;

    public String getNaslovPretraga() {
        return naslovPretraga;
    }

    public void setNaslovPretraga(String naslovPretraga) {
        this.naslovPretraga = naslovPretraga;
    }

    public String getAlternativniNaslovPretraga() {
        return alternativniNaslovPretraga;
    }

    public void setAlternativniNaslovPretraga(String alternativniNaslovPretraga) {
        this.alternativniNaslovPretraga = alternativniNaslovPretraga;
    }

    public String getVrstaDelaPretraga() {
        return vrstaDelaPretraga;
    }

    public void setVrstaDelaPretraga(String vrstaDelaPretraga) {
        this.vrstaDelaPretraga = vrstaDelaPretraga;
    }

    public String getEmailPretraga() {
        return emailPretraga;
    }

    public void setEmailPretraga(String emailPretraga) {
        this.emailPretraga = emailPretraga;
    }


    public String getImePretraga() {
        return imePretraga;
    }

    public String getPrezimePretraga() {
        return prezimePretraga;
    }

    public String getPoslovnoImePretraga() {
        return poslovnoImePretraga;
    }

    public void setImePretraga(String imePretraga) {
        this.imePretraga = imePretraga;
    }

    public void setPrezimePretraga(String prezimePretraga) {
        this.prezimePretraga = prezimePretraga;
    }

    public void setPoslovnoImePretraga(String poslovnoImePretraga) {
        this.poslovnoImePretraga = poslovnoImePretraga;
    }
}

