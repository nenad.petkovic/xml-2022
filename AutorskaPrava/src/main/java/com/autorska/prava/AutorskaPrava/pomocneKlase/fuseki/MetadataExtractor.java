package com.autorska.prava.AutorskaPrava.pomocneKlase.fuseki;

//import com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl;
//import com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl;
import net.sf.saxon.TransformerFactoryImpl;
import org.xml.sax.SAXException;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

public class MetadataExtractor {
    private TransformerFactory transformerFactory;

    private static final String XSLT_FAJL_PUTANJA = "./src/main/resources/static/data/grddl.xsl";


    public MetadataExtractor() throws SAXException, IOException {

        // Setup the XSLT transformer factory
        transformerFactory = new TransformerFactoryImpl();
    }

    /**
     * Generisanje RDF/XML bazirannog na RDFa metapodacima koji se nalaze u xml fajlu
     * pomocu GRDDL XSL transformacije
     * @param in ulazni xml strema
     * @param out RDF/XML izlazni strim
     */
    public void extractMetadata(InputStream in, OutputStream out) throws FileNotFoundException, TransformerException {

        // Kreiranje sorsa za transformaciju
        StreamSource transformSource = new StreamSource(new File(XSLT_FAJL_PUTANJA));

        // Inicijalizacija GRRDL transformacionog objekta
        Transformer grddlTransformer = transformerFactory.newTransformer(transformSource);

        // Podesavaje identacije
        grddlTransformer.setOutputProperty("{http://xml.apache.org/xalan}indent-amount", "2");
        grddlTransformer.setOutputProperty(OutputKeys.INDENT, "yes");

        // Predmet transformacije inicijalizacija
        StreamSource izvor = new StreamSource(in);

        // Inicijalizacija streama za rezultate
        StreamResult rezultat = new StreamResult(out);

        // Trigerovanje transformacije
        grddlTransformer.transform(izvor, rezultat);

    }


    //pokusaj testiranja
    /*public void test() throws Exception {

        System.out.println("[INFO:] " + MetadataExtractor.class.getSimpleName());

        String putanjaFajla = "gen/grddl_metadata.rdf";

        InputStream in = new FileInputStream(new File("static/data/rdfa/contacts.xml"));

        OutputStream out = new FileOutputStream(putanjaFajla);

        extractMetadata(in, out);

        System.out.println("[INFO] File \"" + putanjaFajla + "\" generated successfully.");

        System.out.println("[INFO] End.");

    }*/

    public static void main(String[] args) throws Exception {
        //new MetadataExtractor().test();
        //testiranje primer
    }
}
