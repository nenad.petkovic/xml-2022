package com.autorska.prava.AutorskaPrava.service;

import com.autorska.prava.AutorskaPrava.model.a1Obrazac.dto.MetapodaciUpitDTO;
import com.autorska.prava.AutorskaPrava.pomocneKlase.SparqlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class PretragaService {

    @Autowired
    SparqlService sparqlService;


    public ArrayList<String> searchMetadataImePrezime(MetapodaciUpitDTO metapodaci) throws IOException {
        //vracamo metapodatke iz hash mape
        //kljuc >> ime
        System.out.println(metapodaci.getImePretraga());
        String imeZaPretragu = metapodaci.getImePretraga();
        //kljuc >> prezime
        String prezimeZaPretragu = metapodaci.getPrezimePretraga();
        System.out.println("Prosledjeni podaci za pretragu po imenu i prezimenu su: " + imeZaPretragu + " "+ prezimeZaPretragu);

        //pozivamo metodu sparql servisa za pretragu podataka
        List<SparqlService.SparqlQueryResult> rezultati = sparqlService.getAllPretragaMetapodaciPoImenuIPrezimenu(imeZaPretragu, prezimeZaPretragu );
        System.out.println(rezultati);

        ArrayList<String> listaIdPronadjenihDokumenata = new ArrayList<>();

        for (SparqlService.SparqlQueryResult res : rezultati){
            System.out.println(res.getVarName());
            System.out.println(res.getVarValue());

            //parsiranje tako da dobijemo samo id pronadjenog dokumenta
            String vrednost = res.getVarValue().toString();
            String izdeljeno[] = vrednost.split("/");
            String id = izdeljeno[izdeljeno.length-1]; //uzimamo poslednju vrednost kao vrednost id
            System.out.println("Ovo je vrednost id dokumenta: " + id);
            listaIdPronadjenihDokumenata.add(id);
            System.out.println("-----------------------------------");
        }
        //vrati osnovne informacije za dokument sa tim id-ijem
        return listaIdPronadjenihDokumenata;
    }

    public ArrayList<String> searchMetadataEmail(MetapodaciUpitDTO metapodaci) throws IOException {

        String email = metapodaci.getEmailPretraga();
        System.out.println("Prosledjeni metapodaci za pretragu po emailu posiljaoca zahteva: " + email);

        //pozivamo metodu sparql servisa za pretragu podataka
        List<SparqlService.SparqlQueryResult> rezultati = sparqlService.getAllPretragaMetapodatakaPoEmailu(email );
        System.out.println(rezultati);

        ArrayList<String> listaIdPronadjenihDokumenata = new ArrayList<>();

        for (SparqlService.SparqlQueryResult res : rezultati){
            System.out.println(res.getVarName());
            System.out.println(res.getVarValue());

            //parsiranje tako da dobijemo samo id pronadjenog dokumenta
            String vrednost = res.getVarValue().toString();
            String izdeljeno[] = vrednost.split("/");
            String id = izdeljeno[izdeljeno.length-1]; //uzimamo poslednju vrednost kao vrednost id
            System.out.println("Ovo je vrednost id dokumenta: " + id);
            listaIdPronadjenihDokumenata.add(id);
            System.out.println("-----------------------------------");
        }
        return listaIdPronadjenihDokumenata;
    }

    public ArrayList<String> searchMetadataNaslovDela(MetapodaciUpitDTO metapodaci) throws IOException {

        String naslov = metapodaci.getNaslovPretraga();
        System.out.println("Prosledjeni metapodaci za pretragu po naslovu dela : " + naslov);

        //pozivamo metodu sparql servisa za pretragu podataka
        List<SparqlService.SparqlQueryResult> rezultati = sparqlService.getAllPretragaMetapodatakaPoNaslovu(naslov );
        System.out.println(rezultati);

        ArrayList<String> listaIdPronadjenihDokumenata = new ArrayList<>();

        for (SparqlService.SparqlQueryResult res : rezultati){
            System.out.println(res.getVarName());
            System.out.println(res.getVarValue());

            //parsiranje tako da dobijemo samo id pronadjenog dokumenta
            String vrednost = res.getVarValue().toString();
            String izdeljeno[] = vrednost.split("/");
            String id = izdeljeno[izdeljeno.length-1]; //uzimamo poslednju vrednost kao vrednost id
            System.out.println("Ovo je vrednost id dokumenta: " + id);
            listaIdPronadjenihDokumenata.add(id);
            System.out.println("-----------------------------------");
        }
        return listaIdPronadjenihDokumenata;
    }

    public ArrayList<String> searchMetadataAlternativniNaslovDela(MetapodaciUpitDTO metapodaci) throws IOException {

        String alternativniNaslov = metapodaci.getAlternativniNaslovPretraga();
        System.out.println("Prosledjeni metapodaci za pretragu po alternativnom naslovu dela : " + alternativniNaslov);

        //pozivamo metodu sparql servisa za pretragu podataka
        List<SparqlService.SparqlQueryResult> rezultati = sparqlService.getAllPretragaMetapodatakaPoAlternativnomNaslovu(alternativniNaslov );
        System.out.println(rezultati);

        ArrayList<String> listaIdPronadjenihDokumenata = new ArrayList<>();

        for (SparqlService.SparqlQueryResult res : rezultati){
            System.out.println(res.getVarName());
            System.out.println(res.getVarValue());

            //parsiranje tako da dobijemo samo id pronadjenog dokumenta
            String vrednost = res.getVarValue().toString();
            String izdeljeno[] = vrednost.split("/");
            String id = izdeljeno[izdeljeno.length-1]; //uzimamo poslednju vrednost kao vrednost id
            System.out.println("Ovo je vrednost id dokumenta: " + id);
            listaIdPronadjenihDokumenata.add(id);
            System.out.println("-----------------------------------");
        }
        return listaIdPronadjenihDokumenata;
    }

    public ArrayList<String> searchMetadataVrstaDela(MetapodaciUpitDTO metapodaci) throws IOException {

        String vrstaDela = metapodaci.getVrstaDelaPretraga();
        System.out.println("Prosledjeni metapodaci za pretragu po vrsti dela: " + vrstaDela);

        //pozivamo metodu sparql servisa za pretragu podataka
        List<SparqlService.SparqlQueryResult> rezultati = sparqlService.getAllPretragaMetapodatakaPoVrstiDela(vrstaDela );
        System.out.println(rezultati);

        ArrayList<String> listaIdPronadjenihDokumenata = new ArrayList<>();

        for (SparqlService.SparqlQueryResult res : rezultati){
            System.out.println(res.getVarName());
            System.out.println(res.getVarValue());

            //parsiranje tako da dobijemo samo id pronadjenog dokumenta
            String vrednost = res.getVarValue().toString();
            String izdeljeno[] = vrednost.split("/");
            String id = izdeljeno[izdeljeno.length-1]; //uzimamo poslednju vrednost kao vrednost id
            System.out.println("Ovo je vrednost id dokumenta: " + id);
            listaIdPronadjenihDokumenata.add(id);
            System.out.println("-----------------------------------");
        }
        return listaIdPronadjenihDokumenata;
    }

    public ArrayList<String> searchMetadataPoslovnoIme(MetapodaciUpitDTO metapodaci) throws IOException {

        String poslovnoIme = metapodaci.getPoslovnoImePretraga();
        System.out.println("Prosledjeni metapodaci za pretragu po poslovnom imenu pravnog lica: " + poslovnoIme);

        //pozivamo metodu sparql servisa za pretragu podataka
        List<SparqlService.SparqlQueryResult> rezultati = sparqlService.getAllPretragaPoPoslovnomImenu(poslovnoIme );
        System.out.println(rezultati);

        ArrayList<String> listaIdPronadjenihDokumenata = new ArrayList<>();

        for (SparqlService.SparqlQueryResult res : rezultati){
            System.out.println(res.getVarName());
            System.out.println(res.getVarValue());

            //parsiranje tako da dobijemo samo id pronadjenog dokumenta
            String vrednost = res.getVarValue().toString();
            String izdeljeno[] = vrednost.split("/");
            String id = izdeljeno[izdeljeno.length-1]; //uzimamo poslednju vrednost kao vrednost id
            System.out.println("Ovo je vrednost id dokumenta: " + id);
            listaIdPronadjenihDokumenata.add(id);
            System.out.println("-----------------------------------");
        }
        return listaIdPronadjenihDokumenata;
    }

    /*********** servis za pretragu po svim metapodacima od jednom or pretraga samo za one koje zelimo *************/
    //svi uslovi u pretrazi moraju biti zadovoljeni >> vraca uniju svih rezultata
    public ArrayList<String> searchMetadataOR(MetapodaciUpitDTO metapodaci) throws IOException {
        //vracamo metapodatke iz hash mape
        //kljuc >> ime
        System.out.println(metapodaci.getImePretraga());
        String imeZaPretragu = metapodaci.getImePretraga();
        //kljuc >> prezime
        String prezimeZaPretragu = metapodaci.getPrezimePretraga();

        //poslovno ime
        String posovnoIme = metapodaci.getPoslovnoImePretraga();

        //email
        String email = metapodaci.getEmailPretraga();

        //naslov dela
        String naslovDela = metapodaci.getNaslovPretraga();

        //alternativni naslov dela
        String alternativniNaslovDela = metapodaci.getAlternativniNaslovPretraga();

        //pozivamo metodu sparql servisa za pretragu podataka

        //pretraga po imenu i prezimenu ako je uneseno
        List<SparqlService.SparqlQueryResult> rezultatiImePrezime = null;
        List<SparqlService.SparqlQueryResult> rezultatiPoslovnoIme = null;
        List<SparqlService.SparqlQueryResult> rezultatiEmail = null;
        List<SparqlService.SparqlQueryResult> rezultatiNaslov = null;
        List<SparqlService.SparqlQueryResult> rezultatiAlternativniNaslov = null;

        //zajednicka pretraga po svim podacima
        if(!(imeZaPretragu.equals("") && prezimeZaPretragu.equals(""))){
            rezultatiImePrezime = sparqlService.getAllPretragaMetapodaciPoImenuIPrezimenu(imeZaPretragu, prezimeZaPretragu);
        }
        if(!(posovnoIme.equals(""))){
            rezultatiPoslovnoIme = sparqlService.getAllPretragaPoPoslovnomImenu(posovnoIme);
        }
        if(!(naslovDela.equals(""))){
            rezultatiPoslovnoIme = sparqlService.getAllPretragaPoPoslovnomImenu(naslovDela);
        }
        if(!alternativniNaslovDela.equals("")){
            rezultatiAlternativniNaslov = sparqlService.getAllPretragaMetapodatakaPoAlternativnomNaslovu(alternativniNaslovDela);
        }
        if(!naslovDela.equals("")){
            rezultatiNaslov = sparqlService.getAllPretragaMetapodatakaPoNaslovu(naslovDela);
        }
        if(!email.equals("")){
            rezultatiEmail = sparqlService.getAllPretragaMetapodatakaPoEmailu(email);
        }

        //radimo uniju svih dobijenih id-podataka
        ArrayList<String> konacniRezultatiId = new ArrayList<>();

        //iteriramo kroz svaku listu i parsiramo id-ijeve ako nisu u konacnoj dodajemo ih kao rezultat
        if (rezultatiImePrezime != null){
            List<String> rezultatiImePrezimeStringId = parsirajListuIdDokumenata(rezultatiImePrezime);

            //prolazimo kroz sve liste i ako im id-ijevi nisu u konacnom rezultatu dodajemo ih
            for(String id : rezultatiImePrezimeStringId){
                if (!konacniRezultatiId.contains(id)) {
                    konacniRezultatiId.add(id);
                }
            }
        }
        if(rezultatiPoslovnoIme != null){
            List<String> rezutatiPoslovnoImeStringId = parsirajListuIdDokumenata(rezultatiPoslovnoIme);
            //prolazimo kroz sve liste i ako im id-ijevi nisu u konacnom rezultatu dodajemo ih
            for(String id : rezutatiPoslovnoImeStringId){
                if (!konacniRezultatiId.contains(id)) {
                    konacniRezultatiId.add(id);
                }
            }
        }
        if(rezultatiEmail != null){
            List<String> emailStringId = parsirajListuIdDokumenata(rezultatiEmail);

            for(String id : emailStringId){
                if (!konacniRezultatiId.contains(id)) {
                    konacniRezultatiId.add(id);
                }
            }

        }
        if(rezultatiNaslov != null){
            List<String> rezultatiNaslovStringId = parsirajListuIdDokumenata(rezultatiNaslov);

            for(String id : rezultatiNaslovStringId){
                if (!konacniRezultatiId.contains(id)) {
                    konacniRezultatiId.add(id);
                }
            }
        }
        if(rezultatiAlternativniNaslov != null){
            List<String> alternativniStringId = parsirajListuIdDokumenata(rezultatiAlternativniNaslov);

            for(String id : alternativniStringId){
                if (!konacniRezultatiId.contains(id)) {
                    konacniRezultatiId.add(id);
                }
            }

        }
        return konacniRezultatiId;
    }


    //***************** PRETRAGA PO SVIM METAPODACIMA >> AND ***************/
    //svi podaci moraju biti uneseni
    public ArrayList<String> searchMetadataAND(MetapodaciUpitDTO metapodaci) throws IOException {
        //kljuc >> ime
        System.out.println(metapodaci.getImePretraga());
        String imeZaPretragu = metapodaci.getImePretraga();
        //kljuc >> prezime
        String prezimeZaPretragu = metapodaci.getPrezimePretraga();

        //poslovno ime
        String posovnoIme = metapodaci.getPoslovnoImePretraga();

        //email
        String email = metapodaci.getEmailPretraga();
        System.out.println("Email je: " + email);

        //naslov dela
        String naslovDela = metapodaci.getNaslovPretraga();

        System.out.println("Naslov je " + naslovDela);
        //alternativni naslov dela
        String alternativniNaslovDela = metapodaci.getAlternativniNaslovPretraga();

        //pozivamo metodu sparql servisa za pretragu podataka

        List<SparqlService.SparqlQueryResult> rezultati = sparqlService.getAllAndPretragaPoSvimParametrima(imeZaPretragu, prezimeZaPretragu, email, naslovDela, alternativniNaslovDela);

        ArrayList<String> parsiraniIdRezultati = parsirajListuIdDokumenata(rezultati);

        return parsiraniIdRezultati;
    }



    ArrayList<String> parsirajListuIdDokumenata(List<SparqlService.SparqlQueryResult> rezultati){
        ArrayList<String> listaIdPronadjenihDokumenata = new ArrayList<>();

        for (SparqlService.SparqlQueryResult res : rezultati){
            System.out.println(res.getVarName());
            System.out.println(res.getVarValue());

            //parsiranje tako da dobijemo samo id pronadjenog dokumenta
            String vrednost = res.getVarValue().toString();
            String izdeljeno[] = vrednost.split("/");
            String id = izdeljeno[izdeljeno.length-1]; //uzimamo poslednju vrednost kao vrednost id
            System.out.println("Ovo je vrednost id dokumenta: " + id);
            listaIdPronadjenihDokumenata.add(id);
            System.out.println("-----------------------------------");
        }
        return  listaIdPronadjenihDokumenata;
    }
}
