package com.autorska.prava.AutorskaPrava.pomocneKlase;

import com.autorska.prava.AutorskaPrava.model.a1Obrazac.ZahtevZaAutorskaISrodnaPrava;
import com.autorska.prava.AutorskaPrava.model.a1Obrazac.resenjeDto.Resenje;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;

@Component
public class KonverzijaXml {

    public String marshall(ZahtevZaAutorskaISrodnaPrava autorskoPravo) throws JAXBException {

        OutputStream os = new ByteArrayOutputStream();
        JAXBContext jaxbContext = JAXBContext.newInstance(ZahtevZaAutorskaISrodnaPrava.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);


        jaxbMarshaller.marshal(autorskoPravo, os);
        return os.toString();
    }

    public String marshallResenje(Resenje autorskoPravo) throws JAXBException {

        OutputStream os = new ByteArrayOutputStream();
        JAXBContext jaxbContext = JAXBContext.newInstance(Resenje.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);


        jaxbMarshaller.marshal(autorskoPravo, os);
        return os.toString();
    }

    public ZahtevZaAutorskaISrodnaPrava unmarhall(String xml, String putanja) throws Exception{
        File file = new File("src/main/resources/static/data/instance/obrazacA1Instanca.xml");

        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = xml.getBytes();
        //write byte array to file
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(ZahtevZaAutorskaISrodnaPrava.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("src/main/resources/static/data/seme/obrazacA1.xsd"));
        //Schema schema = sf.newSchema(new File("../xml-documents/seme/Interesovanje.xsd"));

        unmarshaller.setSchema(schema);

        ZahtevZaAutorskaISrodnaPrava autorsko = (ZahtevZaAutorskaISrodnaPrava) unmarshaller.unmarshal(new File("src/main/resources/static/data/instance/obrazacA1Instanca.xml"));
        return autorsko;
    }

    public void marshallToFile(ZahtevZaAutorskaISrodnaPrava entity, String contextPath, String filePath)
            throws JAXBException, SAXException, FileNotFoundException {
        JAXBContext context = JAXBContext.newInstance(contextPath);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        System.out.println(System.getProperty("user.dir"));
        System.out.println("Putanja je:" + filePath);
        filePath = "src/main/resources/static/data/instance/obrazacA1Instanca.xml";
        OutputStream os = new FileOutputStream(filePath);
        marshaller.marshal(entity, os);
        System.out.println("Izvrseno je marsalovanje");
        try {
            os.flush();
            os.close();
        } catch (IOException e) {
            System.out.println("Desio se errror u marsalovanju..");
            e.printStackTrace();
        }

    }

    //unmarsalovanje resenja
    public Resenje unmarhallResenje(String xml, String putanja) throws Exception{
        File file = new File("src/main/resources/static/data/instance/resenjeInstanca.xml");

        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = xml.getBytes();
        //write byte array to file
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(Resenje.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("src/main/resources/static/data/seme/resenje.xsd"));
        //Schema schema = sf.newSchema(new File("../xml-documents/seme/Interesovanje.xsd"));

        unmarshaller.setSchema(schema);

        Resenje resenje = (Resenje) unmarshaller.unmarshal(new File("src/main/resources/static/data/instance/resenjeInstanca.xml"));
        return resenje;
    }

    //marshall to file >> resenje
    public void marshallToFileResenje(Resenje entity, String contextPath, String filePath)
            throws JAXBException, SAXException, FileNotFoundException {
        JAXBContext context = JAXBContext.newInstance(contextPath);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        System.out.println(System.getProperty("user.dir"));
        System.out.println("Putanja je:" + filePath);
        filePath = "src/main/resources/static/data/instance/resenjeInstanca.xml";
        OutputStream os = new FileOutputStream(filePath);
        marshaller.marshal(entity, os);
        System.out.println("Izvrseno je marsalovanje");
        try {
            os.flush();
            os.close();
        } catch (IOException e) {
            System.out.println("Desio se errror u marsalovanju..");
            e.printStackTrace();
        }
    }
}
