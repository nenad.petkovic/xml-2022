package com.autorska.prava.AutorskaPrava.controller;

import com.autorska.prava.AutorskaPrava.service.ResenjeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;

@RestController
@RequestMapping(value = "/api/resenje")
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class ResenjeController {

    @Autowired
    ResenjeService resenjeService;



    /*********** cuvanje resenja u bazi ************/
    //prilikom generisanja resenja vrsi se njegovo cuvanje u bazi i slanje e-maila sa generisanim resenjem u pdf-u
    @PostMapping(value = "/posaljiResenje/{idResenja}")
    public ResponseEntity<InputStreamResource> posaljiResenje(@RequestBody String resenjeXml, @PathVariable String idResenja) throws Exception {
        ByteArrayInputStream stream;
        System.out.println("Pogodjeno je..");

        try{
            String postavljeniGenerisaniBrojResenja = this.resenjeService.saveResenjeXml(resenjeXml, idResenja);

            stream = this.resenjeService.generisiPDFResenje(postavljeniGenerisaniBrojResenja);

            HttpHeaders zaglavlje = new HttpHeaders();
            zaglavlje.add("Content-Disposition", "inline: filename=resenje.pdf");

            InputStreamResource dokumentPdfStrim = new InputStreamResource(stream);
            return new ResponseEntity<>(dokumentPdfStrim, zaglavlje, HttpStatus.OK);

        }
        catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }




}
