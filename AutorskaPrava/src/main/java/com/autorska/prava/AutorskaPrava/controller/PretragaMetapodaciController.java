package com.autorska.prava.AutorskaPrava.controller;

import com.autorska.prava.AutorskaPrava.model.a1Obrazac.dto.MetapodaciUpitDTO;
import com.autorska.prava.AutorskaPrava.pomocneKlase.SparqlService;
import com.autorska.prava.AutorskaPrava.service.PretragaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping(value = "/api/pretraga")
public class PretragaMetapodaciController {

    @Autowired
    private SparqlService sparqService;

    @Autowired
    private PretragaService pretragaService;

    @PostMapping(value = "/pretragaMetapodaci/imePrezime",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<String>> pretragaMetapodatakaPoImePrezime(@RequestBody MetapodaciUpitDTO metapodaci) throws XPathExpressionException, JAXBException, ParserConfigurationException, IOException {
        return new ResponseEntity<>(pretragaService.searchMetadataImePrezime(metapodaci), HttpStatus.OK);
    }

    @PostMapping(value = "/pretragaMetapodaci/poslovnoIme", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<String>> pretragaMetapodatakaPoPoslovnomImenu(@RequestBody MetapodaciUpitDTO metapodaci) throws XPathExpressionException, JAXBException, ParserConfigurationException, IOException {
        return new ResponseEntity<>(pretragaService.searchMetadataPoslovnoIme(metapodaci), HttpStatus.OK);
    }

    @PostMapping(value = "/pretragaMetapodaci/email", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<String>> pretragaMetapodatakaPoEmailu (@RequestBody MetapodaciUpitDTO metapodaci) throws XPathExpressionException, JAXBException, ParserConfigurationException, IOException {
        return new ResponseEntity<>(pretragaService.searchMetadataEmail(metapodaci), HttpStatus.OK);
    }

    @PostMapping(value = "/pretragaMetapodaci/naslovDela", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<String>> pretragaMetapodatakaPoNaslovuDela (@RequestBody MetapodaciUpitDTO metapodaci) throws XPathExpressionException, JAXBException, ParserConfigurationException, IOException {
        return new ResponseEntity<>(pretragaService.searchMetadataNaslovDela(metapodaci), HttpStatus.OK);
    }

    @PostMapping(value = "/pretragaMetapodaci/alternativniNaslovDela", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<String>> pretragaMetapodatakaPoAlternativnomNaslovuDela (@RequestBody MetapodaciUpitDTO metapodaci) throws XPathExpressionException, JAXBException, ParserConfigurationException, IOException {
        return new ResponseEntity<>(pretragaService.searchMetadataAlternativniNaslovDela(metapodaci), HttpStatus.OK);
    }

    @PostMapping(value = "/pretragaMetapodaci/vrstaDela", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<String>> pretragaMetapodatakaPoVrstiDela (@RequestBody MetapodaciUpitDTO metapodaci) throws XPathExpressionException, JAXBException, ParserConfigurationException, IOException {
        return new ResponseEntity<>(pretragaService.searchMetadataVrstaDela(metapodaci), HttpStatus.OK);
    }

    /*************** AND PRETRAGA >> po svim metapodacima ******************/
    @PostMapping(value = "/pretragaMetapodaci/andPretraga", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<String>> pretragaPoSvimMetapodacimaAND (@RequestBody MetapodaciUpitDTO metapodaci) throws XPathExpressionException, JAXBException, ParserConfigurationException, IOException {
        return new ResponseEntity<>(pretragaService.searchMetadataAND(metapodaci), HttpStatus.OK);
    }

    /*************** OR PRETRAGA >> vraca uniju svih rezultata kod kojih je zadovoljen bar jedan uslov *******************/
    @PostMapping(value = "/pretragaMetapodaci/orPretraga", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<String>> pretragaPoSvimMetapodacimaOR (@RequestBody MetapodaciUpitDTO metapodaci) throws XPathExpressionException, JAXBException, ParserConfigurationException, IOException {
        return new ResponseEntity<>(pretragaService.searchMetadataOR(metapodaci), HttpStatus.OK);
    }






}
