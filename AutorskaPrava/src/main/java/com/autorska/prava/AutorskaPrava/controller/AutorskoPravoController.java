package com.autorska.prava.AutorskaPrava.controller;

import com.autorska.prava.AutorskaPrava.model.a1Obrazac.ZahtevZaAutorskaISrodnaPrava;
import com.autorska.prava.AutorskaPrava.service.AutorskoPravoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping(value = "/api/zahtevZaAutorskaPrava")
public class AutorskoPravoController {

    @Autowired
    private AutorskoPravoService autorskoPravoService;

    @PostMapping(value = "/createZahtevZaAtorskaPrava/{brojPrijave}")
    public ResponseEntity<String> writeAutorskoPravoXml(@RequestBody String xml, @PathVariable String brojPrijave) {
        try {
            //broj prijave je automatski generisan
            autorskoPravoService.saveAutorskoPravoXml(xml, brojPrijave);
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
        }
    }

    /*@PostMapping(value="/kreiraj")
    public ResponseEntity<String> napisi(@RequestBody String xml){
        return new ResponseEntity<>(HttpStatus.OK);
    }*/


    @GetMapping(value = "/readZahtevZaAutrskaPrava/{id}", produces = "application/xml")
    public ResponseEntity<Object> findAutorskoPravo(@PathVariable String id) {

        try {
            Object pravo = autorskoPravoService.findAutorskoPravoById(id);

            //vrsimo konverziju pronadjenog dokumenta u pdf
            this.autorskoPravoService.generisiPDF(id);

            return new ResponseEntity<>(pravo, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value="/getZahtevObjekat/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAutroskoPravoById(@PathVariable String id){
        try{
            Object pravo = autorskoPravoService.findAutorskoPravoById(id);

            //vrati json podatke o autorskom pravu koje je pronadjeno
            ZahtevZaAutorskaISrodnaPrava pravoObj = (ZahtevZaAutorskaISrodnaPrava) pravo;

            return new ResponseEntity<>(pravoObj, HttpStatus.OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/getSviZahtevi" )
    public ResponseEntity<List<String>> getListaSvihZahteva() throws Exception {

        //vraca listu svih id-ijeva zahteva koji su sortirani u redosledu po datumu koji je najraniji

        List<String> rezultati = this.autorskoPravoService.pronadjiSvaAutorskaPrava();
        return new ResponseEntity<>(rezultati, HttpStatus.OK);
    }
}
