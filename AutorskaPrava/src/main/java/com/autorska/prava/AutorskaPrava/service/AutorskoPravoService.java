
package com.autorska.prava.AutorskaPrava.service;
import com.autorska.prava.AutorskaPrava.model.a1Obrazac.ZahtevZaAutorskaISrodnaPrava;
import com.autorska.prava.AutorskaPrava.pomocneKlase.GenerisanIdDokumenta;
import com.autorska.prava.AutorskaPrava.pomocneKlase.KonverzijaXml;
import com.autorska.prava.AutorskaPrava.pomocneKlase.xslTransformacije.XSLTransformacija;
import com.autorska.prava.AutorskaPrava.repository.AutorskaPravaRepository;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import org.xmldb.api.base.XMLDBException;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class AutorskoPravoService {
    private final String jaxbPutanja = "com.autorska.prava.AutorskaPrava.model.a1Obrazac";

    private static final String SPARQL_GRAF_NAZIV = "/sparql/metapodaciLista";

    @Autowired
    public AutorskaPravaRepository autorskaPravaRepository;

    @Autowired
    public KonverzijaXml konverzijaXml;

    @Autowired
    public RDFService rdfServis;

    @Autowired
    public GenerisanIdDokumenta generisanjeIdDokumenta;

    public void saveAutorskoPravoXml(String xml, String brojPrijave) throws Exception {
        System.out.println("Poslato:::");
        System.out.println(xml);
        System.out.println("--------------------------------------------------");

        //sad od prosledjenog stringa treba da napravimo xml fajl i da ga sacuvamo.

        //File file = new File("src/main/resources/xmlFiles/xhtml/interesovanje.xml");
        /**File file = new File("src/main/resources/static/data/instance/obrazacA1Instanca.xml");

        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = xml.getBytes();
        //write byte array to file
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(ZahtevZaAutorskaISrodnaPrava.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("src/main/resources/static/data/seme/obrazacA1.xsd"));
        //Schema schema = sf.newSchema(new File("../xml-documents/seme/Interesovanje.xsd"));

        unmarshaller.setSchema(schema);

        ZahtevZaAutorskaISrodnaPrava autorsko = (ZahtevZaAutorskaISrodnaPrava) unmarshaller.unmarshal(new File("src/main/resources/static/data/instance/obrazacA1Instanca.xml"));*/
        ZahtevZaAutorskaISrodnaPrava autorsko  = this.konverzijaXml.unmarhall(xml, "src/main/resources/static/data/instance/obrazacA1Instanca.xml");

        //interesovanje.setBrojPrijave(UUID.randomUUID().toString());

        //generisemo string koji predstavlja broj prijave automatski..
        //autorsko.setBrojPrijave(brojPrijave.toString());
        autorsko.setBrojPrijave(this.generisanjeIdDokumenta.getGenerisanId());
        System.out.println("*************** PRE PROSIRIVANJA METAPODACIMA ************************");
        System.out.println(this.konverzijaXml.marshall(autorsko));
        System.out.println("*************************************************************************\t\t");


        handleMetadata(autorsko); //prosirivanje metapodacima
        autorskaPravaRepository.saveAutorskoPravoXml(autorsko); //cuvanje u exist bazi podataka
        System.out.println("**************** Nakon prosirivanja metapodacima **************");
        System.out.println(this.konverzijaXml.marshall(autorsko));
        System.out.println("***************************************************************");

        //EKSTRAKCIJA METAPODATAKA >> RDF
        try {
            //radimo marsalovanje >> treba rdfa

            String xmlEntitet = this.konverzijaXml.marshall(autorsko);
            //System.out.println(xmlEntitet);

            if (!rdfServis.save(xmlEntitet, SPARQL_GRAF_NAZIV)) {
                System.out.println("[ERROR] Neuspesno cuvanje metapodataka zahteva u RDF DB.");
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }


    /**** metoda vraca string sadrzaj xml dokumenta sa prosledjenim id-ijem **/
    public Object findAutorskoPravoById(String id) {
        return  autorskaPravaRepository.findAutorskoPravoXml(id);
    }

    /*** metoda vraca objekat entiteta sa prosledjenim id-ijem iz baze ***/
    public ZahtevZaAutorskaISrodnaPrava findDokumentById(String idEntita) throws Exception {

        try {
            ZahtevZaAutorskaISrodnaPrava obrazacZahteva = this.autorskaPravaRepository.findAutorskoPravoObjekat(idEntita);
            if (obrazacZahteva == null)
                throw new Exception("Nije pronadjen zahtev sa tim id-ijem u bazi...");
                System.out.println("Nije pronadjen zahtev sa tim id-ijem u bazi...");
            return obrazacZahteva;
        } catch (XMLDBException e) {
            throw new Exception(e.getMessage());
        } catch (JAXBException e) {
            throw new Exception("Desio se jax b exception...");
        }
    }


    private void handleMetadata(ZahtevZaAutorskaISrodnaPrava pravo){

        //********* postavljanje metapodataka o podnosiocu zahteva *********/
        pravo.getPodnosilacZahteva().setVocab("http://www.ftn.uns.ac.rs/rdf/database/predicate");
        pravo.getPodnosilacZahteva().setAbout("http://www.ftn.uns.ac.rs/rdf/database/brojPrijaveDokumenta/" + pravo.getBrojPrijave()); //izmeniti da podnosilac zahteva ima svoj id

        //>> ako je podnosilac zahteva fizicko lice >> postavimo mu ime i prezime
        if( pravo.getPodnosilacZahteva().getFizickoLice() != null){
            //postavljanje imena
            pravo.getPodnosilacZahteva().getFizickoLice().getIme().setProperty("pred:jePodneloFizickoLiceImena");
            pravo.getPodnosilacZahteva().getFizickoLice().getIme().setDatatype("xs:#string");

            //postavljanje prezimena
            pravo.getPodnosilacZahteva().getFizickoLice().getPrezime().setProperty("pred:jePodneloFizickoLicePrezimena");
            pravo.getPodnosilacZahteva().getFizickoLice().getPrezime().setDatatype("xs:#string");

        }
        //>> ako je podnosilac zahteva pravno lice >> postavimo mu poslovno ime
        if (pravo.getPodnosilacZahteva().getPravnoLice() != null){
            //postavljanje poslovnog imena
            pravo.getPodnosilacZahteva().getPravnoLice().getPoslovnoIme().setProperty("pred:jePodneloPravnoLicePosolvnogImena");
            pravo.getPodnosilacZahteva().getPravnoLice().getPoslovnoIme().setDatatype("xs:#string");
        }

        // postavljanje e-maila podnosioca zahteva
        pravo.getPodnosilacZahteva().getEMail().setProperty("pred:emailPodnosioca");
        pravo.getPodnosilacZahteva().getEMail().setDatatype("xs:#string");


        /********* postavljanje metapodataka o autorskom delu **********/

        pravo.getAutorskoDelo().setVocab("http://www.ftn.uns.ac.rs/rdf/database/predicate");
        pravo.getAutorskoDelo().setAbout("http://www.ftn.uns.ac.rs/rdf/database/predicate/brojPrijaveDokumentaZaAutorskoDelo/" + pravo.getBrojPrijave());

        //postavljanje naslova autorskog dela pred:sadrziAutorskoDeloNaslova

        String stvarniNaziv = pravo.getAutorskoDelo().getNaslovAutorskogDela().getValue();
        String redefinisanNazivDela = "";

        for (int i = 0; i < pravo.getAutorskoDelo().getNaslovAutorskogDela().getValue().length(); i++){
            if (stvarniNaziv.charAt(i)==' '){
                redefinisanNazivDela += '-';
            }
            else{
                redefinisanNazivDela += stvarniNaziv.charAt(i);
            }
        }

        pravo.getAutorskoDelo().getNaslovAutorskogDela().setProperty("pred:sadrziAutorskoDeloNaslova");
        pravo.getAutorskoDelo().getNaslovAutorskogDela().setDatatype("xs:#string");

        //postavljanje alternativnog naslova autorskog dela >> ukoliko postoji

        List<ZahtevZaAutorskaISrodnaPrava.AutorskoDelo.AlternativniNaslovAutorskogDela> stvarniAlternativniNaziviLista = pravo.getAutorskoDelo().getAlternativniNaslovAutorskogDela();

        System.out.println("??????????????????????????????????????????");
        System.out.println(stvarniAlternativniNaziviLista);
        System.out.println("?????????????????????????????????????????");
        if (stvarniAlternativniNaziviLista.size() != 0){
            //postoje alternativni naslovi >> redefinisemo ih

            for (ZahtevZaAutorskaISrodnaPrava.AutorskoDelo.AlternativniNaslovAutorskogDela stvarniAlternativniNaziv : stvarniAlternativniNaziviLista){
                String stvarniAlternativniNazivVrednost = stvarniAlternativniNaziv.getValue();

                //redefinisemo alternativne nazive..
                String redefinisanAlternativniNazivDela = "";

                for (int i = 0; i < stvarniAlternativniNazivVrednost.length(); i++){
                    if (stvarniAlternativniNazivVrednost.charAt(i)==' '){
                        redefinisanAlternativniNazivDela += '-';
                    }
                    else{
                        redefinisanAlternativniNazivDela += stvarniAlternativniNazivVrednost.charAt(i);
                    }
                }

                //dodajemo taj redefinisani alternativni naziv dela za delo koje ima tu vrednost alternativnog naziva
                for (ZahtevZaAutorskaISrodnaPrava.AutorskoDelo.AlternativniNaslovAutorskogDela al: pravo.getAutorskoDelo().getAlternativniNaslovAutorskogDela()){
                    String vrednostKojuPodesavamo = al.getValue();
                    System.out.println("Ovo je vrednost koju podesavamo: " + vrednostKojuPodesavamo);

                    if (vrednostKojuPodesavamo.equals(stvarniAlternativniNazivVrednost)){
                        //pronasli smo alternativni naslov koji podesavamo
                        System.out.println("Podesili smo properti alternativnog naslova...");
                        al.setProperty("pred:alternativniNaslovAutorskogDela");
                        al.setDatatype("xs:#string");
                    }
                }
            }
        }

        //*****postavljanje vrste autroskog dela *****/
        pravo.getAutorskoDelo().getVrstaAutorskogDela().setProperty("pred:imaVrstuAutorskogDela");
        pravo.getAutorskoDelo().getVrstaAutorskogDela().setDatatype("xs:#string");


        //***** postavljanje podataka o autorima dela *****/
        /////////////////////////***************************************
        //********* postavljanje metapodataka o podnosiocu zahteva *********/
        /***pravo.getPodnosilacZahteva().setVocab("http://www.ftn.uns.ac.rs/rdf/database/predicate");
        pravo.getPodnosilacZahteva().setAbout("http://www.ftn.uns.ac.rs/rdf/database/podnosilacZahteva/" + pravo.getPodnosilacZahteva().getEMail().getValue() ); //izmeniti da podnosilac zahteva ima svoj id

        pravo.getPodnosilacZahteva().getEMail().setProperty("pred:email");
        pravo.getPodnosilacZahteva().getEMail().setDatatype("xs:#string");*/


        //******** postavljanje metapodataka o autorskom delu **********/
        /**pravo.getAutorskoDelo().setVocab("http://www.ftn.uns.ac.rs/rdf/database/predicate");
        //proverimo da li naziv ima razmake >> menjamo ih crticama
        //iteriramo kroz naziv i menjamo razmake crticama
        String stvarniNaziv = pravo.getAutorskoDelo().getNaslovAutorskogDela().getValue();
        String redefinisanNazivDela = "";

        for (int i = 0; i < pravo.getAutorskoDelo().getNaslovAutorskogDela().getValue().length(); i++){
            if (stvarniNaziv.charAt(i)==' '){
                redefinisanNazivDela += '-';
            }
            else{
                redefinisanNazivDela += stvarniNaziv.charAt(i);
            }
        }
        pravo.getAutorskoDelo().setAbout("http://www.ftn.uns.ac.rs/rdf/database/predicate/" + redefinisanNazivDela);

        pravo.getAutorskoDelo().getNaslovAutorskogDela().setProperty("pred:naslovAutorskogDela");
        pravo.getAutorskoDelo().getNaslovAutorskogDela().setDatatype("xs:#string");

        //pravo.getAutorskoDelo().getAlternativniNaslovAutorskogDela().setDatatype("xs:#string");
        //pravo.getAutorskoDelo().getAlternativniNaslovAutorskogDela().setProperty()

        pravo.getAutorskoDelo().getVrstaAutorskogDela().setProperty("pred:vrstaAutorskogDela");
        pravo.getAutorskoDelo().getVrstaAutorskogDela().setDatatype("xs:#string");*/

        //******* postavljanje metapodataka o autorima dela ********//
        /**if (pravo.getAutorskoDelo().getAutoriDela() != null) {
            pravo.getAutorskoDelo().getAutoriDela().setVocab("http://www.ftn.uns.ac.rs/rdf/database/predicate");
            pravo.getAutorskoDelo().getAutoriDela().setAbout("http://www.ftn.uns.ac.rs/rdf/database/predicate/autoriDela/" + pravo.getAutorskoDelo().getNaslovAutorskogDela().getValue());

            ArrayList<ZahtevZaAutorskaISrodnaPrava.AutorskoDelo.AutoriDela.Autor> listaAutora = (ArrayList<ZahtevZaAutorskaISrodnaPrava.AutorskoDelo.AutoriDela.Autor>) pravo.getAutorskoDelo().getAutoriDela().getAutor();
            ArrayList<ZahtevZaAutorskaISrodnaPrava.AutorskoDelo.AutoriDela.Koautor> listaKoautora = (ArrayList<ZahtevZaAutorskaISrodnaPrava.AutorskoDelo.AutoriDela.Koautor>) pravo.getAutorskoDelo().getAutoriDela().getKoautor();


            for (ZahtevZaAutorskaISrodnaPrava.AutorskoDelo.AutoriDela.Autor a : listaAutora) {
                //za svakog autora postavljamo metapodatak

                a.setProperty("pred:autor");
                a.setDatatype("xs:#string");
            }

            for (ZahtevZaAutorskaISrodnaPrava.AutorskoDelo.AutoriDela.Koautor k : listaKoautora) {

                k.setProperty("pred:koautor");
                k.setDatatype("xs:#string");
            }
        }*/
    }

    public ByteArrayInputStream generisiPDF(String dokumentId) throws Exception {
        String xslFile = "src/main/resources/static/data/xsl-transformacije/autorsko_pravo.xsl";
        String outputHtmlFile = "src/main/resources/static/data/xsl-transformacije/autorsko_pravo.html";
        String outputPdfFile = "src/main/resources/static/data/xsl-transformacije/autorsko_pravo.pdf";
        String outputXmlFile = "src/main/resources/static/data/instance/obrazacA1Instanca.xml";

        XSLTransformacija xslTransformer = new XSLTransformacija();
        xslTransformer.setXSLT_FILE(xslFile);
        xslTransformer.setOUTPUT_FAJL_HTML(outputHtmlFile);
        xslTransformer.setOUTPUT_FILE_PDF(outputPdfFile);


        ZahtevZaAutorskaISrodnaPrava pravoObj = this.findDokumentById(dokumentId);

        //System.out.println("************************************ MMMMMMMMMMMMMMMMMMMMMmmmmm");
        //System.out.println(pravoXml.toString());

        //pretvorimo to u objekat >> unmarshalujemo string
        //ZahtevZaAutorskaISrodnaPrava zahtevObj = this.konverzijaXml.unmarhall(pravoXml.toString(), "");
        try {
            this.konverzijaXml.marshallToFile( pravoObj, this.jaxbPutanja, outputXmlFile
            );

            xslTransformer.generisiPDF_HTML(outputXmlFile);

            ByteArrayInputStream res = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(outputPdfFile)));
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public ByteArrayInputStream generisiHtml(String id) throws Exception {
        String xslFile = "src/main/resources/static/data/xsl-transformacije/autorsko_pravo.xsl";
        String outputHtmlFile = "src/main/resources/static/data/xsl-transformacije/autorsko_pravo.html";
        String outputXmlFile = "src/main/resources/static/data/instance/obrazacA1Instanca.xml";

        XSLTransformacija xslTransformer = new XSLTransformacija();
        xslTransformer.setXSLT_FILE(xslFile);
        xslTransformer.setOUTPUT_FAJL_HTML(outputHtmlFile);

        ZahtevZaAutorskaISrodnaPrava zahtev = this.findDokumentById(id);

        System.out.println("Pronadjeni zahtev: " + zahtev);

        try {
            this.konverzijaXml.marshallToFile(
                    zahtev,
                    this.jaxbPutanja,
                    outputXmlFile
            );
            System.out.println("Izvrseno marsalovanje");

            xslTransformer.generisiHTML(
                    outputXmlFile
            );

            System.out.println("*******Lokalno generisan html fajl...");
            return new ByteArrayInputStream(FileUtils.readFileToByteArray(
                    new File(outputHtmlFile)
            ));
        } catch (Exception e) {
            System.out.println("Ovde se desio error..");
            e.printStackTrace();
        }
        return  null;
    }


    public String getRdfStringMetapodatke(String idDokumenta) throws JAXBException {

        Object dokumentZahtevaString = this.findAutorskoPravoById(idDokumenta);
        String dokumentString = dokumentZahtevaString.toString();

        System.out.println("Metapodaci rdf format: ");
        System.out.println(dokumentZahtevaString);
        return dokumentString;

    }

    /** vraca sve zahteve **/
    public List<String> pronadjiSvaAutorskaPrava() throws Exception {

        try {
            List<ZahtevZaAutorskaISrodnaPrava> rez =  this.autorskaPravaRepository.getListaSvihEntiteta();

            //sortiranje entiteta po datumu slanja zahteva
            //List<ZahtevZaAutorskaISrodnaPrava> sortirana = new ArrayList<>();

            //sad prodjemo kroz listu ovih entiteta i pokusamo da je sortiramo po datumu
            Collections.sort(rez, new Comparator<ZahtevZaAutorskaISrodnaPrava>() {
                public int compare(ZahtevZaAutorskaISrodnaPrava z1, ZahtevZaAutorskaISrodnaPrava z2) {
                    return z1.getDatumPodnosenja().compare(z2.getDatumPodnosenja());
                }
            });

            List<String> listaRezultata = new ArrayList<>();

            //marsallujemo sve rezultate da dobijemo string objekte u xmlu
            for (ZahtevZaAutorskaISrodnaPrava z : rez){
                String xmlStr = this.konverzijaXml.marshall(z);
                listaRezultata.add(xmlStr);
            }

            return listaRezultata;


        } catch (XMLDBException e) {
            throw new Exception(e.getMessage());
        }
    }



}
