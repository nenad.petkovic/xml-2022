package com.autorska.prava.AutorskaPrava.pomocneKlase;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.modules.CollectionManagementService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DatabaseConnection {

    /***private static String konekcioniUri = "xmldb:sov://%1$s:%2$s/exist/xmlrpc";

    static public class KonekcionaPodesavanja {

        public String host;
        public int port = 8080;
        public String korisnik;
        public String pass;
        public String drajver;
        public String uri;

        public KonekcionaPodesavanja(Properties props) {
            super();

            korisnik = props.getProperty("conn.user").trim();
            pass = props.getProperty("conn.password").trim();
            System.out.println("Korisnik je: " + props.getProperty("conn.user"));
            System.out.println("Pasword je: " + props.getProperty("conn.password"));

            host = props.getProperty("conn.host").trim();
            port = Integer.parseInt(props.getProperty("conn.port"));

            uri = String.format(konekcioniUri, host, port);

            drajver = props.getProperty("conn.driver").trim();
        }
    }

    public static KonekcionaPodesavanja ucitajPodesavanja() throws IOException {
        String nazivPodesavanja = "exist.properties";

        InputStream propsStream = otvoriStream(nazivPodesavanja);
        if (propsStream == null)
            throw new IOException("Ne mogu se ucitati podesavanja " + nazivPodesavanja);

        Properties props = new Properties();
        props.load(propsStream);

        return new KonekcionaPodesavanja(props);
    }*/

    /**
     * Citanje resursa primer..
     *
     * @param nazivFajla >> naziv fajla koji predstavlja resource
     *
     * @return input stream za resurs
     * @throws IOException
     */
   /** public static InputStream otvoriStream(String nazivFajla) throws IOException {
        return DatabaseConnection.class.getClassLoader().getResourceAsStream(nazivFajla);
    }

    public static Collection preuzmiIliKreirajKolekciju(String nazivKolekcije) throws Exception {
        return getOrCreateCollection(nazivKolekcije, 0);
    }

    private static Collection getOrCreateCollection(String nazivKolekcije, int pathSegmentOffset) throws Exception {

        KonekcionaPodesavanja conn = ucitajPodesavanja();

        Class clas = Class.forName(conn.drajver);
        Database database = (Database) clas.newInstance();
        database.setProperty("create-database", "true");
        database.setProperty("database-id", "sov");
        database.setProperty("configuration", "C:/eXist-db/test/conf.xml");
        DatabaseManager.registerDatabase(database);

        Collection kol = DatabaseManager.getCollection(conn.uri + nazivKolekcije, conn.korisnik, conn.pass);


        // kreiranje kolekicje ako ona ne postoji

        if(kol == null) {

            if(konekcioniUri.startsWith("/")) {
                nazivKolekcije = nazivKolekcije.substring(1);
            }

            String pathSegments[] = nazivKolekcije.split("/");

            if(pathSegments.length > 0) {
                StringBuilder path = new StringBuilder();

                for(int i = 0; i <= pathSegmentOffset; i++) {
                    path.append("/" + pathSegments[i]);
                }

                Collection startCol = DatabaseManager.getCollection(conn.uri + path, conn.korisnik, conn.pass);

                if (startCol == null) {

                    // podkolekcije kolekcije ne postoje

                    String parentPath = path.substring(0, path.lastIndexOf("/"));
                    System.out.println("Konekciona podesavanja" + conn.uri);

                    System.out.println("Podesavanje kolekcije: " + conn.uri);
                    System.out.println("Parent path: " + parentPath);
                    System.out.println("Korisnik konekcija: " + conn.korisnik);
                    System.out.println("Pasword konekcija: " + conn.pass);

                    Collection parentCol = DatabaseManager.getCollection(conn.uri + parentPath, conn.korisnik, conn.pass);
                    System.out.println("**** Kolekcija je na putanji: " + conn.uri + parentPath);

                    CollectionManagementService mgt = (CollectionManagementService) parentCol.getService("CollectionManagementService", "1.0");

                    System.out.println("[INFO] Kreiranje kolekcije: " + pathSegments[pathSegmentOffset]);
                    kol = mgt.createCollection(pathSegments[pathSegmentOffset]);

                    kol.close();
                    parentCol.close();

                } else {
                    startCol.close();
                }
            }
            return getOrCreateCollection(nazivKolekcije, ++pathSegmentOffset);
        } else {
            return kol;
        }
    }*/
   private static String connectionUri = "xmldb:sov://%1$s:%2$s/exist/xmlrpc";

    static public class ConnectionProperties {

        public String driver;

        public String uri;
        public String host;
        public int port = 8080;
        public String user;
        public String password;


        public ConnectionProperties(Properties props) {
            super();

            host = props.getProperty("conn.host").trim();
            port = Integer.parseInt(props.getProperty("conn.port"));

            user = props.getProperty("conn.user").trim();
            password = props.getProperty("conn.password").trim();
            uri = String.format(connectionUri, host, port);

            driver = props.getProperty("conn.driver").trim();
        }
    }

    public static ConnectionProperties loadProperties() throws IOException {
        String propsName = "exist.properties";

        InputStream propsStream = openStream(propsName);
        if (propsStream == null)
            throw new IOException("Could not read properties " + propsName);

        Properties props = new Properties();
        props.load(propsStream);

        return new ConnectionProperties(props);
    }

    /**
     * Citanje resursa kao primer
     *
     * @param fileName
     *            naziv resursa
     * @return input strim resursa
     * @throws IOException
     */
    public static InputStream openStream(String fileName) throws IOException {
        return DatabaseConnection.class.getClassLoader().getResourceAsStream(fileName);
    }

    public static Collection getOrCreateCollection(String collectionUri) throws Exception {
        return getOrCreateCollection(collectionUri, 0);
    }

    private static Collection getOrCreateCollection(String collectionUri, int pathSegmentOffset) throws Exception {

        ConnectionProperties conn = loadProperties();

        Class cl = Class.forName(conn.driver);
        Database database = (Database) cl.newInstance();
        database.setProperty("create-database", "true");
        database.setProperty("database-id", "sov");
        database.setProperty("configuration", "C:/eXist-db/test/conf.xml");
        DatabaseManager.registerDatabase(database);

        Collection col = DatabaseManager.getCollection(conn.uri + collectionUri, conn.user, conn.password);

        // kreiranje kolekcije ako ne postoji
        if(col == null) {

            if(collectionUri.startsWith("/")) {
                collectionUri = collectionUri.substring(1);
            }

            String pathSegments[] = collectionUri.split("/");

            if(pathSegments.length > 0) {
                StringBuilder path = new StringBuilder();

                for(int i = 0; i <= pathSegmentOffset; i++) {
                    path.append("/" + pathSegments[i]);
                }

                Collection startCol = DatabaseManager.getCollection(conn.uri + path, conn.user, conn.password);

                if (startCol == null) {

                    // child collection does not exist

                    String parentPath = path.substring(0, path.lastIndexOf("/"));
                    Collection parentCol = DatabaseManager.getCollection(conn.uri + parentPath, conn.user, conn.password);

                    CollectionManagementService mgt = (CollectionManagementService) parentCol.getService("CollectionManagementService", "1.0");

                    System.out.println("[INFO] Creating the collection: " + pathSegments[pathSegmentOffset]);
                    col = mgt.createCollection(pathSegments[pathSegmentOffset]);

                    col.close();
                    parentCol.close();

                } else {
                    startCol.close();
                }
            }
            return getOrCreateCollection(collectionUri, ++pathSegmentOffset);
        } else {
            return col;
        }
    }

}
