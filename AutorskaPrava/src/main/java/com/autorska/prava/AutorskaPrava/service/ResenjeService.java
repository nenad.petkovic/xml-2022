package com.autorska.prava.AutorskaPrava.service;

import com.autorska.prava.AutorskaPrava.model.a1Obrazac.resenjeDto.Resenje;
import com.autorska.prava.AutorskaPrava.pomocneKlase.GenerisanIdDokumenta;
import com.autorska.prava.AutorskaPrava.pomocneKlase.KonverzijaXml;
import com.autorska.prava.AutorskaPrava.pomocneKlase.xslTransformacije.XSLTransformacija;
import com.autorska.prava.AutorskaPrava.repository.ResenjeRepository;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xmldb.api.base.XMLDBException;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.File;

@Service
public class ResenjeService {

    private final String jaxbPutanja = "com.autorska.prava.AutorskaPrava.model.a1Obrazac.resenjeDto";


    @Autowired
    public ResenjeRepository resenjeRepository;

    @Autowired
    public KonverzijaXml konverzijaXml;

    @Autowired
    public RDFService rdfServis;

    @Autowired
    public GenerisanIdDokumenta generisanjeIdDokumenta;



    public String saveResenjeXml(String xml, String brojResenja) throws Exception {
        System.out.println("Poslato:::");
        System.out.println(xml);
        System.out.println("--------------------------------------------------");

        //sad od prosledjenog stringa treba da napravimo xml fajl i da ga sacuvamo.
        Resenje resenje  = this.konverzijaXml.unmarhallResenje(xml, "src/main/resources/static/data/instance/resenjeInstanca.xml");

        //interesovanje.setBrojPrijave(UUID.randomUUID().toString());

        //generisemo string koji predstavlja broj prijave automatski..
        //autorsko.setBrojPrijave(brojPrijave.toString());
        resenje.setBrojResenja(this.generisanjeIdDokumenta.getGenerisaniIdResenje());
        System.out.println("Postavljeni broj resenja je: "  + resenje.getBrojResenja());

        System.out.println("*************** PRE PROSIRIVANJA METAPODACIMA ************************");
        System.out.println(this.konverzijaXml.marshallResenje(resenje));
        System.out.println("*************************************************************************\t\t");


        //handleMetadata(autorsko); //prosirivanje metapodacima
        resenjeRepository.saveResenjeXml(resenje); //cuvanje u exist bazi podataka
        System.out.println("**************** Nakon prosirivanja metapodacima **************");
        System.out.println(this.konverzijaXml.marshallResenje(resenje));
        System.out.println("***************************************************************");

        //EKSTRAKCIJA METAPODATAKA >> RDF
        /*try {
            //radimo marsalovanje >> treba rdfa

            String xmlEntitet = this.konverzijaXml.marshall(autorsko);
            //System.out.println(xmlEntitet);

            if (!rdfServis.save(xmlEntitet, SPARQL_GRAF_NAZIV)) {
                System.out.println("[ERROR] Neuspesno cuvanje metapodataka zahteva u RDF DB.");
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }*/
        return resenje.getBrojResenja();
    }


    /**** metoda vraca string sadrzaj xml dokumenta sa prosledjenim id-ijem **/
    public Object findResenjeById(String id) {
        return  resenjeRepository.findResenjeXml(id);
    }

    /*** metoda vraca objekat entiteta sa prosledjenim id-ijem iz baze ***/
    public Resenje findDokumentById(String idEntita) throws Exception {

        try {
            Resenje resenje = this.resenjeRepository.findResenjeObjekat(idEntita);
            if (resenje == null)
                throw new Exception("Nije pronadjen zahtev sa tim id-ijem u bazi...");
            System.out.println("Nije pronadjen zahtev sa tim id-ijem u bazi...");
            return resenje;
        } catch (XMLDBException e) {
            throw new Exception(e.getMessage());
        } catch (JAXBException e) {
            throw new Exception("Desio se jax b exception...");
        }
    }


    /************* generisanje pdf-a resenja ****************/
    public ByteArrayInputStream generisiPDFResenje(String dokumentId) throws Exception {
        String xslFajl = "src/main/resources/static/data/xsl-transformacije/resenje.xsl";
        String izlazniHtmlFajl = "src/main/resources/static/data/xsl-transformacije/resenje.html";
        String izlazniPdfFajl = "src/main/resources/static/data/xsl-transformacije/resenje.pdf";
        String izlazniXmlFajl = "src/main/resources/static/data/instance/resenjeInstanca.xml";

        XSLTransformacija xslTransformer = new XSLTransformacija();
        xslTransformer.setXSLT_FILE(xslFajl);
        xslTransformer.setOUTPUT_FAJL_HTML(izlazniHtmlFajl);
        xslTransformer.setOUTPUT_FILE_PDF(izlazniPdfFajl);


        Resenje resenjeObj = this.findDokumentById(dokumentId);

        //System.out.println("************************************ MMMMMMMMMMMMMMMMMMMMMmmmmm");
        //System.out.println(pravoXml.toString());

        //pretvorimo to u objekat >> unmarshalujemo string
        //ZahtevZaAutorskaISrodnaPrava zahtevObj = this.konverzijaXml.unmarhall(pravoXml.toString(), "");
        try {
            this.konverzijaXml.marshallToFileResenje(
                    resenjeObj, this.jaxbPutanja, izlazniXmlFajl
            );
            xslTransformer.generisiPDF_HTML(izlazniXmlFajl);

            ByteArrayInputStream res = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(izlazniPdfFajl)));

            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


}
