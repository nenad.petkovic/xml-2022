package com.autorska.prava.AutorskaPrava.pomocneKlase.fuseki;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class SparqlUtil {


    /* The following operation causes all of the triples in all of the graphs to be deleted */
    private static final String DROP_ALL = "DROP ALL";

    /* Removes all of the triples from a named graphed */
    private static final String DROP_GRAPH_TEMPLATE = "DROP GRAPH <%s>";

    /**
     * A template for creating SPARUL (SPARQL Update) query can be found here:
     * https://www.w3.org/TR/sparql11-update/
     */
    /* Insert RDF data into the default graph */
    private static final String UPDATE_TEMPLATE = "INSERT DATA { %s }";

    /* Insert RDF data to an arbitrary named graph */
    private static final String UPDATE_TEMPLATE_NAMED_GRAPH = "INSERT DATA { GRAPH <%1$s> { %2$s } }";


    /* Simple SPARQL query on a named graph */
    private static final String SELECT_NAMED_GRAPH_TEMPLATE = "SELECT * FROM <%1$s> WHERE { %2$s }";

    private static final String SELECT_DISTINCT_NAMED_GRAPH_TEMPLATE = "SELECT DISTINCT ?s FROM <%1$s> WHERE { %2$s }";

    private static final String SELECT_OBJECT_ONLY_NAMED_GRAPH_TEMPLATE = "SELECT ?o FROM <%1$s> WHERE { %2$s }";

    private static final String SELECT_PREDICATE_AND_OBJECT_NAMED_GRAPH_TEMPLATE = "SELECT ?p ?o FROM <%1$s> WHERE { %2$s }";

    private static final String DESCRIBE_NAMED_GRAPH_TEMPLATE = "DESCRIBE <%1$s> FROM <%2$s> WHERE { %3$s }";

    /* Plain text RDF serialization format */
    public static final String NTRIPLES = "N-TRIPLES";

    /* An XML serialization format for RDF data */
    public static final String RDF_XML = "RDF/XML";

    /* An JSON serialization format for RDF data */
    public static final String RDF_JSON = "RDF/JSON";

    public String dropAll() {
        return DROP_ALL;
    }

    public String dropGraph(String graphURI) {
        return String.format(DROP_GRAPH_TEMPLATE, graphURI);
    }

    /* Inserts data to the default graph */
    public String insertData(String ntriples) {
        return String.format(UPDATE_TEMPLATE, ntriples);
    }

    public static String insertData(String graphURI, String ntriples) {
        return String.format(UPDATE_TEMPLATE_NAMED_GRAPH, graphURI, ntriples);
    }

    public static String selectData(String graphURI, String sparqlCondition) {
        return String.format(SELECT_NAMED_GRAPH_TEMPLATE, graphURI, sparqlCondition);
    }

    public String selectObjectOnly(String graphURI, String sparqlCondition) {
        return String.format(SELECT_OBJECT_ONLY_NAMED_GRAPH_TEMPLATE, graphURI, sparqlCondition);
    }

    public String selectPredicateAndObject(String graphURI, String sparqlCondition) {
        return String.format(SELECT_PREDICATE_AND_OBJECT_NAMED_GRAPH_TEMPLATE, graphURI, sparqlCondition);
    }

    public String describe(String type, String graphURI, String sparqlCondition) {
        return String.format(DESCRIBE_NAMED_GRAPH_TEMPLATE, type, graphURI, sparqlCondition);
    }

    public static String selectDistinctData(String graphURI, String sparqlCondition) {
        return String.format(SELECT_DISTINCT_NAMED_GRAPH_TEMPLATE, graphURI, sparqlCondition);
    }


    public static String selectPoImenuIPrezimenu(String imePretraga,String prezimePretraga, String dataEndpointString) {
        return "SELECT * FROM <" + dataEndpointString + "/sparql/metapodaciLista>\n" +
                "WHERE {\n" +
                "  ?dokumentId <http://www.ftn.uns.ac.rs/rdf/database/predicate/jePodneloFizickoLiceImena> \""+ imePretraga +"\" ; \n" +
                "<http://www.ftn.uns.ac.rs/rdf/database/predicate/jePodneloFizickoLicePrezimena> \"" + prezimePretraga + "\" . " +

                "}";
    }

    public static String selectPoPoslovnomImenu(String poslovnoIme, String dataEndpointString) {
        return "SELECT * FROM <" + dataEndpointString + "/sparql/metapodaciLista>\n" +
                "WHERE {\n" +
                "  ?dokumentId <http://www.ftn.uns.ac.rs/rdf/database/predicate/jePodneloPravnoLicePosolvnogImena> \""+ poslovnoIme +"\" ; \n" +
                //"<http://www.ftn.uns.ac.rs/rdf/database/predicate/jePodneloFizickoLicePrezimena> \"" + prezimePretraga + "\" . " +

                "}";
    }


    public static String selectPoEmailu(String email, String dataEndpointString) {
        return "SELECT * FROM <" + dataEndpointString + "/sparql/metapodaciLista>\n" +
                "WHERE {\n" +
                "  ?dokumentId <http://www.ftn.uns.ac.rs/rdf/database/predicate/emailPodnosioca> \""+ email +"\" ; \n" +
                //"<http://www.ftn.uns.ac.rs/rdf/database/predicate/jePodneloFizickoLicePrezimena> \"" + prezimePretraga + "\" . " +

                "}";
    }

    public static String selectPoNaslovuAutorskogDela(String naslov, String dataEndpointString) {
        return "SELECT * FROM <" + dataEndpointString + "/sparql/metapodaciLista>\n" +
                "WHERE {\n" +
                "  ?dokumentId <http://www.ftn.uns.ac.rs/rdf/database/predicate/sadrziAutorskoDeloNaslova> \""+ naslov +"\" ; \n" +
                //"<http://www.ftn.uns.ac.rs/rdf/database/predicate/jePodneloFizickoLicePrezimena> \"" + prezimePretraga + "\" . " +

                "}";
    }

    public static String selectPoAlternativnomNaslovuAutorskogDela(String alternativniNaslov, String dataEndpointString) {
        return "SELECT * FROM <" + dataEndpointString + "/sparql/metapodaciLista>\n" +
                "WHERE {\n" +
                "  ?dokumentId <http://www.ftn.uns.ac.rs/rdf/database/predicate/alternativniNaslovAutorskogDela> \""+ alternativniNaslov +"\" ; \n" +
                //"<http://www.ftn.uns.ac.rs/rdf/database/predicate/jePodneloFizickoLicePrezimena> \"" + prezimePretraga + "\" . " +

                "}";
    }

    public static String selectPoVrstiDela(String vrsta, String dataEndpointString) {
        return "SELECT * FROM <" + dataEndpointString + "/sparql/metapodaciLista>\n" +
                "WHERE {\n" +
                "  ?dokumentId <http://www.rokzasok.rs/rdf/database/predicate/imaVrstuAutorskogDela> \""+ vrsta +"\" ; \n" +
                //"<http://www.ftn.uns.ac.rs/rdf/database/predicate/jePodneloFizickoLicePrezimena> \"" + prezimePretraga + "\" . " +

                "}";
    }

    //fizicka lica pretraga AND
    public static String selectANDFizickaLica(String ime,String prezime, String email, String naslov, String alternativniNaslov, String dataEndpointString) {
        return "SELECT * FROM <" + dataEndpointString + "/sparql/metapodaciLista>\n" +
                "WHERE {\n" +
                "  ?dokumentId <http://www.ftn.uns.ac.rs/rdf/database/predicate/emailPodnosioca> \""+ email +"\" ; \n" +
                //"  <http://www.ftn.uns.ac.rs/rdf/database/predicate/sadrziAutorskoDeloNaslova> \""+ naslov +"\" ; \n" +
                "  <http://www.ftn.uns.ac.rs/rdf/database/predicate/jePodneloFizickoLiceImena> \""+ ime +"\" ; \n" +
                "  <http://www.ftn.uns.ac.rs/rdf/database/predicate/jePodneloFizickoLicePrezimena> \"" + prezime + "\" . " +

                "}";
    }

}
