package com.autorska.prava.AutorskaPrava.pomocneKlase;

import org.springframework.stereotype.Service;

import java.util.UUID;


@Service
public class GenerisanIdDokumenta {

    //generisanje id-ija dokumenta
    private String generisanId ="A-" +  UUID.randomUUID().getLeastSignificantBits() * -1;

    private String generisaniIdResenje  = UUID.randomUUID().getLeastSignificantBits() * -1 + "";

    public String getGenerisanId(){
        return this.generisanId;
    }

    public String getGenerisaniIdResenje(){ return  this.generisaniIdResenje; }
}
