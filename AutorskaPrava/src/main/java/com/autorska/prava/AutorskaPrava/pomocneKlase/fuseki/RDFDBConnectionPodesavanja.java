package com.autorska.prava.AutorskaPrava.pomocneKlase.fuseki;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "rdfdb.conn")
public class RDFDBConnectionPodesavanja {


    private String query;
    private String update;
    private String data;

    private String endpoint;
    private String dataset;


    public String getDataEndpoint() {
        return String.join("/", endpoint, dataset, data.trim());
    }

    public String getEndpoint() {
        return this.endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getDataset() {
        return this.dataset;
    }

    public void setDataset(String dataset) {
        this.dataset = dataset;
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getUpdate() {
        return this.update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getQueryEndpoint() {
        return String.join("/", endpoint, dataset, query.trim());
    }

    public String getUpdateEndpoint() {
        return String.join("/", endpoint, dataset, update.trim());
    }


    public RDFDBConnectionPodesavanja endpoint(String endpoint) {
        this.endpoint = endpoint;
        return this;
    }

    public RDFDBConnectionPodesavanja dataset(String dataset) {
        this.dataset = dataset;
        return this;
    }

    public RDFDBConnectionPodesavanja query(String query) {
        this.query = query;
        return this;
    }

    public RDFDBConnectionPodesavanja update(String update) {
        this.update = update;
        return this;
    }

    public RDFDBConnectionPodesavanja data(String data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
                " endpoint='" + getEndpoint() + "'" +
                ", dataset='" + getDataset() + "'" +
                ", query='" + getQuery() + "'" +
                ", update='" + getUpdate() + "'" +
                ", data='" + getData() + "'" +
                "}";
    }

}
