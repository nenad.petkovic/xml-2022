package com.autorska.prava.AutorskaPrava.pomocneKlase.fuseki;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SparqlAutentifikacioniPodaci {
    static public class ConectionPodesavanja {

        public String updateEndp;
        public String dataEndp;
        public String endp; //endpoint
        public String dataset;
        public String queryEndp;



        public ConectionPodesavanja(Properties props) {
            super();
            dataset = props.getProperty("conn.dataset").trim();
            endp = props.getProperty("conn.endpoint").trim();

            queryEndp = String.join("/", endp, dataset, props.getProperty("conn.query").trim());
            updateEndp = String.join("/", endp, dataset, props.getProperty("conn.update").trim());
            dataEndp = String.join("/", endp, dataset, props.getProperty("conn.data").trim());

            System.out.println("[INFO] Parsiranje konekcionih podesavanja:");
            System.out.println("[INFO] Query endpoint: " + queryEndp);
            System.out.println("[INFO] Update endpoint: " + updateEndp);
            System.out.println("[INFO] Graph store endpoint: " + dataEndp);
        }
    }

    /**
     * Citanje konfiguracionih podesavanja primer
     *
     * @return vraca objekat konfiguracije
     *
     * testna metoda
     */
    public static ConectionPodesavanja ucitajPodesavanja() throws IOException {
        String propsName = "connection.properties";

        InputStream propsStream = openStream(propsName);
        if (propsStream == null)
            throw new IOException("Ne mogu se ucitati podesavanja " + propsName);

        Properties props = new Properties();
        props.load(propsStream);

        return new ConectionPodesavanja(props);
    }

    /**
     * Ucitavanje resursa primer
     *
     * @param nazivFjala >> naziv fajla resursa
     * @return vraca output stream za resurs
     * @throws IOException >> u slucaju greske
     */
    public static InputStream openStream(String nazivFjala) throws IOException {

        return SparqlAutentifikacioniPodaci.class.getClassLoader().getResourceAsStream(nazivFjala);
    }
}
