package com.autorska.prava.AutorskaPrava.pomocneKlase.xslTransformacije;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import net.sf.saxon.TransformerFactoryImpl;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.springframework.util.ResourceUtils;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

public class XSLTransformacija {
    private FopFactory factoryFop;

    private TransformerFactory factoryTransformer;

    private static DocumentBuilderFactory documentBuilderFactory;

    private String OUTPUT_FAJL_PDF;
    private String OUTPUT_FAJL_HTML;
    private String XSLT_FAJL;
    private String XSL_FO_FAJL;


    public static final String FOP_CONFIGURACIJA = "src/main/resources/fop.xconf";


    public XSLTransformacija() throws SAXException, IOException{
        documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setIgnoringElementContentWhitespace(true);
        documentBuilderFactory.setIgnoringComments(true);

        //Inicijalizacija FOP factory objekta
        factoryFop = FopFactory.newInstance(ResourceUtils.getFile(FOP_CONFIGURACIJA));
        // Podesavanje XSLT transformer factory
        factoryTransformer = new TransformerFactoryImpl();
    }

    public String getXSLT_FAJL() {
        return XSLT_FAJL;
    }

    public void setXSLT_FILE(String XSLT_FAJL_NAZIV) {
        this.XSLT_FAJL = XSLT_FAJL_NAZIV;
    }

    public String getXSL_FO_FAJL() {
        return XSL_FO_FAJL;
    }

    public void setXSL_FO_FAJL(String XSL_FO_FILE_NAZIV) {
        this.XSL_FO_FAJL = XSL_FO_FILE_NAZIV;
    }

    public String getOUTPUT_FAJL_PDF() {
        return OUTPUT_FAJL_PDF;
    }

    public void setOUTPUT_FILE_PDF(String OUTPUT_FILE_PDF_NAZIV) {
        this.OUTPUT_FAJL_PDF = OUTPUT_FILE_PDF_NAZIV;
    }

    public String getOUTPUT_FAJL_HTML() {
        return OUTPUT_FAJL_HTML;
    }

    public void setOUTPUT_FAJL_HTML(String OUTPUT_FILE_HTML_NAZIV) {
        this.OUTPUT_FAJL_HTML = OUTPUT_FILE_HTML_NAZIV;
    }

    public void generisiPDF_HTML(String ulazniFajlPutanja) throws IOException, DocumentException {
        this.generisiHTML(ulazniFajlPutanja);

        // korak 1
        Document dokument = new Document();

        // korak 2
        PdfWriter writer = PdfWriter.getInstance(dokument, new FileOutputStream(OUTPUT_FAJL_PDF));

        // korak 3
        dokument.open();

        // korak 4
        XMLWorkerHelper.getInstance().parseXHtml(writer, dokument, new FileInputStream(OUTPUT_FAJL_HTML));

        // korak 5
        dokument.close();
    }

    public void generisiPDF_FO(String inputFilePath) throws Exception {

        System.out.println("[INFO] " + XSLTransformacija.class.getSimpleName());

        // Kreiranje tranformacijonog sorsa
        StreamSource transformSource = new StreamSource(ResourceUtils.getFile(this.XSL_FO_FAJL));

        // Inicijalizacija user agenta za transformaciju
        FOUserAgent userAgentObj = factoryFop.newFOUserAgent();

        // Inicijalizacija transformacionog objekta
        StreamSource sourceObj = new StreamSource(inputFilePath);

        // Kreiranje output streama za cuvanje rezultata
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        // Inicijalizacija xsl-fo transformer objekta
        Transformer xslFoTransformerObj = factoryTransformer.newTransformer(transformSource);

        // Konstrukcija fop instance sa odgovarajucim formatom
        Fop fop = factoryFop.newFop(MimeConstants.MIME_PDF, userAgentObj, outStream);

        // Rezultati sax dogadjaja
        Result rezultat = new SAXResult(fop.getDefaultHandler());

        // Pocetak XSLT transforamcija i FOP procesiranja
        xslFoTransformerObj.transform(sourceObj, rezultat);

        // Generisanje PDF fajla
        File pdfFile = ResourceUtils.getFile(this.OUTPUT_FAJL_PDF);
        if (!pdfFile.getParentFile().exists()) {
            System.out.println("[INFO] Kreiran novi direktorijum: " + pdfFile.getParentFile().getAbsolutePath() + ".");
            pdfFile.getParentFile().mkdir();
        }

        OutputStream outStr = new BufferedOutputStream(new FileOutputStream(pdfFile));
        outStr.write(outStream.toByteArray());

        System.out.println("[INFO] Fajl \"" + pdfFile.getCanonicalPath() + "\" generisan uspesno.");
        outStr.close();

        System.out.println("[INFO] Kraj.");
    }

    public org.w3c.dom.Document buildDokument(String fajlPutanja) {
        org.w3c.dom.Document dokument = null;
        try {

            DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
            dokument = builder.parse(ResourceUtils.getFile(fajlPutanja));

            if (dokument != null)
                System.out.println("[INFO] Fajl parsiran bez errora.");
            else
                System.out.println("[WARN] Dokument je null.");

        } catch (Exception e) {
            return null;

        }

        return dokument;
    }

    public void generisiHTML(String inputFajlPutanja) throws FileNotFoundException {
        try {
            // Inicijalizacija tranformer instance
            StreamSource transformSource = new StreamSource(ResourceUtils.getFile(this.XSLT_FAJL));
            Transformer transformer = factoryTransformer.newTransformer(transformSource);
            transformer.setOutputProperty("{http://xml.apache.org/xalan}indent-amount", "2");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            // Generisi XHTML
            transformer.setOutputProperty(OutputKeys.METHOD, "xhtml");

            // Tranformacija DOM u HTML
            DOMSource source = new DOMSource(buildDokument(inputFajlPutanja));
            StreamResult result = new StreamResult(new FileOutputStream(this.OUTPUT_FAJL_HTML));
            transformer.transform(source, result);

        } catch (IOException | TransformerException | TransformerFactoryConfigurationError e) {
            e.printStackTrace();
        }
    }
}
