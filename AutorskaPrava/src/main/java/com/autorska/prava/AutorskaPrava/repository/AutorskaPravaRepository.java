package com.autorska.prava.AutorskaPrava.repository;

import com.autorska.prava.AutorskaPrava.model.a1Obrazac.ZahtevZaAutorskaISrodnaPrava;
import com.autorska.prava.AutorskaPrava.pomocneKlase.DatabaseConnection;

import org.exist.xmldb.EXistResource;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.*;
import org.xmldb.api.modules.XMLResource;
import org.xmldb.api.modules.XQueryService;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AutorskaPravaRepository {
    public final String ZAHTEV_ZA_AUTORSKA_PRAVA_COLLECTION_NAME = "/db/autorskaPravaZahtevi";

    public ZahtevZaAutorskaISrodnaPrava findAutorskoPravoObjekat(String id){
        //metoda vraca objekat autroskog i srodnog prava pronadjen po id-iju
        String collectionId = ZAHTEV_ZA_AUTORSKA_PRAVA_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        ZahtevZaAutorskaISrodnaPrava xmlDokument = null;

        try {
            col = DatabaseConnection.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            System.out.println("**********Id po kome trazimo je: "+ id);
            res = (XMLResource)col.getResource(id);

            JAXBContext context = JAXBContext.newInstance(ZahtevZaAutorskaISrodnaPrava.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("src/main/resources/static/data/seme/obrazacA1.xsd"));
            unmarshaller.setSchema(schema);

            // convert XML file to object
            ZahtevZaAutorskaISrodnaPrava zahtev = (ZahtevZaAutorskaISrodnaPrava) unmarshaller.unmarshal(res.getContentAsDOM());
            zahtev.setBrojPrijave(id); //moramo postaviti borj prijave posto je u pitanju staticki atribut koji se menja
            xmlDokument = zahtev;


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return xmlDokument;
    }

    public Object findAutorskoPravoXml(String id) {
        String collectionId = ZAHTEV_ZA_AUTORSKA_PRAVA_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        Object xml = null;

        try {
            col = DatabaseConnection.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            System.out.println("**********Id po kome trazimo je: "+ id);
            res = (XMLResource)col.getResource(id);

            JAXBContext context = JAXBContext.newInstance(ZahtevZaAutorskaISrodnaPrava.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("src/main/resources/static/data/seme/obrazacA1.xsd"));
            unmarshaller.setSchema(schema);
            
            // convert XML file to object
            ZahtevZaAutorskaISrodnaPrava zahtev = (ZahtevZaAutorskaISrodnaPrava) unmarshaller.unmarshal(res.getContentAsDOM());
            zahtev.setBrojPrijave(id); //moramo postaviti borj prijave posto je u pitanju staticki atribut koji se menja

            JAXBContext zahtevContext = JAXBContext.newInstance(ZahtevZaAutorskaISrodnaPrava.class);

            // create an instance of `Unmarshaller`
            Marshaller marshaller = zahtevContext.createMarshaller();

            SchemaFactory zahtevsf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema zahtevschema = zahtevsf.newSchema(new File("src/main/resources/static/data/seme/obrazacA1.xsd"));
            marshaller.setSchema(zahtevschema);

            marshaller.marshal(zahtev, os);
            XMLResource xmlResource = (XMLResource) res;
            xmlResource.setContent(os);
            xml = xmlResource.getContent();
            System.out.println("********* Procitano je: ");
            System.out.println(xml.toString());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return xml;
    }


    public void saveAutorskoPravoXml(ZahtevZaAutorskaISrodnaPrava autorskoPravo) throws Exception {
        String collectionId = ZAHTEV_ZA_AUTORSKA_PRAVA_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();

        try {

            //extractAndSaveMetadata();
            //System.out.println("********** PROCITANI METAPODACI *******************");
            //readSaglasnostMetadata();
            System.out.println("***************************************************");

            System.out.println("[INFO] Retrieving the collection: " + collectionId);
            col = DatabaseConnection.getOrCreateCollection(collectionId);


            res = (XMLResource) col.createResource(ZahtevZaAutorskaISrodnaPrava.getBrojPrijave(), XMLResource.RESOURCE_TYPE);
            JAXBContext jaxbContext = JAXBContext.newInstance(ZahtevZaAutorskaISrodnaPrava.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);


            jaxbMarshaller.marshal(autorskoPravo, os);

            res.setContent(os);

            col.storeResource(res);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            //don't forget to cleanup
            if (res != null) {
                try {
                    ((EXistResource) res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if (col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
    }

    private String readFile(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, StandardCharsets.UTF_8);

    }

    /** ucitavanje liste svih entiteta ***/
    public List<ZahtevZaAutorskaISrodnaPrava> getListaSvihEntiteta() throws Exception {
        ArrayList<ZahtevZaAutorskaISrodnaPrava> entiteti = new ArrayList<>();
        Collection collection = DatabaseConnection.getOrCreateCollection(ZAHTEV_ZA_AUTORSKA_PRAVA_COLLECTION_NAME);

        XQueryService xQueryService = (XQueryService) collection.getService("XQueryService", "1.0");

        String xqueryPronadjiSve = "xquery version \"3.1\";\n" +
                "declare default element namespace \"http://www.ftn.uns.ac.rs/obrazacA1\";\n" +
                "for $x in collection(\"/db/autorskaPravaZahtevi\")\n" +
                "return $x";



        CompiledExpression compiledExpression = xQueryService.compile(xqueryPronadjiSve);

        ResourceSet res = xQueryService.execute(compiledExpression);
        ResourceIterator iter = res.getIterator();

        while (iter.hasMoreResources()) {
            XMLResource xmlResurs = (XMLResource) iter.nextResource();
            //entiteti.add(this.kon.unmarshall(xmlResource.getContentAsDOM(), this.jaxbContextPath));

            //unmarshall-ovanje
            JAXBContext context = JAXBContext.newInstance(ZahtevZaAutorskaISrodnaPrava.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("src/main/resources/static/data/seme/obrazacA1.xsd"));
            unmarshaller.setSchema(schema);

            // convert XML file to object
            ZahtevZaAutorskaISrodnaPrava zahtev = (ZahtevZaAutorskaISrodnaPrava) unmarshaller.unmarshal(xmlResurs.getContentAsDOM());
            //zahtev.setBrojPrijave(id); //moramo postaviti borj prijave posto je u pitanju staticki atribut koji se menja
            entiteti.add(zahtev);
        }
        return entiteti;
    }
}
