package com.autorska.prava.AutorskaPrava.controller;

import com.autorska.prava.AutorskaPrava.service.AutorskoPravoService;
import com.autorska.prava.AutorskaPrava.service.RDFService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import javax.wsdl.Input;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@RestController
@RequestMapping(value = "/api/dokument")
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class DokumentController {

    @Autowired
    private AutorskoPravoService autorskoPravoService;


    @Autowired
    private RDFService rdfServis;

    @GetMapping(value = "/pogodi")
    ResponseEntity<Void> test(){
        System.out.println("Pogodjena sam..");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /*metoda za dobavljanje generisanog zahteva za autorsko pravo u html-u*/
    @GetMapping(value = "/html/zahtev-za-autrsko-pravo/{id}")
    ResponseEntity<InputStreamResource> getHtmlZahtevZaAutroskoPravo(@PathVariable String id) {

        ByteArrayInputStream strim;
        try {
            strim = this.autorskoPravoService.generisiHtml(id);

        }
        catch (IOException | SAXException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            System.out.println("Desio se exception u repozitorijum metodi za dobavljanje zahteva pd id-iju....");
            throw new RuntimeException(e);
        }

        System.out.println("Prosli smo error...");
        HttpHeaders heder = new HttpHeaders();
        heder.add("Content-Disposition", "inline: filename=potvrda.html");

        InputStreamResource dokumentHtmlStrim = new InputStreamResource(strim);
        return new ResponseEntity<>(dokumentHtmlStrim ,heder, HttpStatus.OK);
    }

    @GetMapping(value = "/pdf/zahtev-za-autrsko-pravo/{id}")
    ResponseEntity<InputStreamResource> getPdfGenerisanZahtevZaAutorskoPravo(@PathVariable String id) {
        ByteArrayInputStream stream;
        try {
            stream = this.autorskoPravoService.generisiPDF(id);
        }
        catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        HttpHeaders zaglavlje = new HttpHeaders();
        zaglavlje.add("Content-Disposition", "inline: filename=zahtev.pdf");

        InputStreamResource dokumentPdfStrim = new InputStreamResource(stream);
        return new ResponseEntity<>(dokumentPdfStrim, zaglavlje, HttpStatus.OK);
    }

    @GetMapping(value = "/metapodaci/rdf/zahtev-za-autrsko-pravo/{id}")
    ResponseEntity<InputStreamResource> getMetapodaciRdf(@PathVariable String id) {
        ByteArrayInputStream strim;
        try {
            String entitet = this.autorskoPravoService.getRdfStringMetapodatke(id);

            strim = new ByteArrayInputStream(this.rdfServis.getRDFAsRDF(entitet).getBytes());
        }
        catch (IOException | SAXException | JAXBException | TransformerException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        HttpHeaders hederi = new HttpHeaders();
        hederi.add("Content-Disposition", "inline: filename=obrazac-metadata.rdf");

        InputStreamResource rez = new InputStreamResource(strim);
        return new ResponseEntity<>(rez, hederi, HttpStatus.OK);
    }

    /******** metoda za generisanje rdf  metapodataka u json formatu za dokument sa prosledjenim id-ijem **********/
    @GetMapping(value = "/metapodaci/json//rdf/zahtev-za-autrsko-pravo/{id}")
    ResponseEntity<InputStreamResource> getMetapodaciJson(@PathVariable String id) {
        ByteArrayInputStream strim;
        try {
            String entitetDokument = this.autorskoPravoService.getRdfStringMetapodatke(id);
            System.out.println(entitetDokument);
            Object rdfServisKonvertovano = this.rdfServis.getRDFAsJSON(entitetDokument).getBytes();

            strim = new ByteArrayInputStream((byte[]) rdfServisKonvertovano);
        }
        catch (IOException | SAXException | JAXBException | TransformerException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        HttpHeaders heder = new HttpHeaders();
        heder.add("Content-Disposition", "inline: filename=obrazac-metadata.json");

        return new ResponseEntity<>(new InputStreamResource(strim), heder, HttpStatus.OK);
    }

}
