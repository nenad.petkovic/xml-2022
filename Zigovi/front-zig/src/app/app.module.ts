import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ZigComponent } from './components/zig/zig.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { PretragaComponent } from './components/pretraga/pretraga.component';
import { ZahtevComponent } from './components/zahtev/zahtev.component';
import { SluzbenikFormaComponent } from './components/sluzbenik-forma/sluzbenik-forma.component';
import { NgSelectModule } from "@ng-select/ng-select";
import {HttpClientModule} from '@angular/common/http';
import { MojiZahteviComponent } from './components/moji-zahtevi/moji-zahtevi.component';
import { ZahteviNaCekanjuComponent } from './components/zahtevi-na-cekanju/zahtevi-na-cekanju.component';
import { NaprednaPretragaComponent } from './components/napredna-pretraga/napredna-pretraga.component';

@NgModule({
  declarations: [
    AppComponent,
    ZigComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    PretragaComponent,
    ZahtevComponent,
    SluzbenikFormaComponent,
    MojiZahteviComponent,
    ZahteviNaCekanjuComponent,
    NaprednaPretragaComponent,    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    NgSelectModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
