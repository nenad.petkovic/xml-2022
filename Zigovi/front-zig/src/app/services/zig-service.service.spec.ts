import { TestBed } from '@angular/core/testing';

import { ZigServiceService } from './zig-service.service';

describe('ZigServiceService', () => {
  let service: ZigServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ZigServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
