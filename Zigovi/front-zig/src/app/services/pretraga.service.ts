import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PretragaService {
  apiURL = 'http://localhost:8085/api/pretraga';
  
  constructor(private http: HttpClient) { }

  prosta(text: string): Observable<any> {
    let body = new HttpParams();
    body = body.set('text', text);
    return this.http.post(this.apiURL + "/prosta", body, { responseType: 'text' });
  }

  emailMeta(text: string): Observable<any> {
    let file = {
      imePretraga: "",
      prezimePretraga: "",
      emailPretraga: text
    }

    return this.http.post(this.apiURL + "/pretragaMetapodaci/email", 
    file, { responseType: 'json' });
  }
  imePrezimeMeta(imePretraga: string,prezimePretraga: string): Observable<any> {
    let file = {
      imePretraga: imePretraga,
      prezimePretraga: prezimePretraga,
      emailPretraga: ""
    }
    console.log(file)
    return this.http.post(this.apiURL + "/pretragaMetapodaci/imePrezime", 
    file, { responseType: 'json' });
  }
  pretragaPoSvimMetapodacimaAND(imePretraga: string,prezimePretraga: string,email: string): Observable<any> {
    let file = {
      imePretraga: imePretraga,
      prezimePretraga: prezimePretraga,
      emailPretraga: email
    }
    console.log(file)
    return this.http.post(this.apiURL + "/pretragaMetapodaci/andPretraga", 
    file, { responseType: 'json' });
  }
  pretragaPoSvimMetapodacimaOR(imePretraga: string,prezimePretraga: string,email: string): Observable<any> {
    let file = {
      imePretraga: imePretraga,
      prezimePretraga: prezimePretraga,
      emailPretraga: email
    }
    console.log(file)

    return this.http.post(this.apiURL + "/pretragaMetapodaci/orPretraga", 
    file, { responseType: 'json' });
  }
}
