import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, retry, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ZigServiceService {

  apiURL = 'http://localhost:8085/api/zig';
  
  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    return this.http.get(this.apiURL, { responseType: 'text' });
  }

  getOne(id: string): Observable<any> {
    return this.http.get(this.apiURL + "/" + id, { responseType: 'text' });
  }

  rdfMeta(id: string): Observable<any> {
    return this.http.get(this.apiURL + "/metadata/rdf/" + id, { responseType: 'text' });
  }

  jsonMeta(id: string): Observable<any> {
    return this.http.get(this.apiURL + "/metadata/json/" + id, { responseType: 'json' });
  }

  getHtml(id: string): Observable<any> {
    return this.http.get(this.apiURL + "/html/" + id, { responseType: 'text' ,
    headers: {
        'Accept': 'text/html;charset=UTF-8'
    } });
  }

  getPdf(id: string): Observable<any> {
    return this.http.get(this.apiURL + "/pdf/" + id, { responseType: 'arraybuffer',
    headers: {
        'Accept': 'application/pdf'
    } });
  }

  upisi(xmlFile: string): Observable<any> {
    return this.http.post(this.apiURL, xmlFile, { responseType: 'text' });
  }

  handleError(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
