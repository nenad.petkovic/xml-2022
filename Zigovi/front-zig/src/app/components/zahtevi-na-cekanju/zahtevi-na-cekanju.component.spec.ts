import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZahteviNaCekanjuComponent } from './zahtevi-na-cekanju.component';

describe('ZahteviNaCekanjuComponent', () => {
  let component: ZahteviNaCekanjuComponent;
  let fixture: ComponentFixture<ZahteviNaCekanjuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZahteviNaCekanjuComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ZahteviNaCekanjuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
