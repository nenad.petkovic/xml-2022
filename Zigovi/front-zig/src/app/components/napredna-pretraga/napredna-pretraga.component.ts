import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { PretragaService } from 'src/app/services/pretraga.service';
import { ZigServiceService } from 'src/app/services/zig-service.service';
import * as xml2js from 'xml2js';
import * as Kolekcija from '../../classes/kolekcija'

@Component({
  selector: 'app-napredna-pretraga',
  templateUrl: './napredna-pretraga.component.html',
  styleUrls: ['./napredna-pretraga.component.scss']
})
export class NaprednaPretragaComponent {
  constructor(private router: Router, private zigServiceService: ZigServiceService, private pretragaService: PretragaService) { 

  }

  xmlfile!: Kolekcija.Root;
  public kolekcija!: Kolekcija.Ns5Zig[];
  ime!: string;
  prezime!: string;
  email!: string;

  ngOnInit(): void {
    let uloga = localStorage.getItem("uloga")?.toString() || "";

  }

  traziEmail(){
    this.pretragaService.emailMeta(this.email).subscribe({
      next: data => {

         console.log(data);
         this.kolekcija = data;
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
  }
  traziIme(){
    this.pretragaService.imePrezimeMeta(this.ime,this.prezime).subscribe({
      next: data => {

         console.log(data);
         this.kolekcija = data;
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
  }
  traziOR(){
    this.pretragaService.pretragaPoSvimMetapodacimaOR(this.ime,this.prezime,this.email).subscribe({
      next: data => {

         console.log(data);
         this.kolekcija = data;
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
  }
  traziAND(){
    this.pretragaService.pretragaPoSvimMetapodacimaAND(this.ime,this.prezime,this.email).subscribe({
      next: data => {

         console.log(data);
         this.kolekcija = data;
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
  }

  open(id: any) {
    this.router.navigate(['/', 'zig', id]);
  }
}
