import { Component } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { ZigServiceService } from 'src/app/services/zig-service.service';
import * as xml2js from 'xml2js';
import * as ZigInter from '../../classes/interfejs';

@Component({
  selector: 'app-zig',
  templateUrl: './zig.component.html',
  styleUrls: ['./zig.component.scss']
})
export class ZigComponent {
  constructor(private route: ActivatedRoute, private zigServiceService: ZigServiceService, private sanitizer: DomSanitizer) { }

  data!: SafeHtml;
  zig!: ZigInter.Root;
  status!: string;

  idAdmin! : boolean;
  

  ngOnInit(): void {
    let uloga = localStorage.getItem("uloga")?.toString() || "";

    if(uloga == "ADMIN")
    {
      this.idAdmin=true;
    }
    else
    {
      this.idAdmin=false;
    }

    let id = this.route.snapshot.paramMap.get('id') || "";
    this.zigServiceService.getHtml(id).subscribe({
      next: data => {
        this.data = this.sanitizer.bypassSecurityTrustHtml(data);
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
    
    this.zigServiceService.getOne(id).subscribe({
      next: data => {
        var parseString = xml2js.parseString;
        var xml = data;
        parseString(xml, (err: any, result: any) => {
          this.zig = result;
          this.status = this.zig['ns5:zig']['ns5:popunjava_zavod'][0]['ns5:stanje'][0]._;
        });
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
  }

  PDF(){
    let id = this.route.snapshot.paramMap.get('id') || "";
    this.zigServiceService.getPdf(id).subscribe({
      next: data => {
        var blob = new Blob([data], {type: 'application/pdf'});
        var blobURL = URL.createObjectURL(blob);
        window.open(blobURL);
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
  }
  HTML(){
    let id = this.route.snapshot.paramMap.get('id') || "";
    this.zigServiceService.getHtml(id).subscribe({
      next: data => {
        var blob = new Blob([data], {type: 'text/html'});
        var blobURL = URL.createObjectURL(blob);
        window.open(blobURL);
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
  }
  RDF(){
    let id = this.route.snapshot.paramMap.get('id') || "";
    this.zigServiceService.rdfMeta(id).subscribe({
      next: data => {
        var blob = new Blob([data], {type: 'text'});
        var blobURL = URL.createObjectURL(blob);
        window.open(blobURL);
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
  }
  JSON_RDF(){
    let id = this.route.snapshot.paramMap.get('id') || "";
    this.zigServiceService.jsonMeta(id).subscribe({
      next: data => {
        var blob = new Blob([JSON.stringify(data)], {type: 'text/json'});
        var blobURL = URL.createObjectURL(blob);
        window.open(blobURL);
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
  }
}
