import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Zig } from 'src/app/classes/zig';
import { ZigServiceService } from 'src/app/services/zig-service.service';
import * as xml2js from 'xml2js';
@Component({
  selector: 'app-zahtev',
  templateUrl: './zahtev.component.html',
  styleUrls: ['./zahtev.component.scss']
})
export class ZahtevComponent implements OnInit {
  constructor(private zigServiceService: ZigServiceService) { }

  numbers = [
    { id: 1, name: "1" },
    { id: 2, name: "2" },
    { id: 3, name: "3" },
    { id: 4, name: "4" },
    { id: 5, name: "5" },
    { id: 6, name: "6" },
    { id: 7, name: "7" },
    { id: 8, name: "8" },
    { id: 9, name: "9" },
    { id: 10, name: "10" },
    { id: 11, name: "11" },
    { id: 12, name: "12" },
    { id: 13, name: "13" },
    { id: 14, name: "14" },
    { id: 15, name: "15" },
    { id: 16, name: "16" },
    { id: 17, name: "17" },
    { id: 18, name: "18" },
    { id: 19, name: "19" },
    { id: 20, name: "20" },
    { id: 21, name: "21" },
    { id: 22, name: "22" },
    { id: 23, name: "23" },
    { id: 24, name: "24" },
    { id: 25, name: "25" },
    { id: 26, name: "26" },
    { id: 27, name: "27" },
    { id: 28, name: "28" },
    { id: 29, name: "29" },
    { id: 30, name: "30" },
    { id: 31, name: "31" },
    { id: 32, name: "32" },
    { id: 33, name: "33" },
    { id: 34, name: "34" },
    { id: 35, name: "35" },
    { id: 36, name: "36" },
    { id: 37, name: "37" },
    { id: 38, name: "38" },
    { id: 39, name: "39" },
    { id: 40, name: "40" },
    { id: 41, name: "41" },
    { id: 42, name: "42" },
    { id: 43, name: "43" },
    { id: 44, name: "44" },
    { id: 45, name: "45" }
  ];

  individualni: boolean = false;
  kolektivni: boolean = false;
  garancije: boolean = false;
  verbalni: boolean = false;
  graficki: boolean = false;
  kombinovni: boolean = false;
  trodimenzioni: boolean = false;




  public selected = [{ id: 10, name: "10" }];

  ngOnInit(): void {
    // this.zigServiceService.getAll().subscribe({
    //   next: data => {
    //     console.log(data);
    //     var parseString = xml2js.parseString;
    //     var xml = data;
    //     parseString(xml, function (err: any, result: any) {
    //       console.dir(result);
    //     });
    //   },
    //   error: error => {
    //     alert(JSON.stringify(error.message));
    //     console.error('There was an error!', error);
    //   }
    // })
  }

  padTo2Digits(num: number) {
    return num.toString().padStart(2, '0');
  }

  getRandomInt(max: number) {
    return Math.floor(Math.random() * max);
  }

  onSubmit(f: NgForm) {
    let ids = [];

    let year =  new Date().getFullYear().toString();

    let datum = [new Date().getFullYear(),
    this.padTo2Digits(new Date().getMonth() + 1),
    this.padTo2Digits(new Date().getDate()),
    ].join('-');
    ids.push(...this.selected.map(emp => emp.id));

    // Ignorisi ovaj json
    let data: Zig = {
      broj_prijave: '',
      datum: datum,
      podnosilac_prijave: {
        ime: f.value.ime,
        prezime: f.value.prezime,
        ulica: f.value.ulica,
        broj: f.value.broj,
        postanski_broj: f.value.postanski_broj,
        mesto: f.value.mesto,
        drzava: f.value.drzava,
        telefon: f.value.telefon,
        email: f.value.email,
        faks: f.value.faks
      },
      punomocnik: {
        ime: f.value.ime2,
        prezime: f.value.prezime2,
        ulica: f.value.ulica2,
        broj: f.value.broj2,
        postanski_broj: f.value.postanski_broj2,
        mesto: f.value.mesto2,
        drzava: f.value.drzava2,
        telefon: f.value.telefon2,
        email: f.value.email2,
        faks: f.value.faks2
      },
      predstavnik: {
        ime: f.value.ime3,
        prezime: f.value.prezime3,
        ulica: f.value.ulica3,
        broj: f.value.broj3,
        postanski_broj: f.value.postanski_broj3,
        mesto: f.value.mesto3,
        drzava: f.value.drzava3,
        telefon: f.value.telefon3,
        email: f.value.email3,
        faks: f.value.faks3
      },
      odnosi_za: {
        a: {
          individualni: this.individualni,
          kolektivni: this.kolektivni,
          garancije: this.garancije
        },
        b: {
          verbalni: this.verbalni,
          graficki: this.graficki,
          kombinovni: this.kombinovni,
          trodimenzioni: this.trodimenzioni,
          drugo: f.value.drugo
        },
        v: {
          izgled: ''
        }
      },
      boje: f.value.boje,
      transliteracija_znaka: f.value.transliteracija_znaka,
      prevod_znaka: f.value.prevod_znaka,
      opis_znaka: f.value.opis_znaka,
      nicanska_skala: ids,
      prvanstvo_i_osnov: f.value.prvanstvo_i_osnov,
      placanje_takse: {
        osnovna_taksa: f.value.osnovna_taksa,
        za_klasa: {
          broj_klasa: f.value.broj_klasa,
          dinara: f.value.dinara
        },
        graficko_resenje: f.value.graficko_resenje,
        ukupno: f.value.graficko_resenje + f.value.dinara + f.value.osnovna_taksa
      },
      popunjava_zavod: {
        primerak_znaka: false,
        spisak_robe_usluga: false,
        punomocje: false,
        generalno_punomocje: false,
        punomocje_naknadno_dostavljeno: false,
        akt_o_kolektivnom_zigu: false,
        dokaz_prava_prvenstva: false,
        dokaz_uplata_takse: false,
        stanje: 'CEKA'
      }
    };


    let zaokruzeno = "<zig:zaokruzeno>1<\/zig:zaokruzeno>\r\n";

    if(ids.length >= 1){
      zaokruzeno = "";
      ids.forEach(element => {
        zaokruzeno = zaokruzeno + "<zig:zaokruzeno>"+element+"<\/zig:zaokruzeno>\r\n";
      });
    }

    let text: string = "<zig:zig broj_prijave=\""
      + "Z-"+ this.getRandomInt(10000) +"-" + year +
      "\" datum=\"" + datum +
      "\" xmlns:zig=\"https:\/\/www.serviszaradsapatentima.rs\/zig\">\r\n  <zig:podnosilac_prijave vocab=\"http:\/\/www.serviszaradsapatentima.rs\/rdf\/database\/predicate\" about=\"string\">\r\n    <zig:ime>" 
      + f.value.ime + 
      "<\/zig:ime>\r\n    <zig:prezime>" + 
      f.value.prezime + "<\/zig:prezime>\r\n    <zig:ulica>" + 
      f.value.ulica +
      "<\/zig:ulica>\r\n    <zig:broj>" + f.value.broj +
      "<\/zig:broj>\r\n    <zig:postanski_broj>" + f.value.postanski_broj +
      "<\/zig:postanski_broj>\r\n    <zig:mesto>" + f.value.mesto +
      "<\/zig:mesto>\r\n    <zig:drzava>" + f.value.drzava +
      "<\/zig:drzava>\r\n    <zig:telefon>" + f.value.telefon +
      "<\/zig:telefon>\r\n    <zig:email>" + f.value.email +
      "<\/zig:email>\r\n    <zig:faks>" + f.value.faks +
      "<\/zig:faks>\r\n  <\/zig:podnosilac_prijave>\r\n  <zig:punomocnik>\r\n    <zig:ime>" 
      + f.value.ime2 + 
      "<\/zig:ime>\r\n    <zig:prezime>" + 
      f.value.prezime2 + "<\/zig:prezime>\r\n    <zig:ulica>" + 
      f.value.ulica2 +
      "<\/zig:ulica>\r\n    <zig:broj>" + f.value.broj2 +
      "<\/zig:broj>\r\n    <zig:postanski_broj>" + f.value.postanski_broj2 +
      "<\/zig:postanski_broj>\r\n    <zig:mesto>" + f.value.mesto2 +
      "<\/zig:mesto>\r\n    <zig:drzava>" + f.value.drzava2 +
      "<\/zig:drzava>\r\n    <zig:telefon>" + f.value.telefon2 +
      "<\/zig:telefon>\r\n    <zig:email>" + f.value.email2 +
      "<\/zig:email>\r\n    <zig:faks>" + f.value.faks2 +
      "<\/zig:faks>\r\n  <\/zig:punomocnik>\r\n  <zig:predstavnik>\r\n    <zig:ime>" 
      + f.value.ime3 + 
      "<\/zig:ime>\r\n    <zig:prezime>" + 
      f.value.prezime3 + "<\/zig:prezime>\r\n    <zig:ulica>" + 
      f.value.ulica3 +
      "<\/zig:ulica>\r\n    <zig:broj>" + f.value.broj3 +
      "<\/zig:broj>\r\n    <zig:postanski_broj>" + f.value.postanski_broj3 +
      "<\/zig:postanski_broj>\r\n    <zig:mesto>" + f.value.mesto3 +
      "<\/zig:mesto>\r\n    <zig:drzava>" + f.value.drzava3 +
      "<\/zig:drzava>\r\n    <zig:telefon>" + f.value.telefon3 +
      "<\/zig:telefon>\r\n    <zig:email>" + f.value.email3 +
      "<\/zig:email>\r\n    <zig:faks>" + f.value.faks3 +
      "<\/zig:faks>\r\n  <\/zig:predstavnik>\r\n  <zig:odnosi_za>\r\n    <zig:a>\r\n      <zig:individualni>" + 
      this.individualni + "<\/zig:individualni>\r\n      <zig:kolektivni>" + 
      this.kolektivni + "<\/zig:kolektivni>\r\n      <zig:garancije>" + 
      this.garancije + "<\/zig:garancije>\r\n    <\/zig:a>\r\n    <zig:b>\r\n      <zig:verbalni>" + 
      this.verbalni + "<\/zig:verbalni>\r\n      <zig:graficki>" + 
      this.graficki + "<\/zig:graficki>\r\n      <zig:kombinovni>" + 
      this.kombinovni + "<\/zig:kombinovni>\r\n      <zig:trodimenzioni>" + 
      this.trodimenzioni + "<\/zig:trodimenzioni>\r\n      <zig:drugo>" + 
      f.value.drugo + "<\/zig:drugo>\r\n    <\/zig:b>\r\n    <zig:v>\r\n      <zig:izgled>dHVyYmluZQ==<\/zig:izgled>\r\n    <\/zig:v>\r\n  <\/zig:odnosi_za>\r\n  <zig:boje>" + 
      f.value.boje + "<\/zig:boje>\r\n  <zig:transliteracija_znaka>" + 
      f.value.transliteracija_znaka + "<\/zig:transliteracija_znaka>\r\n  <zig:prevod_znaka>" + 
      f.value.prevod_znaka + "<\/zig:prevod_znaka>\r\n  <zig:opis_znaka>" + 
      f.value.opis_znaka + "<\/zig:opis_znaka>\r\n  <zig:nicanska_skala>\r\n    <!--1 or more repetitions:-->\r\n    " + 
      zaokruzeno + "  <\/zig:nicanska_skala>\r\n  <zig:prvanstvo_i_osnov>" + 
      f.value.prvanstvo_i_osnov + "<\/zig:prvanstvo_i_osnov>\r\n  <zig:placanje_takse>\r\n    <zig:osnovna_taksa>" + 
      f.value.osnovna_taksa + "<\/zig:osnovna_taksa>\r\n    <zig:za_klasa broj_klasa=\"" + 
      f.value.broj_klasa + "\" dinara=\"" + 
      f.value.dinara + "\"\/>\r\n    <zig:graficko_resenje>" + 
      f.value.graficko_resenje + "<\/zig:graficko_resenje>\r\n    <zig:ukupno>" + 
      data.placanje_takse.ukupno + "<\/zig:ukupno>\r\n  <\/zig:placanje_takse>\r\n  <zig:potpis_podnosioca>Y2lyY3Vt<\/zig:potpis_podnosioca>\r\n  <zig:popunjava_zavod>\r\n    <zig:primerak_znaka>false<\/zig:primerak_znaka>\r\n    <zig:spisak_robe_usluga>false<\/zig:spisak_robe_usluga>\r\n    <zig:punomocje>false<\/zig:punomocje>\r\n    <zig:generalno_punomocje>false<\/zig:generalno_punomocje>\r\n    <zig:punomocje_naknadno_dostavljeno>false<\/zig:punomocje_naknadno_dostavljeno>\r\n    <zig:akt_o_kolektivnom_zigu>false<\/zig:akt_o_kolektivnom_zigu>\r\n    <zig:dokaz_prava_prvenstva>false<\/zig:dokaz_prava_prvenstva>\r\n    <zig:dokaz_uplata_takse>false<\/zig:dokaz_uplata_takse>\r\n    <zig:stanje>CEKA<\/zig:stanje>\r\n  <\/zig:popunjava_zavod>\r\n<\/zig:zig>";
    this.zigServiceService.upisi(text).subscribe({
      next: data => {
        var parseString = xml2js.parseString;
        var xml = data;
        parseString(xml, function (err: any, result: any) {
        });
        alert("Uspesno upisano!");
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
  }

  onMaterialGroupChange(event: any) {
    this.selected = event;
  }

}
