import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { PretragaService } from 'src/app/services/pretraga.service';
import { ZigServiceService } from 'src/app/services/zig-service.service';
import * as xml2js from 'xml2js';
import * as Kolekcija from '../../classes/kolekcija'

@Component({
  selector: 'app-pretraga',
  templateUrl: './pretraga.component.html',
  styleUrls: ['./pretraga.component.scss']
})
export class PretragaComponent {
  constructor(private router: Router, private zigServiceService: ZigServiceService, private pretragaService: PretragaService) { 

  }

  xmlfile!: Kolekcija.Root;
  public kolekcija!: Kolekcija.Ns5Zig[];
  pretraga_text!: string;

  ngOnInit(): void {
    let uloga = localStorage.getItem("uloga")?.toString() || "";



     this.zigServiceService.getAll().subscribe({
       next: data => {
         var parseString = xml2js.parseString;
         var xml = data;
         parseString(xml,  (err: any, result: any) => {
          this.xmlfile = result;
          this.kolekcija = this.xmlfile['ns5:kolekcija-zigova']['ns5:zig'];
          console.log(this.kolekcija);
         });
       },
       error: error => {
         alert(JSON.stringify(error.message));
         console.error('There was an error!', error);
       }
     })
  }
  onSubmit(f: NgForm) {
    this.pretragaService.prosta(this.pretraga_text).subscribe({
      next: data => {
        console.log(data);
        var parseString = xml2js.parseString;
        var xml = data;
        parseString(xml,  (err: any, result: any) => {
         this.xmlfile = result;
         this.kolekcija = this.xmlfile['ns5:kolekcija-zigova']['ns5:zig'];
         console.log(this.kolekcija);
        });
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
  }
  open(id: any) {
    this.router.navigate(['/', 'zig', id]);
  }
}
