import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserServicesService } from 'src/app/services/user-services.service';
import * as xml2js from 'xml2js';
import * as UserInter from '../../classes/user';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  constructor(private router: Router, private userService: UserServicesService) { }

  korisnickoIme: string = '';
  lozinka: string = '';

  user!: UserInter.Root;

  ngOnInit(): void {

  }

  ulogujSe(forma: NgForm) {

    if (this.korisnickoIme == '' || this.lozinka == '') {
      alert("Molimo vas unesite korisnicko ime i lozinku...");
    }
    else {
      this.userService.getOne(this.korisnickoIme).subscribe({
        next: data => {
          var parseString = xml2js.parseString;
          var xml = data;
          parseString(xml, (err: any, result: any) => {
            this.user = result;

            if (this.user['ns5:users']['ns5:podnosilac_prijave'][0]['ns5:password'][0] == this.lozinka) {
              alert("Uspesno logovanje!")
              localStorage.setItem("email", this.user['ns5:users']['ns5:podnosilac_prijave'][0]['ns5:email'][0]._);
              localStorage.setItem("uloga", this.user['ns5:users']['ns5:podnosilac_prijave'][0]['ns5:uloga'][0]);
              localStorage.setItem("password", this.user['ns5:users']['ns5:podnosilac_prijave'][0]['ns5:password'][0]);


              // OVDE STAVI KUDAA DA IDE POSLE LOG
              this.router.navigate(['/', '/']).then(() => {
                window.location.reload();
              });

              
              // OVAKO NA LOGOUT DUGME
              // goTologout() {
              //   localStorage.clear()
              //   this.router.navigate(['/', '/']).then(() => {
              //     window.location.reload();
              //   });
              // }

            }
            else {
              alert("Pogresna sifra!")
            }
          });
        },
        error: error => {
          alert("Ne postoji korisnik!");
          console.error('There was an error!', error);
        }
      })
    }
  }
}