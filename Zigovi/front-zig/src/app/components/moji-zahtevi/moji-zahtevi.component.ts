import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { PretragaService } from 'src/app/services/pretraga.service';
import { ZigServiceService } from 'src/app/services/zig-service.service';
import * as xml2js from 'xml2js';
import * as Kolekcija from '../../classes/kolekcija'

@Component({
  selector: 'app-moji-zahtevi',
  templateUrl: './moji-zahtevi.component.html',
  styleUrls: ['./moji-zahtevi.component.scss']
})
export class MojiZahteviComponent {
  constructor(private router: Router, private zigServiceService: ZigServiceService, private pretragaService: PretragaService) { 

  }

  xmlfile!: Kolekcija.Root;
  public kolekcija!: [];
  pretraga_text!: string;

  ngOnInit(): void {
    let email = localStorage.getItem("email")?.toString() || "";



     this.pretragaService.emailMeta(email).subscribe({
       next: data => {

          console.log(data);
          this.kolekcija = data;
       },
       error: error => {
         alert(JSON.stringify(error.message));
         console.error('There was an error!', error);
       }
     })
    }

  onSubmit(f: NgForm) {

  }

  open(id: any) {
    this.router.navigate(['/', 'zig', id]);
  }
}
