import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SluzbenikFormaComponent } from './sluzbenik-forma.component';

describe('SluzbenikFormaComponent', () => {
  let component: SluzbenikFormaComponent;
  let fixture: ComponentFixture<SluzbenikFormaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SluzbenikFormaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SluzbenikFormaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
