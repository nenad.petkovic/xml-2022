import { Component } from '@angular/core';
import { ZigServiceService } from 'src/app/services/zig-service.service';
import * as xml2js from 'xml2js';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import * as ZigInter from '../../classes/interfejs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-sluzbenik-forma',
  templateUrl: './sluzbenik-forma.component.html',
  styleUrls: ['./sluzbenik-forma.component.scss']
})
export class SluzbenikFormaComponent {
  constructor(private router: Router, private route: ActivatedRoute, private zigServiceService: ZigServiceService, private sanitizer: DomSanitizer) { }

  data!: SafeHtml;
  zig!: ZigInter.Root;

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id') || "";
    this.zigServiceService.getHtml(id).subscribe({
      next: data => {
        this.data = this.sanitizer.bypassSecurityTrustHtml(data);
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
    this.zigServiceService.getOne(id).subscribe({
      next: data => {
        var parseString = xml2js.parseString;
        var xml = data;
        parseString(xml, (err: any, result: any) => {
          this.zig = result;
        });
      },
      error: error => {
        alert(JSON.stringify(error.message));
        console.error('There was an error!', error);
      }
    })
  }

  primerak_znaka: boolean = false;
  spisak_robe_usluga: boolean = false;
  punomocje: boolean = false;
  generalno_punomocje: boolean = false;
  punomocje_naknadno_dostavljeno: boolean = false;
  akt_o_kolektivnom_zigu: boolean = false;
  dokaz_prava_prvenstva: boolean = false;
  dokaz_uplata_takse: boolean = false;

  odbij(){
    let zaokruzeno = "";
    this.zig['ns5:zig']['ns5:nicanska_skala'][0]['ns5:zaokruzeno'].forEach(element => {
    zaokruzeno = zaokruzeno + "<zig:zaokruzeno>"+element+"<\/zig:zaokruzeno>\r\n";
  });

let text: string = "<zig:zig broj_prijave=\""
  + this.zig['ns5:zig'].$.broj_prijave +
  "\" datum=\"" + this.zig['ns5:zig'].$.datum +
  "\" xmlns:zig=\"https:\/\/www.serviszaradsapatentima.rs\/zig\">\r\n  <zig:podnosilac_prijave vocab=\"http:\/\/www.serviszaradsapatentima.rs\/rdf\/database\/predicate\" about=\"string\">\r\n    <zig:ime>"
  + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:ime'][0]._ +
  "<\/zig:ime>\r\n    <zig:prezime>" +
  this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:prezime'][0]._ + "<\/zig:prezime>\r\n    <zig:ulica>" +
  this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:ulica'][0]._ +
  "<\/zig:ulica>\r\n    <zig:broj>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:broj'][0]._ +
  "<\/zig:broj>\r\n    <zig:postanski_broj>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:postanski_broj'][0]._ +
  "<\/zig:postanski_broj>\r\n    <zig:mesto>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:mesto'][0] +
  "<\/zig:mesto>\r\n    <zig:drzava>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:drzava'][0]._ +
  "<\/zig:drzava>\r\n    <zig:telefon>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:telefon'][0]._ +
  "<\/zig:telefon>\r\n    <zig:email>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:email'][0]._ +
  "<\/zig:email>\r\n    <zig:faks>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:faks'][0]._ +
  "<\/zig:faks>\r\n  <\/zig:podnosilac_prijave>\r\n  <zig:punomocnik>\r\n    <zig:ime>"
  + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:ime'][0]._ +
  "<\/zig:ime>\r\n    <zig:prezime>" +
  this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:prezime'][0]._ + "<\/zig:prezime>\r\n    <zig:ulica>" +
  this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:ulica'][0]._ +
  "<\/zig:ulica>\r\n    <zig:broj>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:broj'][0]._ +
  "<\/zig:broj>\r\n    <zig:postanski_broj>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:postanski_broj'][0]._ +
  "<\/zig:postanski_broj>\r\n    <zig:mesto>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:mesto'][0] +
  "<\/zig:mesto>\r\n    <zig:drzava>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:drzava'][0]._ +
  "<\/zig:drzava>\r\n    <zig:telefon>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:telefon'][0]._ +
  "<\/zig:telefon>\r\n    <zig:email>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:email'][0]._ +
  "<\/zig:email>\r\n    <zig:faks>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:faks'][0]._ +
  "<\/zig:faks>\r\n  <\/zig:punomocnik>\r\n  <zig:predstavnik>\r\n    <zig:ime>"
  + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:ime'][0]._ +
  "<\/zig:ime>\r\n    <zig:prezime>" +
  this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:prezime'][0]._ + "<\/zig:prezime>\r\n    <zig:ulica>" +
  this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:ulica'][0]._ +
  "<\/zig:ulica>\r\n    <zig:broj>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:broj'][0]._ +
  "<\/zig:broj>\r\n    <zig:postanski_broj>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:postanski_broj'][0]._ +
  "<\/zig:postanski_broj>\r\n    <zig:mesto>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:mesto'][0] +
  "<\/zig:mesto>\r\n    <zig:drzava>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:drzava'][0]._ +
  "<\/zig:drzava>\r\n    <zig:telefon>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:telefon'][0]._ +
  "<\/zig:telefon>\r\n    <zig:email>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:email'][0]._ +
  "<\/zig:email>\r\n    <zig:faks>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:faks'][0]._ +
  "<\/zig:faks>\r\n  <\/zig:predstavnik>\r\n  <zig:odnosi_za>\r\n    <zig:a>\r\n      <zig:individualni>" +
  this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:a'][0]['ns5:individualni'][0] + "<\/zig:individualni>\r\n      <zig:kolektivni>" +
  this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:a'][0]['ns5:kolektivni'][0] + "<\/zig:kolektivni>\r\n      <zig:garancije>" +
  this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:a'][0]['ns5:garancije'][0] + "<\/zig:garancije>\r\n    <\/zig:a>\r\n    <zig:b>\r\n      <zig:verbalni>" +
  this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:b'][0]['ns5:verbalni'][0] + "<\/zig:verbalni>\r\n      <zig:graficki>" +
  this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:b'][0]['ns5:graficki'][0] + "<\/zig:graficki>\r\n      <zig:kombinovni>" +
  this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:b'][0]['ns5:kombinovni'][0] + "<\/zig:kombinovni>\r\n      <zig:trodimenzioni>" +
  this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:b'][0]['ns5:trodimenzioni'][0] + "<\/zig:trodimenzioni>\r\n      <zig:drugo>" +
  this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:b'][0]['ns5:drugo'][0] + "<\/zig:drugo>\r\n    <\/zig:b>\r\n    <zig:v>\r\n      <zig:izgled>dHVyYmluZQ==<\/zig:izgled>\r\n    <\/zig:v>\r\n  <\/zig:odnosi_za>\r\n  <zig:boje>" +
  this.zig['ns5:zig']['ns5:boje'][0] + "<\/zig:boje>\r\n  <zig:transliteracija_znaka>" +
  this.zig['ns5:zig']['ns5:transliteracija_znaka'][0] + "<\/zig:transliteracija_znaka>\r\n  <zig:prevod_znaka>" +
  this.zig['ns5:zig']['ns5:prevod_znaka'][0] + "<\/zig:prevod_znaka>\r\n  <zig:opis_znaka>" +
  this.zig['ns5:zig']['ns5:opis_znaka'][0] + "<\/zig:opis_znaka>\r\n  <zig:nicanska_skala>\r\n    <!--1 or more repetitions:-->\r\n    " +
  zaokruzeno + "  <\/zig:nicanska_skala>\r\n  <zig:prvanstvo_i_osnov>" +
  this.zig['ns5:zig']['ns5:prvanstvo_i_osnov'][0] + "<\/zig:prvanstvo_i_osnov>\r\n  <zig:placanje_takse>\r\n    <zig:osnovna_taksa>" +
  this.zig['ns5:zig']['ns5:placanje_takse'][0]['ns5:osnovna_taksa'][0]+ "<\/zig:osnovna_taksa>\r\n    <zig:za_klasa broj_klasa=\"" +
  this.zig['ns5:zig']['ns5:placanje_takse'][0]['ns5:za_klasa'][0].$.broj_klasa + "\" dinara=\"" +
  this.zig['ns5:zig']['ns5:placanje_takse'][0]['ns5:za_klasa'][0].$.dinara + "\"\/>\r\n    <zig:graficko_resenje>" +
  this.zig['ns5:zig']['ns5:placanje_takse'][0]['ns5:graficko_resenje'][0] + "<\/zig:graficko_resenje>\r\n    <zig:ukupno>" +
  this.zig['ns5:zig']['ns5:placanje_takse'][0]['ns5:ukupno'][0] + "<\/zig:ukupno>\r\n  <\/zig:placanje_takse>\r\n  <zig:potpis_podnosioca>Y2lyY3Vt<\/zig:potpis_podnosioca>\r\n  <zig:popunjava_zavod>\r\n    <zig:primerak_znaka>" +
  this.primerak_znaka + "<\/zig:primerak_znaka>\r\n    <zig:spisak_robe_usluga>" +
  this.spisak_robe_usluga + "<\/zig:spisak_robe_usluga>\r\n    <zig:punomocje>" +
  this.punomocje + "<\/zig:punomocje>\r\n    <zig:generalno_punomocje>" +
  this.generalno_punomocje + "<\/zig:generalno_punomocje>\r\n    <zig:punomocje_naknadno_dostavljeno>" +
  this.punomocje_naknadno_dostavljeno + "<\/zig:punomocje_naknadno_dostavljeno>\r\n    <zig:akt_o_kolektivnom_zigu>" +
  this.akt_o_kolektivnom_zigu + "<\/zig:akt_o_kolektivnom_zigu>\r\n    <zig:dokaz_prava_prvenstva>" +
  this.dokaz_prava_prvenstva + "<\/zig:dokaz_prava_prvenstva>\r\n    <zig:dokaz_uplata_takse>" +
  this.dokaz_uplata_takse + "<\/zig:dokaz_uplata_takse>\r\n    <zig:stanje>" +
  "ODBIJEN" + "<\/zig:stanje>\r\n  <\/zig:popunjava_zavod>\r\n<\/zig:zig>";
  this.zigServiceService.upisi(text).subscribe({
    next: data => {
      var parseString = xml2js.parseString;
      var xml = data;
      parseString(xml, function (err: any, result: any) {
      });
      alert("Uspesno odbijen!");
      this.router.navigate(['/', 'nacekanju']);
    },
    error: error => {
      alert(JSON.stringify(error.message));
      console.error('There was an error!', error);
    }
  })
  }
  onSubmit() {

    let zaokruzeno = "";
        this.zig['ns5:zig']['ns5:nicanska_skala'][0]['ns5:zaokruzeno'].forEach(element => {
        zaokruzeno = zaokruzeno + "<zig:zaokruzeno>"+element+"<\/zig:zaokruzeno>\r\n";
      });

    let text: string = "<zig:zig broj_prijave=\""
      + this.zig['ns5:zig'].$.broj_prijave +
      "\" datum=\"" + this.zig['ns5:zig'].$.datum +
      "\" xmlns:zig=\"https:\/\/www.serviszaradsapatentima.rs\/zig\">\r\n  <zig:podnosilac_prijave vocab=\"http:\/\/www.serviszaradsapatentima.rs\/rdf\/database\/predicate\" about=\"string\">\r\n    <zig:ime>"
      + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:ime'][0]._ +
      "<\/zig:ime>\r\n    <zig:prezime>" +
      this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:prezime'][0]._ + "<\/zig:prezime>\r\n    <zig:ulica>" +
      this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:ulica'][0]._ +
      "<\/zig:ulica>\r\n    <zig:broj>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:broj'][0]._ +
      "<\/zig:broj>\r\n    <zig:postanski_broj>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:postanski_broj'][0]._ +
      "<\/zig:postanski_broj>\r\n    <zig:mesto>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:mesto'][0] +
      "<\/zig:mesto>\r\n    <zig:drzava>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:drzava'][0]._ +
      "<\/zig:drzava>\r\n    <zig:telefon>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:telefon'][0]._ +
      "<\/zig:telefon>\r\n    <zig:email>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:email'][0]._ +
      "<\/zig:email>\r\n    <zig:faks>" + this.zig['ns5:zig']['ns5:podnosilac_prijave'][0]['ns5:faks'][0]._ +
      "<\/zig:faks>\r\n  <\/zig:podnosilac_prijave>\r\n  <zig:punomocnik>\r\n    <zig:ime>"
      + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:ime'][0]._ +
      "<\/zig:ime>\r\n    <zig:prezime>" +
      this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:prezime'][0]._ + "<\/zig:prezime>\r\n    <zig:ulica>" +
      this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:ulica'][0]._ +
      "<\/zig:ulica>\r\n    <zig:broj>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:broj'][0]._ +
      "<\/zig:broj>\r\n    <zig:postanski_broj>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:postanski_broj'][0]._ +
      "<\/zig:postanski_broj>\r\n    <zig:mesto>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:mesto'][0] +
      "<\/zig:mesto>\r\n    <zig:drzava>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:drzava'][0]._ +
      "<\/zig:drzava>\r\n    <zig:telefon>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:telefon'][0]._ +
      "<\/zig:telefon>\r\n    <zig:email>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:email'][0]._ +
      "<\/zig:email>\r\n    <zig:faks>" + this.zig['ns5:zig']['ns5:punomocnik'][0]['ns5:faks'][0]._ +
      "<\/zig:faks>\r\n  <\/zig:punomocnik>\r\n  <zig:predstavnik>\r\n    <zig:ime>"
      + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:ime'][0]._ +
      "<\/zig:ime>\r\n    <zig:prezime>" +
      this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:prezime'][0]._ + "<\/zig:prezime>\r\n    <zig:ulica>" +
      this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:ulica'][0]._ +
      "<\/zig:ulica>\r\n    <zig:broj>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:broj'][0]._ +
      "<\/zig:broj>\r\n    <zig:postanski_broj>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:postanski_broj'][0]._ +
      "<\/zig:postanski_broj>\r\n    <zig:mesto>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:mesto'][0] +
      "<\/zig:mesto>\r\n    <zig:drzava>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:drzava'][0]._ +
      "<\/zig:drzava>\r\n    <zig:telefon>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:telefon'][0]._ +
      "<\/zig:telefon>\r\n    <zig:email>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:email'][0]._ +
      "<\/zig:email>\r\n    <zig:faks>" + this.zig['ns5:zig']['ns5:predstavnik'][0]['ns5:faks'][0]._ +
      "<\/zig:faks>\r\n  <\/zig:predstavnik>\r\n  <zig:odnosi_za>\r\n    <zig:a>\r\n      <zig:individualni>" +
      this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:a'][0]['ns5:individualni'][0] + "<\/zig:individualni>\r\n      <zig:kolektivni>" +
      this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:a'][0]['ns5:kolektivni'][0] + "<\/zig:kolektivni>\r\n      <zig:garancije>" +
      this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:a'][0]['ns5:garancije'][0] + "<\/zig:garancije>\r\n    <\/zig:a>\r\n    <zig:b>\r\n      <zig:verbalni>" +
      this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:b'][0]['ns5:verbalni'][0] + "<\/zig:verbalni>\r\n      <zig:graficki>" +
      this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:b'][0]['ns5:graficki'][0] + "<\/zig:graficki>\r\n      <zig:kombinovni>" +
      this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:b'][0]['ns5:kombinovni'][0] + "<\/zig:kombinovni>\r\n      <zig:trodimenzioni>" +
      this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:b'][0]['ns5:trodimenzioni'][0] + "<\/zig:trodimenzioni>\r\n      <zig:drugo>" +
      this.zig['ns5:zig']['ns5:odnosi_za'][0]['ns5:b'][0]['ns5:drugo'][0] + "<\/zig:drugo>\r\n    <\/zig:b>\r\n    <zig:v>\r\n      <zig:izgled>dHVyYmluZQ==<\/zig:izgled>\r\n    <\/zig:v>\r\n  <\/zig:odnosi_za>\r\n  <zig:boje>" +
      this.zig['ns5:zig']['ns5:boje'][0] + "<\/zig:boje>\r\n  <zig:transliteracija_znaka>" +
      this.zig['ns5:zig']['ns5:transliteracija_znaka'][0] + "<\/zig:transliteracija_znaka>\r\n  <zig:prevod_znaka>" +
      this.zig['ns5:zig']['ns5:prevod_znaka'][0] + "<\/zig:prevod_znaka>\r\n  <zig:opis_znaka>" +
      this.zig['ns5:zig']['ns5:opis_znaka'][0] + "<\/zig:opis_znaka>\r\n  <zig:nicanska_skala>\r\n    <!--1 or more repetitions:-->\r\n    " +
      zaokruzeno + "  <\/zig:nicanska_skala>\r\n  <zig:prvanstvo_i_osnov>" +
      this.zig['ns5:zig']['ns5:prvanstvo_i_osnov'][0] + "<\/zig:prvanstvo_i_osnov>\r\n  <zig:placanje_takse>\r\n    <zig:osnovna_taksa>" +
      this.zig['ns5:zig']['ns5:placanje_takse'][0]['ns5:osnovna_taksa'][0]+ "<\/zig:osnovna_taksa>\r\n    <zig:za_klasa broj_klasa=\"" +
      this.zig['ns5:zig']['ns5:placanje_takse'][0]['ns5:za_klasa'][0].$.broj_klasa + "\" dinara=\"" +
      this.zig['ns5:zig']['ns5:placanje_takse'][0]['ns5:za_klasa'][0].$.dinara + "\"\/>\r\n    <zig:graficko_resenje>" +
      this.zig['ns5:zig']['ns5:placanje_takse'][0]['ns5:graficko_resenje'][0] + "<\/zig:graficko_resenje>\r\n    <zig:ukupno>" +
      this.zig['ns5:zig']['ns5:placanje_takse'][0]['ns5:ukupno'][0] + "<\/zig:ukupno>\r\n  <\/zig:placanje_takse>\r\n  <zig:potpis_podnosioca>Y2lyY3Vt<\/zig:potpis_podnosioca>\r\n  <zig:popunjava_zavod>\r\n    <zig:primerak_znaka>" +
      this.primerak_znaka + "<\/zig:primerak_znaka>\r\n    <zig:spisak_robe_usluga>" +
      this.spisak_robe_usluga + "<\/zig:spisak_robe_usluga>\r\n    <zig:punomocje>" +
      this.punomocje + "<\/zig:punomocje>\r\n    <zig:generalno_punomocje>" +
      this.generalno_punomocje + "<\/zig:generalno_punomocje>\r\n    <zig:punomocje_naknadno_dostavljeno>" +
      this.punomocje_naknadno_dostavljeno + "<\/zig:punomocje_naknadno_dostavljeno>\r\n    <zig:akt_o_kolektivnom_zigu>" +
      this.akt_o_kolektivnom_zigu + "<\/zig:akt_o_kolektivnom_zigu>\r\n    <zig:dokaz_prava_prvenstva>" +
      this.dokaz_prava_prvenstva + "<\/zig:dokaz_prava_prvenstva>\r\n    <zig:dokaz_uplata_takse>" +
      this.dokaz_uplata_takse + "<\/zig:dokaz_uplata_takse>\r\n    <zig:stanje>" +
      "ODOBREN" + "<\/zig:stanje>\r\n  <\/zig:popunjava_zavod>\r\n<\/zig:zig>";
      this.zigServiceService.upisi(text).subscribe({
        next: data => {
          var parseString = xml2js.parseString;
          var xml = data;
          parseString(xml, function (err: any, result: any) {
          });
          alert("Uspesno upisano!");
          this.router.navigate(['/', 'nacekanju']);
        },
        error: error => {
          alert(JSON.stringify(error.message));
          console.error('There was an error!', error);
        }
      })
    }
}
