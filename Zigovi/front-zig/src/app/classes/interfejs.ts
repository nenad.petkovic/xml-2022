export interface Root {
    "ns5:zig": Ns5Zig
  }
  
  export interface Ns5Zig {
    $: GeneratedType
    "ns5:podnosilac_prijave": Ns5PodnosilacPrijave[]
    "ns5:punomocnik": Ns5Punomocnik[]
    "ns5:predstavnik": Ns5Predstavnik[]
    "ns5:odnosi_za": Ns5OdnosiZa[]
    "ns5:boje": string[]
    "ns5:transliteracija_znaka": string[]
    "ns5:prevod_znaka": string[]
    "ns5:opis_znaka": string[]
    "ns5:nicanska_skala": Ns5NicanskaSkala[]
    "ns5:prvanstvo_i_osnov": string[]
    "ns5:placanje_takse": Ns5PlacanjeTakse[]
    "ns5:potpis_podnosioca": string[]
    "ns5:popunjava_zavod": Ns5PopunjavaZavod[]
  }
  
  export interface GeneratedType {
    "xmlns:pred": string
    "xmlns:r": string
    xmlns: string
    "xmlns:xs": string
    "xmlns:ns5": string
    broj_prijave: string
    datum: string
  }
  
  export interface Ns5PodnosilacPrijave {
    $: GeneratedType2
    "ns5:ime": Ns5Ime[]
    "ns5:prezime": Ns5Prezime[]
    "ns5:ulica": Ns5Ulica[]
    "ns5:broj": Ns5Broj[]
    "ns5:postanski_broj": Ns5PostanskiBroj[]
    "ns5:mesto": string[]
    "ns5:drzava": Ns5Drzava[]
    "ns5:telefon": Ns5Telefon[]
    "ns5:email": Ns5Email[]
    "ns5:faks": Fak[]
  }
  
  export interface GeneratedType2 {
    vocab: string
    about: string
  }
  
  export interface Ns5Ime {
    _: string
    $: GeneratedType3
  }
  
  export interface GeneratedType3 {
    property: string
    datatype: string
  }
  
  export interface Ns5Prezime {
    _: string
    $: GeneratedType4
  }
  
  export interface GeneratedType4 {
    property: string
    datatype: string
  }
  
  export interface Ns5Ulica {
    _: string
    $: GeneratedType5
  }
  
  export interface GeneratedType5 {
    property: string
    datatype: string
  }
  
  export interface Ns5Broj {
    _: string
    $: GeneratedType6
  }
  
  export interface GeneratedType6 {
    property: string
    datatype: string
  }
  
  export interface Ns5PostanskiBroj {
    _: string
    $: GeneratedType7
  }
  
  export interface GeneratedType7 {
    property: string
    datatype: string
  }
  
  export interface Ns5Drzava {
    _: string
    $: GeneratedType8
  }
  
  export interface GeneratedType8 {
    property: string
    datatype: string
  }
  
  export interface Ns5Telefon {
    _: string
    $: GeneratedType9
  }
  
  export interface GeneratedType9 {
    property: string
    datatype: string
  }
  
  export interface Ns5Email {
    _: string
    $: GeneratedType10
  }
  
  export interface GeneratedType10 {
    property: string
    datatype: string
  }
  
  export interface Fak {
    _: string
    $: GeneratedType11
  }
  
  export interface GeneratedType11 {
    property: string
    datatype: string
  }
  
  export interface Ns5Punomocnik {
    $: GeneratedType12
    "ns5:ime": Ns5Ime2[]
    "ns5:prezime": Ns5Prezime2[]
    "ns5:ulica": Ns5Ulica2[]
    "ns5:broj": Ns5Broj2[]
    "ns5:postanski_broj": Ns5PostanskiBroj2[]
    "ns5:mesto": string[]
    "ns5:drzava": Ns5Drzava2[]
    "ns5:telefon": Ns5Telefon2[]
    "ns5:email": Ns5Email2[]
    "ns5:faks": Fak2[]
  }
  
  export interface GeneratedType12 {
    vocab: string
    about: string
  }
  
  export interface Ns5Ime2 {
    _: string
    $: GeneratedType13
  }
  
  export interface GeneratedType13 {
    property: string
    datatype: string
  }
  
  export interface Ns5Prezime2 {
    _: string
    $: GeneratedType14
  }
  
  export interface GeneratedType14 {
    property: string
    datatype: string
  }
  
  export interface Ns5Ulica2 {
    _: string
    $: GeneratedType15
  }
  
  export interface GeneratedType15 {
    property: string
    datatype: string
  }
  
  export interface Ns5Broj2 {
    _: string
    $: GeneratedType16
  }
  
  export interface GeneratedType16 {
    property: string
    datatype: string
  }
  
  export interface Ns5PostanskiBroj2 {
    _: string
    $: GeneratedType17
  }
  
  export interface GeneratedType17 {
    property: string
    datatype: string
  }
  
  export interface Ns5Drzava2 {
    _: string
    $: GeneratedType18
  }
  
  export interface GeneratedType18 {
    property: string
    datatype: string
  }
  
  export interface Ns5Telefon2 {
    _: string
    $: GeneratedType19
  }
  
  export interface GeneratedType19 {
    property: string
    datatype: string
  }
  
  export interface Ns5Email2 {
    _: string
    $: GeneratedType20
  }
  
  export interface GeneratedType20 {
    property: string
    datatype: string
  }
  
  export interface Fak2 {
    _: string
    $: GeneratedType21
  }
  
  export interface GeneratedType21 {
    property: string
    datatype: string
  }
  
  export interface Ns5Predstavnik {
    $: GeneratedType22
    "ns5:ime": Ns5Ime3[]
    "ns5:prezime": Ns5Prezime3[]
    "ns5:ulica": Ns5Ulica3[]
    "ns5:broj": Ns5Broj3[]
    "ns5:postanski_broj": Ns5PostanskiBroj3[]
    "ns5:mesto": string[]
    "ns5:drzava": Ns5Drzava3[]
    "ns5:telefon": Ns5Telefon3[]
    "ns5:email": Ns5Email3[]
    "ns5:faks": Fak3[]
  }
  
  export interface GeneratedType22 {
    vocab: string
    about: string
  }
  
  export interface Ns5Ime3 {
    _: string
    $: GeneratedType23
  }
  
  export interface GeneratedType23 {
    property: string
    datatype: string
  }
  
  export interface Ns5Prezime3 {
    _: string
    $: GeneratedType24
  }
  
  export interface GeneratedType24 {
    property: string
    datatype: string
  }
  
  export interface Ns5Ulica3 {
    _: string
    $: GeneratedType25
  }
  
  export interface GeneratedType25 {
    property: string
    datatype: string
  }
  
  export interface Ns5Broj3 {
    _: string
    $: GeneratedType26
  }
  
  export interface GeneratedType26 {
    property: string
    datatype: string
  }
  
  export interface Ns5PostanskiBroj3 {
    _: string
    $: GeneratedType27
  }
  
  export interface GeneratedType27 {
    property: string
    datatype: string
  }
  
  export interface Ns5Drzava3 {
    _: string
    $: GeneratedType28
  }
  
  export interface GeneratedType28 {
    property: string
    datatype: string
  }
  
  export interface Ns5Telefon3 {
    _: string
    $: GeneratedType29
  }
  
  export interface GeneratedType29 {
    property: string
    datatype: string
  }
  
  export interface Ns5Email3 {
    _: string
    $: GeneratedType30
  }
  
  export interface GeneratedType30 {
    property: string
    datatype: string
  }
  
  export interface Fak3 {
    _: string
    $: GeneratedType31
  }
  
  export interface GeneratedType31 {
    property: string
    datatype: string
  }
  
  export interface Ns5OdnosiZa {
    "ns5:a": Ns5A[]
    "ns5:b": Ns5B[]
    "ns5:v": Ns5V[]
  }
  
  export interface Ns5A {
    "ns5:individualni": string[]
    "ns5:kolektivni": string[]
    "ns5:garancije": string[]
  }
  
  export interface Ns5B {
    "ns5:verbalni": string[]
    "ns5:graficki": string[]
    "ns5:kombinovni": string[]
    "ns5:trodimenzioni": string[]
    "ns5:drugo": string[]
  }
  
  export interface Ns5V {
    "ns5:izgled": string[]
  }
  
  export interface Ns5NicanskaSkala {
    "ns5:zaokruzeno": string[]
  }
  
  export interface Ns5PlacanjeTakse {
    "ns5:osnovna_taksa": string[]
    "ns5:za_klasa": Ns5ZaKlasa[]
    "ns5:graficko_resenje": string[]
    "ns5:ukupno": string[]
  }
  
  export interface Ns5ZaKlasa {
    $: GeneratedType32
  }
  
  export interface GeneratedType32 {
    broj_klasa: string
    dinara: string
  }
  
  export interface Ns5PopunjavaZavod {
    $: GeneratedType33
    "ns5:primerak_znaka": string[]
    "ns5:spisak_robe_usluga": string[]
    "ns5:punomocje": string[]
    "ns5:generalno_punomocje": string[]
    "ns5:punomocje_naknadno_dostavljeno": string[]
    "ns5:akt_o_kolektivnom_zigu": string[]
    "ns5:dokaz_prava_prvenstva": string[]
    "ns5:dokaz_uplata_takse": string[]
    "ns5:stanje": Ns5Stanje[]
  }
  
  export interface GeneratedType33 {
    vocab: string
    about: string
  }
  
  export interface Ns5Stanje {
    _: string
    $: GeneratedType34
  }
  
  export interface GeneratedType34 {
    property: string
    datatype: string
  }
  