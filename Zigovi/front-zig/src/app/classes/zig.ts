export class Zig {

    constructor(
        public broj_prijave: string,
        public datum: string,
        public podnosilac_prijave: {
            ime: string,
            prezime: string,
            ulica: string,
            broj: string,
            postanski_broj: string,
            mesto: string,
            drzava: string,
            telefon: string,
            email: string,
            faks: string
          },
        public punomocnik: {
            ime: string,
            prezime: string,
            ulica: string,
            broj: string,
            postanski_broj: string,
            mesto: string,
            drzava: string,
            telefon: string,
            email: string,
            faks: string
          },
        public predstavnik: {
            ime: string,
            prezime: string,
            ulica: string,
            broj: string,
            postanski_broj: string,
            mesto: string,
            drzava: string,
            telefon: string,
            email: string,
            faks: string
          },
          public odnosi_za: {
            a: {
                individualni: boolean,
                kolektivni: boolean,
                garancije: boolean
            },
            b: {
                verbalni: boolean,
                graficki: boolean,
                kombinovni: boolean,
                trodimenzioni: boolean,
                drugo: string
            },
            v: {
                izgled: string
            }
          },
        public boje: string,
        public transliteracija_znaka: string,
        public prevod_znaka: string,
        public opis_znaka: string,
        public nicanska_skala: number[],
        public prvanstvo_i_osnov: string,
        public placanje_takse: {
            osnovna_taksa: number,
            za_klasa: {
                broj_klasa: number,
                dinara:number
            },
            graficko_resenje: number,
            ukupno: number,
          },
          public popunjava_zavod: {
            primerak_znaka: boolean,
            spisak_robe_usluga: boolean,
            punomocje: boolean,
            generalno_punomocje: boolean,
            punomocje_naknadno_dostavljeno: boolean,
            akt_o_kolektivnom_zigu: boolean,
            dokaz_prava_prvenstva: boolean,
            dokaz_uplata_takse: boolean,
            stanje: string
          }
    ) {  }
  
  }