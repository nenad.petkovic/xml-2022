export interface Root {
    "ns5:users": Ns5Users
  }
  
  export interface Ns5Users {
    $: GeneratedType
    "ns5:podnosilac_prijave": Ns5PodnosilacPrijave[]
  }
  
  export interface GeneratedType {
    "xmlns:pred": string
    "xmlns:r": string
    "xmlns:xs": string
    xmlns: string
    "xmlns:ns5": string
  }
  
  export interface Ns5PodnosilacPrijave {
    $: GeneratedType2
    "ns5:password": string[]
    "ns5:uloga": string[]
    "ns5:email": Ns5Email[]
  }
  
  export interface GeneratedType2 {
    vocab: string
    about: string
  }
  
  export interface Ns5Email {
    _: string
    $: GeneratedType3
  }
  
  export interface GeneratedType3 {
    property: string
    datatype: string
  }
  