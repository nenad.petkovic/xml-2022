import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { MojiZahteviComponent } from './components/moji-zahtevi/moji-zahtevi.component';
import { NaprednaPretragaComponent } from './components/napredna-pretraga/napredna-pretraga.component';
import { PretragaComponent } from './components/pretraga/pretraga.component';
import { RegisterComponent } from './components/register/register.component';
import { SluzbenikFormaComponent } from './components/sluzbenik-forma/sluzbenik-forma.component';
import { ZahtevComponent } from './components/zahtev/zahtev.component';
import { ZahteviNaCekanjuComponent } from './components/zahtevi-na-cekanju/zahtevi-na-cekanju.component';
import { ZigComponent } from './components/zig/zig.component';

const routes: Routes = [
  { path: 'zahtev', component: ZahtevComponent },
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'pretraga', component: PretragaComponent },
  { path: 'nacekanju', component: ZahteviNaCekanjuComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'sluzbenik-forma/:id', component: SluzbenikFormaComponent },
  { path: 'napredna', component: NaprednaPretragaComponent },
  { path: 'moji', component: MojiZahteviComponent },
  { path: 'zig/:id', component: ZigComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
