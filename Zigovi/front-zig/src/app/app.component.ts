import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private router: Router) {
  }
  isAdmin: boolean = false;
  isUser: boolean = false;
  isLogedIn: boolean = false;

  ngOnInit(): void {
    let uloga = localStorage.getItem("uloga")?.toString() || "";
    let email = localStorage.getItem("email")?.toString() || "";

    if (email != "" && email != null) {
      this.isLogedIn = true;
      if (uloga == "ADMIN") {
        this.isAdmin = true;
        this.isUser = false;
      }
      else {
        this.isAdmin = false;
        this.isUser = true;
      }
    }
    else {
      this.isLogedIn = false;
      this.isUser = false;
      this.isAdmin = false;
    }
  }

  goToHome() {
    this.router.navigate(['/', '/']);
  }
  goTologin() {
    this.router.navigate(['/', 'login']);
  }
  goTozahtev() {
    this.router.navigate(['/', 'zahtev']);
  }
  goTopretraga() {
    this.router.navigate(['/', 'pretraga']);
  }
  goToMoji() {
    this.router.navigate(['/', 'moji']);
  }
  goToNapredna() {
    this.router.navigate(['/', 'napredna']);
  }
  goToNaCekanju() {
    this.router.navigate(['/', 'nacekanju']);
  }
  goToregister() {
    this.router.navigate(['/', 'register']);
  }
  goTosluzbenikforma() {
    this.router.navigate(['/', 'sluzbenik-forma']);
  }
  goTologout() {
    localStorage.clear()
    this.router.navigate(['/', '/']).then(() => {
      window.location.reload();
    });
  }
}
