package www.serviszaradsapatentima.rs.zigovi.service;

import org.apache.commons.io.FileUtils;
import www.serviszaradsapatentima.rs.zigovi.exception.EntityNotFoundException;
import www.serviszaradsapatentima.rs.zigovi.exception.InvalidXmlDatabaseException;
import www.serviszaradsapatentima.rs.zigovi.exception.InvalidXmlException;
import www.serviszaradsapatentima.rs.zigovi.exception.XmlDatabaseException;
import www.serviszaradsapatentima.rs.zigovi.helper.UUIDHelper;
import www.serviszaradsapatentima.rs.zigovi.helper.XmlConversionAgent;
import www.serviszaradsapatentima.rs.zigovi.model.zig.Zig;
import www.serviszaradsapatentima.rs.zigovi.repository.AbstractXmlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xmldb.api.base.XMLDBException;
import www.serviszaradsapatentima.rs.zigovi.transformation.XSLTransformer;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static www.serviszaradsapatentima.rs.zigovi.helper.XQueryExpressions.X_QUERY_FIND_ALL_ZIG_EXPRESSION;
import static www.serviszaradsapatentima.rs.zigovi.helper.XQueryExpressions.X_UPDATE_REMOVE_ZIG_BY_ID_EXPRESSION;

@Service
public class ZigService implements AbstractXmlService<Zig> {
    private final String jaxbContextPath = "www.serviszaradsapatentima.rs.zigovi.model.zig";

    private static final String SPARQL_NAMED_GRAPH_URI = "/sparql/metadata";

    public static final String OUTPUT_FOLDER_XML = "output_xml";
    public static final String OUTPUT_FOLDER_PDF = "output_pdf";
    public static final String OUTPUT_FOLDER_HTML = "output_html";
    public static final String OUTPUT_FOLDER_METADATA = "output_metadata";

    @Autowired
    private AbstractXmlRepository<Zig> zigRepository;

    @Autowired
    private XmlConversionAgent<Zig> zigConverionAgent;

    @Autowired
    private UUIDHelper uuidHelper;

    @Autowired
    private RDFService rdfService;

    public void injectRepositoryProperties() {
        this.zigRepository.injectRepositoryProperties(
                "/db/intelektualna_svojina/zigovi",
                jaxbContextPath,
                X_QUERY_FIND_ALL_ZIG_EXPRESSION,
                X_UPDATE_REMOVE_ZIG_BY_ID_EXPRESSION);
    }

    @Override
    public List<Zig> findAll() {
        injectRepositoryProperties();

        try {
            return this.zigRepository.getAllEntities();
        } catch (XMLDBException e) {
            throw new XmlDatabaseException(e.getMessage());
        } catch (JAXBException e) {
            throw new InvalidXmlDatabaseException(Zig.class, e.getMessage());
        }
    }

    public List<Zig> findByString(String text) {
        injectRepositoryProperties();
        System.out.println("[Pretraga] za:");
        System.out.println(text);
        try {
            List<Zig> lista = this.zigRepository.getAllEntities();
            List<Zig> nova = new ArrayList<Zig>();
            for (Zig z:
                 lista) {

                if(z.toString().toUpperCase().contains(text.toUpperCase()))
                {
                    nova.add(z);

                }
            }
            return nova;
        } catch (XMLDBException e) {
            throw new XmlDatabaseException(e.getMessage());
        } catch (JAXBException e) {
            throw new InvalidXmlDatabaseException(Zig.class, e.getMessage());
        }
    }

    @Override
    public Zig findById(String entityId) {
        injectRepositoryProperties();

        try {
            Zig zig = this.zigRepository.getEntity(entityId);
            if (zig == null) {
                throw new EntityNotFoundException(entityId, Zig.class);
            }
            return zig;
        } catch (JAXBException e) {
            throw new XmlDatabaseException(e.getMessage());
        } catch (XMLDBException e) {
            throw new InvalidXmlDatabaseException(Zig.class, e.getMessage());
        }
    }



    @Override
    public Zig create(String entityXml) {
        injectRepositoryProperties();
        Zig zig;

        try {
            zig = this.zigConverionAgent.unmarshall(entityXml, this.jaxbContextPath);
            this.handleMetadata(zig);
        } catch (JAXBException e) {
            throw new InvalidXmlException(Zig.class, e.getMessage());
        }

        try {
            zig = zigRepository.createEntity(zig);
        } catch (JAXBException e) {
            throw new XmlDatabaseException(e.getMessage());
        } catch (XMLDBException e) {
            throw new InvalidXmlDatabaseException(Zig.class, e.getMessage());
        }

        //EKSTRAKCIJA METAPODATAKA >> RDF
        try {
            //radimo marsalovanje >> treba rdfa

            String xmlEntitet = this.zigConverionAgent.marshall(zig, this.jaxbContextPath);
            //System.out.println(xmlEntitet);

            if (!rdfService.save(xmlEntitet, SPARQL_NAMED_GRAPH_URI)) {
                System.out.println("[ERROR] Neuspesno cuvanje metapodataka zahteva u RDF DB.");
            }
        } catch (JAXBException e) {
            throw new XmlDatabaseException(e.getMessage());
        }
        return zig;
    }

    @Override
    public Zig update(String entityXml) {
        injectRepositoryProperties();

        Zig zig;
        try {
            zig = this.zigConverionAgent.unmarshall(entityXml, this.jaxbContextPath);
        } catch (JAXBException e) {
            throw new InvalidXmlException(Zig.class, e.getMessage());
        }
        try {
            if (!this.zigRepository.updateEntity(zig)) {
                throw new EntityNotFoundException(zig.getBrojPrijave(), Zig.class);
            }
            return zig;

        } catch (JAXBException e) {
            throw new XmlDatabaseException(e.getMessage());
        } catch (XMLDBException e) {
            throw new InvalidXmlException(Zig.class, e.getMessage());
        }
    }

    @Override
    public String getRdfaString(String brojPrijave) throws JAXBException {
        injectRepositoryProperties();

        Zig dokument;
        dokument = this.findById(brojPrijave);
        String entityXml = this.zigConverionAgent.marshall(dokument, this.jaxbContextPath);
        System.out.println(entityXml);
        return entityXml;

    }

    @Override
    public boolean deleteById(String entityId) {
        injectRepositoryProperties();
        try {
            return this.zigRepository.deleteEntity(entityId);
        } catch (XMLDBException e) {
            throw new XmlDatabaseException(e.getMessage());
        }
    }


    private void handleMetadata(Zig zig) {
        zig.getPodnosilacPrijave().setVocab("http://www.serviszaradsapatentima.rs/rdf/database/predicate");
        zig.getPodnosilacPrijave().setAbout("http://www.serviszaradsapatentima.rs/rdf/database/zig/" + zig.getBrojPrijave());

        zig.getPodnosilacPrijave().getEmail().setDatatype("xs:#string");
        zig.getPodnosilacPrijave().getEmail().setProperty("pred:email");

        zig.getPodnosilacPrijave().getIme().setDatatype("xs:#string");
        zig.getPodnosilacPrijave().getIme().setProperty("pred:ime");

        zig.getPodnosilacPrijave().getPrezime().setDatatype("xs:#string");
        zig.getPodnosilacPrijave().getPrezime().setProperty("pred:prezime");

        zig.getPodnosilacPrijave().getUlica().setDatatype("xs:#string");
        zig.getPodnosilacPrijave().getUlica().setProperty("pred:ulica");

        zig.getPodnosilacPrijave().getBroj().setDatatype("xs:#string");
        zig.getPodnosilacPrijave().getBroj().setProperty("pred:broj");

        zig.getPodnosilacPrijave().getPostanskiBroj().setDatatype("xs:#string");
        zig.getPodnosilacPrijave().getPostanskiBroj().setProperty("pred:postanski_broj");

        zig.getPodnosilacPrijave().getDrzava().setDatatype("xs:#string");
        zig.getPodnosilacPrijave().getDrzava().setProperty("pred:drzava");

        zig.getPodnosilacPrijave().getTelefon().setDatatype("xs:#string");
        zig.getPodnosilacPrijave().getTelefon().setProperty("pred:telefon");

        zig.getPodnosilacPrijave().getFaks().setDatatype("xs:#string");
        zig.getPodnosilacPrijave().getFaks().setProperty("pred:faks");

        zig.getPredstavnik().setVocab("http://www.serviszaradsapatentima.rs/rdf/database/predicate");
        zig.getPredstavnik().setAbout("http://www.serviszaradsapatentima.rs/rdf/database/zig/" + zig.getBrojPrijave());

        zig.getPredstavnik().getEmail().setDatatype("xs:#string");
        zig.getPredstavnik().getEmail().setProperty("pred:email");

        zig.getPredstavnik().getIme().setDatatype("xs:#string");
        zig.getPredstavnik().getIme().setProperty("pred:ime");

        zig.getPredstavnik().getPrezime().setDatatype("xs:#string");
        zig.getPredstavnik().getPrezime().setProperty("pred:prezime");

        zig.getPredstavnik().getUlica().setDatatype("xs:#string");
        zig.getPredstavnik().getUlica().setProperty("pred:ulica");

        zig.getPredstavnik().getBroj().setDatatype("xs:#string");
        zig.getPredstavnik().getBroj().setProperty("pred:broj");

        zig.getPredstavnik().getPostanskiBroj().setDatatype("xs:#string");
        zig.getPredstavnik().getPostanskiBroj().setProperty("pred:postanski_broj");

        zig.getPredstavnik().getDrzava().setDatatype("xs:#string");
        zig.getPredstavnik().getDrzava().setProperty("pred:drzava");

        zig.getPredstavnik().getTelefon().setDatatype("xs:#string");
        zig.getPredstavnik().getTelefon().setProperty("pred:telefon");

        zig.getPredstavnik().getFaks().setDatatype("xs:#string");
        zig.getPredstavnik().getFaks().setProperty("pred:faks");

        zig.getPunomocnik().setVocab("http://www.serviszaradsapatentima.rs/rdf/database/predicate");
        zig.getPunomocnik().setAbout("http://www.serviszaradsapatentima.rs/rdf/database/zig/" + zig.getBrojPrijave());

        zig.getPunomocnik().getEmail().setDatatype("xs:#string");
        zig.getPunomocnik().getEmail().setProperty("pred:email");

        zig.getPunomocnik().getIme().setDatatype("xs:#string");
        zig.getPunomocnik().getIme().setProperty("pred:ime");

        zig.getPunomocnik().getPrezime().setDatatype("xs:#string");
        zig.getPunomocnik().getPrezime().setProperty("pred:prezime");

        zig.getPunomocnik().getUlica().setDatatype("xs:#string");
        zig.getPunomocnik().getUlica().setProperty("pred:ulica");

        zig.getPunomocnik().getBroj().setDatatype("xs:#string");
        zig.getPunomocnik().getBroj().setProperty("pred:broj");

        zig.getPunomocnik().getPostanskiBroj().setDatatype("xs:#string");
        zig.getPunomocnik().getPostanskiBroj().setProperty("pred:postanski_broj");

        zig.getPunomocnik().getDrzava().setDatatype("xs:#string");
        zig.getPunomocnik().getDrzava().setProperty("pred:drzava");

        zig.getPunomocnik().getTelefon().setDatatype("xs:#string");
        zig.getPunomocnik().getTelefon().setProperty("pred:telefon");

        zig.getPunomocnik().getFaks().setDatatype("xs:#string");
        zig.getPunomocnik().getFaks().setProperty("pred:faks");

        zig.getPopunjavaZavod().setVocab("http://www.serviszaradsapatentima.rs/rdf/database/predicate");
        zig.getPopunjavaZavod().setAbout("http://www.serviszaradsapatentima.rs/rdf/database/zig/" + zig.getBrojPrijave());

        zig.getPopunjavaZavod().getStanje().setDatatype("xs:#string");
        zig.getPopunjavaZavod().getStanje().setProperty("pred:stanje");
    }

    public ByteArrayInputStream generatePDF(String dokumentId) throws Exception {
        String xslFile = "src/main/resources/data/xsl-transformations/zig.xsl";
        String outputHtmlFile = "src/main/resources/data/xsl-transformations/zig.html";
        String outputPdfFile = "src/main/resources/data/xsl-transformations/zig.pdf";
        String outputXmlFile = "src/main/resources/data/instance/zig.xml";

        XSLTransformer xslTransformer = new XSLTransformer();
        xslTransformer.setXSLT_FILE(xslFile);
        xslTransformer.setOUTPUT_FILE_HTML(outputHtmlFile);
        xslTransformer.setOUTPUT_FILE_PDF(outputPdfFile);


        Zig zigXml = this.findById(dokumentId);

        System.out.println("************************************ MMMMMMMMMMMMMMMMMMMMMmmmmm");
        System.out.println(zigXml.toString());

        try {
            this.zigConverionAgent.marshallToFile(
                    zigXml,
                    this.jaxbContextPath,
                    outputXmlFile
            );
            xslTransformer.generatePDF_HTML(outputXmlFile);
            return new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(outputPdfFile)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public ByteArrayInputStream generateHtml(String dokumentId) throws Exception {
        String xslFile = "src/main/resources/data/xsl-transformations/zig.xsl";
        String outputHtmlFile = "src/main/resources/data/xsl-transformations/zig.html";
        String outputPdfFile = "src/main/resources/data/xsl-transformations/zig.pdf";
        String outputXmlFile = "src/main/resources/data/instance/zig.xml";

        XSLTransformer xslTransformer = new XSLTransformer();
        xslTransformer.setXSLT_FILE(xslFile);
        xslTransformer.setOUTPUT_FILE_HTML(outputHtmlFile);
        xslTransformer.setOUTPUT_FILE_PDF(outputPdfFile);


        Zig zigXml = this.findById(dokumentId);

        System.out.println("************************************ MMMMMMMMMMMMMMMMMMMMMmmmmm");
        System.out.println(zigXml.toString());

        try {
            this.zigConverionAgent.marshallToFile(
                    zigXml,
                    this.jaxbContextPath,
                    outputXmlFile
            );
            xslTransformer.generatePDF_HTML(outputXmlFile);
            return new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(outputHtmlFile)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
