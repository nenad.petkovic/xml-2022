package www.serviszaradsapatentima.rs.zigovi.fuseki.util;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class SparqlUtil {

    /* Insert RDF data to an arbitrary named graph */
    private static final String UPDATE_TEMPLATE_NAMED_GRAPH = "INSERT DATA { GRAPH <%1$s> { %2$s } }";

    private static final String SELECT_DISTINCT_NAMED_GRAPH_TEMPLATE = "SELECT DISTINCT ?s FROM <%1$s> WHERE { %2$s }";

    /* Plain text RDF serialization format */
    public static final String NTRIPLES = "N-TRIPLES";

    /* An XML serialization format for RDF data */
    public static final String RDF_XML = "RDF/XML";

    /* An JSON serialization format for RDF data */
    public static final String RDF_JSON = "RDF/JSON";


    public static String insertData(String graphURI, String ntriples) {
        return String.format(UPDATE_TEMPLATE_NAMED_GRAPH, graphURI, ntriples);
    }

    public static String selectDistinctData(String graphURI, String sparqlCondition) {
        return String.format(SELECT_DISTINCT_NAMED_GRAPH_TEMPLATE, graphURI, sparqlCondition);
    }


    public static String selectPoImenuIPrezimenu(String imePretraga,String prezimePretraga, String dataEndpointString) {
        return "SELECT * FROM <" + dataEndpointString + "/sparql/metadata>\n" +
                "WHERE {\n" +
                "  ?dokumentId <http://www.serviszaradsapatentima.rs/rdf/database/predicate/ime> \""+ imePretraga +"\" ; \n" +
                "<http://www.serviszaradsapatentima.rs/rdf/database/predicate/prezime> \"" + prezimePretraga + "\" . " +

                "}";
    }


    public static String selectPoEmailu(String email, String dataEndpointString) {
        return "SELECT * FROM <" + dataEndpointString + "/sparql/metadata>\n" +
                "WHERE {\n" +
                "  ?dokumentId <http://www.serviszaradsapatentima.rs/rdf/database/predicate/email> \""+ email +"\" ; \n" +
                "}";
    }

    public static String selectPoStatus(String status, String dataEndpointString) {
        return "SELECT * FROM <" + dataEndpointString + "/sparql/metadata>\n" +
                "WHERE {\n" +
                "  ?dokumentId <http://www.serviszaradsapatentima.rs/rdf/database/predicate/stanje> \""+ status +"\" ; \n" +
                "}";
    }


    //fizicka lica pretraga AND
    public static String selectANDFizickaLica(String ime,String prezime, String email, String dataEndpointString) {
        return "SELECT * FROM <" + dataEndpointString + "/sparql/metadata>\n" +
                "WHERE {\n" +
                "  ?dokumentId <http://www.serviszaradsapatentima.rs/rdf/database/predicate/email> \""+ email +"\" ; \n" +
                "  <http://www.serviszaradsapatentima.rs/rdf/database/predicate/ime> \""+ ime +"\" ; \n" +
                "  <http://www.serviszaradsapatentima.rs/rdf/database/predicate/prezime> \"" + prezime + "\" . " +

                "}";
    }


}

