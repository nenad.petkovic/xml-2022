package www.serviszaradsapatentima.rs.zigovi.model.zig.dto;

public class MetapodaciUpitDTO {
    /*private Map<String, String> mapaMetapodataka = new HashMap<String, String>();

public Map<String, String> getAddressMap() {
    return mapaMetapodataka;
}

public void setAddressMap(Map<String, String> metadataMap) {
    this.mapaMetapodataka = metadataMap;*/
    private String imePretraga;
    private String prezimePretraga;

    private String emailPretraga;

    public String getEmailPretraga() {
        return emailPretraga;
    }

    public void setEmailPretraga(String emailPretraga) {
        this.emailPretraga = emailPretraga;
    }


    public String getImePretraga() {
        return imePretraga;
    }

    public String getPrezimePretraga() {
        return prezimePretraga;
    }

    public void setImePretraga(String imePretraga) {
        this.imePretraga = imePretraga;
    }

    public void setPrezimePretraga(String prezimePretraga) {
        this.prezimePretraga = prezimePretraga;
    }
}
