package www.serviszaradsapatentima.rs.zigovi.helper;

public class XQueryExpressions {


    //XQUERY I XUPDATE ZA IZVESTAJ

    public static final String X_QUERY_FIND_ALL_ZIG_EXPRESSION = "xquery version \"3.1\";\n" +
            "declare default element namespace \"https://www.serviszaradsapatentima.rs\";\n" +
            "for $x in collection(\"/db/intelektualna_svojina/zigovi\")\n" +
            "return $x";

    public static final String X_UPDATE_REMOVE_ZIG_BY_ID_EXPRESSION =
            "xquery version \"3.1\";\n" +
                    "xmldb:remove('/db/intelektualna_svojina/zigovi', '%s')";


}
