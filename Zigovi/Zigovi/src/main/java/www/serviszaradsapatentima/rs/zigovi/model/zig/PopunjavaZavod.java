
package www.serviszaradsapatentima.rs.zigovi.model.zig;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for popunjava_zavod complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="popunjava_zavod">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="primerak_znaka" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="spisak_robe_usluga" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="punomocje" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="generalno_punomocje" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="punomocje_naknadno_dostavljeno" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="akt_o_kolektivnom_zigu" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="dokaz_prava_prvenstva" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="dokaz_uplata_takse" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="stanje">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                 &lt;attribute name="property" type="{http://www.w3.org/2001/XMLSchema}string" default="pred:stanje" />
 *                 &lt;attribute name="datatype" type="{http://www.w3.org/2001/XMLSchema}string" default="xs:string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="vocab" type="{http://www.w3.org/2001/XMLSchema}string" fixed="http://www.serviszaradsapatentima.rs/rdf/database/predicate" />
 *       &lt;attribute name="about" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "popunjava_zavod", propOrder = {
    "primerakZnaka",
    "spisakRobeUsluga",
    "punomocje",
    "generalnoPunomocje",
    "punomocjeNaknadnoDostavljeno",
    "aktOKolektivnomZigu",
    "dokazPravaPrvenstva",
    "dokazUplataTakse",
    "stanje"
})
public class PopunjavaZavod {

    @XmlElement(name = "primerak_znaka")
    protected boolean primerakZnaka;
    @XmlElement(name = "spisak_robe_usluga")
    protected boolean spisakRobeUsluga;
    protected boolean punomocje;
    @XmlElement(name = "generalno_punomocje")
    protected boolean generalnoPunomocje;
    @XmlElement(name = "punomocje_naknadno_dostavljeno")
    protected boolean punomocjeNaknadnoDostavljeno;
    @XmlElement(name = "akt_o_kolektivnom_zigu")
    protected boolean aktOKolektivnomZigu;
    @XmlElement(name = "dokaz_prava_prvenstva")
    protected boolean dokazPravaPrvenstva;
    @XmlElement(name = "dokaz_uplata_takse")
    protected boolean dokazUplataTakse;
    @XmlElement(required = true)
    protected PopunjavaZavod.Stanje stanje;
    @XmlAttribute(name = "vocab")
    protected String vocab;
    @XmlAttribute(name = "about")
    protected String about;

    /**
     * Gets the value of the primerakZnaka property.
     *
     */
    public boolean isPrimerakZnaka() {
        return primerakZnaka;
    }

    /**
     * Sets the value of the primerakZnaka property.
     *
     */
    public void setPrimerakZnaka(boolean value) {
        this.primerakZnaka = value;
    }

    /**
     * Gets the value of the spisakRobeUsluga property.
     *
     */
    public boolean isSpisakRobeUsluga() {
        return spisakRobeUsluga;
    }

    /**
     * Sets the value of the spisakRobeUsluga property.
     *
     */
    public void setSpisakRobeUsluga(boolean value) {
        this.spisakRobeUsluga = value;
    }

    /**
     * Gets the value of the punomocje property.
     *
     */
    public boolean isPunomocje() {
        return punomocje;
    }

    /**
     * Sets the value of the punomocje property.
     *
     */
    public void setPunomocje(boolean value) {
        this.punomocje = value;
    }

    /**
     * Gets the value of the generalnoPunomocje property.
     *
     */
    public boolean isGeneralnoPunomocje() {
        return generalnoPunomocje;
    }

    /**
     * Sets the value of the generalnoPunomocje property.
     *
     */
    public void setGeneralnoPunomocje(boolean value) {
        this.generalnoPunomocje = value;
    }

    /**
     * Gets the value of the punomocjeNaknadnoDostavljeno property.
     *
     */
    public boolean isPunomocjeNaknadnoDostavljeno() {
        return punomocjeNaknadnoDostavljeno;
    }

    /**
     * Sets the value of the punomocjeNaknadnoDostavljeno property.
     *
     */
    public void setPunomocjeNaknadnoDostavljeno(boolean value) {
        this.punomocjeNaknadnoDostavljeno = value;
    }

    /**
     * Gets the value of the aktOKolektivnomZigu property.
     *
     */
    public boolean isAktOKolektivnomZigu() {
        return aktOKolektivnomZigu;
    }

    /**
     * Sets the value of the aktOKolektivnomZigu property.
     *
     */
    public void setAktOKolektivnomZigu(boolean value) {
        this.aktOKolektivnomZigu = value;
    }

    /**
     * Gets the value of the dokazPravaPrvenstva property.
     *
     */
    public boolean isDokazPravaPrvenstva() {
        return dokazPravaPrvenstva;
    }

    /**
     * Sets the value of the dokazPravaPrvenstva property.
     *
     */
    public void setDokazPravaPrvenstva(boolean value) {
        this.dokazPravaPrvenstva = value;
    }

    /**
     * Gets the value of the dokazUplataTakse property.
     *
     */
    public boolean isDokazUplataTakse() {
        return dokazUplataTakse;
    }

    /**
     * Sets the value of the dokazUplataTakse property.
     *
     */
    public void setDokazUplataTakse(boolean value) {
        this.dokazUplataTakse = value;
    }

    /**
     * Gets the value of the stanje property.
     *
     * @return
     *     possible object is
     *     {@link PopunjavaZavod.Stanje }
     *
     */
    public PopunjavaZavod.Stanje getStanje() {
        return stanje;
    }

    /**
     * Sets the value of the stanje property.
     *
     * @param value
     *     allowed object is
     *     {@link PopunjavaZavod.Stanje }
     *
     */
    public void setStanje(PopunjavaZavod.Stanje value) {
        this.stanje = value;
    }

    /**
     * Gets the value of the vocab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVocab() {
        if (vocab == null) {
            return "http://www.serviszaradsapatentima.rs/rdf/database/predicate";
        } else {
            return vocab;
        }
    }

    /**
     * Sets the value of the vocab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVocab(String value) {
        this.vocab = value;
    }

    /**
     * Gets the value of the about property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAbout() {
        return about;
    }

    /**
     * Sets the value of the about property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAbout(String value) {
        this.about = value;
    }

    @Override
    public String toString() {
        return "PopunjavaZavod{" +
                "primerakZnaka=" + primerakZnaka +
                ", spisakRobeUsluga=" + spisakRobeUsluga +
                ", punomocje=" + punomocje +
                ", generalnoPunomocje=" + generalnoPunomocje +
                ", punomocjeNaknadnoDostavljeno=" + punomocjeNaknadnoDostavljeno +
                ", aktOKolektivnomZigu=" + aktOKolektivnomZigu +
                ", dokazPravaPrvenstva=" + dokazPravaPrvenstva +
                ", dokazUplataTakse=" + dokazUplataTakse +
                ", stanje=" + stanje +
                ", vocab='" + vocab + '\'' +
                ", about='" + about + '\'' +
                '}';
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *       &lt;attribute name="property" type="{http://www.w3.org/2001/XMLSchema}string" default="pred:stanje" />
     *       &lt;attribute name="datatype" type="{http://www.w3.org/2001/XMLSchema}string" default="xs:string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Stanje {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "property")
        protected String property;
        @XmlAttribute(name = "datatype")
        protected String datatype;

        @Override
        public String toString() {
            return "Stanje{" +
                    "value='" + value + '\'' +
                    ", property='" + property + '\'' +
                    ", datatype='" + datatype + '\'' +
                    '}';
        }

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Gets the value of the property property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProperty() {
            if (property == null) {
                return "pred:stanje";
            } else {
                return property;
            }
        }

        /**
         * Sets the value of the property property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProperty(String value) {
            this.property = value;
        }

        /**
         * Gets the value of the datatype property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDatatype() {
            if (datatype == null) {
                return "xs:string";
            } else {
                return datatype;
            }
        }

        /**
         * Sets the value of the datatype property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDatatype(String value) {
            this.datatype = value;
        }

    }

}
