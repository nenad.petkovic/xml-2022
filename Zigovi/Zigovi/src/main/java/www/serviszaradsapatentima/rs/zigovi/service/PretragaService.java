package www.serviszaradsapatentima.rs.zigovi.service;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xmldb.api.base.XMLDBException;
import www.serviszaradsapatentima.rs.zigovi.exception.EntityNotFoundException;
import www.serviszaradsapatentima.rs.zigovi.exception.InvalidXmlDatabaseException;
import www.serviszaradsapatentima.rs.zigovi.exception.InvalidXmlException;
import www.serviszaradsapatentima.rs.zigovi.exception.XmlDatabaseException;
import www.serviszaradsapatentima.rs.zigovi.fuseki.SparqlService;
import www.serviszaradsapatentima.rs.zigovi.helper.UUIDHelper;
import www.serviszaradsapatentima.rs.zigovi.helper.XmlConversionAgent;
import www.serviszaradsapatentima.rs.zigovi.model.zig.Zig;
import www.serviszaradsapatentima.rs.zigovi.model.zig.dto.MetapodaciUpitDTO;
import www.serviszaradsapatentima.rs.zigovi.repository.AbstractXmlRepository;
import www.serviszaradsapatentima.rs.zigovi.transformation.XSLTransformer;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static www.serviszaradsapatentima.rs.zigovi.helper.XQueryExpressions.X_QUERY_FIND_ALL_ZIG_EXPRESSION;
import static www.serviszaradsapatentima.rs.zigovi.helper.XQueryExpressions.X_UPDATE_REMOVE_ZIG_BY_ID_EXPRESSION;

@Service
public class PretragaService {
    @Autowired
    SparqlService sparqlService;


    public ArrayList<String> searchMetadataImePrezime(MetapodaciUpitDTO metapodaci) throws IOException {
        //vracamo metapodatke iz hash mape
        //kljuc >> ime
        System.out.println(metapodaci.getImePretraga());
        String imeZaPretragu = metapodaci.getImePretraga();
        //kljuc >> prezime
        String prezimeZaPretragu = metapodaci.getPrezimePretraga();
        System.out.println("Prosledjeni podaci za pretragu po imenu i prezimenu su: " + imeZaPretragu + " "+ prezimeZaPretragu);

        //pozivamo metodu sparql servisa za pretragu podataka
        List<SparqlService.SparqlQueryResult> rezultati = sparqlService.getAllPretragaMetapodaciPoImenuIPrezimenu(imeZaPretragu, prezimeZaPretragu );
        System.out.println(rezultati);

        ArrayList<String> listaIdPronadjenihDokumenata = new ArrayList<>();

        for (SparqlService.SparqlQueryResult res : rezultati){
            System.out.println(res.getVarName());
            System.out.println(res.getVarValue());

            //parsiranje tako da dobijemo samo id pronadjenog dokumenta
            String vrednost = res.getVarValue().toString();
            String izdeljeno[] = vrednost.split("/");
            String id = izdeljeno[izdeljeno.length-1]; //uzimamo poslednju vrednost kao vrednost id
            System.out.println("Ovo je vrednost id dokumenta: " + id);
            listaIdPronadjenihDokumenata.add(id);
            System.out.println("-----------------------------------");
        }
        //vrati osnovne informacije za dokument sa tim id-ijem
        return listaIdPronadjenihDokumenata;
    }

    public ArrayList<String> searchMetadataEmail(MetapodaciUpitDTO metapodaci) throws IOException {

        String email = metapodaci.getEmailPretraga();
        System.out.println("Prosledjeni metapodaci za pretragu po emailu posiljaoca zahteva: " + email);

        //pozivamo metodu sparql servisa za pretragu podataka
        List<SparqlService.SparqlQueryResult> rezultati = sparqlService.getAllPretragaMetapodatakaPoEmailu(email );
        System.out.println(rezultati);

        ArrayList<String> listaIdPronadjenihDokumenata = new ArrayList<>();

        for (SparqlService.SparqlQueryResult res : rezultati){
            System.out.println(res.getVarName());
            System.out.println(res.getVarValue());

            //parsiranje tako da dobijemo samo id pronadjenog dokumenta
            String vrednost = res.getVarValue().toString();
            String izdeljeno[] = vrednost.split("/");
            String id = izdeljeno[izdeljeno.length-1]; //uzimamo poslednju vrednost kao vrednost id
            System.out.println("Ovo je vrednost id dokumenta: " + id);
            listaIdPronadjenihDokumenata.add(id);
            System.out.println("-----------------------------------");
        }
        return listaIdPronadjenihDokumenata;
    }

    /*********** servis za pretragu po svim metapodacima od jednom or pretraga samo za one koje zelimo *************/
    //svi uslovi u pretrazi moraju biti zadovoljeni >> vraca uniju svih rezultata
    public ArrayList<String> searchMetadataOR(MetapodaciUpitDTO metapodaci) throws IOException {
        //vracamo metapodatke iz hash mape
        //kljuc >> ime
        System.out.println(metapodaci.getImePretraga());
        String imeZaPretragu = metapodaci.getImePretraga();
        //kljuc >> prezime
        String prezimeZaPretragu = metapodaci.getPrezimePretraga();

        //email
        String email = metapodaci.getEmailPretraga();

        //pozivamo metodu sparql servisa za pretragu podataka

        //pretraga po imenu i prezimenu ako je uneseno
        List<SparqlService.SparqlQueryResult> rezultatiImePrezime = null;
        List<SparqlService.SparqlQueryResult> rezultatiPoslovnoIme = null;
        List<SparqlService.SparqlQueryResult> rezultatiEmail = null;
        List<SparqlService.SparqlQueryResult> rezultatiNaslov = null;
        List<SparqlService.SparqlQueryResult> rezultatiAlternativniNaslov = null;

        //zajednicka pretraga po svim podacima
        if(!(imeZaPretragu.equals("") && prezimeZaPretragu.equals(""))){
            rezultatiImePrezime = sparqlService.getAllPretragaMetapodaciPoImenuIPrezimenu(imeZaPretragu, prezimeZaPretragu);
        }
        if(!email.equals("")){
            rezultatiEmail = sparqlService.getAllPretragaMetapodatakaPoEmailu(email);
        }

        //radimo uniju svih dobijenih id-podataka
        ArrayList<String> konacniRezultatiId = new ArrayList<>();

        //iteriramo kroz svaku listu i parsiramo id-ijeve ako nisu u konacnoj dodajemo ih kao rezultat
        if (rezultatiImePrezime != null){
            List<String> rezultatiImePrezimeStringId = parsirajListuIdDokumenata(rezultatiImePrezime);

            //prolazimo kroz sve liste i ako im id-ijevi nisu u konacnom rezultatu dodajemo ih
            for(String id : rezultatiImePrezimeStringId){
                if (!konacniRezultatiId.contains(id)) {
                    konacniRezultatiId.add(id);
                }
            }
        }
        if(rezultatiPoslovnoIme != null){
            List<String> rezutatiPoslovnoImeStringId = parsirajListuIdDokumenata(rezultatiPoslovnoIme);
            //prolazimo kroz sve liste i ako im id-ijevi nisu u konacnom rezultatu dodajemo ih
            for(String id : rezutatiPoslovnoImeStringId){
                if (!konacniRezultatiId.contains(id)) {
                    konacniRezultatiId.add(id);
                }
            }
        }
        if(rezultatiEmail != null){
            List<String> emailStringId = parsirajListuIdDokumenata(rezultatiEmail);

            for(String id : emailStringId){
                if (!konacniRezultatiId.contains(id)) {
                    konacniRezultatiId.add(id);
                }
            }

        }
        if(rezultatiNaslov != null){
            List<String> rezultatiNaslovStringId = parsirajListuIdDokumenata(rezultatiNaslov);

            for(String id : rezultatiNaslovStringId){
                if (!konacniRezultatiId.contains(id)) {
                    konacniRezultatiId.add(id);
                }
            }
        }
        if(rezultatiAlternativniNaslov != null){
            List<String> alternativniStringId = parsirajListuIdDokumenata(rezultatiAlternativniNaslov);

            for(String id : alternativniStringId){
                if (!konacniRezultatiId.contains(id)) {
                    konacniRezultatiId.add(id);
                }
            }

        }
        return konacniRezultatiId;
    }


    //***************** PRETRAGA PO SVIM METAPODACIMA >> AND ***************/
    //svi podaci moraju biti uneseni
    public ArrayList<String> searchMetadataAND(MetapodaciUpitDTO metapodaci) throws IOException {
        //kljuc >> ime
        System.out.println(metapodaci.getImePretraga());
        String imeZaPretragu = metapodaci.getImePretraga();
        //kljuc >> prezime
        String prezimeZaPretragu = metapodaci.getPrezimePretraga();

        //email
        String email = metapodaci.getEmailPretraga();
        System.out.println("Email je: " + email);

        //pozivamo metodu sparql servisa za pretragu podataka

        List<SparqlService.SparqlQueryResult> rezultati = sparqlService.getAllAndPretragaPoSvimParametrima(imeZaPretragu, prezimeZaPretragu, email);

        ArrayList<String> parsiraniIdRezultati = parsirajListuIdDokumenata(rezultati);

        return parsiraniIdRezultati;
    }



    ArrayList<String> parsirajListuIdDokumenata(List<SparqlService.SparqlQueryResult> rezultati){
        ArrayList<String> listaIdPronadjenihDokumenata = new ArrayList<>();

        for (SparqlService.SparqlQueryResult res : rezultati){
            System.out.println(res.getVarName());
            System.out.println(res.getVarValue());

            //parsiranje tako da dobijemo samo id pronadjenog dokumenta
            String vrednost = res.getVarValue().toString();
            String izdeljeno[] = vrednost.split("/");
            String id = izdeljeno[izdeljeno.length-1]; //uzimamo poslednju vrednost kao vrednost id
            System.out.println("Ovo je vrednost id dokumenta: " + id);
            listaIdPronadjenihDokumenata.add(id);
            System.out.println("-----------------------------------");
        }
        return  listaIdPronadjenihDokumenata;
    }
}
