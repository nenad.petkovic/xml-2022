
package www.serviszaradsapatentima.rs.zigovi.model.zig;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for placanje_takse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="placanje_takse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="osnovna_taksa" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="za_klasa">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="broj_klasa" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="dinara" type="{http://www.w3.org/2001/XMLSchema}float" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="graficko_resenje" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="ukupno" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "placanje_takse", propOrder = {
    "osnovnaTaksa",
    "zaKlasa",
    "grafickoResenje",
    "ukupno"
})
public class PlacanjeTakse {

    @XmlElement(name = "osnovna_taksa")
    protected float osnovnaTaksa;
    @XmlElement(name = "za_klasa", required = true)
    protected PlacanjeTakse.ZaKlasa zaKlasa;
    @XmlElement(name = "graficko_resenje")
    protected float grafickoResenje;
    protected float ukupno;

    @Override
    public String toString() {
        return "PlacanjeTakse{" +
                "osnovnaTaksa=" + osnovnaTaksa +
                ", zaKlasa=" + zaKlasa +
                ", grafickoResenje=" + grafickoResenje +
                ", ukupno=" + ukupno +
                '}';
    }

    /**
     * Gets the value of the osnovnaTaksa property.
     *
     */
    public float getOsnovnaTaksa() {
        return osnovnaTaksa;
    }

    /**
     * Sets the value of the osnovnaTaksa property.
     *
     */
    public void setOsnovnaTaksa(float value) {
        this.osnovnaTaksa = value;
    }

    /**
     * Gets the value of the zaKlasa property.
     *
     * @return
     *     possible object is
     *     {@link PlacanjeTakse.ZaKlasa }
     *
     */
    public PlacanjeTakse.ZaKlasa getZaKlasa() {
        return zaKlasa;
    }

    /**
     * Sets the value of the zaKlasa property.
     *
     * @param value
     *     allowed object is
     *     {@link PlacanjeTakse.ZaKlasa }
     *
     */
    public void setZaKlasa(PlacanjeTakse.ZaKlasa value) {
        this.zaKlasa = value;
    }

    /**
     * Gets the value of the grafickoResenje property.
     * 
     */
    public float getGrafickoResenje() {
        return grafickoResenje;
    }

    /**
     * Sets the value of the grafickoResenje property.
     * 
     */
    public void setGrafickoResenje(float value) {
        this.grafickoResenje = value;
    }

    /**
     * Gets the value of the ukupno property.
     * 
     */
    public float getUkupno() {
        return ukupno;
    }

    /**
     * Sets the value of the ukupno property.
     * 
     */
    public void setUkupno(float value) {
        this.ukupno = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="broj_klasa" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="dinara" type="{http://www.w3.org/2001/XMLSchema}float" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ZaKlasa {
        @Override
        public String toString() {
            return "ZaKlasa{" +
                    "brojKlasa=" + brojKlasa +
                    ", dinara=" + dinara +
                    '}';
        }

        @XmlAttribute(name = "broj_klasa")
        protected Integer brojKlasa;
        @XmlAttribute(name = "dinara")
        protected Float dinara;

        /**
         * Gets the value of the brojKlasa property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getBrojKlasa() {
            return brojKlasa;
        }

        /**
         * Sets the value of the brojKlasa property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setBrojKlasa(Integer value) {
            this.brojKlasa = value;
        }

        /**
         * Gets the value of the dinara property.
         * 
         * @return
         *     possible object is
         *     {@link Float }
         *     
         */
        public Float getDinara() {
            return dinara;
        }

        /**
         * Sets the value of the dinara property.
         * 
         * @param value
         *     allowed object is
         *     {@link Float }
         *     
         */
        public void setDinara(Float value) {
            this.dinara = value;
        }

    }

}
