package www.serviszaradsapatentima.rs.zigovi.controller;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.xml.sax.SAXException;
import www.serviszaradsapatentima.rs.zigovi.model.zig.KolekcijaZigova;
import www.serviszaradsapatentima.rs.zigovi.model.zig.Zig;
import www.serviszaradsapatentima.rs.zigovi.service.RDFService;
import www.serviszaradsapatentima.rs.zigovi.service.ZigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@Controller
@RequestMapping(value = "api/zig")
public class ZigController {
    @Autowired
    ZigService zigService;

    @Autowired
    RDFService rdfService;

    @GetMapping(produces = MediaType.APPLICATION_XML_VALUE)
    ResponseEntity<KolekcijaZigova> findAll() {
        KolekcijaZigova kolekcijaZigova = new KolekcijaZigova();
        kolekcijaZigova.setZigovi(this.zigService.findAll());
        return new ResponseEntity<>(kolekcijaZigova, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    ResponseEntity<Zig> findOne(@PathVariable("id") String id) {
        return new ResponseEntity<>(this.zigService.findById(id), HttpStatus.OK);
    }

    @PostMapping(produces = MediaType.APPLICATION_XML_VALUE)
    ResponseEntity<Zig> create(@RequestBody String body) {
        return new ResponseEntity<>(this.zigService.create(body), HttpStatus.OK);
    }

    @PutMapping(produces = MediaType.APPLICATION_XML_VALUE)
    ResponseEntity<Zig> update(@RequestBody String body) {
        return new ResponseEntity<>(this.zigService.update(body), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    ResponseEntity<Void> delete(@PathVariable("id") String id) {
        this.zigService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/metadata/rdf/{dokumentId}")
    ResponseEntity<InputStreamResource> getMetadataRdf(@PathVariable String dokumentId) {
        ByteArrayInputStream is;
        try {
            String xmlEntity = this.zigService.getRdfaString(dokumentId);
            System.out.println(xmlEntity);
            is = new ByteArrayInputStream(this.rdfService.getRDFAsRDF(xmlEntity).getBytes());
        }
        catch (IOException | SAXException | JAXBException | TransformerException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline: filename=zig-metadata.rdf");

        return new ResponseEntity<>(new InputStreamResource(is), headers, HttpStatus.OK);
    }

    @GetMapping(value = "/metadata/json/{dokumentId}")
    ResponseEntity<InputStreamResource> getMetadataJson(@PathVariable String dokumentId) {
        ByteArrayInputStream is;
        try {
            String xmlEntity = this.zigService.getRdfaString(dokumentId);
            System.out.println(xmlEntity);
            is = new ByteArrayInputStream(this.rdfService.getRDFAsJSON(xmlEntity).getBytes());
        }
        catch (IOException | SAXException | JAXBException | TransformerException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline: filename=zig-metadata.json");

        return new ResponseEntity<>(new InputStreamResource(is), headers, HttpStatus.OK);
    }

    @GetMapping(value = "/pdf/{dokumentId}", produces = MediaType.APPLICATION_PDF_VALUE)
    ResponseEntity<InputStreamResource> getPdf(@PathVariable String dokumentId) throws Exception {
        ByteArrayInputStream pdf = this.zigService.generatePDF(dokumentId);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline: filename=zig.pdf");
        return new ResponseEntity<>(new InputStreamResource(pdf), headers, HttpStatus.OK);
    }

    @GetMapping(value = "/html/{dokumentId}", produces = "text/html;charset=UTF-8")
    ResponseEntity<InputStreamResource> getHtml(@PathVariable String dokumentId) throws Exception {
        ByteArrayInputStream html = this.zigService.generateHtml(dokumentId);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline: filename=zig.html");
        return new ResponseEntity<>(new InputStreamResource(html), headers, HttpStatus.OK);
    }
}
