package www.serviszaradsapatentima.rs.zigovi.fuseki;

import www.serviszaradsapatentima.rs.zigovi.fuseki.util.SparqlAuthenticationUtilities;
import www.serviszaradsapatentima.rs.zigovi.fuseki.util.SparqlUtil;
import www.serviszaradsapatentima.rs.zigovi.helper.RDFDBConnectionProperties;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.RDFNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class SparqlService {
    private static final String SPARQL_NAMED_GRAPH_URI = "/data/sparql/metadata";
    public static SparqlAuthenticationUtilities.ConnectionProperties conn;

    @Autowired
    private RDFDBConnectionProperties rdfdbConnectionProperties;

    //***** pretraga po metapodacima >> po imenu i prezimenu fizickog lica ****/
    public List<SparqlQueryResult> getAllPretragaMetapodaciPoImenuIPrezimenu(String imePretraga, String prezimePretraga) throws IOException {
        System.out.println("[INFO] Dobavljanje podataka o podnetim dokumentima korisnika sa imenom " + imePretraga + " i prezimenom " + prezimePretraga + " iz RDF stora");
        System.out.println("[INFO] Koriscenjem grafa " + SPARQL_NAMED_GRAPH_URI);

        String sparqlUpit = SparqlUtil.selectPoImenuIPrezimenu(imePretraga, prezimePretraga, rdfdbConnectionProperties.getDataEndpoint());
        System.out.println(sparqlUpit);
        System.out.println("Endpoint je: "  + rdfdbConnectionProperties.getDataEndpoint());


        return getResults(sparqlUpit); //vracanje rezultata za taj upit
    }


    //***** pretraga po metapodacima >> po imenu i prezimenu fizickog lica ****/
    public List<SparqlQueryResult> getAllPretragaMetapodatakaPoEmailu(String emailPretraga) throws IOException {
        System.out.println("[INFO] Dobavljanje podataka o podnetim dokumentima korisnika sa email-om: " + emailPretraga + " iz RDF stora");
        System.out.println("[INFO] Koriscenjem grafa " + SPARQL_NAMED_GRAPH_URI);

        String sparqlUpit = SparqlUtil.selectPoEmailu(emailPretraga, rdfdbConnectionProperties.getDataEndpoint());
        System.out.println(sparqlUpit);
        System.out.println("Endpoint je: "  + rdfdbConnectionProperties.getDataEndpoint());


        return getResults(sparqlUpit); //vracanje rezultata za taj upit
    }



    /********** AND pretraga *********/
    public List<SparqlQueryResult> getAllAndPretragaPoSvimParametrima(String ime, String prezime, String email) throws IOException {
        System.out.println("[INFO] Dobavljanje podataka o podnetim dokumentima sa prosledjenim podacima " + " iz RDF stora");
        System.out.println("[INFO] Koriscenjem grafa " + SPARQL_NAMED_GRAPH_URI);

        System.out.println("Ime je: " + ime);
        System.out.println("Prezime je: " + prezime);
        System.out.println("Email je: " + email);

        String sparqlUpit = SparqlUtil.selectANDFizickaLica(ime, prezime, email, rdfdbConnectionProperties.getDataEndpoint());
        System.out.println(sparqlUpit);
        System.out.println("Endpoint je: "  + rdfdbConnectionProperties.getDataEndpoint());


        return getResults(sparqlUpit); //vracanje rezultata za taj upit
    }
    /***************************** KRAJ *********************/














    // parsiranje rezultata
    public List<SparqlQueryResult> getResults(String queryString) throws IOException {

        // Create a QueryExecution that will access a SPARQL service over HTTP
        QueryExecution query = QueryExecutionFactory.sparqlService(this.rdfdbConnectionProperties.getQueryEndpoint(), queryString);
        // Query the SPARQL endpoint, iterate over the result set...
        ResultSet results = query.execSelect();

        String varName;
        RDFNode varValue;
        List<SparqlQueryResult> retval = new ArrayList<>();

        System.out.println("******************* Ispis rezultata: ");
        while (results.hasNext()) {

            // A single answer from a SELECT query
            QuerySolution querySolution = results.next();
            Iterator<String> variableBindings = querySolution.varNames();

            // Retrieve variable bindings
            while (variableBindings.hasNext()) {

                varName = variableBindings.next();
                varValue = querySolution.get(varName);

                System.out.println(varName + ": " + varValue);

                SparqlQueryResult sparqlQueryResult = new SparqlQueryResult();
                sparqlQueryResult.setVarName(varName);
                sparqlQueryResult.setVarValue(varValue);
                retval.add(sparqlQueryResult);
            }
        }
        query.close();
        return retval;
    }



    public static class SparqlQueryResult {
        private String varName;

        private RDFNode varValue;

        public String getVarName() {
            return varName;
        }

        public void setVarName(String varName) {
            this.varName = varName;
        }

        public RDFNode getVarValue() {
            return varValue;
        }

        public void setVarValue(RDFNode varValue) {
            this.varValue = varValue;
        }
    }
}
