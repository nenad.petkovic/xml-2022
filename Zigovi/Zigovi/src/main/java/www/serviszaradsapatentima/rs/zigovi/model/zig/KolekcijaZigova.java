package www.serviszaradsapatentima.rs.zigovi.model.zig;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"zigovi"})
@XmlRootElement(name = "kolekcija-zigova")
public class KolekcijaZigova {
    @XmlElement(name = "zig")
    protected List<Zig> zigovi;

    public List<Zig> getZigovi() {
        if (zigovi == null) {
            zigovi = new ArrayList<>();
        }
        return zigovi;
    }

    public void setZigovi(List<Zig> zigovi) {
        this.zigovi = zigovi;
    }

    @Override
    public String toString() {
        return "KolekcijaZigova{" +
                "zigovi=" + zigovi +
                '}';
    }
}

