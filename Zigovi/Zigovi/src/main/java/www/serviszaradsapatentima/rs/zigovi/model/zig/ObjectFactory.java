
package www.serviszaradsapatentima.rs.zigovi.model.zig;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the zig package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: zig
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PopunjavaZavod }
     * 
     */
    public PopunjavaZavod createPopunjavaZavod() {
        return new PopunjavaZavod();
    }

    /**
     * Create an instance of {@link PlacanjeTakse }
     * 
     */
    public PlacanjeTakse createPlacanjeTakse() {
        return new PlacanjeTakse();
    }

    /**
     * Create an instance of {@link OdnosiZa }
     * 
     */
    public OdnosiZa createOdnosiZa() {
        return new OdnosiZa();
    }

    /**
     * Create an instance of {@link PodnosilacPrijave }
     * 
     */
    public PodnosilacPrijave createPodnosilacPrijave() {
        return new PodnosilacPrijave();
    }

    /**
     * Create an instance of {@link Zig }
     *
     */
    public Zig createZig() {
        return new Zig();
    }

    /**
     * Create an instance of {@link NicanskaSkala }
     * 
     */
    public NicanskaSkala createNicanskaSkala() {
        return new NicanskaSkala();
    }

    /**
     * Create an instance of {@link PopunjavaZavod.Stanje }
     * 
     */
    public PopunjavaZavod.Stanje createPopunjavaZavodStanje() {
        return new PopunjavaZavod.Stanje();
    }

    /**
     * Create an instance of {@link PlacanjeTakse.ZaKlasa }
     * 
     */
    public PlacanjeTakse.ZaKlasa createPlacanjeTakseZaKlasa() {
        return new PlacanjeTakse.ZaKlasa();
    }

    /**
     * Create an instance of {@link OdnosiZa.A }
     * 
     */
    public OdnosiZa.A createOdnosiZaA() {
        return new OdnosiZa.A();
    }

    /**
     * Create an instance of {@link OdnosiZa.B }
     * 
     */
    public OdnosiZa.B createOdnosiZaB() {
        return new OdnosiZa.B();
    }

    /**
     * Create an instance of {@link OdnosiZa.V }
     * 
     */
    public OdnosiZa.V createOdnosiZaV() {
        return new OdnosiZa.V();
    }

    /**
     * Create an instance of {@link PodnosilacPrijave.Ime }
     * 
     */
    public PodnosilacPrijave.Ime createPodnosilacPrijaveIme() {
        return new PodnosilacPrijave.Ime();
    }

    /**
     * Create an instance of {@link PodnosilacPrijave.Prezime }
     * 
     */
    public PodnosilacPrijave.Prezime createPodnosilacPrijavePrezime() {
        return new PodnosilacPrijave.Prezime();
    }

    /**
     * Create an instance of {@link PodnosilacPrijave.Ulica }
     * 
     */
    public PodnosilacPrijave.Ulica createPodnosilacPrijaveUlica() {
        return new PodnosilacPrijave.Ulica();
    }

    /**
     * Create an instance of {@link PodnosilacPrijave.Broj }
     * 
     */
    public PodnosilacPrijave.Broj createPodnosilacPrijaveBroj() {
        return new PodnosilacPrijave.Broj();
    }

    /**
     * Create an instance of {@link PodnosilacPrijave.PostanskiBroj }
     * 
     */
    public PodnosilacPrijave.PostanskiBroj createPodnosilacPrijavePostanskiBroj() {
        return new PodnosilacPrijave.PostanskiBroj();
    }

    /**
     * Create an instance of {@link PodnosilacPrijave.Mesto }
     * 
     */
    public PodnosilacPrijave.Mesto createPodnosilacPrijaveMesto() {
        return new PodnosilacPrijave.Mesto();
    }

    /**
     * Create an instance of {@link PodnosilacPrijave.Drzava }
     * 
     */
    public PodnosilacPrijave.Drzava createPodnosilacPrijaveDrzava() {
        return new PodnosilacPrijave.Drzava();
    }

    /**
     * Create an instance of {@link PodnosilacPrijave.Telefon }
     * 
     */
    public PodnosilacPrijave.Telefon createPodnosilacPrijaveTelefon() {
        return new PodnosilacPrijave.Telefon();
    }

    /**
     * Create an instance of {@link PodnosilacPrijave.Email }
     * 
     */
    public PodnosilacPrijave.Email createPodnosilacPrijaveEmail() {
        return new PodnosilacPrijave.Email();
    }

    /**
     * Create an instance of {@link PodnosilacPrijave.Faks }
     * 
     */
    public PodnosilacPrijave.Faks createPodnosilacPrijaveFaks() {
        return new PodnosilacPrijave.Faks();
    }

}
