
package www.serviszaradsapatentima.rs.zigovi.model.zig;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for nicanska_skala complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nicanska_skala">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="zaokruzeno" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nicanska_skala", propOrder = {
    "zaokruzeno"
})
public class NicanskaSkala {

    @XmlElement(type = Integer.class)
    protected List<Integer> zaokruzeno;

    @Override
    public String toString() {
        return "NicanskaSkala{" +
                "zaokruzeno=" + zaokruzeno +
                '}';
    }

    /**
     * Gets the value of the zaokruzeno property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zaokruzeno property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZaokruzeno().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getZaokruzeno() {
        if (zaokruzeno == null) {
            zaokruzeno = new ArrayList<Integer>();
        }
        return this.zaokruzeno;
    }

}
