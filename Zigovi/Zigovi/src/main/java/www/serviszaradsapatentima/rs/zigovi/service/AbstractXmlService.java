package www.serviszaradsapatentima.rs.zigovi.service;

import org.w3c.dom.Document;

import javax.xml.bind.JAXBException;
import java.util.List;

public interface AbstractXmlService<T> {

    List<T> findAll();

    T findById(String entityId);

    T create(String entityXml);

    T update(String entityXml);

    //Document getDocument(Long entityId);

    boolean deleteById(String entityId);

    String getRdfaString(String brojPrijave) throws JAXBException;
}

