package www.serviszaradsapatentima.rs.zigovi.interfaces;

import javax.xml.datatype.XMLGregorianCalendar;

public interface Identifiable {

    String getBrojPrijave();

}
