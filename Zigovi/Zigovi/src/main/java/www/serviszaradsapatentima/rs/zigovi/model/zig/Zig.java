
package www.serviszaradsapatentima.rs.zigovi.model.zig;

import www.serviszaradsapatentima.rs.zigovi.interfaces.Identifiable;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Arrays;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="podnosilac_prijave" type="{https://www.serviszaradsapatentima.rs/zig}podnosilac_prijave"/>
 *         &lt;element name="punomocnik" type="{https://www.serviszaradsapatentima.rs/zig}podnosilac_prijave"/>
 *         &lt;element name="predstavnik" type="{https://www.serviszaradsapatentima.rs/zig}podnosilac_prijave"/>
 *         &lt;element name="odnosi_za" type="{https://www.serviszaradsapatentima.rs/zig}odnosi_za"/>
 *         &lt;element name="boje" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transliteracija_znaka" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="prevod_znaka" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="opis_znaka" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nicanska_skala" type="{https://www.serviszaradsapatentima.rs/zig}nicanska_skala"/>
 *         &lt;element name="prvanstvo_i_osnov" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="placanje_takse" type="{https://www.serviszaradsapatentima.rs/zig}placanje_takse"/>
 *         &lt;element name="potpis_podnosioca" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="popunjava_zavod" type="{https://www.serviszaradsapatentima.rs/zig}popunjava_zavod"/>
 *       &lt;/sequence>
 *       &lt;attribute name="broj_prijave" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *       &lt;attribute name="datum" type="{http://www.w3.org/2001/XMLSchema}date" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "podnosilacPrijave",
    "punomocnik",
    "predstavnik",
    "odnosiZa",
    "boje",
    "transliteracijaZnaka",
    "prevodZnaka",
    "opisZnaka",
    "nicanskaSkala",
    "prvanstvoIOsnov",
    "placanjeTakse",
    "potpisPodnosioca",
    "popunjavaZavod"
})
@XmlRootElement(name = "zig")
public class Zig implements Identifiable {

    @XmlElement(name = "podnosilac_prijave", required = true)
    protected PodnosilacPrijave podnosilacPrijave;
    @XmlElement(required = true)
    protected PodnosilacPrijave punomocnik;
    @XmlElement(required = true, nillable = true)
    protected PodnosilacPrijave predstavnik;
    @XmlElement(name = "odnosi_za", required = true)
    protected OdnosiZa odnosiZa;
    @XmlElement(required = true)
    protected String boje;
    @XmlElement(name = "transliteracija_znaka", required = true, nillable = true)
    protected String transliteracijaZnaka;
    @XmlElement(name = "prevod_znaka", required = true, nillable = true)
    protected String prevodZnaka;
    @XmlElement(name = "opis_znaka", required = true)
    protected String opisZnaka;
    @XmlElement(name = "nicanska_skala", required = true)
    protected NicanskaSkala nicanskaSkala;
    @XmlElement(name = "prvanstvo_i_osnov", required = true)
    protected String prvanstvoIOsnov;
    @XmlElement(name = "placanje_takse", required = true)
    protected PlacanjeTakse placanjeTakse;
    @XmlElement(name = "potpis_podnosioca", required = true, nillable = true)
    protected byte[] potpisPodnosioca;
    @XmlElement(name = "popunjava_zavod", required = true)
    protected PopunjavaZavod popunjavaZavod;
    @XmlAttribute(name = "broj_prijave")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String brojPrijave;
    @XmlAttribute(name = "datum")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datum;

    @Override
    public String toString() {
        return "Zig{" +
                "podnosilacPrijave=" + podnosilacPrijave +
                ", punomocnik=" + punomocnik +
                ", predstavnik=" + predstavnik +
                ", odnosiZa=" + odnosiZa +
                ", boje='" + boje + '\'' +
                ", transliteracijaZnaka='" + transliteracijaZnaka + '\'' +
                ", prevodZnaka='" + prevodZnaka + '\'' +
                ", opisZnaka='" + opisZnaka + '\'' +
                ", nicanskaSkala=" + nicanskaSkala +
                ", prvanstvoIOsnov='" + prvanstvoIOsnov + '\'' +
                ", placanjeTakse=" + placanjeTakse +
                ", potpisPodnosioca=" + Arrays.toString(potpisPodnosioca) +
                ", popunjavaZavod=" + popunjavaZavod +
                ", brojPrijave='" + brojPrijave + '\'' +
                ", datum=" + datum +
                '}';
    }

    /**
     * Gets the value of the podnosilacPrijave property.
     * 
     * @return
     *     possible object is
     *     {@link PodnosilacPrijave }
     *     
     */
    public PodnosilacPrijave getPodnosilacPrijave() {
        return podnosilacPrijave;
    }

    /**
     * Sets the value of the podnosilacPrijave property.
     * 
     * @param value
     *     allowed object is
     *     {@link PodnosilacPrijave }
     *     
     */
    public void setPodnosilacPrijave(PodnosilacPrijave value) {
        this.podnosilacPrijave = value;
    }

    /**
     * Gets the value of the punomocnik property.
     * 
     * @return
     *     possible object is
     *     {@link PodnosilacPrijave }
     *     
     */
    public PodnosilacPrijave getPunomocnik() {
        return punomocnik;
    }

    /**
     * Sets the value of the punomocnik property.
     * 
     * @param value
     *     allowed object is
     *     {@link PodnosilacPrijave }
     *     
     */
    public void setPunomocnik(PodnosilacPrijave value) {
        this.punomocnik = value;
    }

    /**
     * Gets the value of the predstavnik property.
     * 
     * @return
     *     possible object is
     *     {@link PodnosilacPrijave }
     *     
     */
    public PodnosilacPrijave getPredstavnik() {
        return predstavnik;
    }

    /**
     * Sets the value of the predstavnik property.
     * 
     * @param value
     *     allowed object is
     *     {@link PodnosilacPrijave }
     *     
     */
    public void setPredstavnik(PodnosilacPrijave value) {
        this.predstavnik = value;
    }

    /**
     * Gets the value of the odnosiZa property.
     * 
     * @return
     *     possible object is
     *     {@link OdnosiZa }
     *     
     */
    public OdnosiZa getOdnosiZa() {
        return odnosiZa;
    }

    /**
     * Sets the value of the odnosiZa property.
     * 
     * @param value
     *     allowed object is
     *     {@link OdnosiZa }
     *     
     */
    public void setOdnosiZa(OdnosiZa value) {
        this.odnosiZa = value;
    }

    /**
     * Gets the value of the boje property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoje() {
        return boje;
    }

    /**
     * Sets the value of the boje property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoje(String value) {
        this.boje = value;
    }

    /**
     * Gets the value of the transliteracijaZnaka property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransliteracijaZnaka() {
        return transliteracijaZnaka;
    }

    /**
     * Sets the value of the transliteracijaZnaka property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransliteracijaZnaka(String value) {
        this.transliteracijaZnaka = value;
    }

    /**
     * Gets the value of the prevodZnaka property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrevodZnaka() {
        return prevodZnaka;
    }

    /**
     * Sets the value of the prevodZnaka property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrevodZnaka(String value) {
        this.prevodZnaka = value;
    }

    /**
     * Gets the value of the opisZnaka property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpisZnaka() {
        return opisZnaka;
    }

    /**
     * Sets the value of the opisZnaka property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpisZnaka(String value) {
        this.opisZnaka = value;
    }

    /**
     * Gets the value of the nicanskaSkala property.
     * 
     * @return
     *     possible object is
     *     {@link NicanskaSkala }
     *     
     */
    public NicanskaSkala getNicanskaSkala() {
        return nicanskaSkala;
    }

    /**
     * Sets the value of the nicanskaSkala property.
     * 
     * @param value
     *     allowed object is
     *     {@link NicanskaSkala }
     *     
     */
    public void setNicanskaSkala(NicanskaSkala value) {
        this.nicanskaSkala = value;
    }

    /**
     * Gets the value of the prvanstvoIOsnov property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrvanstvoIOsnov() {
        return prvanstvoIOsnov;
    }

    /**
     * Sets the value of the prvanstvoIOsnov property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrvanstvoIOsnov(String value) {
        this.prvanstvoIOsnov = value;
    }

    /**
     * Gets the value of the placanjeTakse property.
     * 
     * @return
     *     possible object is
     *     {@link PlacanjeTakse }
     *     
     */
    public PlacanjeTakse getPlacanjeTakse() {
        return placanjeTakse;
    }

    /**
     * Sets the value of the placanjeTakse property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlacanjeTakse }
     *     
     */
    public void setPlacanjeTakse(PlacanjeTakse value) {
        this.placanjeTakse = value;
    }

    /**
     * Gets the value of the potpisPodnosioca property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getPotpisPodnosioca() {
        return potpisPodnosioca;
    }

    /**
     * Sets the value of the potpisPodnosioca property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setPotpisPodnosioca(byte[] value) {
        this.potpisPodnosioca = value;
    }

    /**
     * Gets the value of the popunjavaZavod property.
     * 
     * @return
     *     possible object is
     *     {@link PopunjavaZavod }
     *     
     */
    public PopunjavaZavod getPopunjavaZavod() {
        return popunjavaZavod;
    }

    /**
     * Sets the value of the popunjavaZavod property.
     * 
     * @param value
     *     allowed object is
     *     {@link PopunjavaZavod }
     *     
     */
    public void setPopunjavaZavod(PopunjavaZavod value) {
        this.popunjavaZavod = value;
    }

    /**
     * Gets the value of the brojPrijave property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrojPrijave() {
        return brojPrijave;
    }

    /**
     * Sets the value of the brojPrijave property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrojPrijave(String value) {
        this.brojPrijave = value;
    }

    /**
     * Gets the value of the datum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatum() {
        return datum;
    }

    /**
     * Sets the value of the datum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatum(XMLGregorianCalendar value) {
        this.datum = value;
    }

}
