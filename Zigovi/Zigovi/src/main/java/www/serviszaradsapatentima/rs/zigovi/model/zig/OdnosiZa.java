
package www.serviszaradsapatentima.rs.zigovi.model.zig;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Arrays;


/**
 * <p>Java class for odnosi_za complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="odnosi_za">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="a">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="individualni" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="kolektivni" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="garancije" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="b">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="verbalni" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="graficki" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="kombinovni" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="trodimenzioni" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="drugo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="v">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="izgled" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "odnosi_za", propOrder = {
    "a",
    "b",
    "v"
})
public class OdnosiZa {

    @XmlElement(required = true)
    protected OdnosiZa.A a;
    @XmlElement(required = true)
    protected OdnosiZa.B b;
    @XmlElement(required = true)
    protected OdnosiZa.V v;

    @Override
    public String toString() {
        return "OdnosiZa{" +
                "a=" + a +
                ", b=" + b +
                ", v=" + v +
                '}';
    }

    /**
     * Gets the value of the a property.
     *
     * @return
     *     possible object is
     *     {@link OdnosiZa.A }
     *
     */
    public OdnosiZa.A getA() {
        return a;
    }

    /**
     * Sets the value of the a property.
     *
     * @param value
     *     allowed object is
     *     {@link OdnosiZa.A }
     *
     */
    public void setA(OdnosiZa.A value) {
        this.a = value;
    }

    /**
     * Gets the value of the b property.
     *
     * @return
     *     possible object is
     *     {@link OdnosiZa.B }
     *
     */
    public OdnosiZa.B getB() {
        return b;
    }

    /**
     * Sets the value of the b property.
     *
     * @param value
     *     allowed object is
     *     {@link OdnosiZa.B }
     *
     */
    public void setB(OdnosiZa.B value) {
        this.b = value;
    }

    /**
     * Gets the value of the v property.
     *
     * @return
     *     possible object is
     *     {@link OdnosiZa.V }
     *
     */
    public OdnosiZa.V getV() {
        return v;
    }

    /**
     * Sets the value of the v property.
     *
     * @param value
     *     allowed object is
     *     {@link OdnosiZa.V }
     *
     */
    public void setV(OdnosiZa.V value) {
        this.v = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="individualni" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="kolektivni" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="garancije" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "individualni",
        "kolektivni",
        "garancije"
    })
    public static class A {

        protected boolean individualni;
        protected boolean kolektivni;
        protected boolean garancije;

        @Override
        public String toString() {
            return "A{" +
                    "individualni=" + individualni +
                    ", kolektivni=" + kolektivni +
                    ", garancije=" + garancije +
                    '}';
        }

        /**
         * Gets the value of the individualni property.
         * 
         */
        public boolean isIndividualni() {
            return individualni;
        }

        /**
         * Sets the value of the individualni property.
         * 
         */
        public void setIndividualni(boolean value) {
            this.individualni = value;
        }

        /**
         * Gets the value of the kolektivni property.
         * 
         */
        public boolean isKolektivni() {
            return kolektivni;
        }

        /**
         * Sets the value of the kolektivni property.
         * 
         */
        public void setKolektivni(boolean value) {
            this.kolektivni = value;
        }

        /**
         * Gets the value of the garancije property.
         * 
         */
        public boolean isGarancije() {
            return garancije;
        }

        /**
         * Sets the value of the garancije property.
         * 
         */
        public void setGarancije(boolean value) {
            this.garancije = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="verbalni" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="graficki" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="kombinovni" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="trodimenzioni" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="drugo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "verbalni",
        "graficki",
        "kombinovni",
        "trodimenzioni",
        "drugo"
    })
    public static class B {

        protected boolean verbalni;
        protected boolean graficki;
        protected boolean kombinovni;
        protected boolean trodimenzioni;
        @XmlElement(required = true)
        protected String drugo;

        @Override
        public String toString() {
            return "B{" +
                    "verbalni=" + verbalni +
                    ", graficki=" + graficki +
                    ", kombinovni=" + kombinovni +
                    ", trodimenzioni=" + trodimenzioni +
                    ", drugo='" + drugo + '\'' +
                    '}';
        }

        /**
         * Gets the value of the verbalni property.
         * 
         */
        public boolean isVerbalni() {
            return verbalni;
        }

        /**
         * Sets the value of the verbalni property.
         * 
         */
        public void setVerbalni(boolean value) {
            this.verbalni = value;
        }

        /**
         * Gets the value of the graficki property.
         * 
         */
        public boolean isGraficki() {
            return graficki;
        }

        /**
         * Sets the value of the graficki property.
         * 
         */
        public void setGraficki(boolean value) {
            this.graficki = value;
        }

        /**
         * Gets the value of the kombinovni property.
         * 
         */
        public boolean isKombinovni() {
            return kombinovni;
        }

        /**
         * Sets the value of the kombinovni property.
         * 
         */
        public void setKombinovni(boolean value) {
            this.kombinovni = value;
        }

        /**
         * Gets the value of the trodimenzioni property.
         * 
         */
        public boolean isTrodimenzioni() {
            return trodimenzioni;
        }

        /**
         * Sets the value of the trodimenzioni property.
         * 
         */
        public void setTrodimenzioni(boolean value) {
            this.trodimenzioni = value;
        }

        /**
         * Gets the value of the drugo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDrugo() {
            return drugo;
        }

        /**
         * Sets the value of the drugo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDrugo(String value) {
            this.drugo = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="izgled" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "izgled"
    })
    public static class V {

        @XmlElement(required = true, nillable = true)
        protected byte[] izgled;

        @Override
        public String toString() {
            return "V{" +
                    "izgled=" + Arrays.toString(izgled) +
                    '}';
        }

        /**
         * Gets the value of the izgled property.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getIzgled() {
            return izgled;
        }

        /**
         * Sets the value of the izgled property.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setIzgled(byte[] value) {
            this.izgled = value;
        }

    }

}
