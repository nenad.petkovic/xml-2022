package www.serviszaradsapatentima.rs.zigovi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;
import www.serviszaradsapatentima.rs.zigovi.model.zig.KolekcijaZigova;
import www.serviszaradsapatentima.rs.zigovi.model.zig.Zig;
import www.serviszaradsapatentima.rs.zigovi.model.zig.dto.MetapodaciUpitDTO;
import www.serviszaradsapatentima.rs.zigovi.service.PretragaService;
import www.serviszaradsapatentima.rs.zigovi.service.RDFService;
import www.serviszaradsapatentima.rs.zigovi.service.ZigService;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping(value = "api/pretraga/")
public class PretragaController {
    @Autowired
    PretragaService pretragaService;

    @Autowired
    ZigService zigService;

    @Autowired
    RDFService rdfService;

    @PostMapping(value = "/prosta", produces = MediaType.APPLICATION_XML_VALUE)
    ResponseEntity<KolekcijaZigova> prostaPretraga(@RequestParam String text) {
        KolekcijaZigova kolekcijaZigova = new KolekcijaZigova();
        kolekcijaZigova.setZigovi(this.zigService.findByString(text));
        return new ResponseEntity<>(kolekcijaZigova, HttpStatus.OK);
    }

    @PostMapping(value = "/pretragaMetapodaci/imePrezime",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<String>> pretragaMetapodatakaPoImePrezime(@RequestBody MetapodaciUpitDTO metapodaci) throws XPathExpressionException, JAXBException, ParserConfigurationException, IOException {
        return new ResponseEntity<>(pretragaService.searchMetadataImePrezime(metapodaci), HttpStatus.OK);
    }


    @PostMapping(value = "/pretragaMetapodaci/email", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<String>> pretragaMetapodatakaPoEmailu (@RequestBody MetapodaciUpitDTO metapodaci) throws XPathExpressionException, JAXBException, ParserConfigurationException, IOException {
        return new ResponseEntity<>(pretragaService.searchMetadataEmail(metapodaci), HttpStatus.OK);
    }

    /*************** AND PRETRAGA >> po svim metapodacima ******************/
    @PostMapping(value = "/pretragaMetapodaci/andPretraga", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<String>> pretragaPoSvimMetapodacimaAND (@RequestBody MetapodaciUpitDTO metapodaci) throws XPathExpressionException, JAXBException, ParserConfigurationException, IOException {
        return new ResponseEntity<>(pretragaService.searchMetadataAND(metapodaci), HttpStatus.OK);
    }

    /*************** OR PRETRAGA >> vraca uniju svih rezultata kod kojih je zadovoljen bar jedan uslov *******************/
    @PostMapping(value = "/pretragaMetapodaci/orPretraga", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<String>> pretragaPoSvimMetapodacimaOR (@RequestBody MetapodaciUpitDTO metapodaci) throws XPathExpressionException, JAXBException, ParserConfigurationException, IOException {
        return new ResponseEntity<>(pretragaService.searchMetadataOR(metapodaci), HttpStatus.OK);
    }
}
