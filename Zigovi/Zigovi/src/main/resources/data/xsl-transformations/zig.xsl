<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:z1="https://www.serviszaradsapatentima.rs/zig">
<xsl:template match="/">
        <html>
            <head>

                <style>
                    #naslov{
                    text-align: center;
                    font-size: 20pt;
                    margin-bottom: 0;
                    }
                    #opis{
                    margin-top: 0;
                    text-align: center;
                    font-size: 12pt;
                    }
                    #uputstvo{
                    text-align: center;
                    font-size: 12pt;
                    margin-bottom: 0;
                    }
                    #bordiraj{
                    margin-top: 0;
                    margin-bottom: 0;
                    border-style: solid;
                    border-width: thin;
                    }
                    #page{
                    margin-top: 2.97cm;
                    margin-bottom: 2.97cm;
                    margin-top: 2.1cm;
                    margin-bottom: 2.1cm;
                    }
                    table{
                    width:100%;
                    position: flex;
                    }
                    table, td, th{
                    border: 1px solid black;
                    border-collapse: collapse;
                    }

                </style>
            </head>
            <body>
                <div id="page">
                    <p id="naslov"><b>ZAHTEV ZA PRIZNANJE ŽIGA </b> </p>
                    <p id="opis"><b>Zavodu za intelektualnu svojinu, Kneginje Ljubice 5, 11000 Beograd </b></p>
                    <p id="uputstvo">(popuniti na računaru)</p>
                    <br/>
                    <div>
                        <p id="bordiraj"><b>1. Podnosilac prijave:</b> ime i prezime/poslovno ime, ulica i broj, poštanski broj, mesto, država prebivališta/sedišta:</p>
                        <p id="bordiraj"><i>
                            <xsl:value-of select="//z1:podnosilac_prijave//z1:ime"></xsl:value-of>&#160;
                            <xsl:value-of select="//z1:podnosilac_prijave//z1:prezime"></xsl:value-of><br />
                            <xsl:value-of select="//z1:podnosilac_prijave//z1:ulica"></xsl:value-of> &#160;
                            <xsl:value-of select="//z1:podnosilac_prijave//z1:broj"></xsl:value-of><br />
                            <xsl:value-of select="//z1:podnosilac_prijave//z1:postanski_broj"></xsl:value-of>&#160;
                            <xsl:value-of select="//z1:podnosilac_prijave//z1:mesto"></xsl:value-of>&#160;
                            <xsl:value-of select="//z1:podnosilac_prijave//z1:drzava"></xsl:value-of><br /></i>
                        </p>
                        <p id="bordiraj">
                            telefon:&#160;<i><xsl:value-of select="//z1:podnosilac_prijave//z1:telefon"></xsl:value-of>&#09;</i>&#160;&#160;&#160;
                            e-mail:&#160;<i><xsl:value-of select="//z1:podnosilac_prijave//z1:email"></xsl:value-of>&#09;</i>&#160;&#160;&#160;
                            faks:&#160;<i><xsl:value-of select="//z1:podnosilac_prijave//z1:faks"></xsl:value-of></i>
                        </p>
                    </div>
                    <div>
                        <p id="bordiraj"><b>2. Punomoćnik:</b> ime i prezime/poslovno ime, ulica i broj, poštanski broj, mesto, država prebivališta/sedišta:</p>
                        <p id="bordiraj"><i>
                            <xsl:value-of select="//z1:punomocnik//z1:ime"></xsl:value-of>&#160;
                            <xsl:value-of select="//z1:punomocnik//z1:prezime"></xsl:value-of><br />
                            <xsl:value-of select="//z1:punomocnik//z1:ulica"></xsl:value-of> &#160;
                            <xsl:value-of select="//z1:punomocnik//z1:broj"></xsl:value-of><br />
                            <xsl:value-of select="//z1:punomocnik//z1:postanski_broj"></xsl:value-of>&#160;
                            <xsl:value-of select="//z1:punomocnik//z1:mesto"></xsl:value-of>&#160;
                            <xsl:value-of select="//z1:punomocnik//z1:drzava"></xsl:value-of><br /></i>
                        </p>
                        <p id="bordiraj">
                            telefon:&#160;<i><xsl:value-of select="//z1:punomocnik//z1:telefon"></xsl:value-of></i>&#160;&#160;&#160;&#160;
                            e-mail:&#160;<i><xsl:value-of select="//z1:punomocnik//z1:email"></xsl:value-of></i>&#160;&#160;&#160;&#160;
                            faks:&#160;<i><xsl:value-of select="//z1:punomocnik//z1:faks"></xsl:value-of></i>
                        </p>
                    </div>
                    <div>
                        <p id="bordiraj"><b>3. Podaci o zajedničkom predstavniku ako postoji više podnosilaca prijave:</b></p>
                        <p id="bordiraj"><i>
                            <xsl:value-of select="//z1:predstavnik//z1:ime"></xsl:value-of>&#160;
                            <xsl:value-of select="//z1:predstavnik//z1:prezime"></xsl:value-of><br />
                            <xsl:value-of select="//z1:predstavnik//z1:ulica"></xsl:value-of> &#160;
                            <xsl:value-of select="//z1:predstavnik//z1:broj"></xsl:value-of><br />
                            <xsl:value-of select="//z1:predstavnik//z1:postanski_broj"></xsl:value-of>&#160;
                            <xsl:value-of select="//z1:predstavnik//z1:mesto"></xsl:value-of>&#160;
                            <xsl:value-of select="//z1:predstavnik//z1:drzava"></xsl:value-of><br /></i>
                        </p>
                        <p id="bordiraj">
                            telefon:&#160;<i><xsl:value-of select="//z1:predstavnik//z1:telefon"></xsl:value-of></i>&#160;&#160;&#160;&#160;
                            e-mail:&#160;<i><xsl:value-of select="//z1:predstavnik//z1:email"></xsl:value-of></i>&#160;&#160;&#160;&#160;
                            faks:&#160;<i><xsl:value-of select="//z1:predstavnik//z1:faks"></xsl:value-of></i>
                        </p>
                    </div>
                    <div>
                        <p id="bordiraj"><b>4. Prijava se podnosi za (upisati X):</b></p>
                        <div style="position:flex">
                            <div id="bordiraj">
                                <p><b>a)</b></p>
                                <p>
                                    individualni žig&#160;&#160;
                                    <xsl:if test="//z1:odnosi_za//z1:individualni='true'">
                                        X
                                    </xsl:if>
                                </p>
                                <p>kolektivni žig&#160;&#160;
                                    <xsl:if test="//z1:odnosi_za//z1:kolektivni='true'">
                                        X
                                    </xsl:if>
                                </p>
                                <p>žig garancije&#160;&#160;
                                    <xsl:if test="//z1:odnosi_za//z1:garancije='true'">
                                        X
                                    </xsl:if>
                                </p>
                            </div>
                            <div id="bordiraj">
                                <p><b>b)</b></p>
                                    <p>verbalni znak (znak u reči)&#160;&#160;
                                        <xsl:if test="//z1:odnosi_za//z1:verbalni='true'">
                                            X
                                        </xsl:if>
                                </p>
                                <p>grafički znak; boju, kombinaciju boja&#160;&#160;
                                    <xsl:if test="//z1:odnosi_za//z1:graficki='true'">
                                        X
                                    </xsl:if>
                                </p>
                                <p>kombinovani znak&#160;&#160;
                                    <xsl:if test="//z1:odnosi_za//z1:kombinovni='true'">
                                        X
                                    </xsl:if>
                                </p>
                                <p>trodimenzionalni znak&#160;&#160;
                                    <xsl:if test="//z1:odnosi_za//z1:trodimenzioni='true'">
                                        X
                                    </xsl:if>
                                </p>
                                <p>drugu vrstu znaka (navesti koju)&#160;&#160;
                                    <xsl:if test="//z1:odnosi_za//z1:drugo='true'">
                                        X
                                    </xsl:if>
                                </p>
                            </div>
                            <div id="bordiraj">
                                <p><b>v)</b></p>
                                <p>SLIKA</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <p id="bordiraj"><b>5. Naznačenje boje, odnosno boja iz kojih se znak sastoji:</b>
                            <br/><i><xsl:value-of select="//z1:boje"></xsl:value-of></i>
                        </p>
                    </div>
                    <div>
                        <p id="bordiraj"><b>6. Transliteracija znaka*:</b>
                            <br/><i><xsl:value-of select="//z1:transliteracija_znaka"></xsl:value-of></i>
                        </p>
                    </div>
                    <div>
                        <p id="bordiraj"><b>7.  Prevod znaka*:</b>
                            <br/><i><xsl:value-of select="//z1:prevod_znaka"></xsl:value-of></i>
                        </p>
                    </div>
                    <div>
                        <p id="bordiraj"><b>8. Opis znaka:</b>
                            <br/><i><xsl:value-of select="//z1:opis_znaka"></xsl:value-of></i>
                        </p>
                    </div>
                    <div>
                        <p id="bordiraj"><b>9. Zaokružiti brojeve klasa robe i usluga prema Ničanskoj klasifikaciji :  </b></p>
                    </div>
                    <div>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        1
                                        <xsl:if  test="//z1:zaokruzeno = '1'">
                                        X
                                        </xsl:if>
                                    </td>
                                    <td>2
                                        <xsl:if  test="//z1:zaokruzeno = '2'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>
                                        3
                                        <xsl:if  test="//z1:zaokruzeno = '3'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>4
                                        <xsl:if  test="//z1:zaokruzeno = '4'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>5
                                        <xsl:if  test="//z1:zaokruzeno = '5'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>6
                                        <xsl:if  test="//z1:zaokruzeno = '6'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>7
                                        <xsl:if  test="//z1:zaokruzeno = '7'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>8
                                        <xsl:if  test="//z1:zaokruzeno = '8'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>9
                                        <xsl:if  test="//z1:zaokruzeno = '9'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>10
                                        <xsl:if  test="//z1:zaokruzeno = '10'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>11
                                        <xsl:if  test="//z1:zaokruzeno = '11'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>12
                                        <xsl:if  test="//z1:zaokruzeno = '12'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>13
                                        <xsl:if  test="//z1:zaokruzeno = '13'">
                                        X
                                    </xsl:if>
                                    </td>
                                    <td>14
                                        <xsl:if  test="//z1:zaokruzeno = '14'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>15
                                        <xsl:if  test="//z1:zaokruzeno = '15'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>16
                                        <xsl:if  test="//z1:zaokruzeno = '16'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>17
                                        <xsl:if  test="//z1:zaokruzeno = '17'">
                                        X
                                    </xsl:if>
                                    </td>
                                    <td>18
                                        <xsl:if  test="//z1:zaokruzeno = '18'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>19
                                        <xsl:if  test="//z1:zaokruzeno = '19'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>20
                                        <xsl:if  test="//z1:zaokruzeno = '20'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>21
                                        <xsl:if  test="//z1:zaokruzeno = '21'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>22
                                        <xsl:if  test="//z1:zaokruzeno = '22'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>23
                                        <xsl:if  test="//z1:zaokruzeno = '23'">
                                            X
                                        </xsl:if>
                                    </td>
                                </tr>
                                <tr>
                                    <td>24
                                        <xsl:if  test="//z1:zaokruzeno = '24'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>25
                                        <xsl:if  test="//z1:zaokruzeno = '25'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>26
                                        <xsl:if  test="//z1:zaokruzeno = '26'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>27
                                        <xsl:if  test="//z1:zaokruzeno = '27'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>28
                                        <xsl:if  test="//z1:zaokruzeno = '28'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>29
                                        <xsl:if  test="//z1:zaokruzeno = '29'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>30
                                        <xsl:if  test="//z1:zaokruzeno = '30'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>31
                                        <xsl:if  test="//z1:zaokruzeno = '31'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>32
                                        <xsl:if  test="//z1:zaokruzeno = '32'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>33
                                        <xsl:if  test="//z1:zaokruzeno = '33'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>34
                                        <xsl:if  test="//z1:zaokruzeno = '34'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>35
                                        <xsl:if  test="//z1:zaokruzeno = '35'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>36
                                        <xsl:if  test="//z1:zaokruzeno = '36'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>37
                                        <xsl:if  test="//z1:zaokruzeno = '37'">
                                        X
                                    </xsl:if></td>
                                    <td>38
                                        <xsl:if  test="//z1:zaokruzeno = '38'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>39
                                        <xsl:if  test="//z1:zaokruzeno = '39'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>40 <xsl:if  test="//z1:zaokruzeno = '40'">
                                        X
                                    </xsl:if>
                                    </td>
                                    <td>41
                                        <xsl:if  test="//z1:zaokruzeno = '41'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>42
                                        <xsl:if  test="//z1:zaokruzeno = '42'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td>43 <xsl:if  test="//z1:zaokruzeno = '43'">
                                        X
                                    </xsl:if>
                                    </td>
                                    <td>44 <xsl:if  test="//z1:zaokruzeno = '44'">
                                        X
                                    </xsl:if>
                                    </td>
                                    <td>45
                                        <xsl:if  test="//z1:zaokruzeno = '45'">
                                            X
                                        </xsl:if>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <p id="bordiraj"><b>10. Zatraženo pravo prvenstva i osnov:</b>
                            <br/><i><xsl:value-of select="//z1:prvanstvo_i_osnov"></xsl:value-of></i>
                        </p>
                    </div>
                    <p>*Popuniti samo ako je znak ili element znaka ispisan slovima koja nisu ćirilična ili latinična</p>
                    <div>
                        <p id="bordiraj"><b>11. Plaćene takse:</b></p>
                        <p id="bordiraj"><b>a) osnovna taksa&#160;&#160;<i><xsl:value-of select="//z1:placanje_takse//z1:osnovna_taksa"></xsl:value-of></i></b></p>
                        <p id="bordiraj"><b>b) za&#160;&#160;<i><xsl:value-of select="//z1:za_klasa/@broj_klasa"></xsl:value-of></i> &#160;&#160;klasa&#160;&#160;<i><xsl:value-of select="//z1:za_klasa/@dinara"></xsl:value-of></i></b></p>
                        <p id="bordiraj"><b>v) za grafičko rešenje&#160;&#160;<i><xsl:value-of select="//z1:placanje_takse//z1:graficko_resenje"></xsl:value-of></i></b></p>
                        <p id="bordiraj"><b>UKUPNO&#160;&#160;<i><xsl:value-of select="//z1:placanje_takse//z1:ukupno"></xsl:value-of></i> &#160;&#160;Dinara</b></p>
                    </div>
                    <div>
                        <p id="bordiraj"><b>Potpis  podnosioca zahteva:</b>
                            <br/>
                            <br/>
                            * Pečat, ukoliko je potreban u skladu sa zakonom
                        </p>
                    </div>
                    <br />
                    <p style="text-align:center" id="bordiraj"><b>POPUNJAVA ZAVOD</b></p>
                    <p id="bordiraj"><b>Prilozi uz zahtev:</b></p>
                    <p id="bordiraj">Primerak znaka&#160;&#160;
                        <xsl:if test="//z1:popunjava_zavod//z1:primerak_znaka='true'">
                            X
                        </xsl:if>
                    </p>
                    <p id="bordiraj">Spisak robe i usluga**&#160;&#160;
                        <xsl:if test="//z1:popunjava_zavod//z1:spisak_robe_usluga='true'">
                            X
                        </xsl:if>
                    </p>
                    <p id="bordiraj">Punomoćje&#160;&#160;
                        <xsl:if test="//z1:popunjava_zavod//z1:punomocje='true'">
                            X
                        </xsl:if>
                    </p>
                    <p id="bordiraj">Generalno punomoćje ranije priloženo&#160;&#160;
                        <xsl:if test="//z1:popunjava_zavod//z1:generalno_punomocje='true'">
                            X
                        </xsl:if>
                    </p>
                    <p id="bordiraj">Punomoćje će biti naknadno dostavljeno&#160;&#160;
                        <xsl:if test="//z1:popunjava_zavod//z1:punomocje_naknadno_dostavljeno='true'">
                            X
                        </xsl:if>
                    </p>
                    <p id="bordiraj">Opšti akt o kolektivnom žigu/žigu garancije&#160;&#160;
                        <xsl:if test="//z1:popunjava_zavod//z1:akt_o_kolektivnom_zigu='true'">
                            X
                        </xsl:if>
                    </p>
                    <p id="bordiraj">Dokaz o pravu prvenstva&#160;&#160;
                        <xsl:if test="//z1:popunjava_zavod//z1:dokaz_prava_prvenstva='true'">
                            X
                        </xsl:if>
                    </p>
                    <p id="bordiraj">Dokaz o uplati takse&#160;&#160;
                        <xsl:if test="//z1:popunjava_zavod//z1:dokaz_uplata_takse='true'">
                            X
                        </xsl:if>
                    </p>
                    <div style="text-align:center">
                        <p>Broj prijave žiga:</p>
                        <p><xsl:value-of select="//z1:zig/@broj_prijave"></xsl:value-of></p>
                        <p>Datum podnošenja:</p>
                        <p><xsl:value-of select="//z1:zig/@datum"></xsl:value-of></p>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>