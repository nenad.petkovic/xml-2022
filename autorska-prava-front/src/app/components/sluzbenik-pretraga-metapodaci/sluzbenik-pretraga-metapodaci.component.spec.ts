import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SluzbenikPretragaMetapodaciComponent } from './sluzbenik-pretraga-metapodaci.component';

describe('SluzbenikPretragaMetapodaciComponent', () => {
  let component: SluzbenikPretragaMetapodaciComponent;
  let fixture: ComponentFixture<SluzbenikPretragaMetapodaciComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SluzbenikPretragaMetapodaciComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SluzbenikPretragaMetapodaciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
