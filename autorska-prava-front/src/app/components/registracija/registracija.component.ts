import { NgFor } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LogovanjeRegistracijaService } from 'src/service/logovanje-registracija.service';

@Component({
  selector: 'app-registracija',
  templateUrl: './registracija.component.html',
  styleUrls: ['./registracija.component.css']
})
export class RegistracijaComponent implements OnInit{

  constructor(private router: Router, private userService: LogovanjeRegistracijaService) { }

  lozinka!: string;
  email!: string;
  
  ngOnInit(): void {
      
    }
  

  registrujSe(f: NgForm){
    //ako nisu uneseni podaci >>ne dozvoli registraciju

    if(this.lozinka == ""){
      alert("Molimo vas unesite lozinku korisnika...");
    }
    else if(this.email == ""){
      alert("Molimo vas unesite korisnicko ime korisnika..");
    }
    else{
      let xml = "<user:users xmlns:user=\"https:\/\/www.serviszaradsapatentima.rs\/users\">\r\n  <user:podnosilac_prijave vocab=\"http:\/\/www.serviszaradsapatentima.rs\/rdf\/database\/predicate\" about=\"string\">\r\n    <user:password>"+
      this.lozinka+"<\/user:password>\r\n    <user:uloga>"+
      "USER"+"<\/user:uloga>\r\n    <user:email property=\"pred:email\" datatype=\"xs:string\">"+
      this.email+"<\/user:email>\r\n  <\/user:podnosilac_prijave>\r\n<\/user:users>";

      this.userService.upisi(xml).subscribe({
        next: data => {
          console.log(data);

          // Uspesna reg
          alert("Registrovan!")
          this.router.navigate(['/', '/']).then(() => {
            window.location.reload();
          });
        },
        error: error => {
          alert("Ne postoji korisnik!");
          console.error('There was an error!', error);
        }
      })
    }

  }

}
