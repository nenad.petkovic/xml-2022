import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as FileSaver from 'file-saver';
import { SluzbenikPretragaService } from 'src/service/sluzbenik-pretraga.service';
import { XtmlPdfGeneratorService } from 'src/service/xtml-pdf-generator.service';
import { xml2js } from 'xml-js';
import { SluzbenikPretragaMetapodaciComponent } from '../sluzbenik-pretraga-metapodaci/sluzbenik-pretraga-metapodaci.component';
//import { DatePipe } from '@angular/common';
//import * as moment from 'moment';

@Component({
  selector: 'app-pregledaj-dokument',
  templateUrl: './pregledaj-dokument.component.html',
  styleUrls: ['./pregledaj-dokument.component.css']
})
export class PregledajDokumentComponent implements OnInit{

  idDokumentaZaPregled: any = '';

  //podaci o poslatom zahtevu
  brojZahteva: any = '';
  podnosilac: any = '';
  nazivDela: any = '';
  emailPodnosioca: any = '';


  //izbor statusa zahteva
  prihvacenZahtev: boolean = false;
  odbijenZahtev: boolean = false;

  obrazlozenje: string = '';

  constructor( private route: ActivatedRoute,
               
               private sluzbenikPretragaServis: SluzbenikPretragaService,
               private xmlPdfGeneratorServis: XtmlPdfGeneratorService
               //private pipe: DatePipe
    ){

  }

  ngOnInit(): void {
    //podaci o id-iju dokumenta koji pregledamo iz Path parametara putanje>> preuzimamo
    this.route.queryParamMap.subscribe(
      (params) => {
        this.idDokumentaZaPregled = params.get('idDokumenta');

        console.log("Id dokumenta koji prikazujemo je: " + this.idDokumentaZaPregled);
        this.ucitajDokument(this.idDokumentaZaPregled);

        this.prihvacenZahtev = true;
        this.odbijenZahtev = false;

      }
    )

  }

  ucitajDokument(id: string){
    //ucitavamo dokument sa prosledjenim id-ijem >> ucitavamo njegove podatke u json formatu
   
      this.sluzbenikPretragaServis.getZahtevZaAutorskoPravoPoId(id).subscribe(
        (response: any)=>{
          console.log("Izvrseno rezultat je: " + response);
          console.log(typeof response);
  
          if (response == null){
            alert("Dokument sa tim id-ijem ne postoji..");
          }
          
          let konverzija = xml2js(response);
          
          console.log(konverzija);

          //postavljamo nakon ucitavanja vrednosti osnovnih podataka o tom zahtevu
          this.brojZahteva = konverzija.elements[0].attributes.BrojPrijave;

          //naziv autorskog dela
          this.nazivDela=konverzija.elements[0].elements[2].elements[0].elements[0].text;
        
          //ime podnosioca
          this.podnosilac = konverzija.elements[0].elements[1].elements[0].elements[0].elements[0].text;

          //email podnosioca
          this.emailPodnosioca = konverzija.elements[0].elements[1].elements[2].elements[0].text;
          if(this.emailPodnosioca == undefined){
            this.emailPodnosioca =  konverzija.elements[0].elements[1].elements[1].elements[0].text;
          }
        }
      );
  }


  /****** izbor da li je zahtev prihvacen ili ne *******/
  handleChangeRadioButton(event: any){
    let target= event.target;

    if(target.checked){
      if (target.value== "PRIHVACEN" ){
        this.prihvacenZahtev = true;
        this.odbijenZahtev = false;
      }
      if( target.value == "ODBIJEN"){
        this.prihvacenZahtev = false;
        this.odbijenZahtev = true;
      }
    }
    
  }
  /*** izabrano generisanje html ****/
  generisiHtml(event: any){
      
      this.xmlPdfGeneratorServis.getDokumentXHTML( this.brojZahteva, 'tip').subscribe(
        (response: any) =>
          {
            console.log("Pronadjeni dokument...");
            console.log(response);

            //document.open("", '_blank');
            //document.write(response);
            
            var blob = new Blob([response], {type: "text/plain;charset=utf-8"});
            FileSaver.saveAs(blob, "ZahtevZaAutorskoPravo"+this.brojZahteva+".xhtml");

          }
      );
  }

  /*** izabrano generisanje pdf dokuementa ****/
  generisiPdf(event: any){
      this.xmlPdfGeneratorServis.getDokumentPDF( this.brojZahteva, 'tip').subscribe(
        (response: any) =>
          {
            console.log("Pronadjeni dokument...");
            console.log(response);

            //document.open("", '_blank');
            //document.write(response);
            
            var blob = new Blob([response], {type: "text/plain;charset=utf-8"});
            FileSaver.saveAs(blob, "ZahtevZaAutorskoPravo"+this.brojZahteva+".pdf");

          }
      );
    }

    /**** json metapodaci >> generisanje  ****/
    generisiJsonMetapodatke(event: any){
      this.xmlPdfGeneratorServis.getDokumentMetapodaciJSON( this.brojZahteva, 'tip').subscribe(
        (response: any) =>
          {
            console.log("Pronadjeni dokument...");
            console.log(response);

            //document.open("", '_blank');
            //document.write(response);
            
            var blob = new Blob([response], {type: "text/plain;charset=utf-8"});
            FileSaver.saveAs(blob, "ZahtevZaAutorskoPravo-metapodaci"+this.brojZahteva+".json");

          }
      ); 
    }

    /**** rdf metapodaci >> generisanje  ****/
    generisiRdfMetapodatke(event: any){
      this.xmlPdfGeneratorServis.getDokumentMetapodaciRDF( this.brojZahteva, 'tip').subscribe(
        (response: any) =>
          {
            console.log("Pronadjeni dokument...");
            console.log(response);

            //document.open("", '_blank');
            //document.write(response);
            
            var blob = new Blob([response], {type: "text/plain;charset=utf-8"});
            FileSaver.saveAs(blob, "ZahtevZaAutorskoPravo-metapodaci"+this.brojZahteva+".json");

          }
      ); 
    }

    /*********** slanje resenja korisniku  **********/
    posaljiResenje(event: any){
      let imeUlogovanogSluzbenika:string = 'mika';
      let prezimeUlogovanogSluzbenika: string= 'mikic';
      let statusResenja: string = '';
       //proverimo da li je resenje odbijeno ili je prihvaceno
       if(this.prihvacenZahtev == true){
        //resenje je prihvaceno
        let datumPrihvatanja: Date = new Date();

        let formatiranDatumPrihvatanja: string = '';
        formatiranDatumPrihvatanja += datumPrihvatanja.getFullYear();
        formatiranDatumPrihvatanja += '-' + '02';//datumPrihvatanja.getMonth();
        formatiranDatumPrihvatanja += '-' + '07';//datumPrihvatanja.getDay();
        
        console.log("Datum prihvatanja zahteva je: "+ formatiranDatumPrihvatanja);
        

        statusResenja = `
            <PrihvacenZahtev>
              <SifraZahteva>`+ this.brojZahteva +`</SifraZahteva>
              <DatumPrihvatanja>` + formatiranDatumPrihvatanja + `</DatumPrihvatanja>
            </PrihvacenZahtev>
        `;
      }
      else{
        //resenje odbijeno
        let datumOdbijanja = new Date();
        
        let formatiranDatumOdbijanja: string = '';
        formatiranDatumOdbijanja += datumOdbijanja.getFullYear();
        formatiranDatumOdbijanja += '-' + '02';//datumOdbijanja.getMonth();
        formatiranDatumOdbijanja += '-' + '07';//datumOdbijanja.getDay();

        statusResenja=`
            <OdbijenZahtev>
              <Obrazlozenje>` + this.obrazlozenje+ ` </Obrazlozenje>
              <DatumOdbijanja>` + formatiranDatumOdbijanja + `</DatumOdbijanja>
            </OdbijenZahtev>
        `;
      }

      let broj: string = "1";
      
      let xmlResenje: string = `<?xml version="1.0" encoding="UTF-8"?>
      <res:Resenje
          xmlns:res="http://www.ftn.uns.ac.rs/resenje"
          xmlns="http://www.ftn.uns.ac.rs/resenje"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://www.ftn.uns.ac.rs/resenje file:/C:/Users/DM/Desktop/resenje.xsd" BrojResenja="` + broj + `">
          <res:TipZahtevaNaKojiSeResenjeOdnosi>AUTORSKO_DELO</res:TipZahtevaNaKojiSeResenjeOdnosi>
          <res:Sluzbenik>
              <res:Ime>` + imeUlogovanogSluzbenika +`</res:Ime>
              <res:Prezime>` + prezimeUlogovanogSluzbenika + `</res:Prezime>
          </res:Sluzbenik>
          <res:ReferencaNaZahtev>`+ this.brojZahteva +`</res:ReferencaNaZahtev>
          <res:StatusZahteva>
              `+ statusResenja +`
          </res:StatusZahteva>
      </res:Resenje>`;


     
      /// poziv metode na backendu za slanje resenja
      this.sluzbenikPretragaServis.posaljiResenje(xmlResenje).subscribe(
        (response: any) =>
          {
            console.log("Pronadjeni dokument...");
            console.log(response);
  
            //document.open("", '_blank');
            //document.write(response);
            
            //preuzimanje izgenerisanog resenja u Pdf formatu
            var blob = new Blob([response], {type: "text/plain;charset=utf-8"});
            FileSaver.saveAs(blob, "rsenje"+ this.brojZahteva +".pdf");
  
          }
      );

    }

}
function moment(datumPrihvatanja: Date) {
  throw new Error('Function not implemented.');
}

