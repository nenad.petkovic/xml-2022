import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PregledajDokumentComponent } from './pregledaj-dokument.component';

describe('PregledajDokumentComponent', () => {
  let component: PregledajDokumentComponent;
  let fixture: ComponentFixture<PregledajDokumentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PregledajDokumentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PregledajDokumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
