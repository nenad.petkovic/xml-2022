import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorskoPravoComponent } from './autorsko-pravo.component';

describe('AutorskoPravoComponent', () => {
  let component: AutorskoPravoComponent;
  let fixture: ComponentFixture<AutorskoPravoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutorskoPravoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AutorskoPravoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
