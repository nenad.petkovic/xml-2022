import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Autor, AutorskoPravo, FizickoLice, PodaciOPodnosiocu, PravnoLice } from 'src/app/model/autorskoPravo';
import { AutorskoPravoService } from 'src/service/autorsko-pravo.service';



@Component({
  selector: 'app-autorsko-pravo',
  templateUrl: './autorsko-pravo.component.html',
  styleUrls: ['./autorsko-pravo.component.css']
})
export class AutorskoPravoComponent implements OnInit{

  autorskoPravo: AutorskoPravo  = new AutorskoPravo();

  constructor(
    private autorskoPravoService: AutorskoPravoService,
    
    ) {
      //this.kreiranjeForme()
    }
  
  

  //xml oblik zahteva
  xmlZahtev: string = '';

  //proveravamo sta je selektovano >> sta cemo da prikazemo koji deo forme za unos podataka
  selektovanTipPodnosiocaFizickoLice:boolean =  false;
  selektovanTipPodnosiocaPravnoLice: boolean =  false;

  tipoviPodnosilacaZahteva: string[] = []; //moguci tipovi korisnika koji mogu da podnesu zahteve
  izabraniTipPodnosiocaZahteva: string = '';
  koJePotpisao: string[]=[]; //moguce uloge koje su potpisale zahtev
  potpisnikZahteva: string = "";

  vrsteDelaIzb: string[]=[]; //moguce vrste dela koje mogu biti izabrane
  izabranaVrstaDela: string = '';
  tipVrsteDela: string[]=[];
  izabraniTipVrsteDela: string = '';

  //dugmici ziv mrtav autor >> za promenu forme za unos podataka
  kliknutoDugmeZivAutor: boolean = false;
  kliknutoDugmeMrtavAutor: boolean = false;

  formeZapisaDela: string[]= [];
  izabranaFormaZapisaDela: string='';

  deloNastaloURadnomOdnosu: string[]=[];
  izabraniRadniOdnos:string = '';

  //uneseni podaci o autorima
  listaAutora: any = [];
  //uneseni podaci o listi koautora
  listaKoautora: any = [];

  //izbrana uloga autora dela
  izabranAutorDela: boolean = false;
  izabranKoautorDela: boolean = false;

  //da li je autor/koautor ziv
  autorJeZiv: boolean = false;
  autorJeMrtav: boolean = false;

  //da li je autor anoniman
  jesteAnoniman: boolean = false;
  nijeAnoniman: boolean = false;
  
  ngOnInit(): void {

    console.log("Ovoo ejj okkkkkkkkkk...")
    this.selektovanTipPodnosiocaFizickoLice = true;
    this.selektovanTipPodnosiocaPravnoLice = false;

    this.tipoviPodnosilacaZahteva = this.autorskoPravoService.loadTipoviPodnosilacaZahteva() //ucitavanje tipova podnosilaca zahteva
    console.log("Ucitani tipovi podnosilaca zahteva su: " + this.tipoviPodnosilacaZahteva);

    this.koJePotpisao = this.autorskoPravoService.loadKoJePotpisaoZahtev();
    this.vrsteDelaIzb = this.autorskoPravoService.loadVrsteAutroskogDela();
    console.log("Vrste dela su: " + this.vrsteDelaIzb);

    this.tipVrsteDela = this.autorskoPravoService.loadPisanaDela();

    this.kliknutoDugmeZivAutor = true;
    this.kliknutoDugmeMrtavAutor = false;

    this.formeZapisaDela = this.autorskoPravoService.loadFormeZapisaDela();
  
    this.deloNastaloURadnomOdnosu= this.autorskoPravoService.loadDaLiJeNastaloURadnomOdnosu();

    //izbor tipa autora dela >> uloge
    this.izabranAutorDela = true;
    this.izabranKoautorDela = false;

    //ziv autor?
    this.autorJeZiv = true;
    this.autorJeMrtav = false;
  }


  //metoda koja obradjuje promenu tipa podnosioca zahteva
  handleChangeRadioButton(event: any){
    console.log("Kliknuto je dugme za prikaz tipa podnosioca zahteva..");

    let target = event.target;

    if (target.checked){

      //proverimo da li je selektovano fizicko lice
      if (target.value == "FIZICKO_LICE"){
        //postavicemo prikaz za fizicko lice
        this.selektovanTipPodnosiocaFizickoLice = true;
        this.selektovanTipPodnosiocaPravnoLice = false;
      }
      if (target.value == "PRAVNO_LICE"){
        this.selektovanTipPodnosiocaFizickoLice = false;
        this.selektovanTipPodnosiocaPravnoLice = true;
      }

      console.log("Selektovana je vrednost pravno lice: " + this.selektovanTipPodnosiocaPravnoLice, "\n ",
        "Selektovana je vrednost fizicko lice: " + this.selektovanTipPodnosiocaFizickoLice);
    }

  }
  
  izabranTipPodnosiocaZahteva(event: any) {
    // belezimo koji je izabran kao tip podnosioca zahteva
    let target = event.target;

    this.izabranTipPodnosiocaZahteva = target.value;
    target.selected = target.value;

  }

  koJePotpisaoZahtev(event: any){
    console.log("Ovo je izabrani potpisnik onaj ko je potpisao zahtev..");
    let target = event.target;

    this.potpisnikZahteva = (target.value).split(" ")[1];
  }

  changeIzabraneVrsteDela(event: any){
    console.log("Izabrana je neka vrsta dela...");

    //proveravamo koja vrsta dela je izabrana >> i izlistavamo za tu vrstu moguce tipove dela..
    let target = event.target;

    let selektovanaVrednost = (target.value).split(" ")[1];
    console.log("Selektovano: " + selektovanaVrednost);

    console.log("Izabrano je "+ selektovanaVrednost);

    if (selektovanaVrednost == "PISANO_DELO"){
      this.tipVrsteDela = this.autorskoPravoService.loadPisanaDela();
      target.value = this.tipVrsteDela;
      this.izabranaVrstaDela = "PisanoDelo"
      //target.value = "PISANO_DELO";
    }
    else if (selektovanaVrednost == "GOVORNA_DELA"){
      this.tipVrsteDela = this.autorskoPravoService.loadGovornaDela();
      console.log("Selektovana su govorna dela...");
      console.log("Promenenjena dela: " + this.tipVrsteDela);
      //target.value = this.tipVrsteDela;
      this.izabranaVrstaDela = "GovornaDela";
    }
    else if (selektovanaVrednost == "DRAMSKA_DELA"){
      this.tipVrsteDela = this.autorskoPravoService.loadDramskaDela();
      //target.value = this.tipVrsteDela;
      this.izabranaVrstaDela = "DramskaDela";
    }
    else if (selektovanaVrednost == "MUZICKO_DELO"){
      this.tipVrsteDela = this.autorskoPravoService.loadMuzickaDela();
      //target.value = this.tipVrsteDela;
      this.izabranaVrstaDela = "MuzickoDelo";
    }
    else if (selektovanaVrednost == "FILMSKA_DELA"){
      this.tipVrsteDela = this.autorskoPravoService.loadFilmskaDela();
      //target.value = this.tipVrsteDela;
      this.izabranaVrstaDela = "FilmskaDela";
    }
    else if(selektovanaVrednost == "DELA_LIKOVNE_UMETNOSTI"){
      this.tipVrsteDela = this.autorskoPravoService.loadDelaLikovneUmetnosti();
      //target.value = this.tipVrsteDela;
      this.izabranaVrstaDela = "DelaLikovneUmetnosti";
      
    }
  }

  changeIzabraniTipVrsteDela(event: any){
    //promena izabranog tipa vrste dela
    let target = event.target;

    let vrednost = target.value;

    this.izabraniTipVrsteDela = vrednost;
  }

  // provera da li je kliknuto da je autor anoniman
  daLiJeAnonimanAutor(event: any){
    let target = event.target;

    if (target.checked){
      
      if (target.value == "anoniman"){
        this.jesteAnoniman = true;
        this.nijeAnoniman = false;
      }
      else if(target.value == "nijeAnoniman"){
        this.jesteAnoniman = false;
        this.nijeAnoniman = true;
      }
    }
  }

  //kliknuto na dugme ziv autor dela
  handleChangeZivAutorDugme(event: any){
    let target = event.target;

    if (target.checked){
      //ako je kliknuto dugme ziv autor postavljamo sledece indikatore
      let vrednost = target.value;
      console.log("Izabrana vrednsost je: "+ vrednost);

      if(target.value == "zivAutor"){
        this.kliknutoDugmeZivAutor = true;
        this.kliknutoDugmeMrtavAutor = false;
      }
      if (target.value == "mrtavAutor"){
        this.kliknutoDugmeMrtavAutor = true;
        this.kliknutoDugmeZivAutor = false;
      }

    }

  }

  changeIzabranaFormaZapisaDela(event: any){
    console.log("izabrana forma zapisa dela je: ");
    let target = event.target;

    this.izabranaFormaZapisaDela = (target.value).split(" ")[1];
  }

  changeIzabraniRadniOdnos(event: any){
    console.log("izabrani randni odnos je:");
    let target = event.target;

    this.izabraniRadniOdnos = (target.value).split(" ")[1];
  }

  //  provera da li je autor ziv
  handleChangeZivAutorUnosDugme(event: any){
    let target = event.target;

    if (target.checked){
      if (target.value == "zivAutor"){
        //ziv autor
        this.autorJeMrtav = false;
        this.autorJeZiv = true;
      }
    }

      if (target.value == "mrtavAutor"){
        //autor je mrtav
        this.autorJeMrtav = true;
        this.autorJeZiv = false;
      }
  }

  /*** dodavanje novog autora ili koautora u listu izabranih objekata ***/
  dodajAutora(event: any){
    //proverimo da li je dodat autor ili je koautor

    
      //dodajemo autora
      //pravimo objekat autora
      let autorObj: Autor = new Autor();
      
      //proveravamo da li unosimo podatke za zivog ili preminulog autora
      if(this.autorJeZiv == true){
        autorObj.zivAutor.ime = this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.ime;
        autorObj.zivAutor.prezime = this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.prezime;
        autorObj.zivAutor.drzavljanstvo = this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.drzavljanstvo;
        //anonimnost autora ispitujemo
        if (this.jesteAnoniman == true){
          autorObj.zivAutor.daLiJeAnoniman = true;
        }
        else{
          autorObj.zivAutor.daLiJeAnoniman = false;
        }
        autorObj.zivAutor.adresaAutora.ulica = this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.adresaAutora.ulica;
        autorObj.zivAutor.adresaAutora.broj = this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.adresaAutora.broj;
        autorObj.zivAutor.adresaAutora.mesto = this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.adresaAutora.mesto;
        autorObj.zivAutor.adresaAutora.postanskiBroj = this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.adresaAutora.postanskiBroj;
      
        //ciscenje sadrzaja polja
        this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.ime='';
        this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.prezime = '';
        this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.drzavljanstvo = '';
        this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.adresaAutora.broj = '';
        this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.adresaAutora.mesto = '';
        this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.adresaAutora.ulica = '';
        this.autorskoPravo.podaci_o_autorskom_delu.autorDela.zivAutor.adresaAutora.postanskiBroj = '';

      }
      else if (this.autorJeMrtav == true){
        autorObj.preminuoAutor.ime = this.autorskoPravo.podaci_o_autorskom_delu.autorDela.preminuoAutor.ime;
        autorObj.preminuoAutor.godinaSmrti = this.autorskoPravo.podaci_o_autorskom_delu.autorDela.preminuoAutor.godinaSmrti;
        if (this.jesteAnoniman == true){
          autorObj.preminuoAutor.daLiJeAnoniman = true;
        }
        else {
          autorObj.preminuoAutor.daLiJeAnoniman = false;
        }
       
      }
      autorObj.pseudonimAutora = this.autorskoPravo.podaci_o_autorskom_delu.autorDela.pseudonimAutora;
      autorObj.znakAutora = this.autorskoPravo.podaci_o_autorskom_delu.autorDela.znakAutora;
      
      //*** brisanje sadrzaja polja ***/
      this.autorskoPravo.podaci_o_autorskom_delu.autorDela.preminuoAutor.godinaSmrti = 0;
      this.autorskoPravo.podaci_o_autorskom_delu.autorDela.preminuoAutor.ime = '';

      

    if (this.izabranAutorDela == true){
      //dodajemo u listu autora 
      //sacuvamo ovaj objekat
      this.listaAutora.push(autorObj);

      //ispis dodatih objekata
      console.log("******** LISTA DODATIH AUTORA *******");
      for( let obj of this.listaAutora){
        console.log(obj);
        console.log("--------------------");
      }
    }
    else if (this.izabranKoautorDela == true){
      //dodajemo u listu koautora
      this.listaKoautora.push(autorObj);

      console.log("************ LISTA DODATIH KOAUTORA ***********");
      //ispis dodatih koautora
      for( let obj of this.listaKoautora){
        console.log(obj);
        console.log("--------------------");
      }
    }
    
    
  }

  izabranaUlogaAutora(event: any){
    let target = event.target;
    
    if(target.checked){
      if (target.value == "AUTOR"){
        this.izabranAutorDela = true;
        this.izabranKoautorDela = false;
        
      }
      if (target.value=="KOAUTOR"){
        this.izabranAutorDela = false;
        this.izabranKoautorDela = true;
      }
    }
  }

  
  /********************* VALIDACIJA PODATAKA  ********************/
  //validacija email-a
  validacijaEmail(): boolean{
    let paternValidacije = new RegExp('[a-zA-z0-9\\.]+[@]{1,1}[a-z\\.a-z]+');
    return paternValidacije.test(this.autorskoPravo.podaci_o_podnosiocu_zahteva.email);
  }

  //validacija broja telefona
  validacijaBrojaTelefona(): boolean{
    let paternValidacije = new RegExp("[\+]?[0-9]+");
    //mora imati min 8 cifara a max 15

    if (this.autorskoPravo.podaci_o_podnosiocu_zahteva.brojTelefona != null){
      let duzina = this.autorskoPravo.podaci_o_podnosiocu_zahteva.brojTelefona.length;

      if (duzina < 8 && duzina >15){
        return false;
      }
    }
    
    return paternValidacije.test(this.autorskoPravo.podaci_o_podnosiocu_zahteva.brojTelefona);
  }

  //validacija za unos brojeva >> postanski broj i broj ulice
  validacijaBroja( vrednostKojuValidiramo: any){
    let paternBroj = new RegExp("[0-9]+");

    return paternBroj.test(vrednostKojuValidiramo);
  }

  validacijaAdrese(adresa: any): boolean{
    let validna = true;

    if(adresa.mesto == '' || adresa.broj == '' || adresa.postanskiBroj=='' || adresa.ulica==''){
      console.log("Nisu uneseni podaci o adresi...");
      validna =  false;
    }
    else if(this.validacijaBroja(adresa.postanskiBroj)== false || this.validacijaBroja(adresa.broj) == false){
      console.log("Neispravan unos nije broj...");
      //provera ispravnosti broja
      validna = false;
    }
    return validna;
  }

  //proveravamo da li su svi podaci uneseni i da li su ispravnog formata
  validacijaUnosaForme(): Boolean{
    
    
    
    if (this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.ime == '' && this.autorskoPravo.podaci_o_podnosiocu_zahteva.pravnoLice.poslovnoIme == ''){
      //nisu uneseni podaci ni za pravno lice ni za fizicko lice
      alert("Molim vas unesite podatke za podnosioca zahteva(pravno/fizicko) lice..")
      return false;
    }
    
    if (this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.ime != ''){
      //proveravamo da li su uneseni svi podaci za fizicko lice
      let podnosilacFizickoLice = this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice;

      //provera unesene adrese
      let unesenaAdresa = this.validacijaAdrese(podnosilacFizickoLice.adresa);
      
      if(unesenaAdresa == false){
        alert("Molimo unesite adresu podnosioca zahteva u ispravnom formatu..");
        return false;
      }

      console.log("INfo: " + podnosilacFizickoLice.ime + ", " + podnosilacFizickoLice.prezime + ", " + this.izabraniTipPodnosiocaZahteva + ", " + unesenaAdresa);
      if (podnosilacFizickoLice.ime == '' || podnosilacFizickoLice.prezime== '' || this.izabraniTipPodnosiocaZahteva=='' ){
        alert("Molim vas unesite sve podatke za podnosioca zahteva - fizicko lice..");  
        return false;
      }
      else if (podnosilacFizickoLice.drzavljanstvo == null){
        alert("Molim vas unesite drzavljanstvo podnosioca zahteva..");
        return false;
      }
        
    }
    if(this.autorskoPravo.podaci_o_podnosiocu_zahteva.pravnoLice.poslovnoIme != ''){
      //proveravamo da li su uneseni svi podaci za pravno lice
      let pravnoLice = this.autorskoPravo.podaci_o_podnosiocu_zahteva.pravnoLice;
      
      if(pravnoLice.poslovnoIme == ''){
        alert("Molim vas unesite poslovno ime za podnosioca zahteva pravno lice..");
        return false;
      }
      else if(this.validacijaAdrese(pravnoLice.adresaSedistaOrganizacije) == false){
        alert("Molim vas unesite ispravne informacije o sedistu organizacije za pravno lice..");
        return false;
      }
    }

    
    if(this.autorskoPravo.podaci_o_podnosiocu_zahteva.email == ''){
      //provera da li je unesen email
      console.log("Email nije unesen...");
      alert("Molim vas unesite e-mail podnosioca zahteva...");
      return false;
    }
    if( this.validacijaEmail()== false){
      //nije dobar format unosa emaila
      alert("Molimo vas unesite e-mail adresu podnosioca zahteva u ispravnom formatu..");
      return false;
    }
    if( this.autorskoPravo.podaci_o_podnosiocu_zahteva.brojTelefona !=  null && this.validacijaBrojaTelefona()== false){
      //nije dobar foramat broja telefona
      alert("Molimo vas unesite broj telefona podnosioca zahteva u ispravnom formatu...");
      return false;
    }
    if(this.potpisnikZahteva == ''){
      //provera da li je potpisan zahtev
      alert("Molimo vas unesite ko je potpisnik zahteva...");
      return false;
    }

    ///// ******* provera informacija o autorskom delu *******////
    
    let autorskoDelo = this.autorskoPravo.podaci_o_autorskom_delu;
    
    if(autorskoDelo.naslovDela == ''){
      //provera da li je unesen naslov dela
      console.log("Ovo je mejl" + this.autorskoPravo.podaci_o_podnosiocu_zahteva.email);
      alert("Molim vas unesite naslov autorskog dela..");
      return false;
    }
    else if(this.izabranaVrstaDela == ''){
      //nije izabrana vrsta dela
      alert("Molimo vas unesite vrstu autorskog dela..");
      return false;
    }
    else if(this.izabraniTipVrsteDela== ''){
      //nije izabran tip vrste dela
      alert("Molimo vas izaberite kog je tipa vrsta autorskog dela koju registrujete..");
      return false;
    }
    else if(this.izabranaFormaZapisaDela==''){
      //nije unesena forma zapisa dela
      alert("Molimo vas izaberite formu zapisa autorskog dela..");
      return false;
    }

    //**** provera da li se prijava podnosi preko zastupnika punomocnika >> neophodan je unos podataka o punomocniku  ***/
    if (this.potpisnikZahteva == "ZASTUPNIK_LICA"){
      //moraju biti uneseni podaci o punomocniku
      if (this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.ime == "" || this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.prezime == ""){
        alert("Molimo vas unesite podatke o punomocniku preko koga podnosite zahtev, posto je on potpisnik prijave...");
        return false;
      }
    }

    return true;
  }

  //*******klik na submit >>slanje zahteva za autorska i srodna prava ******//
  onSubmit(form: NgForm){
    console.log("************************************");
    /*console.log("Unesene vrednosti o podnosiocu zahteva: ");



    //koji je tip fizicko lice ili pravno lice izabran
    if (this.selektovanTipPodnosiocaFizickoLice == true){
      console.log("Ime podnosioca: " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.ime);
      console.log("Prezime podnosioca zahteva" + this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.prezime);
      console.log("Adresa podnosioca: " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.adresa.mesto);
      console.log("Adresa postanski broj: " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.adresa.postanskiBroj);
      console.log("Ul i broj: " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.adresa.ulica + " " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.adresa.broj);

      
    }
    else{
      //izabrano je pravno lice
      console.log("Poslovno ime: " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.pravnoLice.poslovnoIme);
      console.log("Adresa: ");
      console.log("Mesto: " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.pravnoLice.adresaSedistaOrganizacije.mesto + " " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.pravnoLice.adresaSedistaOrganizacije.postanskiBroj);
      console.log("Adresa i broj: " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.pravnoLice.adresaSedistaOrganizacije.ulica + " " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.pravnoLice.adresaSedistaOrganizacije.broj);
    }

    console.log("Ostali podaci uneseni o fizickom: " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.drzavljanstvo + " tip podnosioca: " + this.izabranTipPodnosiocaZahteva);
    console.log("Email: " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.email);
    console.log("Broj telefona: " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.brojTelefona);
    console.log("\n");
    console.log("Podaci o punomocniku: ");
    console.log("Punomocnik ime: " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.ime);
    console.log("Punomocnik prezime: " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.prezime);
    let adresaPunomocnika = this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.adresaPunomocnika;
    console.log("Adresa punomocnika: " + this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.adresaPunomocnika.mesto +  
     ", " + adresaPunomocnika.broj +", "+ adresaPunomocnika.postanskiBroj + " , " + adresaPunomocnika.postanskiBroj);
    console.log("Ko je potpisao: " + this.potpisnikZahteva);
    */

    //gadjanje metode kontrolera 

    let koPodnosiTip = ``;

    if (this.selektovanTipPodnosiocaFizickoLice == true){
      koPodnosiTip = `<a1:FizickoLice>
      <a1:Ime>`+ this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.ime +`</a1:Ime>
      <a1:Prezime>`+ this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.prezime +`</a1:Prezime>
        <a1:Adresa>
          <a1:Mesto>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.adresa.mesto +`</a1:Mesto>
          <a1:PostanskiBroj>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.adresa.postanskiBroj + `</a1:PostanskiBroj>
          <a1:Ulica>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.adresa.ulica +`</a1:Ulica>
          <a1:Broj>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.adresa.broj + `</a1:Broj>
        </a1:Adresa>
    
        <a1:Drzavljanstvo>`+ this.autorskoPravo.podaci_o_podnosiocu_zahteva.fizickoLice.drzavljanstvo +`</a1:Drzavljanstvo>
        <a1:TipPodnosioca>` + this.izabraniTipPodnosiocaZahteva + `</a1:TipPodnosioca>
      </a1:FizickoLice>`;
    }
    if (this.selektovanTipPodnosiocaPravnoLice == true){
      koPodnosiTip = `
      <a1:PravnoLice>
        <a1:PoslovnoIme>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.pravnoLice.poslovnoIme + `</a1:PoslovnoIme>
          <a1:SedisteOrganizacije>
            <a1:Adresa>
              <a1:Mesto>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.pravnoLice.adresaSedistaOrganizacije.mesto +`</a1:Mesto>
              <a1:PostanskiBroj>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.pravnoLice.adresaSedistaOrganizacije.postanskiBroj + `</a1:PostanskiBroj>
              <a1:Ulica>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.pravnoLice.adresaSedistaOrganizacije.ulica +`</a1:Ulica>
              <a1:Broj>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.pravnoLice.adresaSedistaOrganizacije.broj + `</a1:Broj>
            </a1:Adresa>
          </a1:SedisteOrganizacije>
      </a1:PravnoLice>
    `;
    }

    //podaci o punomocniku ako postoje
    let podaciOPunomocniku = ``;
    if (this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.ime != '' && this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.prezime != ''){
      //postoje podaci o punomocniku dodajemo ih
      console.log("*********** POSTOJE PODACI O PUNOMOCNIKU ZAHTEVA **********************");

      podaciOPunomocniku = `
      <a1:Punomocnik>
      <a1:Ime>`+ this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.ime +`</a1:Ime>
      <a1:Prezime>`+ this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.prezime +`</a1:Prezime>
        <a1:Adresa>
          <a1:Mesto>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.adresaPunomocnika.mesto +`</a1:Mesto>
          <a1:PostanskiBroj>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.adresaPunomocnika.postanskiBroj + `</a1:PostanskiBroj>
          <a1:Ulica>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.adresaPunomocnika.ulica +`</a1:Ulica>
          <a1:Broj>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.punomocnik.adresaPunomocnika.broj + `</a1:Broj>
        </a1:Adresa>
      </a1:Punomocnik>
      `;
    }
    
    //**** podaci o izabranoj vrsti autorskog dela
    //PISANO_DELO", "GOVORNA_DELA", "DRAMSKA_DELA", "MUZICKO_DELO", "FILMSKA_DELA", "DELA_LIKOVNE_UMETNOSTI", "DELA_ARHITEKTRUE", "KARTOGRAFSKA_DELA", "PLANOVI_SLKICE", "POZORISNA_REZIJA"
    
    //**** alternativni naslov autorskog dela  *****/
    let alternativniNaslovDela = '';

    if (this.autorskoPravo.podaci_o_autorskom_delu.alternativniNaslovDela != ''){
      alternativniNaslovDela = `
      <a1:AlternativniNaslovAutorskogDela>` + this.autorskoPravo.podaci_o_autorskom_delu.alternativniNaslovDela + `</a1:AlternativniNaslovAutorskogDela>
      `;
    }

    //*** da li je delo zasnovano na delu prerade */
    let zasnovanoNaDeluPrerade = ``;

    if (this.autorskoPravo.podaci_o_autorskom_delu.deloNaKomeSeZasniva.naslovDelaNaKomeSeZasniva != ''){
      //postoji to delo na kome se zasniva

      zasnovanoNaDeluPrerade = `
      <a1:ZasnovanoNaDeluPrerade>
        <a1:NaslovDelaNaKomeSeZasniva> ` + this.autorskoPravo.podaci_o_autorskom_delu.deloNaKomeSeZasniva.naslovDelaNaKomeSeZasniva + `</a1:NaslovDelaNaKomeSeZasniva>
      </a1:ZasnovanoNaDeluPrerade>`;

      //dodati jos informacije o autrima izvornog dela ako one postoje..
      //?????????????????????????????//
    }


    //******* autori dela *********/
    let autoriDela = '';
    //iteriramo kroz listu autora i dodajemo autore ukoliko postoje
    if (this.listaAutora.length != 0){
      let sviAutori = `
      <a1:AutoriDela>`;

      for (let obj of this.listaAutora){
        let statusAutora = ``;


        if (obj.zivAutor.ime !=''){
          //ako je autor ziv
          let daLiJeAnoniman = '';

          if (obj.zivAutor.daLiJeAnoniman == true){
            daLiJeAnoniman = '<a1:AnonimniAutor> </a1:AnonimniAutor>'
          }
          statusAutora = `
          <a1:ZivAutor>
            <a1:Ime>` + obj.zivAutor.ime + ` </a1:Ime>
            <a1:Prezime>` + obj.zivAutor.prezime + ` </a1:Prezime>
            <a1:Adresa> 
              <a1:Mesto>` + obj.zivAutor.adresaAutora.mesto + `</a1:Mesto>
              <a1:PostanskiBroj>` + obj.zivAutor.adresaAutora.postanskiBroj + `</a1:PostanskiBroj>
              <a1:Ulica>` + obj.zivAutor.adresaAutora.ulica + ` </a1:Ulica>
              <a1:Broj>` + obj.zivAutor.adresaAutora.broj + ` </a1:Broj>
              
            </a1:Adresa>
            <a1:Drzavljanstvo>` + obj.zivAutor.drzavljanstvo + ` </a1:Drzavljanstvo>`
            + daLiJeAnoniman +`
          </a1:ZivAutor>
          `
          let jedanAutor = `
            <a1:Autor>
            `+ statusAutora +`
            </a1:Autor>
            `;

          sviAutori += jedanAutor;
        }
        else{
          //autor je mrtav

          let daLiJeAnoniman = '';

          if (obj.preminuoAutor.daLiJeAnoniman == true){
            daLiJeAnoniman = '<a1:AnonimniAutor> </a1:AnonimniAutor>'
          }


          statusAutora = `
          <a1:PreminuoAutor>
            <a1:Ime>` + obj.preminuoAutor.ime + `</a1:Ime>
            <a1:GodinaSmrti>` + obj.preminuoAutor.godinaSmrti + `</a1:GodinaSmrti>
            `+ daLiJeAnoniman +`
          </a1:PreminuoAutor>
          `;

          let znakAutora = '';
          let pseudonimAutora = '';

          let jedanAutor = `
            <a1:Autor>
              `+ statusAutora +`
              ` + znakAutora + `
              ` + pseudonimAutora + ` 
              
            </a1:Autor>
            `;

          //ako postoji pisemo pseudonim i znak autora
          if (obj.znakAutora != ''){
            znakAutora = `
              <a1:ZnakAutora>` + obj.znakAutora + `</a1:ZnakAutora>
            `;
          }
          if (obj.pseudonimAutora != ''){
            pseudonimAutora = `
            <a1:PseudonimAutora>` + obj.pseudonimAutora + `</a1:PseudonimAutora>
          `;
          }

          sviAutori += jedanAutor;
        }
      }
      sviAutori += `
      </a1:AutoriDela>`;

      autoriDela = sviAutori; //upisemo sve procitane autore
    }


    //****iteriramo kroz listu koautora i dodajemo koautore ukoliko postoje
    if (this.listaKoautora.length != 0){
      let sviKoautori = `
      <a1:AutoriDela>`;

      for (let obj of this.listaKoautora){
        let statusKoautora = ``;


        if (obj.zivAutor.ime !=''){
          //ako je autor ziv
          let daLiJeAnoniman = '';

          if (obj.zivAutor.daLiJeAnoniman == true){
            daLiJeAnoniman = '<a1:AnonimniKoautor> </a1:AnonimniKoautor>'
          }
          statusKoautora = `
          <a1:ZivKoautor>
            <a1:Ime>` + obj.zivAutor.ime + ` </a1:Ime>
            <a1:Prezime>` + obj.zivAutor.prezime + ` </a1:Prezime>
            <a1:Adresa> 
              <a1:Mesto>` + obj.zivAutor.adresaAutora.mesto + `</a1:Mesto>
              <a1:PostanskiBroj>` + obj.zivAutor.adresaAutora.postanskiBroj + `</a1:PostanskiBroj>
              <a1:Ulica>` + obj.zivAutor.adresaAutora.ulica + ` </a1:Ulica>
              <a1:Broj>` + obj.zivAutor.adresaAutora.broj + ` </a1:Broj>
              
            </a1:Adresa>
            <a1:Drzavljanstvo>` + obj.zivAutor.drzavljanstvo + ` </a1:Drzavljanstvo>`
            + daLiJeAnoniman +`
          </a1:ZivKoautor>
          `
          let znakAutora = '';
          let pseudonimAutora = '';

          let jedanKoautor = `
            <a1:Autor>
              `+ statusKoautora +`
              ` + znakAutora + `
              ` + pseudonimAutora + `
            </a1:Autor>
            `;

            //ako postoji pisemo pseudonim i znak autora
            if (obj.znakAutora != ''){
              znakAutora = `
                <a1:ZnakKoautora>` + obj.znakAutora + `</a1:ZnakKoautora>
              `;
            }
            if (obj.pseudonimAutora != ''){
              pseudonimAutora = `
              <a1:PseudonimKoautora>` + obj.pseudonimAutora + `</a1:PseudonimKoautora>
            `;
            }

          sviKoautori += jedanKoautor;
        }
        else{
          //koautor je mrtav

          let daLiJeAnoniman = '';

          if (obj.preminuoAutor.daLiJeAnoniman == true){
            daLiJeAnoniman = '<a1:AnonimniKoautor> </a1:AnonimniKoautor>'
          }


          statusKoautora = `
          <a1:PreminuoKoautor>
            <a1:Ime>` + obj.preminuoAutor.ime + `</a1:Ime>
            <a1:GodinaSmrti>` + obj.preminuoAutor.godinaSmrti + `</a1:GodinaSmrti>
            `+ daLiJeAnoniman +`
          </a1:PreminuoKoautor>
          `;

          let jedanKoautor = `
            <a1:Autor>
            `+ statusKoautora +`
            </a1:Autor>
            `;

          sviKoautori += jedanKoautor;
        }
      }
      sviKoautori += `
      </a1:AutoriDela>`;

      autoriDela = sviKoautori; //upisemo sve procitane koautore
    }


    //***** delo stvoreno u radnom odnosu *****/
    let deloStvorenoURadnomOdnosu = '';
    if ( this.izabraniRadniOdnos != ''){
      deloStvorenoURadnomOdnosu = `
      <a1:DeloStvorenoURadnomOdnosu>` + this.izabraniRadniOdnos + `</a1:DeloStvorenoURadnomOdnosu>
      `;
    }
    
    //***** nacin koriscenja dela ****/
    let nacinKoriscenjaDela= "";
    if( this.autorskoPravo.podaci_o_autorskom_delu.nacinKoriscenjaDela != ''){
      nacinKoriscenjaDela = `
      <a1:NacinKoriscenjaDela>` +this.autorskoPravo.podaci_o_autorskom_delu.nacinKoriscenjaDela+ `</a1:NacinKoriscenjaDela>
      `
    }


    this.xmlZahtev = `<?xml version="1.0" encoding="UTF-8"?>
    <a1:ZahtevZaAutorskaISrodnaPrava 
        xmlns:a1="http://www.ftn.uns.ac.rs/obrazacA1"
        xmlns:r="http://www.w3.org/ns/rdfa#"
        xmlns:pred="http://www.ftn.uns.ac.rs/rdf/database/predicate/"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.ftn.uns.ac.rs/obrazacA1 file:/C:/Users/DM/Desktop/obrasciRdf/obrazacA1.xsd" BrojPrijave="A-55" DatumPodnosenja="2022-01-01">
        
        <a1:Zavod>
            <a1:NazivUstanove>Zavod za intelektualnu svojinu</a1:NazivUstanove>
            <a1:Adresa>
                <a1:Mesto>Beograd</a1:Mesto>
                <a1:Ulica>Kneginje Ljubice</a1:Ulica>
                <a1:Broj>22</a1:Broj>
            </a1:Adresa>
        </a1:Zavod>
        
        <a1:PodnosilacZahteva>
           `+ koPodnosiTip + `
            <a1:BrojTelefona>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.brojTelefona + `</a1:BrojTelefona>
            <a1:E-mail>` + this.autorskoPravo.podaci_o_podnosiocu_zahteva.email + `</a1:E-mail>`
            + podaciOPunomocniku +`
            <a1:PotipsPodnosiocaPrijave>
                <a1:Potpisao>` + this.potpisnikZahteva + `</a1:Potpisao>
                <a1:Potpis></a1:Potpis>
            </a1:PotipsPodnosiocaPrijave>
        </a1:PodnosilacZahteva>
        
        <a1:AutorskoDelo>
            <a1:NaslovAutorskogDela>` + this.autorskoPravo.podaci_o_autorskom_delu.naslovDela + `</a1:NaslovAutorskogDela>
            `+ alternativniNaslovDela + `
            <a1:VrstaAutorskogDela>
                <a1:`+this.izabranaVrstaDela.trim()+`>` + this.izabraniTipVrsteDela.trim() + `</a1:`+this.izabranaVrstaDela+`>
            </a1:VrstaAutorskogDela>
            `+ zasnovanoNaDeluPrerade +`
            
            <a1:FormaZapisaDela>`+ this.izabranaFormaZapisaDela +`</a1:FormaZapisaDela>` +
            autoriDela  + `
            ` + 
            deloStvorenoURadnomOdnosu + `
            ` +
            nacinKoriscenjaDela +
            `
        </a1:AutorskoDelo>
        
        <a1:PriloziUzZahtev>
            
        </a1:PriloziUzZahtev>
        
    </a1:ZahtevZaAutorskaISrodnaPrava>
    `;

    let validnaForma = this.validacijaUnosaForme(); //proveravamo da li su prosledjeni podaci validni

    if(validnaForma){
      console.log("Slanje zahteva na backend...");
      //gadjanje metode servisa za slanje i upis zahteva na backend
      /*this.autorskoPravoService.add(this.xmlZahtev).subscribe((data: any) => {
        console.log("Ovo su podaci nakon upisa zahteva za autorska i srodna prava...");
        console.log(data);
        alert("Dodato je: ");
      
      });*/

      this.autorskoPravoService.add(this.xmlZahtev).subscribe({
        next: (response: any) => {
          console.log('Uspesno poslato:', response);
          //window.location.href = '/moji-dokumenti'
          alert("Vas zahtev je poslat..."); 
        },
        error: (error: HttpErrorResponse) => {
          console.log(error.message);
          //alert('Zahtev je vec kreiran..');
          
        },
      });
    }
    else{
      console.log("Podaci uneseni u formi su nevalidni...");
    }

    
    

    //console.log(this.xmlZahtev);

    

    }


    
    
}
