import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LogovanjeRegistracijaService } from 'src/service/logovanje-registracija.service';
import { xml2js } from 'xml-js';

@Component({
  selector: 'app-logovanje',
  templateUrl: './logovanje.component.html',
  styleUrls: ['./logovanje.component.css']
})
export class LogovanjeComponent implements OnInit{

  constructor(private router: Router, private userService: LogovanjeRegistracijaService) { }

  korisnickoIme: string = '';
  lozinka: string = '';

  ngOnInit(): void {

  }

  ulogujSe(forma: NgForm) {
    //ako nije uneseno baci upozorenje

    if (this.korisnickoIme == '' || this.lozinka == '') {
      alert("Molimo vas unesite korisnicko ime i lozinku...");
    }
    else {
      this.userService.getOne(this.korisnickoIme).subscribe({
        next: data => {
            let user = xml2js(data);
            console.log(user)
            if (user.elements[0].elements[0].elements[0].elements[0].text == this.lozinka) {
              alert("Uspesno logovanje!")
              localStorage.setItem("password", user.elements[0].elements[0].elements[0].elements[0].text);
              localStorage.setItem("uloga", user.elements[0].elements[0].elements[1].elements[0].text);
              localStorage.setItem("email", user.elements[0].elements[0].elements[2].elements[0].text);


              // OVDE STAVI KUDA DA IDE POSLE LOG
              let uloga = user.elements[0].elements[0].elements[1].elements[0].text;
              if(uloga == "ADMIN"){
                this.router.navigate(["zahtevZaAutorskoPravo"]); //stranica za popunjavanje forme

                 //stranica za sluzbenika
                 this.router.navigate(["sluzbenik/pretragaMetapodaci"]);
              } 
              else{
                this.router.navigate(["zahtevZaAutorskoPravo"]); //stranica za popunjavanje forme
              }             

              /*this.router.navigate(['/', '/']).then(() => {
                window.location.reload();
              });*/

              //Pogledaj app.component.ts kod zig-front za podelu aplikacije!!!!!

              // OVAKO NA LOGOUT DUGME
              // goTologout() {
              //   localStorage.clear()
              //   this.router.navigate(['/', '/']).then(() => {
              //     window.location.reload();
              //   });
              // }
            }
            else {
              alert("Pogresna sifra!")
            }

        },
        error: error => {
          alert("Ne postoji korisnik!");
          console.error('There was an error!', error);
        }
      })
    }
  }
}
