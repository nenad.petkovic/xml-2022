import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AutorskoPravoComponent } from './components/autorsko-pravo/autorsko-pravo.component';
import { LogovanjeComponent } from './components/logovanje/logovanje.component';
import { PregledajDokumentComponent } from './components/pregledaj-dokument/pregledaj-dokument.component';
import { RegistracijaComponent } from './components/registracija/registracija.component';
import { SluzbenikPretragaMetapodaciComponent } from './components/sluzbenik-pretraga-metapodaci/sluzbenik-pretraga-metapodaci.component';
import { NovaKomponentaComponent } from './nova-komponenta/nova-komponenta.component';

const routes: Routes = [
  { path: 'zahtevZaAutorskoPravo', component: AutorskoPravoComponent},
  {path: 'nova', component:NovaKomponentaComponent},
  {path: 'sluzbenik/pretragaMetapodaci', component: SluzbenikPretragaMetapodaciComponent},
  {path: 'sluzbenik/pregledDokumenta', component: PregledajDokumentComponent},
  {path: 'registracija',component:RegistracijaComponent},
  {path: 'logovanje', component:LogovanjeComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
