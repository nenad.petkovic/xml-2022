import { Component } from '@angular/core';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'autorska-prava-front';

  constructor(private ruter: Router){

  }

  //kliknuto dugme uloguj se

  ulogujSe(event: any){
    console.log("Izabrano logovanje..")

    //preusmeri na stranicu za logovanje
    this.ruter.navigate(["logovanje"]);
  }

  registrujSe(event: any){
    console.log("Izabrana registracija...");

    //preusmeri na stranicu registracije.. 
    this.ruter.navigate(["registracija"]);
  }
}
