export class Korisnik {

    ime: string;
    prezime: string;
    uloga: string;
    korisnickoIme: string;
    lozinka: string;
    
    constructor(obj?:any){
        this.ime = '';
        this.prezime = '';
        this.korisnickoIme = '';
        this.lozinka = '';
        this.uloga = '';
    }
  }

 