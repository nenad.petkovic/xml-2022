export class AutorskoPravo {

    podaci_o_podnosiocu_zahteva: PodaciOPodnosiocu;
    podaci_o_autorskom_delu: PodaciOAutorskomDelu;
    podaci_o_prilozima_uz_zahtev: PodaciOPrilozima;
    dokument_id: string;

    //podaci_o_osobi: PodaciOOsobi;
    //opsti_podaci: OpstiPodaci;
    //dokument_id: number;
  
    
    constructor(obj?:any){
        this.podaci_o_podnosiocu_zahteva = (obj && obj.podaci_o_podnosiocu_zahteva) || new PodaciOPodnosiocu();
        this.podaci_o_autorskom_delu = (obj && obj.podaci_o_autorskom_delu) || new PodaciOAutorskomDelu();
        this.podaci_o_prilozima_uz_zahtev = (obj && obj.podaci_o_prilozima_uz_zahtev) || new PodaciOPrilozima();
        this.dokument_id = (obj && obj.dokument_id) || 1;
    }
  }

  export class PodaciOPodnosiocu{
    fizickoLice: FizickoLice;
    pravnoLice: PravnoLice;
    email: string;
    brojTelefona: string;
    punomocnik: Punomocnik;
    potpisao: string;

    constructor (obj?: any){
        this.fizickoLice = (obj && obj.fizickoLice) || new FizickoLice();
        this.pravnoLice = (obj && obj.pravnoLice) || new PravnoLice();
        this.email  = (obj && obj.email) || "";
        this.brojTelefona = (obj && obj.brojTelefona) || null;
        this.punomocnik = (obj && obj.Punomocnik) || new Punomocnik() || null;
        this.potpisao = (obj && obj.potpisao) || "";
    }

  }

  export class FizickoLice {
    ime: string;
    prezime: string;
    adresa: Adresa;
    drzavljanstvo: string;
    tipPodnosioca: string;

    constructor ( obj?: any){
        this.ime = (obj && obj.ime) || "";
        this.prezime = (obj && obj.prezime) || "";
        this.adresa = (obj && obj.adresa) || new Adresa();
        this.drzavljanstvo = (obj && obj.drzavljanstvo) || null;
        this.tipPodnosioca = (obj && obj.tipPodnosioca) || "";
    }
  }

  export class PravnoLice {
    poslovnoIme: string;
    adresaSedistaOrganizacije: Adresa;

    constructor (obj?: any) {
        this.poslovnoIme = (obj && obj.poslovnoIme) || "";
        this.adresaSedistaOrganizacije = (obj && obj.adresaSedistaOrganizacije) || new Adresa();
    }
  }

  export class Punomocnik{
    ime: string;
    prezime: string;
    adresaPunomocnika: Adresa;

    constructor (obj?: any) {
       this.ime = (obj && obj.ime) || "";
       this.prezime = (obj && obj.prezime) || "";
       this.adresaPunomocnika  = (obj && obj.adresa) || new Adresa(); 
    }
  }

  export class PodaciOAutorskomDelu{
    naslovDela: string;
    alternativniNaslovDela: string;
    vrstaDela: VrstaAutorskogDela;

    //dodati jos dodatne opise koji fale u informacijama
    deloNaKomeSeZasniva: DeloNaKomeSeZasniva;
    autorDela: Autor;    
    deloStvorenoURadnomOdnosu: boolean;
    nacinKoriscenjaDela: string;

    constructor (obj?: any){
        this.naslovDela = (obj && obj.naslovDela) || "";
        this.alternativniNaslovDela = (obj && obj.alternativniNaslovDela) || "";
        this.vrstaDela = (obj && obj.vrstaDela) || new VrstaAutorskogDela();
        this.deloNaKomeSeZasniva = (obj && obj.deloNaKomeSeZasniva) || new DeloNaKomeSeZasniva();
        this.autorDela = (obj && obj.autorDela) || new Autor();
        this.deloStvorenoURadnomOdnosu = (obj && obj.deloStvorenoURadnomOdnosu) || false;
        this.nacinKoriscenjaDela = (obj && obj.nacinKoriscenjaDela) || '';
    }

  }

  export class DeloNaKomeSeZasniva{
    naslovDelaNaKomeSeZasniva: string;
    autoriIzvornogDela: Autor;
    

    constructor(obj?: any){
        this.naslovDelaNaKomeSeZasniva = (obj && obj.naslovDelaNaKomeSeZasniva) || "";
        this.autoriIzvornogDela = (obj && obj.autoriIzvornogDela) || new Autor() ;
        
    }
  }

  export class Autor{
    zivAutor: Ziv;
    preminuoAutor: Preminuo;
    znakAutora: string;
    pseudonimAutora: string;


    constructor( obj?: any){
        this.zivAutor = (obj && obj.zivAutor) || new Ziv();
        this.preminuoAutor = (obj && obj.preminuoAutor) || new Preminuo();
        this.znakAutora = (obj && obj.znakAutora) || "";
        this.pseudonimAutora = (obj && obj.pseudonimAutora) || "";
    }

  };

  export class Ziv{
    ime: string;
    prezime: string;
    adresaAutora: Adresa;
    daLiJeAnoniman: boolean;
    drzavljanstvo: string;

    constructor( obj?: any){
        this.ime = (obj && obj.ime) || "",
        this.prezime = (obj && obj.prezime) || "",
        this.adresaAutora = (obj && obj.adresa) || new Adresa(),
        this.drzavljanstvo = (obj && obj.drzavljanstvo) || "",
        this.daLiJeAnoniman  = (obj && obj.daLiJeAnoniman) || false
    }
  };

  export class Preminuo{
    ime: string;
    godinaSmrti: number;
    daLiJeAnoniman: boolean;

    constructor( obj?: any){
        this.ime = (obj && obj.ime) || "",
        this.godinaSmrti = (obj && obj.godinaSmrti) || null,
        this.daLiJeAnoniman = (obj && obj.daLiJeAnoniman) || false
    }


  };

  

  export class PodaciOPrilozima{

  }
  export class VrstaAutorskogDela{
    nazivTipaDela: string;
    vrstaDela: string;

    constructor (obj?: any){
        this.nazivTipaDela = (obj && obj.vrstaDela) || "";
        this.vrstaDela = (obj && obj.nazivTipaDela) || "";
    }
  }

  export class PodaciOOsobi {
    drzavljanstvo: string;
    JMBG: string;
    ime: string;
    prezime: string;
    email: string;
    broj_mobilnog_telefona: string;
    broj_fiksnog_telefona: string;
    constructor(obj?: any) {
      this.drzavljanstvo = (obj && obj.drzavljanstvo) || "";
      this.JMBG = (obj && obj.JMBG) || "";
      this.ime = (obj && obj.ime) || null;
      this.prezime = (obj && obj.prezime) || null;
      this.email = (obj && obj.email) || null;
      this.broj_mobilnog_telefona = (obj && obj.broj_mobilnog_telefona) || null;
      this.broj_fiksnog_telefona = (obj && obj.broj_fiksnog_telefona) || null;
    }
  }

  export class OpstiPodaci {
    lokacija_opstina: string;
    tip_vakcine: string;
    davalac_krvi: boolean;
    datum_podnosenja: string;
    idPodnosioca: string;
   
    constructor(obj?: any) {
      this.lokacija_opstina = (obj && obj.lokacija_opstina) || null;
      this.tip_vakcine = (obj && obj.tip_vakcine) || null;
      this.davalac_krvi = (obj && obj.davalac_krvi) || null;
      this.datum_podnosenja = (obj && obj.datum_podnosenja) || "2022-01-03";
      this.idPodnosioca = (obj && obj.idPodnosioca) || "2";
    }
  }

  export class Adresa{
    mesto: string;
    ulica: string;
    broj: string;
    postanskiBroj: string;

    constructor (obj?: any){
        this.mesto = (obj && obj.mesto) || "";
        this.ulica = (obj && obj.ulica) || "";
        this.broj = (obj && obj.broj) || "";
        this.postanskiBroj = (obj && obj.postanskiBroj) || "";
    }

  }