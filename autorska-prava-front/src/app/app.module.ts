import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';


import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AutorskoPravoComponent } from './components/autorsko-pravo/autorsko-pravo.component';
import { NovaKomponentaComponent } from './nova-komponenta/nova-komponenta.component';
import { SluzbenikPretragaMetapodaciComponent } from './components/sluzbenik-pretraga-metapodaci/sluzbenik-pretraga-metapodaci.component';
import { PregledajDokumentComponent } from './components/pregledaj-dokument/pregledaj-dokument.component';
import { RegistracijaComponent } from './components/registracija/registracija.component';
import { LogovanjeComponent } from './components/logovanje/logovanje.component';

@NgModule({
  declarations: [
    AppComponent,
    AutorskoPravoComponent,
    NovaKomponentaComponent,
    SluzbenikPretragaMetapodaciComponent,
    PregledajDokumentComponent,
    RegistracijaComponent,
    LogovanjeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,  
    FormsModule,
    HttpClientModule
        
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
