import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogovanjeRegistracijaService {

  apiURL = 'http://localhost:9092/api/users';

  constructor(private http: HttpClient) { }

  getOne(id: string): Observable<any> {
    return this.http.get(this.apiURL + "/" + id, { responseType: 'text' });
  }

  upisi(xmlFile: string): Observable<any> {
    return this.http.post(this.apiURL, xmlFile, { responseType: 'text' });
  }
  

}
