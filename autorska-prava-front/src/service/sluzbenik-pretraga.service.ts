import { HttpHeaders, HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Metapodaci } from 'src/app/model/metapodaci';
//import {xml2js} from 'xml2js' 

@Injectable({
  providedIn: 'root'
})
export class SluzbenikPretragaService {

  

   // Define API
   apiURL = 'http://localhost:8085/api/';
   private authHeder = new HttpHeaders();
   //private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
   private headers = new HttpHeaders({ "Content-Type": "application/json" });
 
   
   constructor( private http:HttpClient){
    //this.headers.append('Access-Control-Allow-Origin', '*');
    //this.headers.append('Access-Control-Allow-Credentials', 'true');
   }



  //ucitavanje svih mogucih vrsta autorskih dela
  loadVrsteAutroskogDela(): string[]{
    return ["PISANO_DELO", "GOVORNA_DELA", "DRAMSKA_DELA", "MUZICKO_DELO", "FILMSKA_DELA", "DELA_LIKOVNE_UMETNOSTI", "DELA_ARHITEKTRUE", "KARTOGRAFSKA_DELA", "PLANOVI_SLKICE", "POZORISNA_REZIJA"];
  }

  public pretragaPoImenuIPrezimenu( metapodaci: any): Observable<any> {
    console.log(metapodaci)

    return this.http.post<any>(
      `${this.apiURL}pretraga/pretragaMetapodaci/imePrezime`,metapodaci, {  responseType: 'json'}
    );
  }

  public pretragaPoPoslovnomImenu(metapodaci: any): Observable<any>{
    return this.http.post(
      `${this.apiURL}pretraga/pretragaMetapodaci/poslovnoIme`, metapodaci, {responseType: 'json'}
    )
  }

  public pretragaPoEmailu(metapodaci: any): Observable<any>{
    return this.http.post(
      `${this.apiURL}pretraga/pretragaMetapodaci/email`, metapodaci, {responseType: 'json'}
    )
  }
  public pretragaPoNazivuDela(metapodaci: any): Observable<any>{
    console.log("Pogodjena metoda...");
    console.log("Metapodaci" + metapodaci);
    return this.http.post(
      `${this.apiURL}pretraga/pretragaMetapodaci/naslovDela`, metapodaci, {responseType: 'json'}
    )
  }
  public pretragaPoAlternativnomNazivuDela(metapodaci: any): Observable<any>{
    return this.http.post(
      `${this.apiURL}pretraga/pretragaMetapodaci/alternativniNaslovDela`, metapodaci, {responseType: 'json'}
    )

  }

  public getZahtevZaAutorskoPravoPoId(id: any){
    console.log("Gadjamo metodu...");
    return this.http.get(
      `${this.apiURL}zahtevZaAutorskaPrava/readZahtevZaAutrskaPrava/`+id, {responseType: "text"}
    )
  }

  public pretragaAND(metapodaci: any): Observable<any>{
    return this.http.post(
      `${this.apiURL}pretraga/pretragaMetapodaci/andPretraga`, metapodaci, {responseType: 'json'}
    )

  }
  public pretragaOR(metapodaci: any): Observable<any>{
    return this.http.post(
      `${this.apiURL}pretraga/pretragaMetapodaci/orPretraga`, metapodaci, {responseType: 'json'}
    )

  }
  

  /******* slanje resenja  ****/
  public posaljiResenje(resenje: any): Observable<Blob>{
    return this.http.post(
      `${this.apiURL}resenje/posaljiResenje/1`,resenje, {responseType: "blob"}
    );
  }

  //ucitavanje svih zahteva u redosledu datuma
  public ucitajSveZahteve():Observable<any>{
    return this.http.get(
      `${this.apiURL}zahtevZaAutorskaPrava/getSviZahtevi`
    )
  }
  
}
