import { TestBed } from '@angular/core/testing';

import { SluzbenikPretragaService } from './sluzbenik-pretraga.service';

describe('SluzbenikPretragaService', () => {
  let service: SluzbenikPretragaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SluzbenikPretragaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
