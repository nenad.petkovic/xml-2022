import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class XtmlPdfGeneratorService {

  public apiUrl: string = 'http://localhost:8085/api/';
  private authHeder = new HttpHeaders();
  //private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  private headers = new HttpHeaders({ "Content-Type": "application/json" });

  

  constructor(private http:HttpClient) { 
    
  }

  public getDokumentXHTML(id: string, tipDokumenta: string): Observable<string> {
    return this.http.get(
      this.apiUrl + 'dokument/html/zahtev-za-autrsko-pravo/' + id,
      { responseType: 'text' }
    );
  }

  public getDokumentPDF(id: string, tipDokumenta: string): Observable<Blob> {
    return this.http.get(
      this.apiUrl + 'dokument/pdf/zahtev-za-autrsko-pravo/'+ id,
      { responseType: 'blob' }
    );
  }

  public getDokumentMetapodaciRDF(id: string, tipDokumenta: string): Observable<string> {
    return this.http.get(
      this.apiUrl + 'dokument/metapodaci/rdf/zahtev-za-autrsko-pravo/'+ id,
      { responseType: 'text' }
    );
  }
  public getDokumentMetapodaciJSON(id: string, tipDokumenta: string): Observable<string> {
    return this.http.get(
      this.apiUrl + 'dokument/metapodaci/json//rdf/zahtev-za-autrsko-pravo/'+ id,
      { responseType: 'text' }
    );
  }

}
