import { TestBed } from '@angular/core/testing';

import { AutorskoPravoService } from './autorsko-pravo.service';

describe('AutorskoPravoService', () => {
  let service: AutorskoPravoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AutorskoPravoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
