import { TestBed } from '@angular/core/testing';

import { LogovanjeRegistracijaService } from './logovanje-registracija.service';

describe('LogovanjeRegistracijaService', () => {
  let service: LogovanjeRegistracijaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LogovanjeRegistracijaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
