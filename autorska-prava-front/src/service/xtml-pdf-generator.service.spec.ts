import { TestBed } from '@angular/core/testing';

import { XtmlPdfGeneratorService } from './xtml-pdf-generator.service';

describe('XtmlPdfGeneratorService', () => {
  let service: XtmlPdfGeneratorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(XtmlPdfGeneratorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
