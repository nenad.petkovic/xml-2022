import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { AutorskoPravo } from 'src/app/model/autorskoPravo';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class AutorskoPravoService {
  

  // Define API
  apiURL = 'http://localhost:8085/api/';
  private authHeder = new HttpHeaders();
  //private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  private headers = new HttpHeaders({ "Content-Type": "application/json" });

  
  constructor( private http:HttpClient){

  }


  //ucitavanje svih mogucih tipova podnosilaca zahteva
  loadTipoviPodnosilacaZahteva(): string[]{
    return ["AUTOR", "DRUGI_NOSIOC_AUTORSKOG_PRAVA"];
  }

  //ucitaj tipove uloga osoba koje mogu da potpisu zahtev kao podnosioci
  loadKoJePotpisaoZahtev(): string[]{
    return ["FIZICKO_LICE", "ZASTUPNIK_LICA", "OVLASCENI_PREDSTAVNIK_PRAVNOG_LICA"];
  }

  //ucitavanje svih mogucih vrsta autorskih dela
  loadVrsteAutroskogDela(): string[]{
    return ["PISANO_DELO", "GOVORNA_DELA", "DRAMSKA_DELA", "MUZICKO_DELO", "FILMSKA_DELA", "DELA_LIKOVNE_UMETNOSTI", "DELA_ARHITEKTRUE", "KARTOGRAFSKA_DELA", "PLANOVI_SLKICE", "POZORISNA_REZIJA"];
  }

  loadPisanaDela(): string[]{
    return ["KNJIGA", "CLANAK"];
  }

  loadGovornaDela(): string[]{
    return ["PREDAVANJA", "GOVORI", "BESEDE", "DRUGO"];
  }

  loadDramskaDela(): string[]{
    return ["DRAMSKO_DELO", "DRAMSKO_MUZICKO_DELO", "KOREOGRAFSKO_PANTOMIMSKO_DELO", "DELO_POTEKLO_IZ_FOLKLORA"];
  }
  
  loadMuzickaDela(): string[]{
    return ["DELO_SA_RECIMA", "DELO_BEZ_RECI"];
  }
  loadFilmskaDela(): string[]{
    return ["KINEMATOGRAFSKO", "TELEVIZIJSKO"];
  }

  loadDelaLikovneUmetnosti(): string[]{
    return ["SLIKA", "GRAFIKA", "SKULPTURA", "DRUGO"];
  }

  //ucitaj forme zapisa dela
  loadFormeZapisaDela(): string[]{
    return ["STAMPANI_TEKST", "OPTICKI_DISK", "DRUGO"];
  }

  //ucitaj da li je nastalo u radnom odnosu
  loadDaLiJeNastaloURadnomOdnosu(): string[]{
    return ["DA", "NE"];
  }

  

  //************* kreiranje novog zahteva za autorska i srodna prava i slanje **********
  /*add(noviZahtevXml: string): Observable<AutorskoPravo> {
    return this.http.post<AutorskoPravo>(this.apiURL + "zahtevZaAutorskaPrava/createZahtevZaAtorskaPrava/A-304", noviZahtevXml, {headers: this.authHeder, responseType: 'json'})
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }*/

  public add( noviZahtevXml: any): Observable<string> {
    console.log(noviZahtevXml)
    return this.http.post(
      `${this.apiURL}zahtevZaAutorskaPrava/createZahtevZaAtorskaPrava/A-305`,noviZahtevXml, {responseType: "text"}
    );
  }


  handleError(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}

